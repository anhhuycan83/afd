CREATE PROCEDURE SpInsertWithYesIdentityBusposition
(
@buspositionID bigint,
@dtWriteTimeStamp datetime,
@messageIdOutput bigint output
)
AS
-- STEP 2 & 3: Issue the Insert statement, checking @@ERROR after each statement
SET DATEFORMAT MDY 
INSERT INTO [BusPosition]
           ([DistrictID]
           ,[UnitID]
           ,[Latitude]
           ,[Longitude]
           ,[WriteTimeStamp]
           ,[PositionTimeStamp]
           ,[EventID]
           ,[Speed]
           ,[RSSI]
           ,[RouteMiles])
select 
            [DistrictID]
           ,[UnitID]
           ,[Latitude]
           ,[Longitude]
           ,@dtWriteTimeStamp
           ,@dtWriteTimeStamp
           ,[EventID]
           ,[Speed]
           ,[RSSI]
           ,[RouteMiles]
from BusPosition
where
id = @buspositionID

SELECT @messageIdOutput = @@IDENTITY

Return

-------------------
--use ETGPSDenton
--use master
CREATE PROCEDURE SpInsertWithYesIdentityMessage
(
@messageID bigint,
@dtAdjusted datetime,
@messageIdOutput bigint output
)
AS
-- STEP 1: Start the transaction

BEGIN TRANSACTION
	SET DATEFORMAT MDY 
	insert into message
	 ([message_dt_received]
			   ,[message_dt_event]
			   ,[message_event_id]
			   ,[message_vehicle_id]
			   ,[message_latitude]
			   ,[message_longitude]
			   ,[message_comment]
			   ,[message_dt_adjusted])
	select 
	[message_dt_received]
			   ,[message_dt_event]
			   ,[message_event_id]
			   ,[message_vehicle_id]
			   ,[message_latitude]
			   ,[message_longitude]
			   ,[message_comment]
			   ,@dtAdjusted
	from message
	where
	message_id = @messageID
	IF @@ERROR = 0
	BEGIN
		SELECT @messageIdOutput = @@IDENTITY


		INSERT INTO [message_attribute_student]
				   ([mas_message_id]
				   ,[mas_student_id]
				   ,[mas_status])
		           
		select @messageIdOutput
				   ,[mas_student_id]
				   ,[mas_status]
		from [message_attribute_student]
		where
		[mas_message_id] = @messageID
		
		IF @@ERROR = 0
		BEGIN
			INSERT INTO [message_attribute_vehicle]
					   ([mav_message_id]
					   ,[mav_speed]
					   ,[mav_status]
					   ,[mav_mileage]
					   ,[mav_sensor]
					   ,[mav_idle_seconds]
					   ,[mav_stop_seconds]
					   ,[mav_heading])
			select @messageIdOutput
					   ,[mav_speed]
					   ,[mav_status]
					   ,[mav_mileage]
					   ,[mav_sensor]
					   ,[mav_idle_seconds]
					   ,[mav_stop_seconds]
					   ,[mav_heading]
			from [message_attribute_vehicle]
			where
			[mav_message_id] = @messageID
			IF @@ERROR = 0
			BEGIN
				COMMIT TRANSACTION
			END
			ELSE
			BEGIN
				ROLLBACK TRANSACTION
			END
		END			
		ELSE
		BEGIN
			ROLLBACK TRANSACTION
		END			
	END
	ELSE
	BEGIN
		ROLLBACK TRANSACTION
	END	
Return