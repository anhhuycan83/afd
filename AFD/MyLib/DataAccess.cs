using System;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;
using Microsoft.Win32;
using System.Windows.Forms;
using System.Data.OleDb;

namespace nsDataAccess
{		
	
	#region class DataAccess
	public class DataAccess
	{
		/// <summary>
		/// Private variables for connect to DB and perform data mining
		/// </summary>
								
		//private int configID = 0;
		//private string userID;

        private string m_strConn = "Data Source=huy;Initial Catalog=TKB;User ID=sa";        
		private SqlConnection m_Conn;
		private SqlCommand m_sqlCmd;
		private SqlTransaction m_sqlTrans;

		private SqlDataReader m_sqlDataReader;
		private SqlDataAdapter m_sqlDataAdapter;
		private DataSet m_dataSet;		

		private string m_sqlStatement;

		
		/// <summary>
		/// Public variables
		/// </summary>
		public bool m_isConnected = false;
		public string m_strErr = "";		
		public int m_errCode;

		public DataAccess()
		{
			setConnectionString();
			try
			{
				m_Conn = new SqlConnection(m_strConn);
			}
			catch(Exception ex)
			{
				m_isConnected = false;
				m_strErr = ex.Message;
			}
		}// End of DataAccess pro
        public DataAccess(string ins_strConnect)
        {
            setConnectionString(ins_strConnect);
            try
            {
                m_Conn = new SqlConnection(m_strConn);
            }
            catch (Exception ex)
            {
                m_isConnected = false;
                m_strErr = ex.Message;
            }
        }// End of DataAccess pro

		public string ReadRegistry(string ins_keyName)
		{
			RegistryKey rk= Registry.LocalMachine;
			RegistryKey skl = rk.OpenSubKey("SOFTWARE\\" + Application.ProductName);

			if(skl == null)
				return null;
			else
			{
				try
				{
					return (string)skl.GetValue(ins_keyName.ToUpper());
				}
				catch(Exception ex)
				{
					System.Console.WriteLine(ex.ToString());
					return null;
				}
			}

		}// End of ReadRegistry pro

		public void open()
		{
			try
			{
				if(m_Conn != null)
				{
					m_Conn.Open();
					m_sqlCmd = m_Conn.CreateCommand();					
					m_isConnected = true;
				}
			}
			catch(Exception ex)
			{
				m_isConnected = false;
				m_strErr = ex.Message;
			}
		}// End of open pro

		/// <summary>
		/// Close and dispose Oracle connection and Oracle command
		/// Always call this when finish using this class
		/// </summary>
		public void close()
		{
			try
			{
				if(m_sqlDataReader != null)
					m_sqlDataReader.Close();
				m_sqlCmd.Dispose();
				m_Conn.Close();
				m_Conn.Dispose();
			}
			catch(Exception){}
		}// End of close pro

		private string generateLogString(string in_sqlString)
		{
			string ls_mysqlString = in_sqlString.ToUpper();
			string ls_sqlType = "NONE";
			ls_mysqlString = ls_mysqlString.Replace("'","''");

			
			if(ls_mysqlString.StartsWith("INSERT"))
				ls_sqlType = "INSERT";
			else
				if(ls_mysqlString.StartsWith("DELETE"))
				ls_sqlType = "DELETE";
			else
				if(ls_mysqlString.StartsWith("UPDATE"))
				ls_sqlType = "UPDATE";

			string ls_sqlLog = "INSERT INTO TBL_LOG(USR_ID, LOG_DATETIME, LOG_TYPE, LOG_INFO) VALUES('"
				+ ReadRegistry("sctvUserID") + "',getdate(),'"
				+ ls_sqlType + "','"
				+ ls_mysqlString + "')";

			return ls_sqlLog;
		}// End of generateLogString pro

		/// <summary>
		/// Call insert, update sql query. return -1 if occurs an error
		/// </summary>
		/// <param name="str">sql query</param>
		/// <returns>return number of record changed</returns>
		public int UpdateToDB(string str)
		{			
			int error = 0;
			//m_sqlStatement = fixVietNameseSqlString(str);
            m_sqlStatement = str;
			m_sqlCmd.CommandText = m_sqlStatement;
		
			try
			{
				error = m_sqlCmd.ExecuteNonQuery();

//				// Write log
//				m_sqlCmd.CommandText = generateLogString(m_sqlStatement);
//				m_sqlCmd.ExecuteNonQuery();

			}
			catch
			{
                
                //ErrorEntity err = new ErrorEntity();
                //err.alert_string("Error: " + ex.Message);                  
				return -1;
			}
			
			return error;
		}// End of UpdateToDB pro

		private string fixVietNameseSqlString(string ins_sqlString)
		{
			ins_sqlString = ins_sqlString.Replace("''","null");
			string[] sql = ins_sqlString.Split('\'');
			ins_sqlString = "";
			for(int i = 0; i < sql.Length; i++)
			{
				ins_sqlString += sql[i];
				if( i < sql.Length -1)
				{
					if(i % 2 == 0)
						ins_sqlString += "N'";
					else
						ins_sqlString += "'";
				}
			}
			return ins_sqlString;
		}


		/// <summary>
		/// Get data into DataReader
		/// </summary>
		/// <param name="str">sql String</param>
		/// <returns>DataReader, return null if error</returns>
		public SqlDataReader GetDataReaderFromDB(string str)
		{			
			//m_sqlStatement = fixVietNameseSqlString(str);			
            m_sqlStatement = str;
			m_sqlCmd.CommandText = m_sqlStatement;
			try
			{
				m_sqlDataReader = m_sqlCmd.ExecuteReader();
			}
			catch(Exception ex)
			{
				System.Console.WriteLine(str);
				System.Console.WriteLine(ex.ToString());
				m_strErr = ex.Message;				
				return null;
			}
			return m_sqlDataReader;
		}// End of GetDataReaderFromDB
				
		/// <summary>
		/// Get data into DataSet
		/// </summary>
		/// <param name="str">sql String</param>
		/// <returns>DataSet, return null if error</returns>
		public DataSet GetDataSetFromDB(string str)
		{
			//m_sqlStatement = fixVietNameseSqlString(str);
            m_sqlStatement = str;
			m_sqlCmd.CommandText = m_sqlStatement;
			try
			{				
				m_sqlDataAdapter = new SqlDataAdapter(m_sqlCmd);								
				m_dataSet = new DataSet();
				m_sqlDataAdapter.Fill(m_dataSet);
			}
			catch(Exception ex)
			{
				System.Console.WriteLine(str);
				System.Console.WriteLine(ex.ToString());
				m_strErr = ex.Message;								
				return null;
			}
			return m_dataSet;
		}//End of GetDataSetFromDB

        /// <summary>
        /// if row[0][ins_coloumnName].toString() has values: return true, else return false
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="ins_tableName"></param>
        /// <param name="ins_columnName"></param>
        /// <returns></returns>
        public bool testExistValueInTable(DataSet ds)
        {
            if (ds.Tables[0] == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// if rowcount != 0 return true, if table not exist or table exist but rowcount = 0: return false
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public bool testExistValueInTableAndHaveRow(DataSet ds)
        {
            bool valueReturn = false;
            if (ds == null)
                return false;
            if (ds.Tables[0] == null)
            {
                valueReturn = false;
            }
            else
            {
                if (ds.Tables[0].Rows.Count != 0)
                {
                    valueReturn = true;
                }
                else
                {
                    valueReturn = false;
                }
            }
            return valueReturn;
        }


        //---------------------------------------------------------------------------
        public DataSet getDataSet_Table(string str, string tableName)
        {
            m_sqlCmd.CommandText = str;
            try
            {
                m_sqlDataAdapter = new SqlDataAdapter(m_sqlCmd);
                m_dataSet = new DataSet();
                m_sqlDataAdapter.Fill(m_dataSet, tableName);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(str);
                System.Console.WriteLine(ex.ToString());
                m_strErr = ex.Message;
                return null;
            }
            return m_dataSet;
        }//End of GetDataSetFromDB
        //---------------------------------------------------------------------------
        public int executeSQL(string SQL)
        {
            try
            {
                m_sqlCmd.CommandText = SQL;
                return (m_sqlCmd.ExecuteNonQuery());
            }
            catch (Exception) { return 0; }
        }
        //---------------------------------------------------------------------------
        public SqlDataReader executeSQLDataReader(string SQL)
        {
            try
            {
                m_sqlCmd.CommandText = SQL;
                return (m_sqlCmd.ExecuteReader());
            }
            catch (Exception) { return null; }
        }
        //---------------------------------------------------------------------------

		public void startTransaction()
		{	
			m_sqlTrans = m_Conn.BeginTransaction();			
			m_sqlCmd.Transaction = m_sqlTrans;
		}// End of startTransaction pro

		private SqlDbType getDataType(string ins_code)
		{
			switch(ins_code)
			{							
				case "34": return SqlDbType.Image;
				case "35": return SqlDbType.Text;
				case "48": return SqlDbType.TinyInt;
				case "52": return SqlDbType.SmallInt;
				case "56": return SqlDbType.Int;
				case "58": return SqlDbType.SmallDateTime;
				case "59": return SqlDbType.Real;
				case "60": return SqlDbType.Money;
				case "61": return SqlDbType.DateTime;
				case "62": return SqlDbType.Float;
				case "99": return SqlDbType.NText;
				case "104": return SqlDbType.Bit;
				case "106": return SqlDbType.Decimal;
				case "122": return SqlDbType.SmallMoney;
				case "127": return SqlDbType.BigInt;
				case "165": return SqlDbType.VarBinary;
				case "167": return SqlDbType.VarChar;
				case "173": return SqlDbType.Binary;
				case "175": return SqlDbType.Char;
				case "231": return SqlDbType.NVarChar;
				case "239": return SqlDbType.NChar;
				default: return SqlDbType.UniqueIdentifier;
			}
		}// End of getDataType pro
			
		public int execProcedure(string ins_spName, string ins_value)
		{
			string sqlString = "SELECT"
				+ " dbo.syscolumns.name AS ColName,"
				+ " dbo.systypes.xtype,"
				+ " dbo.syscolumns.length AS ColLen"				
				+ " FROM"
				+ " dbo.syscolumns INNER JOIN"
				+ " dbo.sysobjects ON dbo.syscolumns.id = dbo.sysobjects.id INNER JOIN"
				+ " dbo.systypes ON dbo.syscolumns.xtype = dbo.systypes.xtype"
				+ " WHERE"
				+ " (dbo.sysobjects.name = '" + ins_spName + "')"
				+ " AND"
				+ " (dbo.systypes.status <> 1)"
				+ " ORDER BY"
				+ " dbo.sysobjects.name,"
				+ " dbo.syscolumns.colorder";

			DataSet ds = GetDataSetFromDB(sqlString);

			string[] ls_value = ins_value.Split(';');
			m_sqlCmd.CommandType = CommandType.StoredProcedure;
			m_sqlCmd.CommandText = ins_spName;
			for(int i = 0; i < ls_value.Length; i++)
			{
				
				m_sqlCmd.Parameters.Add(ds.Tables[0].Rows[i][0].ToString(),
					getDataType(ds.Tables[0].Rows[i][1].ToString()),Convert.ToInt32(ds.Tables[0].Rows[i][2].ToString()));				
				m_sqlCmd.Parameters[i].Value = ls_value[i];
			}


			int li_return = 0;
			try
			{
				li_return = m_sqlCmd.ExecuteNonQuery();				
			}
			catch(Exception ex)
			{			
				m_strErr = ex.ToString();				
			}			
			
			return li_return;					
		}// End of execProcedure pro


		public void Commit()
		{			
			m_sqlTrans.Commit();
		}// End of Commit pro


		public void Rollback()
		{			
			m_sqlTrans.Rollback();
		}// End of Rollback pro

		private void setConnectionString()
		{
            m_strConn = "Data Source=huy;Initial Catalog=TKB;User ID=sa";			
		}// End of setConnectionString pro
        private void setConnectionString(string ins_strConnect)
        {
            m_strConn = ins_strConnect;
        }// End of setConnectionString pro
		
		#region Not Used
/*		
		public DataAccess()
		{					
//			setConnectionString();
						
			try
			{
				m_Conn = new OracleConnection(m_strConn);				
			}
			catch(OracleException oe)
			{				
				m_isConnected = false;
				m_strErr = oe.Message;
				System.Console.WriteLine(m_strErr);
			}			
		}
		
		public void setNewConnection(int i_configID)
		{
			configID = i_configID;
			setConnectionString(configID);			

			try
			{
				m_Conn = new OracleConnection(m_strConn);				
			}
			catch(OracleException oe)
			{				
				m_isConnected = false;
				m_strErr = oe.Message;
				System.Console.WriteLine(m_strErr);
			}			
		}


		public DataAccess(int i_configID)
		{
			configID = i_configID;
			setConnectionString(configID);
		
			try
			{
				m_Conn = new OracleConnection(m_strConn);				
			}
			catch(OracleException oe)
			{				
				m_isConnected = false;
				m_strErr = oe.Message;
				System.Console.WriteLine(m_strErr);
			}
		}


		/// <summary>
		/// Using to open Oracle connection adn Oracle command.
		/// Always call this before using this class
		/// </summary>
		public void open()
		{
			try
			{
				if(m_Conn != null)
				{
					m_Conn.Open();
					m_oraCmd = m_Conn.CreateCommand();
					m_isConnected = true;
				}
			}
			catch(Exception)
			{
				MessageBox.Show("Can't not connect to database");
			}
		}// End of open pro


		public void open(int i_configID)
		{
			setNewConnection(i_configID);
			open();
		}


		/// <summary>
		/// Close and dispose Oracle connection and Oracle command
		/// Always call this when finish using this class
		/// </summary>
		public void close()
		{
			try
			{
				if(m_oraDataReader != null)
					m_oraDataReader.Close();
				m_oraCmd.Dispose();
				m_Conn.Close();
				m_Conn.Dispose();
			}
			catch(Exception){}
		}// End of close pro


		public DataAccess(string sConn)
		{
			this.m_strConn = sConn;			
			try
			{
				m_Conn = new OracleConnection(m_strConn);
			}
			catch(OracleException oe)
			{
				m_isConnected = false;
				m_strErr = oe.Message;
				System.Console.WriteLine(m_strErr);
			}
		} // End of DataAccess pro

		private void setConnectionString()
		{
			ConfigReader in_configReader = new ConfigReader(0,"cdrReportConfig.xml");			
						
			string user = in_configReader.getTag("username");
			string source = in_configReader.getTag("datasource");
			string password = in_configReader.getTag("password");
				
			m_strConn = "User ID=" + user + ";Data Source="
					+ source + ";password=" + password;				
					
"data source=192.168.1.252;initial catalog=SCTV;user id=sctv;password=sctv";										
		}


		private bool setConnectionString(int i_configID)
		{			
			ConfigReader in_configReader = new ConfigReader(i_configID);
			try
			{				
				string user = in_configReader.getTag("USER");
				string source = in_configReader.getTag("SOURCE");
				string password = in_configReader.getTag("PASSWORD");

				m_strConn = "User ID=" + user + ";Data Source="
					+ source + ";password=" + password;								
				return true;
			}
			catch(Exception ex)
			{								
				ex.ToString();
				return false;
			}
		}// End of setFileName pro

					
		public void startTransaction()
		{									
			m_oraTrans = m_Conn.BeginTransaction();			
		}// End of startTransaction pro


		public void Commit()
		{			
			m_oraTrans.Commit();
		}// End of Commit pro


		public void Rollback()
		{			
			m_oraTrans.Rollback();
		}// End of Rollback pro


		/// <summary>
		/// Call insert, update sql query.
		/// </summary>
		/// <param name="str">sql query</param>
		/// <returns>return number of record changed</returns>
		public int UpdateToDB(string str)
		{			
			int error = 0;
			m_sqlStatement = str;
			m_oraCmd.CommandText = m_sqlStatement;			
			try
			{
				error = m_oraCmd.ExecuteNonQuery();
			}
			catch(Exception e)
			{
				m_strErr = e.Message;				
				System.Console.WriteLine(m_strErr);
				return -1;
			}
			return error;
		}

		/// <summary>
		/// Exec Oracle store procedure to insert large data contained in input arrays
		/// </summary>
		/// <param name="ins_SPName">Store procedure name (redundance)</param>
		/// <param name="ini_height">number of records</param>
		/// <param name="ini_width">number of values in one record (redundance)</param>
		/// <param name="ina_value">input data</param>
		/// <returns>return number of records inserted (if fail, return -1)</returns>
		public int ExecProcedureWithLargeInput(string ins_SPName, int ini_height,
			int ini_width, string[][] ina_value)
		{
			int li_return = 1;
			if(ini_height > 30000)
				return -1;
			this.startTransaction();
			m_oraCmd.CommandType = CommandType.StoredProcedure;
			m_oraCmd.CommandText = ins_SPName;
			m_oraCmd.ArrayBindCount = ini_height;
			OracleParameter[] lo_op = new OracleParameter[ini_width];

			for(int i = 0; i <= ini_width - 1; i++)
			{				
				lo_op[i] = new OracleParameter("OP" + i.ToString(),OracleDbType.NVarchar2);
				lo_op[i].Direction = ParameterDirection.Input;
				lo_op[i].Value = ina_value[i];
				m_oraCmd.Parameters.Add(lo_op[i]);				
			}			
			try
			{
				li_return = m_oraCmd.ExecuteNonQuery();
				System.Console.WriteLine(li_return);
			}
			catch(Exception ex)
			{				
				System.Console.WriteLine(ex.ToString());
				this.Rollback();
				li_return = -1;
			}

			if(li_return != -1)
				this.Commit();

			for(int i = 0; i <= ini_width - 1; i++)
			{				
				lo_op[i].Dispose();
				lo_op[i] = null;
			}

			return li_return;
		}// End of ExecProcedureWithLargeInput pro


		/// <summary>
		/// Repair parameters before call [ExecProcedure] procedure
		/// </summary>
		/// <param name="ins_SPName">Store procedure name</param>
		/// <param name="ini_width">number of values in one record</param>
		public void PrepareProcedure(string ins_SPName, int ini_width)
		{											
			m_oraCmd.CommandType = CommandType.StoredProcedure;
			m_oraCmd.CommandText = ins_SPName;						

			for(int i = 0; i <= ini_width - 1; i++)			
				m_oraCmd.Parameters.Add("OP" + i.ToString(),Oracle.DataAccess.Client.OracleDbType.NVarchar2);			
		}// End of PrepareProcedure pro


		/// <summary>
		/// Exec Oracle store procedure to insert one record.
		/// Call after PrepareProcedure procedure.
		/// </summary>
		/// <param name="ini_width">number of values in one record</param>
		/// <param name="ina_value">input data</param>
		/// <returns>1: if success, 2: if fail</returns>
		public int ExecProcedure(int ini_width, string[] ina_value)
		{
			int li_return = 1;
			for(int i = 0; i <= ini_width - 1; i++)			
				m_oraCmd.Parameters["OP" + i.ToString()].Value = ina_value[i];
		
			try
			{
				m_oraCmd.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				ex.ToString();
				li_return = 0;
			}			
			
			return li_return;
		}// End of ExecProcedure pro
		
/*
		public OracleDataReader GetDataReaderFromDB(string str)
		{
			//System.Console.WriteLine(str);
			m_sqlStatement = str;			
			m_oraCmd.CommandText = m_sqlStatement;
			try
			{
				m_oraDataReader = m_oraCmd.ExecuteReader();				
			}
			catch(Exception e)
			{
				m_strErr = e.Message;
				System.Console.WriteLine(m_strErr);
				return null;
			}
			return m_oraDataReader;
		}// End of GetDataReaderFromDB

		
		// Input: sql String
		// Output: DataSet
		// If error: return null
		public DataSet GetDataSetFromDB(string str)
		{
			System.Console.WriteLine(str);
			m_sqlStatement = str;
			try
			{
				m_oraAdp = new OracleDataAdapter(m_sqlStatement, m_Conn);
				m_dataSet = new DataSet();
				m_oraAdp.Fill(m_dataSet);
			}
			catch(Exception e)
			{
				m_strErr = e.Message;				
				System.Console.WriteLine(m_strErr);
				return null;
			}
			return m_dataSet;
		}//End of GetDataSetFromDB
*/
		#endregion

	}// End of DataAccess class	
	#endregion

    #region class DataAccess
    public class DataAccessFox
    {
        /// <summary>
        /// Private variables for connect to DB and perform data mining
        /// </summary>

        //private int configID = 0;
        //private string userID;

        private string m_strConn = "Data Source=huy;Initial Catalog=TKB;User ID=sa";
        
        private OleDbConnection m_Conn;
        private OleDbCommand m_OleDbCmd;
        private OleDbTransaction m_OleDbTrans;

        private OleDbDataReader m_OleDbDataReader;
        private OleDbDataAdapter m_OleDbDataAdapter;
        private DataSet m_dataSet;

        private string m_OleDbStatement;


        /// <summary>
        /// Public variables
        /// </summary>
        public bool m_isConnected = false;
        public string m_strErr = "";
        public int m_errCode;

        public DataAccessFox()
        {
            setConnectionString();
            try
            {
                m_Conn = new OleDbConnection(m_strConn);
            }
            catch (Exception ex)
            {
                m_isConnected = false;
                m_strErr = ex.Message;
            }
        }// End of DataAccess pro
        public DataAccessFox(string ins_strConnect)
        {
            setConnectionString(ins_strConnect);
            try
            {
                m_Conn = new OleDbConnection(m_strConn);
            }
            catch (Exception ex)
            {
                m_isConnected = false;
                m_strErr = ex.Message;
            }
        }// End of DataAccess pro

        public string ReadRegistry(string ins_keyName)
        {
            RegistryKey rk = Registry.LocalMachine;
            RegistryKey skl = rk.OpenSubKey("SOFTWARE\\" + Application.ProductName);

            if (skl == null)
                return null;
            else
            {
                try
                {
                    return (string)skl.GetValue(ins_keyName.ToUpper());
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.ToString());
                    return null;
                }
            }

        }// End of ReadRegistry pro

        public void open()
        {
            try
            {
                if (m_Conn != null)
                {
                    m_Conn.Open();
                    m_OleDbCmd = m_Conn.CreateCommand();
                    m_isConnected = true;
                }
            }
            catch (Exception ex)
            {
                m_isConnected = false;
                m_strErr = ex.Message;
            }
        }// End of open pro

        /// <summary>
        /// Close and dispose Oracle connection and Oracle command
        /// Always call this when finish using this class
        /// </summary>
        public void close()
        {
            try
            {
                if (m_OleDbDataReader != null)
                    m_OleDbDataReader.Close();
                m_OleDbCmd.Dispose();
                m_Conn.Close();
                m_Conn.Dispose();
            }
            catch (Exception) { }
        }// End of close pro

        private string generateLogString(string in_sqlString)
        {
            string ls_mysqlString = in_sqlString.ToUpper();
            string ls_sqlType = "NONE";
            ls_mysqlString = ls_mysqlString.Replace("'", "''");


            if (ls_mysqlString.StartsWith("INSERT"))
                ls_sqlType = "INSERT";
            else
                if (ls_mysqlString.StartsWith("DELETE"))
                    ls_sqlType = "DELETE";
                else
                    if (ls_mysqlString.StartsWith("UPDATE"))
                        ls_sqlType = "UPDATE";

            string ls_sqlLog = "INSERT INTO TBL_LOG(USR_ID, LOG_DATETIME, LOG_TYPE, LOG_INFO) VALUES('"
                + ReadRegistry("sctvUserID") + "',getdate(),'"
                + ls_sqlType + "','"
                + ls_mysqlString + "')";

            return ls_sqlLog;
        }// End of generateLogString pro

        /// <summary>
        /// Call insert, update OleDb query. return -1 if occurs an error
        /// </summary>
        /// <param name="str">OleDb query</param>
        /// <returns>return number of record changed</returns>
        public int UpdateToDB(string str)
        {
            int error = 0;
            //m_OleDbStatement = fixVietNameseOleDbString(str);
            m_OleDbStatement = str;
            m_OleDbCmd.CommandText = m_OleDbStatement;

            try
            {
                error = m_OleDbCmd.ExecuteNonQuery();

                //				// Write log
                //				m_OleDbCmd.CommandText = generateLogString(m_OleDbStatement);
                //				m_OleDbCmd.ExecuteNonQuery();

            }
            catch
            {

                //ErrorEntity err = new ErrorEntity();
                //err.alert_string("Error: " + ex.Message);                  
                return -1;
            }

            return error;
        }// End of UpdateToDB pro

        private string fixVietNameseOleDbString(string ins_OleDbString)
        {
            ins_OleDbString = ins_OleDbString.Replace("''", "null");
            string[] OleDb = ins_OleDbString.Split('\'');
            ins_OleDbString = "";
            for (int i = 0; i < OleDb.Length; i++)
            {
                ins_OleDbString += OleDb[i];
                if (i < OleDb.Length - 1)
                {
                    if (i % 2 == 0)
                        ins_OleDbString += "N'";
                    else
                        ins_OleDbString += "'";
                }
            }
            return ins_OleDbString;
        }


        /// <summary>
        /// Get data into DataReader
        /// </summary>
        /// <param name="str">OleDb String</param>
        /// <returns>DataReader, return null if error</returns>
        public OleDbDataReader GetDataReaderFromDB(string str)
        {
            //m_OleDbStatement = fixVietNameseOleDbString(str);			
            m_OleDbStatement = str;
            m_OleDbCmd.CommandText = m_OleDbStatement;
            try
            {
                m_OleDbDataReader = m_OleDbCmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(str);
                System.Console.WriteLine(ex.ToString());
                m_strErr = ex.Message;
                return null;
            }
            return m_OleDbDataReader;
        }// End of GetDataReaderFromDB

        /// <summary>
        /// Get data into DataSet
        /// </summary>
        /// <param name="str">OleDb String</param>
        /// <returns>DataSet, return null if error</returns>
        public DataSet GetDataSetFromDB(string str)
        {
            //m_OleDbStatement = fixVietNameseOleDbString(str);
            m_OleDbStatement = str;
            m_OleDbCmd.CommandText = m_OleDbStatement;
            try
            {
                m_OleDbDataAdapter = new OleDbDataAdapter(m_OleDbCmd);
                m_dataSet = new DataSet();
                m_OleDbDataAdapter.Fill(m_dataSet);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(str);
                System.Console.WriteLine(ex.ToString());
                m_strErr = ex.Message;
                return null;
            }
            return m_dataSet;
        }//End of GetDataSetFromDB

        /// <summary>
        /// if row[0][ins_coloumnName].toString() has values: return true, else return false
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="ins_tableName"></param>
        /// <param name="ins_columnName"></param>
        /// <returns></returns>
        public bool testExistValueInTable(DataSet ds)
        {
            if (ds.Tables[0] == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// if rowcount != 0 return true, if table not exist or table exist but rowcount = 0: return false
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public bool testExistValueInTableAndHaveRow(DataSet ds)
        {
            bool valueReturn = false;
            if (ds == null)
                return false;
            if (ds.Tables[0] == null)
            {
                valueReturn = false;
            }
            else
            {
                if (ds.Tables[0].Rows.Count != 0)
                {
                    valueReturn = true;
                }
                else
                {
                    valueReturn = false;
                }
            }
            return valueReturn;
        }


        //---------------------------------------------------------------------------
        public DataSet getDataSet_Table(string str, string tableName)
        {
            m_OleDbCmd.CommandText = str;
            try
            {
                m_OleDbDataAdapter = new OleDbDataAdapter(m_OleDbCmd);
                m_dataSet = new DataSet();
                m_OleDbDataAdapter.Fill(m_dataSet, tableName);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(str);
                System.Console.WriteLine(ex.ToString());
                m_strErr = ex.Message;
                return null;
            }
            return m_dataSet;
        }//End of GetDataSetFromDB
        //---------------------------------------------------------------------------
        public int executeOleDb(string OleDb)
        {
            try
            {
                m_OleDbCmd.CommandText = OleDb;
                return (m_OleDbCmd.ExecuteNonQuery());
            }
            catch (Exception) { return 0; }
        }
        //---------------------------------------------------------------------------
        public OleDbDataReader executeOleDbDataReader(string OleDb)
        {
            try
            {
                m_OleDbCmd.CommandText = OleDb;
                return (m_OleDbCmd.ExecuteReader());
            }
            catch (Exception) { return null; }
        }
        //---------------------------------------------------------------------------

        public void startTransaction()
        {
            m_OleDbTrans = m_Conn.BeginTransaction();
            m_OleDbCmd.Transaction = m_OleDbTrans;
        }// End of startTransaction pro

        
        public void Commit()
        {
            m_OleDbTrans.Commit();
        }// End of Commit pro


        public void Rollback()
        {
            m_OleDbTrans.Rollback();
        }// End of Rollback pro

        private void setConnectionString()
        {
            m_strConn = "Data Source=huy;Initial Catalog=TKB;User ID=sa";
        }// End of setConnectionString pro
        private void setConnectionString(string ins_strConnect)
        {
            m_strConn = ins_strConnect;
        }// End of setConnectionString pro

        #region Not Used
        /*		
		public DataAccess()
		{					
//			setConnectionString();
						
			try
			{
				m_Conn = new OracleConnection(m_strConn);				
			}
			catch(OracleException oe)
			{				
				m_isConnected = false;
				m_strErr = oe.Message;
				System.Console.WriteLine(m_strErr);
			}			
		}
		
		public void setNewConnection(int i_configID)
		{
			configID = i_configID;
			setConnectionString(configID);			

			try
			{
				m_Conn = new OracleConnection(m_strConn);				
			}
			catch(OracleException oe)
			{				
				m_isConnected = false;
				m_strErr = oe.Message;
				System.Console.WriteLine(m_strErr);
			}			
		}


		public DataAccess(int i_configID)
		{
			configID = i_configID;
			setConnectionString(configID);
		
			try
			{
				m_Conn = new OracleConnection(m_strConn);				
			}
			catch(OracleException oe)
			{				
				m_isConnected = false;
				m_strErr = oe.Message;
				System.Console.WriteLine(m_strErr);
			}
		}


		/// <summary>
		/// Using to open Oracle connection adn Oracle command.
		/// Always call this before using this class
		/// </summary>
		public void open()
		{
			try
			{
				if(m_Conn != null)
				{
					m_Conn.Open();
					m_oraCmd = m_Conn.CreateCommand();
					m_isConnected = true;
				}
			}
			catch(Exception)
			{
				MessageBox.Show("Can't not connect to database");
			}
		}// End of open pro


		public void open(int i_configID)
		{
			setNewConnection(i_configID);
			open();
		}


		/// <summary>
		/// Close and dispose Oracle connection and Oracle command
		/// Always call this when finish using this class
		/// </summary>
		public void close()
		{
			try
			{
				if(m_oraDataReader != null)
					m_oraDataReader.Close();
				m_oraCmd.Dispose();
				m_Conn.Close();
				m_Conn.Dispose();
			}
			catch(Exception){}
		}// End of close pro


		public DataAccess(string sConn)
		{
			this.m_strConn = sConn;			
			try
			{
				m_Conn = new OracleConnection(m_strConn);
			}
			catch(OracleException oe)
			{
				m_isConnected = false;
				m_strErr = oe.Message;
				System.Console.WriteLine(m_strErr);
			}
		} // End of DataAccess pro

		private void setConnectionString()
		{
			ConfigReader in_configReader = new ConfigReader(0,"cdrReportConfig.xml");			
						
			string user = in_configReader.getTag("username");
			string source = in_configReader.getTag("datasource");
			string password = in_configReader.getTag("password");
				
			m_strConn = "User ID=" + user + ";Data Source="
					+ source + ";password=" + password;				
					
"data source=192.168.1.252;initial catalog=SCTV;user id=sctv;password=sctv";										
		}


		private bool setConnectionString(int i_configID)
		{			
			ConfigReader in_configReader = new ConfigReader(i_configID);
			try
			{				
				string user = in_configReader.getTag("USER");
				string source = in_configReader.getTag("SOURCE");
				string password = in_configReader.getTag("PASSWORD");

				m_strConn = "User ID=" + user + ";Data Source="
					+ source + ";password=" + password;								
				return true;
			}
			catch(Exception ex)
			{								
				ex.ToString();
				return false;
			}
		}// End of setFileName pro

					
		public void startTransaction()
		{									
			m_oraTrans = m_Conn.BeginTransaction();			
		}// End of startTransaction pro


		public void Commit()
		{			
			m_oraTrans.Commit();
		}// End of Commit pro


		public void Rollback()
		{			
			m_oraTrans.Rollback();
		}// End of Rollback pro


		/// <summary>
		/// Call insert, update OleDb query.
		/// </summary>
		/// <param name="str">OleDb query</param>
		/// <returns>return number of record changed</returns>
		public int UpdateToDB(string str)
		{			
			int error = 0;
			m_OleDbStatement = str;
			m_oraCmd.CommandText = m_OleDbStatement;			
			try
			{
				error = m_oraCmd.ExecuteNonQuery();
			}
			catch(Exception e)
			{
				m_strErr = e.Message;				
				System.Console.WriteLine(m_strErr);
				return -1;
			}
			return error;
		}

		/// <summary>
		/// Exec Oracle store procedure to insert large data contained in input arrays
		/// </summary>
		/// <param name="ins_SPName">Store procedure name (redundance)</param>
		/// <param name="ini_height">number of records</param>
		/// <param name="ini_width">number of values in one record (redundance)</param>
		/// <param name="ina_value">input data</param>
		/// <returns>return number of records inserted (if fail, return -1)</returns>
		public int ExecProcedureWithLargeInput(string ins_SPName, int ini_height,
			int ini_width, string[][] ina_value)
		{
			int li_return = 1;
			if(ini_height > 30000)
				return -1;
			this.startTransaction();
			m_oraCmd.CommandType = CommandType.StoredProcedure;
			m_oraCmd.CommandText = ins_SPName;
			m_oraCmd.ArrayBindCount = ini_height;
			OracleParameter[] lo_op = new OracleParameter[ini_width];

			for(int i = 0; i <= ini_width - 1; i++)
			{				
				lo_op[i] = new OracleParameter("OP" + i.ToString(),OracleDbType.NVarchar2);
				lo_op[i].Direction = ParameterDirection.Input;
				lo_op[i].Value = ina_value[i];
				m_oraCmd.Parameters.Add(lo_op[i]);				
			}			
			try
			{
				li_return = m_oraCmd.ExecuteNonQuery();
				System.Console.WriteLine(li_return);
			}
			catch(Exception ex)
			{				
				System.Console.WriteLine(ex.ToString());
				this.Rollback();
				li_return = -1;
			}

			if(li_return != -1)
				this.Commit();

			for(int i = 0; i <= ini_width - 1; i++)
			{				
				lo_op[i].Dispose();
				lo_op[i] = null;
			}

			return li_return;
		}// End of ExecProcedureWithLargeInput pro


		/// <summary>
		/// Repair parameters before call [ExecProcedure] procedure
		/// </summary>
		/// <param name="ins_SPName">Store procedure name</param>
		/// <param name="ini_width">number of values in one record</param>
		public void PrepareProcedure(string ins_SPName, int ini_width)
		{											
			m_oraCmd.CommandType = CommandType.StoredProcedure;
			m_oraCmd.CommandText = ins_SPName;						

			for(int i = 0; i <= ini_width - 1; i++)			
				m_oraCmd.Parameters.Add("OP" + i.ToString(),Oracle.DataAccess.Client.OracleDbType.NVarchar2);			
		}// End of PrepareProcedure pro


		/// <summary>
		/// Exec Oracle store procedure to insert one record.
		/// Call after PrepareProcedure procedure.
		/// </summary>
		/// <param name="ini_width">number of values in one record</param>
		/// <param name="ina_value">input data</param>
		/// <returns>1: if success, 2: if fail</returns>
		public int ExecProcedure(int ini_width, string[] ina_value)
		{
			int li_return = 1;
			for(int i = 0; i <= ini_width - 1; i++)			
				m_oraCmd.Parameters["OP" + i.ToString()].Value = ina_value[i];
		
			try
			{
				m_oraCmd.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				ex.ToString();
				li_return = 0;
			}			
			
			return li_return;
		}// End of ExecProcedure pro
		
/*
		public OracleDataReader GetDataReaderFromDB(string str)
		{
			//System.Console.WriteLine(str);
			m_OleDbStatement = str;			
			m_oraCmd.CommandText = m_OleDbStatement;
			try
			{
				m_oraDataReader = m_oraCmd.ExecuteReader();				
			}
			catch(Exception e)
			{
				m_strErr = e.Message;
				System.Console.WriteLine(m_strErr);
				return null;
			}
			return m_oraDataReader;
		}// End of GetDataReaderFromDB

		
		// Input: OleDb String
		// Output: DataSet
		// If error: return null
		public DataSet GetDataSetFromDB(string str)
		{
			System.Console.WriteLine(str);
			m_OleDbStatement = str;
			try
			{
				m_oraAdp = new OracleDataAdapter(m_OleDbStatement, m_Conn);
				m_dataSet = new DataSet();
				m_oraAdp.Fill(m_dataSet);
			}
			catch(Exception e)
			{
				m_strErr = e.Message;				
				System.Console.WriteLine(m_strErr);
				return null;
			}
			return m_dataSet;
		}//End of GetDataSetFromDB
*/
        #endregion

    }// End of DataAccess class	
    #endregion
}