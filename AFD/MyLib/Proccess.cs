﻿/*
 * Created by SharpDevelop.
 * User: huy
 * Date: 13/01/2007
 * Time: 10:04 SA
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;

namespace MyProccess
{
	/// <summary>
	/// Description of Proccess.
	/// </summary>
	public class Proccess
	{		
		private bool runing;
		private string name;
		private int percentComplete;
		private bool success;
		public Proccess(string ins_name)
		{
			runing = true;
			success = false;
			name = ins_name;
		}		
		public bool Runing
		{
			get
			{
				return runing;
			}
			set
			{
				runing = value;
			}
		}
		public string Name
		{
			get
			{
				return name;
			}
			set
			{
				name = value;
			}
		}
		public int PercentComplete
		{
			get
			{
				return percentComplete;
			}
			set
			{
				percentComplete = value;
			}
		}
		public bool Success
		{
			get
			{
				return success;
			}
			set
			{
				success = value;
			}
		}
	}
}
