using System;
using System.Collections.Generic;
using System.Text;

namespace BAR_AllGPS
{
    class ListValidEvents
    {
        List<string> begin;
        List<string> end;
        public ListValidEvents()
        {
            begin = new List<string>();
            begin.Add("IGNITION ON");
            begin.Add("LIVE");//with no zero
            begin.Add("LEAVE GARAGE");//Leave School Leave Garage
            begin.Add("LEAVE SCHOOL");

            end = new List<string>();
            end.Add("IGNITION OFF");//Ignition Off
            end.Add("LIVE");//with no zero
        }

        public List<string> Begin
        {
            get
            {
                return begin;
            }
            set
            {
                begin = value;
            }
        }
        public List<string> End
        {
            get
            {
                return end;
            }
            set
            {
                end = value;
            }
        }
    }

}
