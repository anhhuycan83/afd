using System;
using System.Collections.Generic;
using System.Text;

namespace StopArrivalDeparture.MyLib
{
    class ClsGPSPEvents
    {
        public enum EventType
        {
            None = 0,
            IgnitionOn,
            IgnitionOff,
            StopBegin,
            StopEnd,
            Live,
            CellLive,
            CellStopBegin,
            CellStopEnd,
            IdleBegin,
            IdleEnd,
            StudentSwipe,
            EmergencyBegin,
            EmergencyEnd,
            EmergencyDoorOpen,
            EmergencyDoorClose,
            EmergencyPanicButtonOn,
            EmergencyPanicButtonOFF,
            ExcessiveSpeedBegin,
            ExcessiveSpeedEnd,
            CellExcessiveSpeedBegin,
            CellExcessiveSpeedEnd,
            ImproperStop,
            UnitShutsOff,
            UnitOn,
            // events below here are used only for reporting
            BusArrived,  // matches planned
            BusDeparted,// matches planned
            AtSchool,
            LeaveSchool,
            MissedStop,
            BeginningOfRun,
            EndOfRun,
            AtGarage,
            LeaveGarage,
            EarlyException,
            LateException,
            IdleException,
            SpeedException,
            UserRunBegin,
            UserRunEnd,
            UserStopBegin,
            UserSchoolBegin,
            UserGarageBegin,
            UserRailroadCrossingBegin,
            UserHopSpotBegin,
            // below here used by performance statistics
            StopBegin_FindStop,
            StopBegin_MultiStopCheck,
            StopBegin_AddUnscheduledStop,
            StopBegin_OnStopVisited,
            OnLive_Checks,
            OnLive_VehicleUpdate,
            EOD_VehicleLocation,
            EventProcessorProcess,
            Sleep,
            UnitWakeCode,
            VehicleSubstitutionBegin,
            VehicleSubstitutionEnd,
            CellIdleBegin,
            CellIdleEnd,
            CellPing,
            CellLowBattery,
            CellRunBegin,
            CellRunEnd,
            CellUserStop,
            CellDriverLogin,
            CellDriverInvalidLogin,
            CellDriverLogout,
            CellDriverInspectionBegin,
            CellDriverInspectionEnd,
            CellDriverBreakBegin,
            CellDriverBreakEnd,
            CellDriverChildCheckBegin,
            CellDriverChildCheckEnd,
            Unknown,
            NumEvents            
        }

        public ClsGPSPEvents()
        {

        }

        public static string GetNameViewFromEnum(EventType eEventType)
        {
            //Enum.GetName(EventType, 1);
            string pstrEventName;
            //Enum.Format(typeof(EventType), e, "D");
            //            Enum.GetName(typeof(EventType), 1);
            switch (eEventType)
            {
                case EventType.IgnitionOn:
                    pstrEventName = "Ignition On";
                    break;
                case EventType.IgnitionOff:
                    pstrEventName = "Ignition Off";
                    break;
                case EventType.StopBegin:
                    pstrEventName = "Stop Begin";
                    break;
                case EventType.StopEnd:
                    pstrEventName = "Stop End";
                    break;
                case EventType.CellStopBegin:
                    pstrEventName = "Stop Begin";
                    break;
                case EventType.CellStopEnd:
                    pstrEventName = "Stop End";
                    break;
                case EventType.IdleBegin:
                    pstrEventName = "Idle Begin";
                    break;
                case EventType.IdleEnd:
                    pstrEventName = "Idle End";
                    break;
                case EventType.Live:
                    pstrEventName = "Live";
                    break;
                case EventType.CellLive:
                    pstrEventName = "Cell Live";
                    break;
                case EventType.StudentSwipe:
                    pstrEventName = "Student Swipe";
                    break;
                case EventType.ImproperStop:
                    pstrEventName = "Improper Stop";
                    break;
                case EventType.UnitShutsOff:
                    pstrEventName = "Unit OFF";
                    break;
                case EventType.UnitOn:
                    pstrEventName = "Unit ON";
                    break;
                case EventType.BusArrived:
                    pstrEventName = "Bus Arrived";
                    break;
                case EventType.BusDeparted:
                    pstrEventName = "Bus Departed";
                    break;
                case EventType.AtSchool:
                    pstrEventName = "At School";
                    break;
                case EventType.LeaveSchool:
                    pstrEventName = "Leave School";
                    break;
                case EventType.MissedStop:
                    pstrEventName = "Missed Stop";
                    break;
                case EventType.BeginningOfRun:
                    pstrEventName = "Beginning Of Run";
                    break;
                case EventType.EndOfRun:
                    pstrEventName = "End Of Run";
                    break;
                case EventType.AtGarage:
                    pstrEventName = "At Garage";
                    break;
                case EventType.LeaveGarage:
                    pstrEventName = "Leave Garage";
                    break;
                case EventType.EarlyException:
                    pstrEventName = "Early Exception";
                    break;
                case EventType.LateException:
                    pstrEventName = "Late Exception";
                    break;
                case EventType.IdleException:
                    pstrEventName = "Idle Exception";
                    break;
                case EventType.SpeedException:
                    pstrEventName = "Speed Exception";
                    break;
                case EventType.UserRunBegin:
                    //pstrEventName = "UserRunBegin";  
                    pstrEventName = "UserRun Begin";
                    break;
                case EventType.UserRunEnd:
                    //pstrEventName = "UserRunEnd";  
                    pstrEventName = "UserRun End";
                    break;
                case EventType.UserStopBegin:
                    //pstrEventName = "UserStopBegin";  
                    pstrEventName = "UserStop";
                    break;
                case EventType.UserSchoolBegin:
                    //pstrEventName = "UserSchoolBegin";  
                    pstrEventName = "UserSchool";
                    break;
                case EventType.UserGarageBegin:
                    //pstrEventName = "UserGarageBegin";  
                    pstrEventName = "UserGarage";
                    break;
                case EventType.UserRailroadCrossingBegin:
                    //pstrEventName = "UserRailroadCrossingBegin";  
                    pstrEventName = "UserRRCrossing";
                    break;
                case EventType.UserHopSpotBegin:
                    //pstrEventName = "UserHopSpotBegin";  
                    pstrEventName = "UserHotSpot";
                    break;
                case EventType.EmergencyBegin:
                    pstrEventName = "Emergency Begin";
                    break;
                case EventType.EmergencyEnd:
                    pstrEventName = "Emergency End";
                    break;
                case EventType.EmergencyDoorOpen:
                    pstrEventName = "Emergency Door Open";
                    break;
                case EventType.EmergencyDoorClose:
                    pstrEventName = "Emergency Door Close";
                    break;
                case EventType.ExcessiveSpeedBegin:
                    pstrEventName = "Excessive Speed Begin";
                    break;
                case EventType.ExcessiveSpeedEnd:
                    pstrEventName = "Excessive Speed End";
                    break;
                case EventType.CellExcessiveSpeedBegin:
                    pstrEventName = "Cell Excessive Speed Begin";
                    break;
                case EventType.CellExcessiveSpeedEnd:
                    pstrEventName = "Cell Excessive Speed End";
                    break;
                case EventType.EmergencyPanicButtonOn:
                    pstrEventName = "Panic Button On";
                    break;
                case EventType.EmergencyPanicButtonOFF:
                    pstrEventName = "Panic Button Off";
                    break;
                case EventType.StopBegin_FindStop:
                    pstrEventName = "StopBegin_FindStop";
                    break;
                case EventType.StopBegin_MultiStopCheck:
                    pstrEventName = "StopBegin_MultiStopCheck";
                    break;
                case EventType.StopBegin_AddUnscheduledStop:
                    pstrEventName = "StopBegin_AddUnscheduledStop";
                    break;
                case EventType.OnLive_Checks:
                    pstrEventName = "OnLive_Checks";
                    break;
                case EventType.OnLive_VehicleUpdate:
                    pstrEventName = "OnLive_VehicleUpdate";
                    break;
                case EventType.StopBegin_OnStopVisited:
                    pstrEventName = "StopBegin_OnStopVisited";
                    break;
                case EventType.EOD_VehicleLocation:
                    pstrEventName = "EOD_VehicleLocation";
                    break;
                case EventType.EventProcessorProcess:
                    pstrEventName = "EventProcessorProcess";
                    break;
                case EventType.Sleep:
                    pstrEventName = "Sleep";
                    break;
                case EventType.UnitWakeCode:
                    pstrEventName = "Unit Wake Message";
                    break;
                case EventType.VehicleSubstitutionBegin:
                    pstrEventName = "Veh Subst Begin";
                    break;
                case EventType.VehicleSubstitutionEnd:
                    pstrEventName = "Veh Subst End";
                    break;
                case EventType.CellIdleBegin:
                    pstrEventName = "Cell Idle Begin";
                    break;
                case EventType.CellIdleEnd:
                    pstrEventName = "Cell Idle End";
                    break;
                case EventType.CellLowBattery:
                    pstrEventName = "Cell Low Battery";
                    break;
                case EventType.CellPing:
                    pstrEventName = "Cell Ping";
                    break;
                case EventType.CellRunBegin:
                    pstrEventName = "Cell Run Begin";
                    break;
                case EventType.CellRunEnd:
                    pstrEventName = "Cell Run End";
                    break;
                case EventType.CellUserStop:
                    pstrEventName = "Stop Begin";
                    break;
                case EventType.CellDriverLogin:
                    pstrEventName = "Cell Driver Login";
                    break;
                case EventType.CellDriverInspectionBegin:
                    pstrEventName = "Cell Driver Inspection Begin";
                    break;
                case EventType.CellDriverInspectionEnd:
                    pstrEventName = "Cell Driver Inspection End";
                    break;
                case EventType.CellDriverBreakBegin:
                    pstrEventName = "Cell Driver Break Begin";
                    break;
                case EventType.CellDriverBreakEnd:
                    pstrEventName = "Cell Driver Break End";
                    break;
                case EventType.CellDriverChildCheckBegin:
                    pstrEventName = "Cell Driver Child Check Begin";
                    break;
                case EventType.CellDriverChildCheckEnd:
                    pstrEventName = "Cell Driver Child Check End";
                    break;
                case EventType.CellDriverInvalidLogin:
                    pstrEventName = "Cell Driver Invalid Login";
                    break;
                case EventType.CellDriverLogout:
                    pstrEventName = "Cell Driver Logout";
                    break;

                default:
                    pstrEventName = "Unknown";
                    break;
            }
            return pstrEventName;
        }

        public static EventType GetEnumFromNameView(string sEventType)
        {
            EventType pstrEventName;
            switch (sEventType)
            {
                case "Ignition On":
                    pstrEventName = EventType.IgnitionOn;
                    break;
                case "Ignition Off":
                    pstrEventName = EventType.IgnitionOff;
                    break;

                case "Stop Begin":
                    pstrEventName = EventType.StopBegin;
                    break;
                case "Stop End":
                    pstrEventName = EventType.StopEnd;
                    break;
                //case "Stop Begin":
                //    pstrEventName = EventType.CellStopBegin;
                //    break;
                //case "Stop End":
                //    pstrEventName = EventType.CellStopEnd;
                //    break;
                case "Idle Begin":
                    pstrEventName = EventType.IdleBegin;
                    break;
                case "Idle End":
                    pstrEventName = EventType.IdleEnd;
                    break;
                case "Live":
                    pstrEventName = EventType.Live;
                    break;
                case "Cell Live":
                    pstrEventName = EventType.CellLive;
                    break;
                case "Student Swipe":
                    pstrEventName = EventType.StudentSwipe;
                    break;
                case "Improper Stop":
                    pstrEventName = EventType.ImproperStop;
                    break;
                case "Unit OFF":
                    pstrEventName = EventType.UnitShutsOff;
                    break;
                case "Unit ON":
                    pstrEventName = EventType.UnitOn;
                    break;
                case "Bus Arrived":
                    pstrEventName = EventType.BusArrived;
                    break;
                case "Bus Departed":
                    pstrEventName = EventType.BusDeparted;
                    break;
                case "At School":
                    pstrEventName = EventType.AtSchool;
                    break;
                case "Leave School":
                    pstrEventName = EventType.LeaveSchool;
                    break;
                case "Missed Stop":
                    pstrEventName = EventType.MissedStop;
                    break;
                case "Beginning Of Run":
                    pstrEventName = EventType.BeginningOfRun;
                    break;
                case "End Of Run":
                    pstrEventName = EventType.EndOfRun;
                    break;
                case "At Garage":
                    pstrEventName = EventType.AtGarage;
                    break;
                case "Leave Garage":
                    pstrEventName = EventType.LeaveGarage;
                    break;
                case "Early Exception":
                    pstrEventName = EventType.EarlyException;
                    break;
                case "Late Exception":
                    pstrEventName = EventType.LateException;
                    break;
                case "Idle Exception":
                    pstrEventName = EventType.IdleException;
                    break;
                case "Speed Exception":
                    pstrEventName = EventType.SpeedException;
                    break;
                case "UserRun Begin":
                    //pstrEventName = case "UserRunBegin": 
                    pstrEventName = EventType.UserRunBegin;
                    break;
                case "UserRun End":
                    //pstrEventName = case "UserRunEnd": 
                    pstrEventName = EventType.UserRunEnd;
                    break;
                case "UserStop":
                    //pstrEventName = case "UserStopBegin": 
                    pstrEventName = EventType.UserStopBegin;
                    break;
                case "UserSchool":
                    //pstrEventName = case "UserSchoolBegin": 
                    pstrEventName = EventType.UserSchoolBegin;
                    break;
                case "UserGarage":
                    //pstrEventName = case "UserGarageBegin": 
                    pstrEventName = EventType.UserGarageBegin;
                    break;
                case "UserRRCrossing":
                    //pstrEventName = case "UserRailroadCrossingBegin": 
                    pstrEventName = EventType.UserRailroadCrossingBegin;
                    break;
                case "UserHotSpot":
                    //pstrEventName = case "UserHopSpotBegin": 
                    pstrEventName = EventType.UserHopSpotBegin;
                    break;
                case "Emergency Begin":
                    pstrEventName = EventType.EmergencyBegin;
                    break;
                case "Emergency End":
                    pstrEventName = EventType.EmergencyEnd;
                    break;
                case "Emergency Door Open":
                    pstrEventName = EventType.EmergencyDoorOpen;
                    break;
                case "Emergency Door Close":
                    pstrEventName = EventType.EmergencyDoorClose;
                    break;
                case "Excessive Speed Begin":
                    pstrEventName = EventType.ExcessiveSpeedBegin;
                    break;
                case "Excessive Speed End":
                    pstrEventName = EventType.ExcessiveSpeedEnd;
                    break;
                case "Cell Excessive Speed Begin":
                    pstrEventName = EventType.CellExcessiveSpeedBegin;
                    break;
                case "Cell Excessive Speed End":
                    pstrEventName = EventType.CellExcessiveSpeedEnd;
                    break;
                case "Panic Button On":
                    pstrEventName = EventType.EmergencyPanicButtonOn;
                    break;
                case "Panic Button Off":
                    pstrEventName = EventType.EmergencyPanicButtonOFF;
                    break;
                case "StopBegin_FindStop":
                    pstrEventName = EventType.StopBegin_FindStop;
                    break;
                case "StopBegin_MultiStopCheck":
                    pstrEventName = EventType.StopBegin_MultiStopCheck;
                    break;
                case "StopBegin_AddUnscheduledStop":
                    pstrEventName = EventType.StopBegin_AddUnscheduledStop;
                    break;
                case "OnLive_Checks":
                    pstrEventName = EventType.OnLive_Checks;
                    break;
                case "OnLive_VehicleUpdate":
                    pstrEventName = EventType.OnLive_VehicleUpdate;
                    break;
                case "StopBegin_OnStopVisited":
                    pstrEventName = EventType.StopBegin_OnStopVisited;
                    break;
                case "EOD_VehicleLocation":
                    pstrEventName = EventType.EOD_VehicleLocation;
                    break;
                case "EventProcessorProcess":
                    pstrEventName = EventType.EventProcessorProcess;
                    break;
                case "Sleep":
                    pstrEventName = EventType.Sleep;
                    break;
                case "Unit Wake Message":
                    pstrEventName = EventType.UnitWakeCode;
                    break;
                case "Veh Subst Begin":
                    pstrEventName = EventType.VehicleSubstitutionBegin;
                    break;
                case "Veh Subst End":
                    pstrEventName = EventType.VehicleSubstitutionEnd;
                    break;
                case "Cell Idle Begin":
                    pstrEventName = EventType.CellIdleBegin;
                    break;
                case "Cell Idle End":
                    pstrEventName = EventType.CellIdleEnd;
                    break;
                case "Cell Low Battery":
                    pstrEventName = EventType.CellLowBattery;
                    break;
                case "Cell Ping":
                    pstrEventName = EventType.CellPing;
                    break;
                case "Cell Run Begin":
                    pstrEventName = EventType.CellRunBegin;
                    break;
                case "Cell Run End":
                    pstrEventName = EventType.CellRunEnd;
                    break;
                //case "Stop Begin":
                //    pstrEventName = EventType.CellUserStop;
                //    break;
                case "Cell Driver Login":
                    pstrEventName = EventType.CellDriverLogin;
                    break;
                case "Cell Driver Inspection Begin":
                    pstrEventName = EventType.CellDriverInspectionBegin;
                    break;
                case "Cell Driver Inspection End":
                    pstrEventName = EventType.CellDriverInspectionEnd;
                    break;
                case "Cell Driver Break Begin":
                    pstrEventName = EventType.CellDriverBreakBegin;
                    break;
                case "Cell Driver Break End":
                    pstrEventName = EventType.CellDriverBreakEnd;
                    break;
                case "Cell Driver Child Check Begin":
                    pstrEventName = EventType.CellDriverChildCheckBegin;
                    break;
                case "Cell Driver Child Check End":
                    pstrEventName = EventType.CellDriverChildCheckEnd;
                    break;
                case "Cell Driver Invalid Login":
                    pstrEventName = EventType.CellDriverInvalidLogin;
                    break;
                case "Cell Driver Logout":
                    pstrEventName = EventType.CellDriverLogout;
                    break;

                default:
                    pstrEventName = EventType.Unknown;
                    break;
            }
            return pstrEventName;
        }
        public static int GetEventIDFromEnum(EventType eEventType)
        {
            int nEventID;
            switch (eEventType)
            {
                case EventType.StopBegin:
                case EventType.CellStopBegin:
                case EventType.CellUserStop:
                    nEventID = 1;
                    break;
                case EventType.StopEnd:
                case EventType.CellStopEnd:
                    nEventID = 2;
                    break;
                case EventType.Live:
                    nEventID = 3;
                    break;
                case EventType.ImproperStop:
                    nEventID = 10;
                    break;
                case EventType.EmergencyDoorClose:
                    nEventID = 42;
                    break;
                case EventType.EmergencyDoorOpen:
                    nEventID = 43;
                    break;
                case EventType.EmergencyPanicButtonOn:
                    nEventID = 48;
                    break;
                case EventType.EmergencyPanicButtonOFF:
                    nEventID = 49;
                    break;
                case EventType.ExcessiveSpeedBegin:
                    nEventID = 58;
                    break;
                case EventType.ExcessiveSpeedEnd:
                    nEventID = 59;
                    break;
                case EventType.EarlyException:
                    nEventID = 60;
                    break;
                case EventType.LateException:
                    nEventID = 61;
                    break;
                case EventType.IdleException:
                    nEventID = 62;
                    break;
                case EventType.SpeedException:
                    nEventID = 63;
                    break;
                case EventType.UserRunBegin:
                    ////nEventID = 64;
                    nEventID = 101;
                    break;
                case EventType.UserRunEnd:
                    ////nEventID = 65;
                    nEventID = 102;
                    break;
                case EventType.UserStopBegin:
                    ////nEventID = 66;
                    nEventID = 105;
                    break;
                case EventType.UserSchoolBegin:
                    ////nEventID = 67;
                    nEventID = 106;
                    break;
                case EventType.UserGarageBegin:
                    ////nEventID = 68;
                    nEventID = 107;
                    break;
                case EventType.UserRailroadCrossingBegin:
                    ////nEventID = 69;
                    nEventID = 108;
                    break;
                case EventType.UserHopSpotBegin:
                    ////nEventID = 70;
                    nEventID = 109;
                    break;
                case EventType.UnitWakeCode:
                    nEventID = 180;
                    break;
                case EventType.StudentSwipe:
                    nEventID = 400;
                    break;
                case EventType.IdleBegin:
                    nEventID = 1200;
                    break;
                case EventType.IdleEnd:
                    nEventID = 1201;
                    break;
                case EventType.IgnitionOn:
                    nEventID = 1300;
                    break;
                case EventType.IgnitionOff:
                    nEventID = 1301;
                    break;
                case EventType.BusArrived:
                    nEventID = 1400;
                    break;
                case EventType.BusDeparted:
                    nEventID = 1401;
                    break;
                case EventType.AtSchool:
                    nEventID = 1402;
                    break;
                case EventType.LeaveSchool:
                    nEventID = 1403;
                    break;
                case EventType.VehicleSubstitutionBegin:
                    nEventID = 1410;
                    break;
                case EventType.VehicleSubstitutionEnd:
                    nEventID = 1411;
                    break;
                case EventType.MissedStop:
                    nEventID = 1500;
                    break;
                case EventType.BeginningOfRun:
                    nEventID = 1600;
                    break;
                case EventType.EndOfRun:
                    nEventID = 1601;
                    break;
                case EventType.AtGarage:
                    nEventID = 1700;
                    break;
                case EventType.LeaveGarage:
                    nEventID = 1701;
                    break;
                case EventType.UnitShutsOff:
                    nEventID = 2000;
                    break;
                case EventType.UnitOn:
                    nEventID = 2001;
                    break;
                case EventType.CellIdleBegin:
                    nEventID = 2101;
                    break;
                case EventType.CellIdleEnd:
                    nEventID = 2102;
                    break;
                case EventType.CellPing:
                    nEventID = 2110;
                    break;
                case EventType.CellLowBattery:
                    nEventID = 2120;
                    break;
                case EventType.CellRunBegin:
                    nEventID = 2130;
                    break;
                case EventType.CellRunEnd:
                    nEventID = 2131;
                    break;
                //case EventType.CellUserStop:
                //    nEventID = 2132;  
                //    break;    
                case EventType.CellDriverLogin:
                    nEventID = 2180;
                    break;
                case EventType.CellDriverInspectionBegin:
                    nEventID = 2181;
                    break;
                case EventType.CellDriverInspectionEnd:
                    nEventID = 2182;
                    break;
                case EventType.CellDriverBreakBegin:
                    nEventID = 2183;
                    break;
                case EventType.CellDriverBreakEnd:
                    nEventID = 2184;
                    break;
                case EventType.CellDriverChildCheckBegin:
                    nEventID = 2185;
                    break;
                case EventType.CellDriverChildCheckEnd:
                    nEventID = 2186;
                    break;
                case EventType.CellDriverInvalidLogin:
                    nEventID = 2188;
                    break;
                case EventType.CellDriverLogout:
                    nEventID = 2189;
                    break;
                default:
                    nEventID = 999;
                    break;
            }
            return nEventID;
        }
        public static EventType GetEnumFromEventID(int nEventID)
        {
            EventType eEventType;
            switch (nEventID)
            {
                case 0: eEventType = EventType.CellPing; break;
                case 1: eEventType = EventType.StopBegin; break;
                case 2: eEventType = EventType.StopEnd; break;
                case 3: eEventType = EventType.Live; break;
                case 10: eEventType = EventType.ImproperStop; break;
                case 60: eEventType = EventType.CellPing; break;
                case 66: eEventType = EventType.CellLowBattery; break;
                case 80: ////*** CELL DRIVER_EVENT_LOGIN
                case 81: ////*** CELL DRIVER_EVENT_INSPECTION_BEGIN 
                case 82: ////*** CELL DRIVER_EVENT_INSPECTION_END 
                case 83: ////*** CELL DRIVER_EVENT_BREAK_BEGIN
                case 84: ////*** CELL DRIVER_EVENT_BREAK_END
                case 85: ////*** CELL DRIVER_EVENT_CHILD_CHECK_BEGIN
                case 86: ////*** CELL DRIVER_EVENT_CHILD_CHECK_END
                case 88: ////*** CELL DRIVER_EVENT_INVALID_LOGIN
                case 89: ////*** CELL DRIVER_EVENT_LOGOUT 
                    eEventType = EventType.None;
                    nEventID = -1;
                    break;
                case 90: eEventType = EventType.CellPing; break;
                case 91: eEventType = EventType.CellStopBegin; break;
                case 92: eEventType = EventType.CellStopEnd; break;
                case 93: eEventType = EventType.CellLive; break;
                case 94: eEventType = EventType.CellIdleBegin; break;
                case 95: eEventType = EventType.CellIdleEnd; break;
                case 98: eEventType = EventType.CellExcessiveSpeedBegin; break;
                case 99: eEventType = EventType.CellExcessiveSpeedEnd; break;
                case 101: eEventType = EventType.UserRunBegin; break;
                case 102: eEventType = EventType.UserRunEnd; break;
                case 105: eEventType = EventType.UserStopBegin; break;
                case 106: eEventType = EventType.UserSchoolBegin; break;
                case 107: eEventType = EventType.UserGarageBegin; break;
                case 108: eEventType = EventType.UserRailroadCrossingBegin; break;
                case 109: eEventType = EventType.UserHopSpotBegin; break;
                case 110: eEventType = EventType.CellRunBegin; break;
                case 111: eEventType = EventType.CellRunEnd; break;
                case 180: eEventType = EventType.UnitWakeCode; break;
                case 191: eEventType = EventType.CellUserStop; break;
                case 500: eEventType = EventType.EmergencyBegin; break;
                case 501: eEventType = EventType.EmergencyEnd; break;
                case 800: eEventType = EventType.ExcessiveSpeedBegin; break;
                case 801: eEventType = EventType.ExcessiveSpeedEnd; break;
                case 1200: eEventType = EventType.IdleBegin; break;
                case 1202: eEventType = EventType.IdleEnd; break;
                case 1300: eEventType = EventType.IgnitionOn; break;
                case 1301: eEventType = EventType.IgnitionOff; break;
                case 1350: eEventType = EventType.UnitShutsOff; break;
                case 1351: eEventType = EventType.UnitOn; break;
                case 1400: eEventType = EventType.StudentSwipe; break;

                default:                    
                        eEventType = EventType.None;                        
                        nEventID = -1;
                        break;
            }
            return eEventType;
            // message_vehicle_id
        }
    }

}
