/*
 * Created by SharpDevelop.
 * User: huy
 * Date: 1/14/2007
 * Time: 3:20 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.IO;
using System.Xml;
using System.Windows.Forms;
using System.Text;

namespace ConfigXmlFile
{
	/// <summary>
	/// Description of ConfigXml.
	/// </summary>
	public class ConfigXml
	{
		XmlDocument doc;		
		string xmlFileName;
		string defaultFineName = Environment.CurrentDirectory + @"\config.xlm";
		public bool gb_result = false;
		/// <summary>
		/// create an instance of class COnfigXml by in put value file name
		/// </summary>
		/// <param name="ins_fileName"></param>
		public ConfigXml(string ins_fileName)
		{			
			try
			{	
				xmlFileName = ins_fileName;
				doc = new XmlDocument();				
				if(File.Exists(ins_fileName))
				{
					doc.Load(ins_fileName);
				}
				else
				{
					CreateConfigFile(ins_fileName);
					doc.Load(ins_fileName);
				}
				
			}
			catch
			{				
			}
		}
		public ConfigXml()
		{			
			try
			{	
				xmlFileName = Environment.CurrentDirectory + @"\config_eTMRport.xml";
				doc = new XmlDocument();				
				if(File.Exists(xmlFileName))
				{
					doc.Load(xmlFileName);
				}
				else
				{
					CreateConfigFile(xmlFileName);
					doc.Load(xmlFileName);
				}				
			}
			catch
			{				
			}
		}
		
		/// <summary>
		/// return a xmldnodelist of Nodes.
		/// </summary>
		/// <param name="ins_elementName"></param>
		/// <returns></returns>
		public XmlNodeList GetElementsByTagName(string ins_elementName)
		{			
			return doc.GetElementsByTagName(ins_elementName);
		}
		public XmlNode GetElementsByStringValue(string ins_value)
		{			
			XmlNode result = null;
			XmlNode root = doc.DocumentElement;
			foreach(XmlNode childNode in root)
			{
				if(childNode.InnerText.ToUpper() == ins_value.ToUpper())
				{
					result = childNode;
					break;
				}
			}
			return result;
		}
		
		/// <summary>
		/// return InnerText if first node exist, else return ""
		/// </summary>
		/// <param name="ins_elementName"></param>
		/// <returns></returns>
		public string GetValeElementByTagName(string ins_elementName)
		{
			if(doc.GetElementsByTagName(ins_elementName).Item(0) != null)
				return doc.GetElementsByTagName(ins_elementName).Item(0).InnerText;
			else
				return "";
		}
		
		public XmlNode CreateElement(string ins_nodeName, string ins_nodeValue)
		{
			XmlNode node = doc.CreateElement(ins_nodeName);
			node.InnerText = ins_nodeValue;
			return node;
		}
		/// <summary>
		/// append one child element to the parent element input, return true if succes, else return false
		/// </summary>
		/// <param name="ins_parentNodeName"></param>
		/// <param name="ins_childNode"></param>
		/// <returns></returns>
		public bool AddElement(string ins_parentNodeName, XmlNode ins_childNode)
		{
			try
			{				
				XmlNode parentNode = doc.GetElementsByTagName(ins_parentNodeName).Item(0);
				if(parentNode != null)
				{					
					parentNode.AppendChild(ins_childNode);
					return true;
				}
				else
					return false;					
			}
			catch(Exception ex)
			{
				MessageBox.Show("Error on add element: " + ex.Message);
				return false;
			}
			return true;			
		}
		
		/// <summary>
		/// append child elements to the parent element input, return true if succes, else return false
		/// </summary>
		/// <param name="ins_parentNodeName"></param>
		/// <param name="ins_childNode"></param>
		/// <returns></returns>
		public bool AddElementArray(string ins_parentNodeName, XmlNodeList ins_childNode)
		{
			try
			{				
				XmlNode parentNode = doc.GetElementsByTagName(ins_parentNodeName).Item(0);
				if(parentNode != null)
				{
					foreach(XmlNode childNode in ins_childNode)
					{
						parentNode.AppendChild(childNode);
					}					
					return true;
				}
				else
					return false;			
			}
			catch(Exception ex)
			{
				MessageBox.Show("Error on add element: " + ex.Message);
				return false;
			}
			return true;
		}
		
		public bool RemoveElement(string ins_elementName)
		{
			try
			{
				XmlNode removeNode = doc.GetElementsByTagName(ins_elementName).Item(0);
				if(removeNode != null)
				{
					
					doc.RemoveChild(removeNode);
					return true;
				}
				else
				{
					return false;
				}				   
			}
			catch(Exception ex)
			{
				MessageBox.Show("Error on add remove element: " + ex.Message);
				return false;
			}
			return true;
		}
		
		public bool RemoveElement(string ins_parentName, string ins_childValue)
		{
			bool result = false;
			try
			{				
				XmlNode parentNode = doc.GetElementsByTagName(ins_parentName).Item(0);
				if(parentNode != null)
				{
					foreach(XmlNode childNode in parentNode.ChildNodes)
					{
						if(childNode.InnerText.ToUpper() == ins_childValue.ToUpper())
						{							
							parentNode.RemoveChild(childNode);
							result = true;
						}
					}
				}
				else
				{
					result = false;
				}				   
			}
			catch(Exception ex)
			{
				MessageBox.Show("Error on add remove element: " + ex.Message);
				return false;
			}
			return result;
		}
		
		/// <summary>
		/// create xml config file of the default struct
		/// </summary>
		/// <param name="ins_fileName"></param>
		/// <returns></returns>
		public bool CreateConfigFile(string ins_fileName)
		{
			try
			{
				// create the root
				XmlElement root = doc.CreateElement("CONFIGS");
				XmlNode configs = doc.InsertAfter(root, null);
				
				// construct a doc fragment for sessions
				XmlDocumentFragment configFrag = doc.CreateDocumentFragment();
				XmlNode rootFragConfig = doc.CreateElement("CONFIG");
				configFrag.InsertAfter(rootFragConfig, null);
				
				//Fragment checkexist
				XmlDocumentFragment checkExistFrag = doc.CreateDocumentFragment();
				XmlNode rootFragChkExist = doc.CreateElement("CHECKEXIST");
				checkExistFrag.InsertAfter(rootFragChkExist, null);
				
				XmlNode node = doc.CreateElement("FILES");				
				rootFragChkExist.AppendChild(node);
				node = doc.CreateElement("FOLDERS");
				rootFragChkExist.AppendChild(node);
				
				doc.DocumentElement.AppendChild(rootFragConfig);
				doc.DocumentElement.AppendChild(rootFragChkExist);				
				
				doc.Save(ins_fileName);				
			}
			catch(Exception ex)
			{
				MessageBox.Show("Error on create xml file: " + ex.Message);
				return false;
			}
			return true;			
		}
		
		public bool SaveConfigFile()
		{
			try
			{
				doc.Save(xmlFileName);
			}
			catch(Exception ex)
			{
				MessageBox.Show("Error on save xml file: " + ex.Message);
				return false;
			}
			return true;			
		}
		
		/// <summary>
		/// check files in config_eTMRport.xml, return the gb_result = true if all files exits, else return fasle
		/// </summary>
		/// <returns></returns>
		public string checkFolders()
		{
			gb_result = true;
			string result = "";
			try
			{
				//string path = @"c:\gs\gs7.05";				
				XmlNode foldersNode = doc.GetElementsByTagName("FOLDERS").Item(0);
				if(foldersNode != null)
				{
					foreach(XmlNode folderNode in foldersNode.ChildNodes)
					{
						if(Directory.Exists(folderNode.InnerText))
						{
							result += folderNode.InnerText + ": OK" + Environment.NewLine;							
						}
						else
						{
							result += folderNode.InnerText + ": Not exist" + Environment.NewLine;
							gb_result = false;
						}						
					}
				}							
			}
			catch(Exception ex)
			{
				MessageBox.Show("Error in check folders: " + ex.Message, "Error", MessageBoxButtons.OK);
				return "Check folders errors";
				gb_result = false;
			}
			return result;
		}
		
		/// <summary>
		/// check folders in config_eTMRport.xml, return the gb_result = true if all folders exits, else return fasle
		/// </summary>
		/// <returns></returns>
		public string checkFiles()
		{
			gb_result = true;
			string result = "";
			try
			{				
				XmlNode filesNode = doc.GetElementsByTagName("FILES").Item(0);
				if(filesNode != null)
				{
					foreach(XmlNode fileNode in filesNode.ChildNodes)
					{
						if(File.Exists(fileNode.InnerText))
						{
							result += fileNode.InnerText + ": OK" + Environment.NewLine;
						}
						else
						{
							result += fileNode.InnerText + ": Not exist" + Environment.NewLine;							
							gb_result = false;
						}						
					}
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show("Error in check files: " + ex.Message, "Error", MessageBoxButtons.OK);
				return "Check files errors";
				gb_result = false;
			}
			return result;
		}
        
		/// <summary>
		/// replace the values in ins_fileName, return true if replaced, else return false
		/// </summary>
		/// <param name="ins_fileName"></param>
		/// <param name="values"></param>
		/// <returns></returns>
		public bool Replace(string ins_fileName, string[,] values)
		{
			bool result = false;
			try
			{				
				StringBuilder sb = new StringBuilder();								
				if(File.Exists(ins_fileName))
				{
					StreamReader reader = new StreamReader(ins_fileName);
					string line = reader.ReadLine();
					bool containt = false;
					while(line != null)
					{						
						for(int k = 0; k < values.Length / 2; k++)
						{							
							if(line.Contains(values[k, 0]))
							{
								string newLine = "";
								//newLine = line.Replace(values[k, 0], values[k, 1]);
								newLine = values[k, 1];
								sb.AppendLine(newLine);
								containt = true;
							}
							else
							{
								sb.AppendLine(line);
							}
							line = reader.ReadLine();
						}
					}
					reader.Close();
					if(containt)
					{
						//write to file
						//FileInfo fi = new FileInfo(ins_fileName);
						StreamWriter writer = new StreamWriter(ins_fileName, false);
						writer.Write(sb.ToString());
						writer.Flush();
						writer.Close();
						result = true;
					}
				}
				else
				{
					result = false;
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show("Error in set replace values: " + ex.Message, "Error", MessageBoxButtons.OK);
				return false;
			}
			return result;
		}
		
		/// <summary>
		/// replace the values in ins_fileName, return true if replaced, else return false.if the line is replaced so it will be ignored
		/// </summary>
		/// <param name="ins_fileName"></param>
		/// <param name="values"></param>
		/// <returns></returns>
		public bool ReplaceTwoDimentionArrayValues(string ins_fileName, string[] values_i, string[] values_j)
		{
			bool result = false;
			bool[] replaced = new bool[values_i.Length];
			for(int m = 0; m < replaced.Length; m++)
			{
				replaced[m] = false;
			}
			try
			{				
				StringBuilder sb = new StringBuilder();								
				if(File.Exists(ins_fileName))
				{
					StreamReader reader = new StreamReader(ins_fileName);
					string line = reader.ReadLine();
					bool containt = false;
					while(line != null)
					{	
						bool appended = false;
						for(int k = 0; k < values_i.Length; k++)
						{	
							if(!replaced[k])
							{
								appended = false;
								if(line.Contains(values_i[k]))
								{
									string newLine = "";
									//newLine = line.Replace(values_i[k], values_j[k]);
									newLine = values_j[k];
									sb.AppendLine(newLine);
									containt = true;
									appended = true;
									replaced[k] = true;
									break;
								}
							}
						}
						if(!appended)
						{
							sb.AppendLine(line);
						}
						line = reader.ReadLine();
					}
					reader.Close();
					if(containt)
					{
						//write to file
						//FileInfo fi = new FileInfo(ins_fileName);						
						StreamWriter writer = new StreamWriter(ins_fileName, false);
						writer.Write(sb.ToString());
						writer.Flush();
						writer.Close();
						result = true;
					}
				}
				else
				{
					result = false;
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show("Error in set replace values: " + ex.Message, "Error", MessageBoxButtons.OK);
				return false;
			}
			return result;
		}
	
		/// <summary>
		/// get File info by string input
		/// </summary>
		/// <param name="ins_fullName">full string file name</param>
		/// <returns>array[0]: File Name, array[1]: File extension, array[2]: full path</returns>
		public string[] getFileInfoByString(string ins_fullName)
		{
			string[] namePathArr = ins_fullName.Split(new char[]{'\\'});
			string nameFileWithExt = namePathArr[namePathArr.Length - 1];
			string[] nameFileArr = nameFileWithExt.Split(new char[]{'.'});
			string ext = nameFileArr[nameFileArr.Length - 1];
			string nameFile = "";
			for(int i = 0; i < nameFileArr.Length - 1; i++)
			{
				if(i == 0)
				{
					nameFile += nameFileArr[i];
				}
				else
				{
					nameFile += "." + nameFileArr[i];
				}
			}
			string fullPath = "";
			for(int j = 0; j < namePathArr.Length - 1; j++)
			{				
				fullPath += namePathArr[j] + @"\";
			}			
			string[] result = new string[]{nameFile, ext, fullPath};
			return result;
		}

        /// <summary>
        /// get the attributes of config file
        /// </summary>
        /// <returns></returns>
        public static string getConfigFile()
        {
            ConfigXml cf = new ConfigXml();
            StringBuilder strConfig = new StringBuilder();

            strConfig.AppendLine("IDLE_START_THRESHOLD: " + cf.GetValeElementByTagName("IDLE_START_THRESHOLD"));

            strConfig.AppendLine("IDLE_MAX_DURATION_MINUTES: " + cf.GetValeElementByTagName("IDLE_MAX_DURATION_MINUTES"));
            strConfig.AppendLine("PAYROLL_SLEEPING_MINUTES: " + cf.GetValeElementByTagName("PAYROLL_SLEEPING_MINUTES"));
            strConfig.AppendLine("MIN_VALID_DP_SECONDS: " + cf.GetValeElementByTagName("MIN_VALID_DP_SECONDS"));
            strConfig.AppendLine("MATCH_EarlyWindowInMinutes: " + cf.GetValeElementByTagName("MATCH_EarlyWindowInMinutes"));
            strConfig.AppendLine("MATCH_LateWindowInMinutes: " + cf.GetValeElementByTagName("MATCH_LateWindowInMinutes"));

            return strConfig.ToString();
        }
		
	}
	
	public class FileType
	{
		public const string Excutable = "EXE;COM;";
		public const string ExcutableAndEdit = "CMD;BAT;";		
		public const string PDF = "PDF";		
		public FileType()
		{			
		}
	}
}
