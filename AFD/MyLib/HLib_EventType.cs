using System;
using System.Collections.Generic;
using System.Text;

namespace BAR_AllGPS
{
    class HLib_EventType
    {
        public HLib_EventType()
        {
        }
        public const string LeaveGarage = "Leave Garage";
        public const string BeginningOfRun = "Beginning Of Run";
        public const string MissedStop = "Missed Stop";
        public const string ImproperStop = "Improper Stop";
        public const string ExcessiveSpeedEnd = "Excessive Speed End";
        public const string BusArrived = "Bus Arrived";
        public const string IgnitionOn = "Ignition On";
        public const string IdleBegin = "Idle Begin";
        public const string IgnitionOff = "Ignition Off";
        public const string ExcessiveSpeedBegin = "Excessive Speed Begin";
        public const string EndOfRun = "End Of Run";
        public const string PanicButtonOff = "Panic Button Off";
        public const string IdleEnd = "Idle End";
        public const string BusDeparted = "Bus Departed";
        public const string PanicButtonOn = "Panic Button On";
        public const string AtSchool = "At School";
        public const string StopBegin = "Stop Begin";
        public const string UnitWakeMessage = "UNIT WAKE MESSAGE";
        public const string AtGarage = "At Garage";
        public const string StopEnd = "Stop End";
        public const string LeaveSchool = "Leave School";
        public const string Skipped = "Skipped";

        public const string EMERGENCY_BEGIN = "EMERGENCY BEGIN";
        public const string EMERGENCY_END = "EMERGENCY END";
        public const string EXCESSIVESPEEDBEGIN = "EXCESSIVESPEEDBEGIN";
        public const string EXCESSIVESPEEDEND = "EXCESSIVESPEEDEND";
        public const string IDLE_BEGIN = "IDLE BEGIN";
        public const string IDLE_END = "IDLE END";
        public const string IGNITION_OFF = "IGNITION OFF";
        public const string IGNITION_ON = "IGNITION ON";
        public const string IMPROPER_STOP_BEGIN = "IMPROPER STOP BEGIN";
        public const string LIVE = "LIVE";
        public const string STOP_BEGIN = "STOP BEGIN";
        public const string STOP_END = "STOP END";
        public const string UNIT_OFF = "UNIT OFF";
        public const string UNIT_ON = "UNIT ON";
        public const string UNIT_WAKE = "UNIT WAKE";
        

        //MessageUnit
        public const string DRIVER_EVENT_LOGIN = "DRIVER_EVENT_LOGIN";//80
        public const string DRIVER_EVENT_INVALID_LOGIN = "DRIVER_EVENT_INVALID_LOGIN";//88
        public const string DRIVER_EVENT_INSPECTION_BEGIN = "DRIVER_EVENT_INSPECTION_BEGIN";//81
        public const string DRIVER_EVENT_INSPECTION_END = "DRIVER_EVENT_INSPECTION_END";//82
        public const string DRIVER_EVENT_BREAK_BEGIN = "DRIVER_EVENT_BREAK_BEGIN";//83
        public const string DRIVER_EVENT_BREAK_END = "DRIVER_EVENT_BREAK_END";//84
        public const string DRIVER_EVENT_CHILD_CHECK_BEGIN = "DRIVER_EVENT_CHILD_CHECK_BEGIN";//85
        public const string DRIVER_EVENT_CHILD_CHECK_END = "DRIVER_EVENT_CHILD_CHECK_END";//86
        public const string DRIVER_EVENT_LOGOUT = "DRIVER_EVENT_LOGOUT";//89

        public const string DRIVER_EVENT_START_ROUTE = "DRIVER_EVENT_START_ROUTE";//1012
        public const string DRIVER_EVENT_END_ROUTE = "DRIVER_EVENT_END_ROUTE";//1013
        public const string DRIVER_EVENT_SLEEPING_CHILD = "DRIVER_EVENT_SLEEPING_CHILD";//1014
        //event values 
        public const string VEH_EVENT_NONE = null;
        public const string VEH_EVENT_PING = "VEH_EVENT_PING";//"60";
        public const string VEH_EVENT_LOW_BATTERY = "VEH_EVENT_LOW_BATTERY";//66
        public const string VEH_EVENT_STOP_BEGIN = "VEH_EVENT_STOP_BEGIN";//91
        public const string VEH_EVENT_STOP_END = "VEH_EVENT_STOP_END";//92
        public const string VEH_EVENT_LIVE = "VEH_EVENT_LIVE";//93
        public const string VEH_EVENT_IDLE_BEGIN = "VEH_EVENT_IDLE_BEGIN";//94
        public const string VEH_EVENT_IDLE_END = "VEH_EVENT_IDLE_END";//95
        public const string VEH_EVENT_EXCESSIVE_SPEED_BEGIN = "VEH_EVENT_EXCESSIVE_SPEED_BEGIN";//98
        public const string VEH_EVENT_EXCESSIVE_SPEED_END = "VEH_EVENT_EXCESSIVE_SPEED_END"; //99

        //cell user stop values 
        public static String VEH_USER_EVENT_RUN_BEGIN = "VEH_USER_EVENT_RUN_BEGIN";//110
        public static String VEH_USER_EVENT_RUN_END = "VEH_USER_EVENT_RUN_END";//111
        public static String VEH_USER_EVENT_STOP_BEGIN = "VEH_USER_EVENT_STOP_BEGIN"; //191

        //array cellphone
        public static string[] DriVehName = new string[22] { 
            "VEH_EVENT_NONE",
            "VEH_EVENT_PING",
            "VEH_EVENT_LOW_BATTERY",
            "VEH_EVENT_STOP_BEGIN",
            "VEH_EVENT_STOP_END",
            "VEH_EVENT_LIVE",
            "VEH_EVENT_IDLE_BEGIN",
            "VEH_EVENT_IDLE_END",
            "VEH_EVENT_EXCESSIVE_SPEED_BEGIN",
            "VEH_EVENT_EXCESSIVE_SPEED_END",
            "DRIVER_EVENT_LOGIN",
            "DRIVER_EVENT_INVALID_LOGIN",
            "DRIVER_EVENT_INSPECTION_BEGIN",
            "DRIVER_EVENT_INSPECTION_END",
            "DRIVER_EVENT_BREAK_BEGIN",
            "DRIVER_EVENT_BREAK_END",
            "DRIVER_EVENT_CHILD_CHECK_BEGIN",
            "DRIVER_EVENT_CHILD_CHECK_END",
            "DRIVER_EVENT_LOGOUT",
            "VEH_USER_EVENT_RUN_BEGIN",
            "VEH_USER_EVENT_RUN_END",
            "VEH_USER_EVENT_STOP_BEGIN"
        };

        public static string[] DriVehId = new string[22] { "", "60", "66", "91", "92", "93", "94", "95", "98", "99", "80", "88", "81", "82", "83", "84", "85", "86", "89", "110", "111", "191" };
        //public const string Skipped = "Skipped";

    }
}
