namespace FreeSMS
{
    partial class frmUserManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUserManager));
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgvUser = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbAccType = new System.Windows.Forms.ComboBox();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnRunCancel = new System.Windows.Forms.Button();
            this.btnRunDelete = new System.Windows.Forms.Button();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.btnRunSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnRunEdit = new System.Windows.Forms.Button();
            this.btnRunNew = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUser)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dgvUser);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 112);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(595, 354);
            this.panel3.TabIndex = 2;
            // 
            // dgvUser
            // 
            this.dgvUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvUser.Location = new System.Drawing.Point(0, 0);
            this.dgvUser.Name = "dgvUser";
            this.dgvUser.ReadOnly = true;
            this.dgvUser.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUser.Size = new System.Drawing.Size(595, 354);
            this.dgvUser.TabIndex = 0;
            this.dgvUser.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUser_CellClick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(595, 112);
            this.panel1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbAccType);
            this.groupBox1.Controls.Add(this.txtNote);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnRunCancel);
            this.groupBox1.Controls.Add(this.btnRunDelete);
            this.groupBox1.Controls.Add(this.txtPassword);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.btnRunSave);
            this.groupBox1.Controls.Add(this.btnClose);
            this.groupBox1.Controls.Add(this.btnRunEdit);
            this.groupBox1.Controls.Add(this.btnRunNew);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtUserName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(595, 112);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Information";
            // 
            // cbAccType
            // 
            this.cbAccType.FormattingEnabled = true;
            this.cbAccType.Location = new System.Drawing.Point(420, 16);
            this.cbAccType.Name = "cbAccType";
            this.cbAccType.Size = new System.Drawing.Size(170, 21);
            this.cbAccType.TabIndex = 42;
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(70, 43);
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(520, 34);
            this.txtNote.TabIndex = 38;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 39;
            this.label1.Text = "Note:";
            // 
            // btnRunCancel
            // 
            this.btnRunCancel.Location = new System.Drawing.Point(488, 84);
            this.btnRunCancel.Name = "btnRunCancel";
            this.btnRunCancel.Size = new System.Drawing.Size(48, 23);
            this.btnRunCancel.TabIndex = 7;
            this.btnRunCancel.Text = "C&ancel";
            this.btnRunCancel.UseVisualStyleBackColor = true;
            this.btnRunCancel.Click += new System.EventHandler(this.btnRunCancel_Click);
            // 
            // btnRunDelete
            // 
            this.btnRunDelete.Location = new System.Drawing.Point(434, 84);
            this.btnRunDelete.Name = "btnRunDelete";
            this.btnRunDelete.Size = new System.Drawing.Size(48, 23);
            this.btnRunDelete.TabIndex = 6;
            this.btnRunDelete.Text = "&Delete";
            this.btnRunDelete.UseVisualStyleBackColor = true;
            this.btnRunDelete.Click += new System.EventHandler(this.btnRunDelete_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(259, 17);
            this.txtPassword.MaxLength = 20;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(101, 20);
            this.txtPassword.TabIndex = 1;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(197, 20);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 13);
            this.label17.TabIndex = 37;
            this.label17.Text = "Password:";
            // 
            // btnRunSave
            // 
            this.btnRunSave.Location = new System.Drawing.Point(380, 84);
            this.btnRunSave.Name = "btnRunSave";
            this.btnRunSave.Size = new System.Drawing.Size(48, 23);
            this.btnRunSave.TabIndex = 5;
            this.btnRunSave.Text = "&Save";
            this.btnRunSave.UseVisualStyleBackColor = true;
            this.btnRunSave.Click += new System.EventHandler(this.btnRunSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(542, 83);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(48, 23);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnRunEdit
            // 
            this.btnRunEdit.Location = new System.Drawing.Point(326, 84);
            this.btnRunEdit.Name = "btnRunEdit";
            this.btnRunEdit.Size = new System.Drawing.Size(48, 23);
            this.btnRunEdit.TabIndex = 4;
            this.btnRunEdit.Text = "&Edit";
            this.btnRunEdit.UseVisualStyleBackColor = true;
            this.btnRunEdit.Click += new System.EventHandler(this.btnRunEdit_Click);
            // 
            // btnRunNew
            // 
            this.btnRunNew.Location = new System.Drawing.Point(272, 84);
            this.btnRunNew.Name = "btnRunNew";
            this.btnRunNew.Size = new System.Drawing.Size(48, 23);
            this.btnRunNew.TabIndex = 3;
            this.btnRunNew.Text = "&Create";
            this.btnRunNew.UseVisualStyleBackColor = true;
            this.btnRunNew.Click += new System.EventHandler(this.btnRunNew_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(367, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Acc Type:";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(70, 16);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(120, 20);
            this.txtUserName.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "User name:";
            // 
            // frmUserManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 466);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmUserManager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phone User Manager";
            this.Load += new System.EventHandler(this.frmUserManager_Load);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUser)).EndInit();
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        

        private System.Windows.Forms.DataGridViewTextBoxColumn TxtEditCol_STT_RUN;
        private System.Windows.Forms.DataGridViewTextBoxColumn TxtEditCol_name;
        //private System.Windows.Forms.DataGridViewTextBoxColumn TxtEditCol_Ten_SK;
        private System.Windows.Forms.DataGridViewTextBoxColumn TxtEditCol_phoneNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn TxtEditCol_address;
        private System.Windows.Forms.DataGridViewTextBoxColumn TxtEditCol_message;
        //private System.Windows.Forms.DataGridViewTextBoxColumn TxtEditCol_RUN_MAX_TIME;
        //private System.Windows.Forms.DataGridViewTextBoxColumn TxtEditCol_RUN_MAX_LOAD;
        //private System.Windows.Forms.DataGridViewTextBoxColumn TxtEditCol_RUN_FREQ;
        //private System.Windows.Forms.DataGridViewTextBoxColumn TxtEditCol_RUN_START_DATE;
        //private System.Windows.Forms.DataGridViewTextBoxColumn TxtEditCol_RUN_END_DATE;

        //STOP
        //private System.Windows.Forms.DataGridViewTextBoxColumn TxtEditCol_STT_STOP;
        //private System.Windows.Forms.DataGridViewTextBoxColumn TxtEditCol_STOP_ID;
        //private System.Windows.Forms.DataGridViewTextBoxColumn TxtEditCol_TYPE_NAME;
        //private System.Windows.Forms.DataGridViewTextBoxColumn TxtEditCol_STOP_LOCATION;
        //private System.Windows.Forms.DataGridViewTextBoxColumn TxtEditCol_STOP_DEST_TIME;
        //private System.Windows.Forms.DataGridViewTextBoxColumn TxtEditCol_TIME_AT_STOP;
        //private System.Windows.Forms.DataGridViewTextBoxColumn TxtEditCol_STOP_DESCRIPTION;


        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvUser;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnRunDelete;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnRunSave;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnRunEdit;
        private System.Windows.Forms.Button btnRunNew;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnRunCancel;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbAccType;
    }
}

