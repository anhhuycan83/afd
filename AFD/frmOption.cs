using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using HPConFigXml;
using AFD.HPLib;
using FreeSMS.MyLib;
using nsDataAccess;


namespace AFD
{
    public partial class frmOption : Form
    {
        private MyClsOption m_option;

        
        private bool isLoaded = false;
        private bool isLoadedSource = false;
        private bool isLoadedDes = false;
        private bool isLoadedSourceBus = false;
        private bool isLoadedDesBus = false;
        private bool gb_isChanged = false;
        

        

        public frmOption(MyClsOption ins_option)
        {
            m_option = ins_option;
            InitializeComponent();
        }
        

        private void frmOption_Load(object sender, EventArgs e)
        {
            setApplyStatus(false);
            LoadOptionFromClsToGUI();
            isLoaded = true;            
        }

        /* LoadOptionFromFileToCls()
        private void LoadOptionFromFileToCls()
        {
            if (xcfg.Settings["SAVETO"].Value == "")
                m_option.SaveTo = xcfg.Settings["SAVETO"].Value = @"C:\";
            else
            {
                if (!Directory.Exists(xcfg.Settings["SAVETO"].Value))
                    Directory.CreateDirectory(xcfg.Settings["SAVETO"].Value);
                m_option.SaveTo = xcfg.Settings["SAVETO"].Value;
            }
            if (xcfg.Settings["LINKNO"].intValue == 0)
                m_option.LinkNo = xcfg.Settings["LINKNO"].intValue = 1;
            else
                m_option.LinkNo = xcfg.Settings["LINKNO"].intValue;

            if (xcfg.Settings["TIMEOUT"].intValue == 0)
            {
                xcfg.Settings["TIMEOUT"].intValue = 30;
                m_option.TimeOut = "30";
            }
            else
                m_option.TimeOut = xcfg.Settings["TIMEOUT"].intValue.ToString();

            if (string.IsNullOrEmpty(xcfg.Settings["DMTYPE"].Value))
            {
                if (rbIDM.Checked)
                    m_option.DownLoadType = xcfg.Settings["DMTYPE"].Value = DMType.IDM.ToString();
                else
                    if (rbFlashGet.Checked)
                        m_option.DownLoadType = xcfg.Settings["DMTYPE"].Value = DMType.FLASHGET.ToString();

            }
            else
            {
                if (Enum.IsDefined(DMType, xcfg.Settings["DMTYPE"].Value))
                {
                    m_option.DownLoadType = Enum.Parse(DMType, xcfg.Settings["DMTYPE"].Value);                    
                }
            }
        }
        */

        private void LoadOptionFromClsToGUI()
        {
            setApplyStatus(false);
            txtSaveTo.Text = m_option.SaveTo;
            nudLinkNo.Value = m_option.LinkNo;
            nudRepeatCycle.Value = m_option.RepeatCycle;
            txtTimeOut.Text = m_option.TimeOut.ToString();
            txtIDMPath.Text = m_option.IDMPath;
            txtFlashGetPath.Text = m_option.FlashGetPath;
            txtCookieHF.Text = m_option.CookieHF;
            txtCookieMU.Text = m_option.CookieMU;
            txtCookieRS.Text = m_option.CookieRS;
            nudWaitAfterSetText.Value = m_option.WaitAfterSetText;
            nudWaitBeforeClose.Value = m_option.WaitBeforeClose;
            nudCycleYahoo.Value = m_option.CycleYahoo;
            nudCycleYahoo.Value = m_option.ReadCbox;
            nudCycleYahoo.Value = m_option.MaxReadCbox;
            nudCycleYahoo.Value = m_option.ShowMsgCbox;
            switch (m_option.DownLoadType)
            {
                case MyClsOption.DMType.IDM:
                    rbIDM.Checked = true;
                    break;
                case MyClsOption.DMType.FLASHGET:
                    rbFlashGet.Checked = true;
                    break;
            }

            //for database tab
            txtDBServer.Text = m_option.DBSERVERNAME;
            txtDBUser.Text = m_option.DBUSER;
            txtDBPass.Text = m_option.DBPASS;
            txtDBName.Text = m_option.DBNAME;
            //for AFD tab
            txtMsgSourceDBServer.Text = m_option.DBSERVERNAMESOURCE;
            txtMsgSourceUser.Text = m_option.DBUSERSOURCE;
            txtMsgSourcePass.Text = m_option.DBPASSSOURCE;
            cbSourceDBName.Text = m_option.DBNAMESOURCE;
                
            txtMsgDesDBServer.Text = m_option.DBSERVERNAMEDES;
            txtMsgDesUser.Text = m_option.DBUSERDES;
            txtMsgDesPass.Text = m_option.DBPASSDES;
            cbDesDBName.Text = m_option.DBNAMEDES;

            //////////
            //for AFD busposition tab
            txtBusSourceDBServer.Text = m_option.DBSERVERNAMESOURCEBUS;
            txtBusSourceUser.Text = m_option.DBUSERSOURCEBUS;
            txtBusSourcePass.Text = m_option.DBPASSSOURCEBUS;
            cbBusSourceDBName.Text = m_option.DBNAMESOURCEBUS;

            txtBusDesDBServer.Text = m_option.DBSERVERNAMEDESBUS;
            txtBusDesUser.Text = m_option.DBUSERDESBUS;
            txtBusDesPass.Text = m_option.DBPASSDESBUS;
            cbBusDesDBName.Text = m_option.DBNAMEDESBUS;

            txtEDTARestService.Text = m_option.EDTARestService;
            txtTimeZone.Text = m_option.TimeZone;

            if (m_option.Reverse4041 == "0")
                chbReverse4041.Checked = false;
            else
                chbReverse4041.Checked = true;

            if (m_option.Reverse4445 == "0")
                chbReverse4445.Checked = false;
            else
                chbReverse4445.Checked = true;

            if (m_option.Reverse4849 == "0")
                chbReverse4849.Checked = false;
            else
                chbReverse4849.Checked = true;

            if (m_option.Reverse5051 == "0")
                chbReverse5051.Checked = false;
            else
                chbReverse5051.Checked = true;

            if (m_option.Reverse5253 == "0")
                chbReverse5253.Checked = false;
            else
                chbReverse5253.Checked = true;

            if (m_option.Reverse6162 == "0")
                chbReverse6162.Checked = false;
            else
                chbReverse6162.Checked = true;

            txtUnitListBusPosition.Text = m_option.UnitListBusPosition;

            txtSwapUnit.Text = m_option.SwapUnit;

            if (m_option.IgnoreDaylight == "0")
                chbIgnoreDaylight.Checked = false;
            else
                chbIgnoreDaylight.Checked = true;

            if (m_option.bIsSkipTime == "0")
                chbSkipTime.Checked = false;
            else
            {
                chbSkipTime.Checked = true;
                dtpSkipFrom.Value = m_option.SkipFrom;
                dtpSkipTo.Value = m_option.SkipTo;
            }   

            //SERVERLEECH
            LoadServerLeech();
        }
        
        private void LoadServerLeech()
        {
            ArrayList alServerLeech = new ArrayList();
            //alServerLeech.Add(new MyClsServer("doilacaidinh", "doilacaidinh"));
            //alServerLeech.Add(new MyClsServer("teenhk", "teenhk"));

            string[] names = Enum.GetNames(typeof(MyServerLeechType));
            //Console.WriteLine("Members of {0}:", typeof(ArrivalStatus).Name);
            Array.Sort(names);
            foreach (string name in names)
            {
                MyServerLeechType serverType = (MyServerLeechType)Enum.Parse(typeof(MyServerLeechType), name);
                //Console.WriteLine("   {0} ({0:D})", status);
                alServerLeech.Add(new MyClsServer(serverType.ToString(), serverType.ToString()));
            }


            cbServerLeech.DisplayMember = "Ten";
            cbServerLeech.ValueMember = "KyHieu";
            cbServerLeech.DataSource = alServerLeech;

            if (m_option.ServerLeech == "")
                cbServerLeech.SelectedIndex = -1;
            else
                cbServerLeech.SelectedIndex = cbServerLeech.FindString(m_option.ServerLeech);

        }

        private void SetOptionFromGUIToCls()
        {
            setApplyStatus(false);
            m_option.SaveTo = txtSaveTo.Text;
            m_option.LinkNo = Convert.ToInt32(nudLinkNo.Value);
            m_option.RepeatCycle = Convert.ToInt32(nudRepeatCycle.Value);
            m_option.TimeOut = Convert.ToInt32(txtTimeOut.Text);
            m_option.IDMPath = txtIDMPath.Text;
            m_option.FlashGetPath = txtFlashGetPath.Text;
            m_option.CookieHF = txtCookieHF.Text;
            m_option.CookieMU = txtCookieMU.Text;
            m_option.CookieRS = txtCookieRS.Text;
            m_option.WaitAfterSetText = Convert.ToInt32(nudWaitAfterSetText.Value);
            m_option.WaitBeforeClose = Convert.ToInt32(nudWaitBeforeClose.Value);
            m_option.CycleYahoo = Convert.ToInt32(nudCycleYahoo.Value);
            //cbox
            m_option.ReadCbox = Convert.ToInt32(nudReadCbox.Value);
            m_option.MaxReadCbox = Convert.ToInt32(nudMaxRead.Value);
            m_option.ShowMsgCbox = Convert.ToInt32(nudShowMsg.Value);
            if (rbIDM.Checked)
                m_option.DownLoadType =  MyClsOption.DMType.IDM;
            else
                if (rbFlashGet.Checked)
                    m_option.DownLoadType = MyClsOption.DMType.FLASHGET;
            //serverleech
            if (cbServerLeech.SelectedIndex == -1)
                m_option.ServerLeech = "";
            else
                m_option.ServerLeech = cbServerLeech.SelectedValue.ToString();

            //for database tab
            m_option.DBSERVERNAME = txtDBServer.Text;
            m_option.DBUSER = txtDBUser.Text;
            m_option.DBPASS = txtDBPass.Text;
            m_option.DBNAME = txtDBName.Text;

            //for AFD tab
            m_option.DBSERVERNAMESOURCE = txtMsgSourceDBServer .Text;
            m_option.DBUSERSOURCE = txtMsgSourceUser.Text;
            m_option.DBPASSSOURCE = txtMsgSourcePass.Text;
            m_option.DBNAMESOURCE = cbSourceDBName.Text;

            m_option.DBSERVERNAMEDES = txtMsgDesDBServer.Text;
            m_option.DBUSERDES = txtMsgDesUser.Text;
            m_option.DBPASSDES = txtMsgDesPass.Text;
            m_option.DBNAMEDES = cbDesDBName.Text;

            //for AFD busposition tab
            m_option.DBSERVERNAMESOURCEBUS = txtBusSourceDBServer.Text;
            m_option.DBUSERSOURCEBUS = txtBusSourceUser.Text;
            m_option.DBPASSSOURCEBUS = txtBusSourcePass.Text;
            m_option.DBNAMESOURCEBUS = cbBusSourceDBName.Text;

            m_option.DBSERVERNAMEDESBUS = txtBusDesDBServer.Text;
            m_option.DBUSERDESBUS = txtBusDesUser.Text;
            m_option.DBPASSDESBUS = txtBusDesPass.Text;
            m_option.DBNAMEDESBUS = cbBusDesDBName.Text;

            m_option.EDTARestService = txtEDTARestService.Text;
            m_option.TimeZone = txtTimeZone.Text;

            if (chbReverse4041.Checked)
                m_option.Reverse4041 = "1";
            else
                m_option.Reverse4041 = "0";

            if (chbReverse4445.Checked)
                m_option.Reverse4445 = "1";
            else
                m_option.Reverse4445 = "0";

            if (chbReverse4849.Checked)
                m_option.Reverse4849 = "1";
            else
                m_option.Reverse4849 = "0";

            if (chbReverse5051.Checked)
                m_option.Reverse5051 = "1";
            else
                m_option.Reverse5051 = "0";

            if (chbReverse5253.Checked)
                m_option.Reverse5253 = "1";
            else
                m_option.Reverse5253 = "0";

            if (chbReverse6162.Checked)
                m_option.Reverse6162 = "1";
            else
                m_option.Reverse6162 = "0";

            m_option.UnitListBusPosition = txtUnitListBusPosition.Text;
            m_option.SwapUnit = txtSwapUnit.Text;
            if (chbIgnoreDaylight.Checked)
                m_option.IgnoreDaylight = "1";
            else
                m_option.IgnoreDaylight = "0";

            //skip time
            if (chbSkipTime.Checked)
                m_option.bIsSkipTime = "1";
            else
                m_option.bIsSkipTime = "0";

            if(chbSkipTime.Checked)
            {
                m_option.SkipFrom = dtpSkipFrom.Value;
                m_option.SkipTo = dtpSkipTo.Value;
            }
            //save config
            m_option.SaveConfig();
        }

        private void btnSaveTo_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbrowser = new FolderBrowserDialog();
            //this.folderBrowserDialog.ShowNewFolderButton = false;
            //this.folderBrowserDialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
            DialogResult result = fbrowser.ShowDialog();
            if (result == DialogResult.OK)
            {
                // the code here will be executed if the user presses Open in
                // the dialog.
                txtSaveTo.Text = fbrowser.SelectedPath;                
            }

            //ConfigSetting newUser = xcfg.Settings["Vcard##"];
            //newUser.Value = "2.1";
            ////ConfigSetting newUser = xcfg.Settings["PhoneBook/Vcard##"];
            //newUser["UserName"].Value = ins_newUserName;
            //newUser["Password"].Value = ins_newPassword;
            //newUser["Server"].Value = ins_newServer;
            //newUser["Note"].Value = ins_newNote;

            //string foldername = this.folderBrowserDialog.SelectedPath;
            //foreach (string f in Directory.GetFiles(foldername))
            //    this.listBox1.Items.Add(f);    
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if(gb_isChanged)
                SetOptionFromGUIToCls();
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            SetOptionFromGUIToCls();
            gb_isChanged = false;            
        }

        private void setApplyStatus(bool isEnable)
        {
            btnApply.Enabled = isEnable;
            gb_isChanged = isEnable;
            
        }

        private void nudLinkNo_ValueChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void txtSaveTo_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void txtTimeOut_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void rbIDM_CheckedChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void rbFlashGet_CheckedChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void nudRepeatCycle_ValueChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void btnIDMPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog oFileDlg;
            oFileDlg = new OpenFileDialog();
            oFileDlg.Title = "Select IDMan.exe";
            oFileDlg.ValidateNames = true;
            //oFileDlg.CheckFileExists = true;
            oFileDlg.CheckPathExists = true;
            //oFileDlg.CreatePrompt = true;
            //oFileDlg.OverwritePrompt = true;
            oFileDlg.AddExtension = true;
            oFileDlg.DefaultExt = "exe";
            //dlg.Filter = "Word Documents|*.doc|Excel Worksheets|*.xls|PowerPoint Presentations|*.ppt" +
            // "|Office Files|*.doc;*.xls;*.ppt" +
            // "|All Files|*.*";

            oFileDlg.Filter = "IDMan.exe|IDMan.exe";
            oFileDlg.RestoreDirectory = true;

            //oFileDlg.FileOk += new CancelEventHandler(oFileDlg_FileOk);
            DialogResult result = oFileDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtIDMPath.Text = oFileDlg.FileName;
                //string path = @"c:\temp\MyTest.txt";

                // This text is added only once to the file.
                //if (!File.Exists(path))
                //{
                // Create a file to write to.
                //string createText = txtAccessDBFilePath.Text + Environment.NewLine;
                //File.WriteAllText(path, createText);
                //}

                // This text is always added, making the file longer over time
                // if it is not deleted.
                //string appendText = "This is extra text" + Environment.NewLine;
                //File.AppendAllText(path, appendText);

                //// Open the file to read from.
                //string readText = File.ReadAllText(path);
                //Console.WriteLine(readText);
            }
        }

        private void btnFGPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog oFileDlg;
            oFileDlg = new OpenFileDialog();
            oFileDlg.Title = "Select flashget.exe";
            oFileDlg.ValidateNames = true;
            //oFileDlg.CheckFileExists = true;
            oFileDlg.CheckPathExists = true;
            //oFileDlg.CreatePrompt = true;
            //oFileDlg.OverwritePrompt = true;
            oFileDlg.AddExtension = true;
            oFileDlg.DefaultExt = "exe";
            //dlg.Filter = "Word Documents|*.doc|Excel Worksheets|*.xls|PowerPoint Presentations|*.ppt" +
            // "|Office Files|*.doc;*.xls;*.ppt" +
            // "|All Files|*.*";

            oFileDlg.Filter = "flashget.exe|flashget.exe";
            oFileDlg.RestoreDirectory = true;

            //oFileDlg.FileOk += new CancelEventHandler(oFileDlg_FileOk);
            DialogResult result = oFileDlg.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtFlashGetPath.Text = oFileDlg.FileName;
                //string path = @"c:\temp\MyTest.txt";

                // This text is added only once to the file.
                //if (!File.Exists(path))
                //{
                // Create a file to write to.
                //string createText = txtAccessDBFilePath.Text + Environment.NewLine;
                //File.WriteAllText(path, createText);
                //}

                // This text is always added, making the file longer over time
                // if it is not deleted.
                //string appendText = "This is extra text" + Environment.NewLine;
                //File.AppendAllText(path, appendText);

                //// Open the file to read from.
                //string readText = File.ReadAllText(path);
                //Console.WriteLine(readText);
            }
        }

        private void txtIDMPath_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void txtFlashGetPath_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void cbServerLeech_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void txtCookieHF_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void txtCookieMU_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void txtCookieRS_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void nudWaitAfterSetText_ValueChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void nudWaitBeforeClose_ValueChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void nudCycleYahoo_ValueChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void btnAcc_Click(object sender, EventArgs e)
        {
            FreeSMS.frmUserManager frmUserManager = new FreeSMS.frmUserManager(m_option);
            frmUserManager.ShowDialog();
            //m_option.SaveConfig();
        }

        private void nudReadCbox_ValueChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void nudMaxRead_ValueChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void nudShowMsg_ValueChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void cbMegauploadMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void cbRapishareMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void cbHotfileMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void cbFileserverMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void cbMediafireMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void cbFilesonicMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void cbNetloadMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void cbDepositefileMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void txtDBServer_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void txtDBUser_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void txtDBPass_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void txtDBName_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void cbSourceDBName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void cbDesDBName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            
        }

        private void txtMsgSourceDBServer_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedSource = false;
        }

        private void txtMsgSourceUser_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedSource = false;
        }

        private void txtMsgSourcePass_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedSource = false;
        }

        private void txtMsgDesDBServer_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedSourceBus = false;
        }

        private void txtMsgDesUser_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedSourceBus = false;
        }

        private void txtMsgDesPass_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedSourceBus = false;
        }

        private void cbSourceDBName_DropDown(object sender, EventArgs e)
        {
            try
            {
                if (!isLoadedSource)
                {
                    isLoadedSource = true;
                    //set values in the cb table

                    string DBSN = txtMsgSourceDBServer.Text;
                    //string DB = doc.GetElementsByTagName("DB").Item(0).InnerText;
                    string User = txtMsgSourceUser.Text;
                    string Password = txtMsgSourcePass.Text;

                    //string DB = (string) cbDB.SelectedValue;

                    string gb_strConn = "";
                    gb_strConn += "Data Source=" + DBSN + ";";
                    //gb_strConn +="Initial Catalog=" + DB +";";
                    gb_strConn += "User ID=" + User + ";";
                    gb_strConn += "Password=" + Password;

                    DataAccess da = new DataAccess(gb_strConn);
                    //string c = gb_strConn;
                    da.open();
                    if (da.m_isConnected)
                    {
                        DataSet dsListDB = da.getDataSet_Table("SELECT name FROM master..sysdatabases", "ListDB");
                        if (da.testExistValueInTableAndHaveRow(dsListDB))
                        {
                            cbSourceDBName.DataSource = dsListDB.Tables[0];
                            cbSourceDBName.DisplayMember = dsListDB.Tables[0].Columns[0].ColumnName;
                            cbSourceDBName.ValueMember = dsListDB.Tables[0].Columns[0].ColumnName;
                            cbSourceDBName.SelectedIndex = 0;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Error: " + da.m_strErr);
                    }
                    da.close();

                    if (isLoaded)
                    {
                        setApplyStatus(true);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void cbDesDBName_DropDown(object sender, EventArgs e)
        {
            try
            {
                if (!isLoadedDes)
                {
                    isLoadedDes = true;
                    //set values in the cb table

                    string DBSN = txtMsgSourceDBServer.Text;
                    //string DB = doc.GetElementsByTagName("DB").Item(0).InnerText;
                    string User = txtMsgSourceUser.Text;
                    string Password = txtMsgSourcePass.Text;

                    //string DB = (string) cbDB.SelectedValue;

                    string gb_strConn = "";
                    gb_strConn += "Data Source=" + DBSN + ";";
                    //gb_strConn +="Initial Catalog=" + DB +";";
                    gb_strConn += "User ID=" + User + ";";
                    gb_strConn += "Password=" + Password;

                    DataAccess da = new DataAccess(gb_strConn);
                    //string c = gb_strConn;
                    da.open();
                    if (da.m_isConnected)
                    {
                        DataSet dsListDB = da.getDataSet_Table("SELECT name FROM master..sysdatabases", "ListDB");
                        if (da.testExistValueInTableAndHaveRow(dsListDB))
                        {
                            cbDesDBName.DataSource = dsListDB.Tables[0];
                            cbDesDBName.DisplayMember = dsListDB.Tables[0].Columns[0].ColumnName;
                            cbDesDBName.ValueMember = dsListDB.Tables[0].Columns[0].ColumnName;
                            cbDesDBName.SelectedIndex = 0;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Error: " + da.m_strErr);
                    }
                    da.close();

                    if (isLoaded)
                    {
                        setApplyStatus(true);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void txtBusSourceDBServer_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedSourceBus = false;
        }

        private void txtBusSourceUser_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedSourceBus = false;
        }

        private void txtBusSourcePass_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedSourceBus = false;
        }

        private void txtBusDesDBServer_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedDesBus = false;
        }

        private void txtBusDesUser_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedDesBus = false;
        }

        private void txtBusDesPass_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedDesBus = false;
        }

        private void cbBusSourceDBName_DropDown(object sender, EventArgs e)
        {
            try
            {
                if (!isLoadedSourceBus)
                {
                    isLoadedSourceBus = true;
                    //set values in the cb table

                    string DBSN = txtBusSourceDBServer.Text;
                    //string DB = doc.GetElementsByTagName("DB").Item(0).InnerText;
                    string User = txtBusSourceUser.Text;
                    string Password = txtBusSourcePass.Text;

                    //string DB = (string) cbDB.SelectedValue;

                    string gb_strConn = "";
                    gb_strConn += "Data Source=" + DBSN + ";";
                    //gb_strConn +="Initial Catalog=" + DB +";";
                    gb_strConn += "User ID=" + User + ";";
                    gb_strConn += "Password=" + Password;

                    DataAccess da = new DataAccess(gb_strConn);
                    //string c = gb_strConn;
                    da.open();
                    if (da.m_isConnected)
                    {
                        DataSet dsListDB = da.getDataSet_Table("SELECT name FROM master..sysdatabases", "ListDB");
                        if (da.testExistValueInTableAndHaveRow(dsListDB))
                        {
                            cbBusSourceDBName.DataSource = dsListDB.Tables[0];
                            cbBusSourceDBName.DisplayMember = dsListDB.Tables[0].Columns[0].ColumnName;
                            cbBusSourceDBName.ValueMember = dsListDB.Tables[0].Columns[0].ColumnName;
                            cbBusSourceDBName.SelectedIndex = 0;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Error: " + da.m_strErr);
                    }
                    da.close();

                    if (isLoaded)
                    {
                        setApplyStatus(true);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void cbBusDesDBName_DropDown(object sender, EventArgs e)
        {
            try
            {
                if (!isLoadedDesBus)
                {
                    isLoadedDesBus = true;
                    //set values in the cb table

                    string DBSN = txtBusDesDBServer.Text;
                    //string DB = doc.GetElementsByTagName("DB").Item(0).InnerText;
                    string User = txtBusDesUser.Text;
                    string Password = txtBusDesPass.Text;

                    //string DB = (string) cbDB.SelectedValue;

                    string gb_strConn = "";
                    gb_strConn += "Data Source=" + DBSN + ";";
                    //gb_strConn +="Initial Catalog=" + DB +";";
                    gb_strConn += "User ID=" + User + ";";
                    gb_strConn += "Password=" + Password;

                    DataAccess da = new DataAccess(gb_strConn);
                    //string c = gb_strConn;
                    da.open();
                    if (da.m_isConnected)
                    {
                        DataSet dsListDB = da.getDataSet_Table("SELECT name FROM master..sysdatabases", "ListDB");
                        if (da.testExistValueInTableAndHaveRow(dsListDB))
                        {
                            cbBusDesDBName.DataSource = dsListDB.Tables[0];
                            cbBusDesDBName.DisplayMember = dsListDB.Tables[0].Columns[0].ColumnName;
                            cbBusDesDBName.ValueMember = dsListDB.Tables[0].Columns[0].ColumnName;
                            cbBusDesDBName.SelectedIndex = 0;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Error: " + da.m_strErr);
                    }
                    da.close();

                    if (isLoaded)
                    {
                        setApplyStatus(true);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void cbBusSourceDBName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cbBusDesDBName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtEDTARestService_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedDesBus = false;
        }

        private void txtTimeZone_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedDesBus = false;
        }

        private void txtBusList_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedDesBus = false;
        }

        private void chbReverse4041_CheckedChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedDesBus = false;
        }

        private void chbReverse4849_CheckedChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedDesBus = false;
        }

        private void chbReverse5253_CheckedChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedDesBus = false;
        }

        private void chbReverse4445_CheckedChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedDesBus = false;
        }

        private void chbReverse5051_CheckedChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedDesBus = false;
        }

        private void chbReverse6162_CheckedChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedDesBus = false;
        }

        private void txtSwapBus_TextChanged(object sender, EventArgs e)
        {
            if (isLoaded)
            {
                setApplyStatus(true);
            }
            isLoadedDesBus = false;
        }

        private void cbhSkipTime_CheckedChanged(object sender, EventArgs e)
        {
            dtpSkipFrom.Enabled = chbSkipTime.Checked;
            dtpSkipTo.Enabled = chbSkipTime.Checked;
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

        private void dtpSkipFrom_ValueChanged(object sender, EventArgs e)
        {
            dtpSkipTo.Value = dtpSkipFrom.Value.AddMinutes(1);
            if (isLoaded)
            {
                setApplyStatus(true);
            }
        }

    }
}