﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("AFD")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("team Black_X")]
[assembly: AssemblyProduct("AFD")]
[assembly: AssemblyCopyright("Copyright © team Black_X 2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("40178035-da0b-4f6e-8d0f-dc16bed7fff6")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.0.9")]
[assembly: AssemblyFileVersion("1.0.0.9")]

//Ved
//version 1.0.0.9
//date: 12/12/2017
//add new feature: skip at a position time that was configed in option menu

//Ved
//version 1.0.0.8
//fix rcManagerBus.RcNext will be asign to the first record in the list before main thread


//Ved
//version 1.0.0.7
//fix swap bus function in stored procedure


//Ved
//version 1.0.0.6
//support buspotion event filter and units filter

//Ved
//version 1.0.0.5
//adding option simulate: only student (event 60), syncronized with current system time...

//version 1.0.0.4
//change condition time from PositionTimeStamp to WriteTimeStamp of buspostion

//version 1.0.0.3
//script to create storeprocedure if not exists

//version 1.0.0.2
//update for supporting of french (canada)

//version 1.0.0.1
//update for filling data to busposition
