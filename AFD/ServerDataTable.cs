using System;

	/// <summary>
	/// Must match ServerDataTable struct in ServerDataTable.h
	/// </summary>
public class ServerFieldDef
{
    public int Type;
    public int Flags;
    public String Name;
    public String Label;
}
public class ServerDataTable
	{		
		public ServerFieldDef[] FieldDefs;
		public ServerRow[] Rows;				
	}
public class ServerRow
{
    public String[] Cols;
}
public class ServerConst
{
    // field types
    public static int TypeEmpty = 0;
    public static int TypeInteger = 1;
    public static int TypeReal = 2;
    public static int TypeDateTime = 3;
    public static int TypeText = 4;

    // field flags
    public static int FlagNone = 0;
    public static int FlagUniqueID = 1;
}
