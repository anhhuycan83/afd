﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Collections;

namespace eDTA.Entities
{
    public sealed class SettingNames
    {

        public const string TimeZone = "TimeZone";
        public const string ETEXT_ConnectString = "ETEXT_ConnectString";
        public const string ETGPS_ConnectString = "ETGPS_ConnectString";
        public const string ETDB_ConnectString = "ETDB_ConnectString";
        public const string ETDB_Name = "ETDB_Name";
        public const string ENAME_Webservice = "ENAME_Webservice";
        public const string LOG_DIR = "LOG_DIR";
        public const string Apk_latest_version = "Apk_latest_version";
        public const string Apk_url = "Apk_url";
        public const string Apk_path = "Apk_path";

    }
    public class EDTASettings
    {
        public static string strTimeZone;
        public static Dictionary<string, string> DicStuTagMap = new Dictionary<string, string>();        
        public static DateTime GetNowClientDate()
        {
            int offset = ClientTimeZoneOffset();
            return DateTime.Now.AddHours(offset);
        }

        static int GetUtcToServerOffset()
        {
            DateTime CurTime = DateTime.Now;
            TimeZone TZ = TimeZone.CurrentTimeZone;
            TimeSpan tsServerOffset = TZ.GetUtcOffset(DateTime.Now);
            return tsServerOffset.Hours;
        }
        public static int GetUtcToClientOffset(string strTimeZone)
        {
            //determine if daylight savings is in effect
            DateTime CurTime = DateTime.Now;
            TimeZone TZ = TimeZone.CurrentTimeZone;
            bool DaylightSavingsInEffect = TZ.IsDaylightSavingTime(CurTime);
            int intGMT_TO_CLIENT_OFFSET_HOURS = int.MinValue;
            //bool Okay = true;

            switch (strTimeZone.ToString().ToLower().Trim())
            {
                case "atlantic":
                    intGMT_TO_CLIENT_OFFSET_HOURS = -4;
                    break;
                case "eastern":
                    if (DaylightSavingsInEffect)
                        intGMT_TO_CLIENT_OFFSET_HOURS = -4;
                    else
                        intGMT_TO_CLIENT_OFFSET_HOURS = -5;
                    break;
                case "central":
                    if (DaylightSavingsInEffect)
                        intGMT_TO_CLIENT_OFFSET_HOURS = -5;
                    else
                        intGMT_TO_CLIENT_OFFSET_HOURS = -6;
                    break;
                case "mountain":
                    if (DaylightSavingsInEffect)
                        intGMT_TO_CLIENT_OFFSET_HOURS = -6;
                    else
                        intGMT_TO_CLIENT_OFFSET_HOURS = -7;
                    break;
                case "pacific":
                    if (DaylightSavingsInEffect)
                        intGMT_TO_CLIENT_OFFSET_HOURS = -7;
                    else
                        intGMT_TO_CLIENT_OFFSET_HOURS = -8;
                    break;
                case "alaska":
                    if (DaylightSavingsInEffect)
                        intGMT_TO_CLIENT_OFFSET_HOURS = -8;
                    else
                        intGMT_TO_CLIENT_OFFSET_HOURS = -9;
                    break;
                case "hawaii":
                    intGMT_TO_CLIENT_OFFSET_HOURS = -10;
                    break;
                case "arizona":
                    intGMT_TO_CLIENT_OFFSET_HOURS = -7;
                    break;
                case "local":
                    TimeSpan tsServerOffset = TZ.GetUtcOffset(DateTime.Now);
                    intGMT_TO_CLIENT_OFFSET_HOURS = tsServerOffset.Hours;
                    break;

                default:
                    //Okay = false;
                    break;
            }
            return intGMT_TO_CLIENT_OFFSET_HOURS;
        }

        public static int GetUtcToClientOffsetNoDaylight(string strTimeZone)
        {
            //determine if daylight savings is in effect
            //DateTime CurTime = DateTime.Now;
            TimeZone TZ = TimeZone.CurrentTimeZone;
            //bool DaylightSavingsInEffect = TZ.IsDaylightSavingTime(CurTime);
            int intGMT_TO_CLIENT_OFFSET_HOURS = int.MinValue;
            //bool Okay = true;

            switch (strTimeZone.ToString().ToLower().Trim())
            {
                case "atlantic":
                    intGMT_TO_CLIENT_OFFSET_HOURS = -4;
                    break;
                case "eastern":

                    intGMT_TO_CLIENT_OFFSET_HOURS = -5;
                    break;
                case "central":
                    intGMT_TO_CLIENT_OFFSET_HOURS = -6;
                    break;
                case "mountain":
                    intGMT_TO_CLIENT_OFFSET_HOURS = -7;
                    break;
                case "pacific":
                    intGMT_TO_CLIENT_OFFSET_HOURS = -8;
                    break;
                case "alaska":
                    intGMT_TO_CLIENT_OFFSET_HOURS = -9;
                    break;
                case "hawaii":
                    intGMT_TO_CLIENT_OFFSET_HOURS = -10;
                    break;
                case "arizona":
                    intGMT_TO_CLIENT_OFFSET_HOURS = -7;
                    break;
                case "local":
                    TimeSpan tsServerOffset = TZ.GetUtcOffset(DateTime.Now);
                    intGMT_TO_CLIENT_OFFSET_HOURS = tsServerOffset.Hours;
                    break;

                default:
                    //Okay = false;
                    break;
            }
            return intGMT_TO_CLIENT_OFFSET_HOURS;
        }
        public static int ClientTimeZoneOffset()
        {   
            int intGMT_TO_CLIENT_OFFSET_HOURS = GetUtcToClientOffset(strTimeZone);
            int intGMT_TO_SERVER_OFFSET_HOURS = GetUtcToServerOffset();
            int offset = 0;
            offset = intGMT_TO_CLIENT_OFFSET_HOURS - intGMT_TO_SERVER_OFFSET_HOURS;
            return offset;
        }
        
    }
}
