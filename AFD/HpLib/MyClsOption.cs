using System;
using System.Collections.Generic;
using System.Text;
using HPConFigXml;
using System.IO;
using FreeSMS.MyLib;

namespace AFD.HPLib
{
    public class MyClsOption
    {
        public enum DMType
        {
            IDM = 1,
            FLASHGET = 2
        }
       
        static public string fileConfig = "HPAFD.xml";
        private MyClsUserManagerment m_userManager;

        internal MyClsUserManagerment UserManager
        {
            get { return m_userManager; }
            set { m_userManager = value; }
        }

        public MyClsOption()
        {
            xcfg = new Xmlconfig(fileConfig, true);
            //Load list user
            m_userManager = new MyClsUserManagerment(xcfg);            
            getAFDConn();
        }

        private Xmlconfig xcfg;

        public Xmlconfig Xcfg
        {
            get { return xcfg; }
            set { xcfg = value; }
        }

        #region Memmbers to store informations

        private string m_iDMPath;

        public string IDMPath
        {
            get {
                if (xcfg.Settings["IDMPATH"].Value == "")
                    return @"c:\Program Files\Internet Download Manager\IDMan.exe";
                else
                {
                    //if (!File.Exists(xcfg.Settings["IDMPATH"].Value))
                    //    return @"c:\Program Files\Internet Download Manager\IDMan.exe";
                    //else
                    //    return xcfg.Settings["IDMPATH"].Value;

                    return xcfg.Settings["IDMPATH"].Value;
                }
            }
            set { xcfg.Settings["IDMPATH"].Value = value; }
        }
        private string m_flashGetPath;

        public string FlashGetPath
        {
            get
            {
                if (xcfg.Settings["FLASHGETPATH"].Value == "")
                    return @"c:\Program Files\FlashGet\flashget.exe";
                else
                {
                    //if (!File.Exists(xcfg.Settings["FLASHGETPATH"].Value))
                    //    return @"c:\Program Files\FlashGet\flashget.exe";
                    //else
                    //    return xcfg.Settings["FLASHGETPATH"].Value;

                    return xcfg.Settings["FLASHGETPATH"].Value;
                }
            }
            set { xcfg.Settings["FLASHGETPATH"].Value = value; }
        }
        private string m_SaveTo;

        public string SaveTo
        {
            get {
                if (xcfg.Settings["SAVETO"].Value == "")
                    return @"C:";
                else
                {
                    //xcfg.Settings["SAVETO"].Value
                    if (!Directory.Exists(xcfg.Settings["SAVETO"].Value))
                        Directory.CreateDirectory(xcfg.Settings["SAVETO"].Value);
                    
                    return xcfg.Settings["SAVETO"].Value;
                }
            }
            set {
                if (value.Substring(value.Length - 1) == @"\")
                    value = value.Substring(0, value.Length - 1);
                xcfg.Settings["SAVETO"].Value = value;  
            }
        }

        private int m_LinkNo;

        public int LinkNo
        {
            get {
                if (xcfg.Settings["LINKNO"].intValue == 0)
                    return 1;
                else
                    return xcfg.Settings["LINKNO"].intValue;

            }
            set { xcfg.Settings["LINKNO"].intValue = value;  }
        }

        private int m_TimeOut;

        public int TimeOut
        {
            get {
                if (xcfg.Settings["TIMEOUT"].intValue == 0)
                    return 30;
                else
                    return xcfg.Settings["TIMEOUT"].intValue;

            }
            set { xcfg.Settings["TIMEOUT"].intValue = value;  }
        }

        private DMType m_DownLoadType;

        public DMType DownLoadType
        {
            get {
                if (string.IsNullOrEmpty(xcfg.Settings["DMTYPE"].Value))
                     return DMType.IDM;
                else
                {
                    if (Enum.IsDefined(typeof(DMType), xcfg.Settings["DMTYPE"].Value))
                    {
                        return (DMType)Enum.Parse(typeof(DMType), xcfg.Settings["DMTYPE"].Value);
                    }
                    else
                        return DMType.IDM;
                }
            }
            set { xcfg.Settings["DMTYPE"].Value = value.ToString();  }
        }

        private int m_repeatCycle;

        public int RepeatCycle
        {
            get {
                if (xcfg.Settings["REPEATCYCLE"].intValue == 0)
                    return 20;
                else
                    return xcfg.Settings["REPEATCYCLE"].intValue;
            }
            set { xcfg.Settings["REPEATCYCLE"].intValue = value; }
        }

        private string m_ServerLeech;

        public string ServerLeech
        {
            get
            {
                if (xcfg.Settings["SERVERLEECH"].Value == "")
                    return "TEENHK";
                else
                    return xcfg.Settings["SERVERLEECH"].Value;
            }
            set
            {                
                xcfg.Settings["SERVERLEECH"].Value = value;
            }
        }

        private MyServerLeechType eServerType;

        public MyServerLeechType EServerType
        {
            get
            {
                return (MyServerLeechType)Enum.Parse(typeof(MyServerLeechType), ServerLeech);
            }
            set { ServerLeech = value.ToString(); }
        }

        private string m_CookieHF;

        public string CookieHF
        {
            get
            {
                if (xcfg.Settings["COOKIEHF"].Value == "")
                    return "";
                else
                    return xcfg.Settings["COOKIEHF"].Value;
            }
            set
            {
                xcfg.Settings["COOKIEHF"].Value = value;
            }
        }

        private string m_CookieMU;

        public string CookieMU
        {
            get
            {
                if (xcfg.Settings["COOKIEMU"].Value == "")
                    return "";
                else
                    return xcfg.Settings["COOKIEMU"].Value;
            }
            set
            {
                xcfg.Settings["COOKIEMU"].Value = value;
            }
        }

        private string m_CookieRS;

        public string CookieRS
        {
            get
            {
                if (xcfg.Settings["COOKIERS"].Value == "")
                    return "";
                else
                    return xcfg.Settings["COOKIERS"].Value;
            }
            set
            {
                xcfg.Settings["COOKIERS"].Value = value;
            }
        }

        public int WaitAfterSetText
        {
            get
            {
                if (xcfg.Settings["WAITAFTERSETTEXT"].intValue == 0)
                    return 1;
                else
                    return xcfg.Settings["WAITAFTERSETTEXT"].intValue;
            }
            set { xcfg.Settings["WAITAFTERSETTEXT"].intValue = value; }
        }

        public int WaitBeforeClose
        {
            get
            {
                if (xcfg.Settings["WAITBEFORECLOSE"].intValue == 0)
                    return 2;
                else
                    return xcfg.Settings["WAITBEFORECLOSE"].intValue;
            }
            set { xcfg.Settings["WAITBEFORECLOSE"].intValue = value; }
        }

        public int CycleYahoo
        {
            get
            {
                if (xcfg.Settings["CYCLEYAHOO"].intValue == 0)
                    return 3;
                else
                    return xcfg.Settings["CYCLEYAHOO"].intValue;
            }
            set { xcfg.Settings["CYCLEYAHOO"].intValue = value; }
        }

        public int ReadCbox
        {
            get
            {
                if (xcfg.Settings["READCBOX"].intValue == 0)
                    return 20;
                else
                    return xcfg.Settings["READCBOX"].intValue;
            }
            set { xcfg.Settings["READCBOX"].intValue = value; }
        }

        public int MaxReadCbox
        {
            get
            {
                if (xcfg.Settings["MAXREADCBOX"].intValue == 0)
                    return 15;
                else
                    return xcfg.Settings["MAXREADCBOX"].intValue;
            }
            set { xcfg.Settings["MAXREADCBOX"].intValue = value; }
        }

        public int ShowMsgCbox
        {
            get
            {
                if (xcfg.Settings["SHOWMSGCBOX"].intValue == 0)
                    return 10;
                else
                    return xcfg.Settings["SHOWMSGCBOX"].intValue;
            }
            set { xcfg.Settings["SHOWMSGCBOX"].intValue = value; }
        }

        //for Database tab
        public string DBSERVERNAME
        {
            get
            {
                if (xcfg.Settings["DBSERVERNAME"].Value == "")
                    return "";
                else
                    return xcfg.Settings["DBSERVERNAME"].Value;
            }
            set
            {
                xcfg.Settings["DBSERVERNAME"].Value = value;
            }
        }

        public string DBUSER
        {
            get
            {
                if (xcfg.Settings["DBUSER"].Value == "")
                    return "";
                else
                    return xcfg.Settings["DBUSER"].Value;
            }
            set
            {
                xcfg.Settings["DBUSER"].Value = value;
            }
        }

        public string DBPASS
        {
            get
            {
                if (xcfg.Settings["DBPASS"].Value == "")
                    return "";
                else
                    return xcfg.Settings["DBPASS"].Value;
            }
            set
            {
                xcfg.Settings["DBPASS"].Value = value;
            }
        }

        public string DBNAME
        {
            get
            {
                if (xcfg.Settings["DBNAME"].Value == "")
                    return "";
                else
                    return xcfg.Settings["DBNAME"].Value;
            }
            set
            {
                xcfg.Settings["DBNAME"].Value = value;
            }
        }

        //for AFD tab
        public string DBSERVERNAMESOURCE
        {
            get
            {
                if (xcfg.Settings["DBSERVERNAMESOURCE"].Value == "")
                    return "xp-testing2\\sqlexpress";
                else
                    return xcfg.Settings["DBSERVERNAMESOURCE"].Value;
            }
            set
            {
                xcfg.Settings["DBSERVERNAMESOURCE"].Value = value;
            }
        }

        public string DBUSERSOURCE
        {
            get
            {
                if (xcfg.Settings["DBUSERSOURCE"].Value == "")
                    return "etm";
                else
                    return xcfg.Settings["DBUSERSOURCE"].Value;
            }
            set
            {
                xcfg.Settings["DBUSERSOURCE"].Value = value;
            }
        }

        public string DBPASSSOURCE
        {
            get
            {
                if (xcfg.Settings["DBPASSSOURCE"].Value == "")
                    return "etm";
                else
                    return xcfg.Settings["DBPASSSOURCE"].Value;
            }
            set
            {
                xcfg.Settings["DBPASSSOURCE"].Value = value;
            }
        }

        public string DBNAMESOURCE
        {
            get
            {
                if (xcfg.Settings["DBNAMESOURCE"].Value == "")
                    return "";
                else
                    return xcfg.Settings["DBNAMESOURCE"].Value;
            }
            set
            {
                xcfg.Settings["DBNAMESOURCE"].Value = value;
            }
        }

        public string DBSERVERNAMEDES
        {
            get
            {
                if (xcfg.Settings["DBSERVERNAMEDES"].Value == "")
                    return "xp-testing2\\sqlexpress";
                else
                    return xcfg.Settings["DBSERVERNAMEDES"].Value;
            }
            set
            {
                xcfg.Settings["DBSERVERNAMEDES"].Value = value;
            }
        }

        public string DBUSERDES
        {
            get
            {
                if (xcfg.Settings["DBUSERDES"].Value == "")
                    return "etm";
                else
                    return xcfg.Settings["DBUSERDES"].Value;
            }
            set
            {
                xcfg.Settings["DBUSERDES"].Value = value;
            }
        }

        public string DBPASSDES
        {
            get
            {
                if (xcfg.Settings["DBPASSDES"].Value == "")
                    return "etm";
                else
                    return xcfg.Settings["DBPASSDES"].Value;
            }
            set
            {
                xcfg.Settings["DBPASSDES"].Value = value;
            }
        }

        public string DBNAMEDES
        {
            get
            {
                if (xcfg.Settings["DBNAMEDES"].Value == "")
                    return "";
                else
                    return xcfg.Settings["DBNAMEDES"].Value;
            }
            set
            {
                xcfg.Settings["DBNAMEDES"].Value = value;
            }
        }

        /// <summary>
        /// for ADF busposition
        /// </summary>
        //for AFD tab
        public string DBSERVERNAMESOURCEBUS
        {
            get
            {
                if (xcfg.Settings["DBSERVERNAMESOURCEBUS"].Value == "")
                    return "xp-testing2\\sqlexpress";
                else
                    return xcfg.Settings["DBSERVERNAMESOURCEBUS"].Value;
            }
            set
            {
                xcfg.Settings["DBSERVERNAMESOURCEBUS"].Value = value;
            }
        }

        public string DBUSERSOURCEBUS
        {
            get
            {
                if (xcfg.Settings["DBUSERSOURCEBUS"].Value == "")
                    return "etm";
                else
                    return xcfg.Settings["DBUSERSOURCEBUS"].Value;
            }
            set
            {
                xcfg.Settings["DBUSERSOURCEBUS"].Value = value;
            }
        }

        public string DBPASSSOURCEBUS
        {
            get
            {
                if (xcfg.Settings["DBPASSSOURCEBUS"].Value == "")
                    return "etm";
                else
                    return xcfg.Settings["DBPASSSOURCEBUS"].Value;
            }
            set
            {
                xcfg.Settings["DBPASSSOURCEBUS"].Value = value;
            }
        }

        public string DBNAMESOURCEBUS
        {
            get
            {
                if (xcfg.Settings["DBNAMESOURCEBUS"].Value == "")
                    return "";
                else
                    return xcfg.Settings["DBNAMESOURCEBUS"].Value;
            }
            set
            {
                xcfg.Settings["DBNAMESOURCEBUS"].Value = value;
            }
        }

        public string DBSERVERNAMEDESBUS
        {
            get
            {
                if (xcfg.Settings["DBSERVERNAMEDESBUS"].Value == "")
                    return "xp-testing2\\sqlexpress";
                else
                    return xcfg.Settings["DBSERVERNAMEDESBUS"].Value;
            }
            set
            {
                xcfg.Settings["DBSERVERNAMEDESBUS"].Value = value;
            }
        }

        public string DBUSERDESBUS
        {
            get
            {
                if (xcfg.Settings["DBUSERDESBUS"].Value == "")
                    return "etm";
                else
                    return xcfg.Settings["DBUSERDESBUS"].Value;
            }
            set
            {
                xcfg.Settings["DBUSERDESBUS"].Value = value;
            }
        }

        public string DBPASSDESBUS
        {
            get
            {
                if (xcfg.Settings["DBPASSDESBUS"].Value == "")
                    return "etm";
                else
                    return xcfg.Settings["DBPASSDESBUS"].Value;
            }
            set
            {
                xcfg.Settings["DBPASSDESBUS"].Value = value;
            }
        }

        public string DBNAMEDESBUS
        {
            get
            {
                if (xcfg.Settings["DBNAMEDESBUS"].Value == "")
                    return "";
                else
                    return xcfg.Settings["DBNAMEDESBUS"].Value;
            }
            set
            {
                xcfg.Settings["DBNAMEDESBUS"].Value = value;
            }
        }

        protected string strConnAFDSource;
        protected string strConnAFDDes;

        public string EDTARestService
        {
            get
            {
                if (xcfg.Settings["EDTARestService"].Value == "")
                    return "http://huy/Denton/eDTARestService/eDTAService.svc";
                else
                    return xcfg.Settings["EDTARestService"].Value;
            }
            set
            {
                xcfg.Settings["EDTARestService"].Value = value;
            }
        }
        public string TimeZone
        {
            get
            {
                if (xcfg.Settings["TimeZone"].Value == "")
                    return "Central";
                else
                    return xcfg.Settings["TimeZone"].Value;
            }
            set
            {
                xcfg.Settings["TimeZone"].Value = value;
            }
        }

        public string UnitListBusPosition
        {
            get
            {
                if (xcfg.Settings["UnitListBusPosition"].Value == "")
                    return "";
                else
                    return xcfg.Settings["UnitListBusPosition"].Value;
            }
            set
            {
                xcfg.Settings["UnitListBusPosition"].Value = value;
            }
        }

        public string Reverse4041
        {
            get
            {
                if (xcfg.Settings["Reverse4041"].Value == "")
                    return "0";
                else
                    return xcfg.Settings["Reverse4041"].Value;
            }
            set
            {
                xcfg.Settings["Reverse4041"].Value = value;
            }
        }

        public string Reverse4445
        {
            get
            {
                if (xcfg.Settings["Reverse4445"].Value == "")
                    return "0";
                else
                    return xcfg.Settings["Reverse4445"].Value;
            }
            set
            {
                xcfg.Settings["Reverse4445"].Value = value;
            }
        }
        public string Reverse4849
        {
            get
            {
                if (xcfg.Settings["Reverse4849"].Value == "")
                    return "0";
                else
                    return xcfg.Settings["Reverse4849"].Value;
            }
            set
            {
                xcfg.Settings["Reverse4849"].Value = value;
            }
        }
        public string Reverse5051
        {
            get
            {
                if (xcfg.Settings["Reverse5051"].Value == "")
                    return "0";
                else
                    return xcfg.Settings["Reverse5051"].Value;
            }
            set
            {
                xcfg.Settings["Reverse5051"].Value = value;
            }
        }

        public string Reverse5253
        {
            get
            {
                if (xcfg.Settings["Reverse5253"].Value == "")
                    return "0";
                else
                    return xcfg.Settings["Reverse5253"].Value;
            }
            set
            {
                xcfg.Settings["Reverse5253"].Value = value;
            }
        }

        public string Reverse6162
        {
            get
            {
                if (xcfg.Settings["Reverse6162"].Value == "")
                    return "0";
                else
                    return xcfg.Settings["Reverse6162"].Value;
            }
            set
            {
                xcfg.Settings["Reverse6162"].Value = value;
            }
        }

        public string SwapUnit
        {
            get
            {
                if (xcfg.Settings["SwapUnit"].Value == "")
                    return "";
                else
                    return xcfg.Settings["SwapUnit"].Value;
            }
            set
            {
                xcfg.Settings["SwapUnit"].Value = value;
            }
        }
        public string IgnoreDaylight
        {
            get
            {
                if (xcfg.Settings["IgnoreDaylight"].Value == "")
                    return "1";
                else
                    return xcfg.Settings["IgnoreDaylight"].Value;
            }
            set
            {
                xcfg.Settings["IgnoreDaylight"].Value = value;
            }
        }
        #endregion

        public void SaveConfig()
        {
            xcfg.Save(fileConfig);
            getAFDConn();
        }

        private void getAFDConn()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Data Source=" + DBSERVERNAMESOURCE + ";");
            sb.Append("Initial Catalog=" + DBNAMESOURCE + ";");
            sb.Append("User ID=" + DBUSERSOURCE + ";");
            sb.Append("Password=" + DBPASSSOURCE);
            strConnAFDSource = sb.ToString();
            //des connection
            sb.Append("Data Source=" + DBSERVERNAMEDES + ";");
            sb.Append("Initial Catalog=" + DBNAMEDES + ";");
            sb.Append("User ID=" + DBUSERDES + ";");
            sb.Append("Password=" + DBPASSDES);
            strConnAFDDes = sb.ToString();
        }

        public List<MyClsFile> GetAllFiles()
        {
            List<MyClsFile> lRs = new List<MyClsFile>();
            MyClsFile fileTemp;
            foreach (ConfigSetting cs in xcfg.Settings.GetNamedChildren("FILEITEM"))
            {
                fileTemp = new MyClsFile();
                fileTemp.FileDirectLink = cs["FileDirectLink"].Value;
                fileTemp.FileDownCount = cs["FileDownCount"].Value;
                fileTemp.FileID = cs["FileID"].Value;
                fileTemp.FileName = cs["FileName"].Value;
                fileTemp.FilePassword = cs["FilePassword"].Value;
                fileTemp.FileSize = cs["FileSize"].Value;
                fileTemp.FileStatus = (MyClsFile.MyFileStatus)Enum.Parse(typeof(MyClsFile.MyFileStatus), cs["FileStatus"].Value);
                fileTemp.FileStrCatcha = cs["FileStrCatcha"].Value;
                fileTemp.FileUrl = cs["FileUrl"].Value;
                fileTemp.FileUrlType = (MyClsFile.MyUrlType)Enum.Parse(typeof(MyClsFile.MyUrlType), cs["FileUrlType"].Value);
                lRs.Add(fileTemp);
            }
            return lRs;            
        }
        

        public void SaveAllFile(List<MyClsFile> lsFiles)
        {
            int index;
            ConfigSetting cs;
            //first remove all files exist in config file
            foreach (ConfigSetting csTemp in xcfg.Settings.GetNamedChildren("FILEITEM"))
            {
                csTemp.Remove();
            }
            //now add all files from list to file config
            foreach (MyClsFile fileTemp in lsFiles)
            {
                //ConfigSetting cs = xcfg.Settings["FILE_ITEM##"];
                
                //insert new
                cs = xcfg.Settings["FILEITEM##"];
                SetFileAttribute(cs, fileTemp);

                //index = GetIndexFile(fileTemp);
                //if (index == -1)
                //{
                //    //insert new
                //    cs = xcfg.Settings["FILEITEM##"];
                //    SetFileAttribute(cs, fileTemp);
                //    //lsFiles.Add(fileTemp);
                //}
                //else
                //{
                //    //update exist
                //    cs = xcfg.Settings["FILEITEM#" + index.ToString()];
                //    SetFileAttribute(cs, fileTemp);
                //}                
            }
            SaveConfig();
        }

        private void SetFileAttribute(ConfigSetting cs, MyClsFile fileTemp)
        {
            cs["FileDirectLink"].Value = fileTemp.FileDirectLink;
            cs["FileDownCount"].Value = fileTemp.FileDownCount;
            cs["FileID"].Value = fileTemp.FileID;
            cs["FileName"].Value = fileTemp.FileName;
            cs["FilePassword"].Value = fileTemp.FilePassword;
            cs["FileSize"].Value = fileTemp.FileSize;
            cs["FileStatus"].Value = fileTemp.FileStatus.ToString();
            cs["FileStrCatcha"].Value = fileTemp.FileStrCatcha;
            cs["FileUrl"].Value = fileTemp.FileUrl;
            cs["FileUrlType"].Value = fileTemp.FileUrlType.ToString();
        }

        private int GetIndexFile(MyClsFile fileTemp)
        {
            int iRs = -1;
            int index = 0;
            foreach (ConfigSetting cs in xcfg.Settings.GetNamedChildren("FILEITEM"))
            {
                index++;
                if (cs["FileUrl"].Value == fileTemp.FileUrl)
                {
                    iRs = index;
                    break;
                }
            }
            return iRs;
        }

        //public DateTime SkipFrom;
        //public DateTime SkipTo;
        //public bool bIsSkipTime;
        public string bIsSkipTime
        {
            get
            {
                if (xcfg.Settings["bIsSkipTime"].Value == "")
                    return "0";
                else
                    return xcfg.Settings["bIsSkipTime"].Value;
            }
            set
            {
                xcfg.Settings["bIsSkipTime"].Value = value;
            }
        }
        public DateTime SkipFrom
        {
            get
            {
                if (xcfg.Settings["SkipFrom"].Value == "")
                    return DateTime.Now;
                else
                {
                    DateTime dtTemp;
                    System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.CurrentCulture;
                    dtTemp = DateTime.MinValue;
                    DateTime.TryParse(xcfg.Settings["SkipFrom"].Value, ci, System.Globalization.DateTimeStyles.None, out dtTemp);                    

                    return dtTemp;
                }
                    
            }
            set
            {
                xcfg.Settings["SkipFrom"].Value = value.ToString("hh:mm:ss tt");
            }
        }

        public DateTime SkipTo
        {
            get
            {
                if (xcfg.Settings["SkipTo"].Value == "")
                    return DateTime.Now;
                else
                {
                    DateTime dtTemp;
                    System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.CurrentCulture;
                    dtTemp = DateTime.MinValue;
                    DateTime.TryParse(xcfg.Settings["SkipTo"].Value, ci, System.Globalization.DateTimeStyles.None, out dtTemp);

                    return dtTemp;
                }

            }
            set
            {
                xcfg.Settings["SkipTo"].Value = value.ToString("hh:mm:ss tt");
            }
        }
    }
}

