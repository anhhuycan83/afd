using System.ComponentModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using AFD.HPLib;
using System.Windows.Forms;
//using AFD.MyLib;

namespace RealMultiThreading.MultiThreadEngine
{
    public class MTWorker : BackgroundWorker
    {
        #region Private members
        private string m_strBtnName;

        public string StrBtnName
        {
            get { return m_strBtnName; }
            set { m_strBtnName = value; }
        }
        private int m_wkID = 0;

        public int WkID
        {
            get { return m_wkID; }
            //set { m_wkID = value; }
        }
        private int _JobId = 0;
        private TextBox txtWorker;

        public TextBox TxtWorker
        {
            get { return txtWorker; }
            set { txtWorker = value; }
        }
        private Label lbWorker;

        public Label LbWorker
        {
            get { return lbWorker; }
            set { lbWorker = value; }
        }
        private ProgressBar pbWorker;

        public ProgressBar PbWorker
        {
            get { return pbWorker; }
            set { pbWorker = value; }
        }

        private Button btnRun;

        public Button BtnRun
        {
            get { return btnRun; }
            set { btnRun = value; }
        }

        private bool m_btnRunEnableDisable;

        public bool BtnRunEnableDisable
        {
            get { return m_btnRunEnableDisable; }
            set { m_btnRunEnableDisable = value; }
        }
        private string m_wkName;

        public string WkName
        {
            get { return m_wkName; }
            set { m_wkName = value; }
        }
        private MyWokerType m_wkType;

        public MyWokerType WkType
        {
            get { return m_wkType; }
            set { m_wkType = value; }
        }

        private bool m_IsPause;

        public bool IsPause
        {
            get { return m_IsPause; }
            set { m_IsPause = value; }
        }

        

        #endregion

        #region Properties
        public int IdxLWorker
        {
            get { return m_wkID; }
            set { m_wkID = value; }
        }
        public int JobId
        {
            get { return _JobId; }
            set { _JobId = value; }
        }
        #endregion

        #region Constructor
        public MTWorker()
        {
            WorkerReportsProgress = true;
            WorkerSupportsCancellation = true;
            m_btnRunEnableDisable = true;
        }
        public MTWorker(int wkID, MyWokerType ins_wkType)
            : this()
        {
            //_FormManager = fFormManager;
            m_wkType = ins_wkType;
            m_wkID = wkID;
            m_wkName = ins_wkType.ToString() + "_" + wkID.ToString();
            m_IsPause = false;
        }
        public void SetTxtWorker(TextBox ins_txtWorker)
        {
            txtWorker = ins_txtWorker;
        }
        public void SetLbWorker(Label ins_lbWorker)
        {
            lbWorker = ins_lbWorker;
        }

        public void SetPbWorker(ProgressBar ins_pbWorker)
        {
            pbWorker = ins_pbWorker;
            //pbWorker.Visible = true;
        }
        //public void SetBtWorker(Button ins_btnRun)
        //{
        //    btnRun = ins_btnRun;
        //}

        public void SetBtWorker(Button ins_btnRun, string ins_btnName, bool isEnableDisable)
        {
            btnRun = ins_btnRun;
            m_strBtnName = ins_btnName;
            m_btnRunEnableDisable = isEnableDisable;
        }

        

        
        
        #endregion

        //protected override void OnDoWork(DoWorkEventArgs e)
        //{
            

        //}

        public void SetBtnBeforRun()
        {
            if (btnRun != null)
            {
                if (BtnRunEnableDisable)
                    btnRun.Enabled = false;
                else
                {
                    btnRun.Enabled = true;
                    btnRun.Text = "&Cancel";
                }
            }
        }

        public void SetProgressBarStyle(ProgressBarStyle pbStyle)
        {
            if (PbWorker != null)
            {
                PbWorker.Style = pbStyle;
            }
        }

        public bool setStatusMsg(string strMessage, bool isPrinTime, bool isAppendLine)
        {
            //TextBox txtLogMsg = (TextBox)_FormManager.Controls["txtLog"];
            if (TxtWorker == null)
                return false;
            bool resultCancel = false;
            //txtLog.AppendText("Converting Long, Lat to X, Y";            
            if (isPrinTime)
                TxtWorker.AppendText(DateTime.Now.ToString("hh:mm:ss tt ") + strMessage);
            else
                TxtWorker.AppendText(strMessage);
            if (isAppendLine)
                TxtWorker.AppendText(Environment.NewLine);

            if (this.CancellationPending)
            {
                // Pause for bit to demonstrate that there is time between 
                // "Cancelling..." and "Canceled".
                Thread.Sleep(1200);

                // Set the e.Cancel flag so that the WorkerCompleted event
                // knows that the process was canceled.
                //e.Cancel = true;
                resultCancel = true; ;
            }
            return resultCancel;
        }

        public bool isWorkerCancelling()
        {
            bool bResult = false;
            //BackgroundWorker bwAsync = sender as BackgroundWorker;
            if (this.CancellationPending)
            {
                Thread.Sleep(1200);
                //e.Cancel = true;
                bResult = true;
            }
            else
            {
                //lblProccess.Text = "100%";
                bResult = false;
            }
            return bResult;
        }

        //protected override void OnRunWorkerCompleted(RunWorkerCompletedEventArgs e)
        //{
            
        //}

        //protected override void OnProgressChanged(ProgressChangedEventArgs e)
        //{
            
        //}

        
    }
}
