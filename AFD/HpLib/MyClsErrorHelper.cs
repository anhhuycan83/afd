using System;
using System.Collections.Generic;
using System.Text;

namespace HPLib
{
    class MyClsErrorHelper
    {
        public enum MyReturnCode
        {
            UN_KNOWN = -1,
            SUCCESSFUL = 1,
            VUI_LONG_CHO_LUOT_DOWN_KE_TIEP = 2,
            FILE_WAS_DELETED = 3,
            INVALID_PASSWORD = 4,
            PASSWORD_REQUIRED_OR_INVALID,
            CAPTCHA_REQUIRED,
            INVALID_URL,
            INVALID_URL_OR_FILE_WAS_DELETED,
            FAIL,
            INVALID_PASSWORD_OR_CAPtCHA_REQUIRED
        }
        private MyReturnCode m_returnCode;

        internal MyReturnCode ReturnCode
        {
            get { return m_returnCode; }
            set { m_returnCode = value; }
        }
        private string m_returnMessageEN;

        public string ReturnMessageEN
        {
            get { return m_returnMessageEN; }
            set { m_returnMessageEN = value; }
        }
        private string m_returnMessageVN;

        public string ReturnMessageVN
        {
            get { return m_returnMessageVN; }
            set { m_returnMessageVN = value; }
        }

        public MyClsErrorHelper()
        {
            m_returnCode = MyReturnCode.UN_KNOWN;
            BuilReturnMessage(m_returnCode);
        }

        public MyClsErrorHelper(MyReturnCode ins_returnCode)
        {
            m_returnCode = ins_returnCode;
            BuilReturnMessage(m_returnCode);
        }

        private void BuilReturnMessage(MyReturnCode ins_returnCode)
        {
            switch (ins_returnCode)
            {
                case MyReturnCode.CAPTCHA_REQUIRED:
                    m_returnMessageEN = "CATCHA REQUIRED!";
                    m_returnMessageVN = "Bạn phải nhập mã an toàn!";
                    break;
                case MyReturnCode.FILE_WAS_DELETED:
                    m_returnMessageEN = "FILE WAS DELETED!";
                    m_returnMessageVN = "Tập tin đã bị xóa!";
                    break;
                case MyReturnCode.INVALID_PASSWORD:
                    m_returnMessageEN = "INVALID PASSWORD!";
                    m_returnMessageVN = "Mật khẩu không chính xác!";
                    break;
                case MyReturnCode.INVALID_URL:
                    m_returnMessageEN = "INVALID URL!";
                    m_returnMessageVN = "Đường dẫn không hợp lệ!";
                    break;
                case MyReturnCode.INVALID_URL_OR_FILE_WAS_DELETED:
                    m_returnMessageEN = "INVALID URL OR FILE WAS DELETED!";
                    m_returnMessageVN = "Đường dẫn không hợp lệ hoặc tập tin đã bị xóa!";
                    break;                
                case MyReturnCode.PASSWORD_REQUIRED_OR_INVALID:
                    m_returnMessageEN = "PASSWORD REQUIRED OR INVALID PASSWORD!";
                    m_returnMessageVN = "Bạn phải nhập mật khẩu hoặc mật khẩu đã nhập không chính xác!";
                    break;
                case MyReturnCode.SUCCESSFUL:
                    m_returnMessageEN = "SUCCESSFUL!";
                    m_returnMessageVN = "Thành công!";
                    break;
                case MyReturnCode.FAIL:
                    m_returnMessageEN = "FAIL!";
                    m_returnMessageVN = "Thất bại!";
                    break;
                case MyReturnCode.UN_KNOWN:
                    m_returnMessageEN = "UNKNOWN ERROR!";
                    m_returnMessageVN = "Lỗi không xác định!";
                    break;
                case MyReturnCode.VUI_LONG_CHO_LUOT_DOWN_KE_TIEP:
                    m_returnMessageEN = "PLEASE WAIT FOR THE NEXT DOWNLOAD TIME!";
                    m_returnMessageVN = "Vui lòng chờ đến lượt tải kết tiếp!";
                    break;
                case MyReturnCode.INVALID_PASSWORD_OR_CAPtCHA_REQUIRED:
                    m_returnMessageEN = "INVALID PASSWORD OR CAPTCHA REQUIRED!";
                    m_returnMessageVN = "Mật khẩu không chính xác hoặc chức năng ngăn chặn download đã được mở (catcha)!";
                    break;
                default:
                    m_returnMessageEN = "UNKNOWN ERRORS!";
                    m_returnMessageVN = "Lỗi không xác định!";
                    break;
            }

        }

        public void SetErrorCode(MyReturnCode ins_returnCode)
        {
            m_returnCode = ins_returnCode;
            BuilReturnMessage(m_returnCode);
        }
    }
}
