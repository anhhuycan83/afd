using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AFD.MyLib;
using System.Globalization;

namespace AFD.HpLib
{
    public class MyClsRecordManager
    {
        public enum eTimeType
        {
            eSpeed = 0,
            ePeriod = 1
        }

        private string strConnSource;

        private string strConnDes;

        private eTimeType enumTT;

        internal eTimeType EnumTT
        {
            get { return enumTT; }
            set { enumTT = value; }
        }

        private DateTime selectedDate;
        private AFD.HPLib.MyClsOption m_option;

        public MyClsRecordManager(string connSource, string connDes, AFD.HPLib.MyClsOption option, bool ins_IsDeleteExistRecords)
        {
            //IsHasRecord = false;
            currentIdProccess = -1;
            strConnSource = connSource;
            strConnDes = connDes;
            m_option = option;
            
            rcFirstDate = new MyClsRecord();
            rcNext = new MyClsRecord();
            IsDeleteExistRecords = ins_IsDeleteExistRecords;
            init();
        }

        private MyClsRecord rcFirstDate;

        private MyClsRecord rcNext;

        internal MyClsRecord RcNext
        {
            get { return rcNext; }
            set { rcNext = value; }
        }

        private DateTime dtConverted;

        public DateTime DtConverted
        {
            get { return dtConverted; }
            set { dtConverted = value; }
        }

        private double speed;

        public double Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        private double periodTime;

        public double PeriodTime
        {
            get { return periodTime; }
            set { periodTime = value; }
        }

        private bool isIdentity;

        public bool IsIdentity
        {
            get { return isIdentity; }
            set { isIdentity = value; }
        }

        private int nextIdInsert;

        public int NextIdInsert
        {
            get { return nextIdInsert; }
            set { nextIdInsert = value; }
        }

        private int currentIdProccess;

        public int CurrentIdProccess
        {
            get { return currentIdProccess; }
            set { currentIdProccess = value; }
        }

        private int m_sleepMilisecond;

        public int SleepMilisecond
        {
            get { return m_sleepMilisecond; }
            set { m_sleepMilisecond = value; }
        }

        private DateTime m_lastDTOfCurrentDate;

        public DateTime LastDTOfCurrentDate
        {
            get { return m_lastDTOfCurrentDate; }
            set { m_lastDTOfCurrentDate = value; }
        }

        private DataTable m_tbListDate;

        public DataTable TbListDate
        {
            get { return m_tbListDate; }
            set { m_tbListDate = value; }
        }

        public DateTime dtCurrentSysTime, dtConvertedSysTime, dtActualRecordTime, dtBeginDate;

        private Int64 m_lastRecordIDOfSelectedDate;

        public Int64 LastRecordIDOfSelectedDate
        {
            get { return m_lastRecordIDOfSelectedDate; }
            set { m_lastRecordIDOfSelectedDate = value; }
        }

        private Int64 m_firstRecordIDOfSelectedDate;

        public Int64 FirstRecordIDOfSelectedDate
        {
            get { return m_firstRecordIDOfSelectedDate; }
            set { m_firstRecordIDOfSelectedDate = value; }
        }

        public bool init()
        {
            //check if messate_id column is identity
            //if no => get next id to insert
            message m = new message(strConnSource, strConnDes);
            isIdentity = m.IsIdentity();
            if (!IsIdentity)
                nextIdInsert = m.getNextId();
            //get last record of current date in message table to check whethere exists data in current date to continue filling or delete
            m_lastDTOfCurrentDate = m.getLastTimeRecordOfCurrentDate(DateTime.Now);
            //get list of date and total records for each date
            m_tbListDate = m.getListOfDateAndTotalRecord();
            
            
            return true;
        }

        //public int deleteMessage(DateTime ins_dateToDelete)
        //{
        //    message m = new message(strConnSource);            
        //}

        public void getFristRecordMsgOfDate()
        {
            //get top 2 record of message order by id, tstam
            message m = new message(strConnSource, strConnDes);
            DataTable tbRecord = m.getFristMsgOfDate("tbRecord", selectedDate.ToString("MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US")));
            if (tbRecord.Rows.Count > 0)
            {
                rcNext.IsHasRecord = true;
                rcNext.message_id = (Int64)tbRecord.Rows[0]["message_id"];
                rcNext.message_dt_received = (DateTime)tbRecord.Rows[0]["message_dt_received"];
                rcNext.message_dt_event = (DateTime)tbRecord.Rows[0]["message_dt_event"];
                rcNext.message_event_id = (int)tbRecord.Rows[0]["message_event_id"];
                rcNext.message_vehicle_id = (string)tbRecord.Rows[0]["message_vehicle_id"];
                rcNext.message_latitude = (double)tbRecord.Rows[0]["message_latitude"];
                rcNext.message_longitude = (double)tbRecord.Rows[0]["message_longitude"];

                if (tbRecord.Rows[0]["message_comment"] != System.DBNull.Value)
                    rcNext.message_comment = (string)tbRecord.Rows[0]["message_comment"];

                rcNext.message_dt_adjusted = (DateTime)tbRecord.Rows[0]["message_dt_adjusted"];
            }
            else
                rcNext.IsHasRecord = false;
        }

        public void getNextRecordMsgOfDateByMsgID(Int64 messageID)
        {
            //get top 2 record of message order by id, tstam
            message m = new message(strConnSource, strConnDes);
            DataTable tbRecord = m.getNextRecordMsgOfDate("tbRecord", selectedDate.ToString("MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US")), messageID);
            if (tbRecord.Rows.Count > 0)
            {
                rcNext.IsHasRecord = true;
                rcNext.message_id = (Int64)tbRecord.Rows[0]["message_id"];
                rcNext.message_dt_received = (DateTime)tbRecord.Rows[0]["message_dt_received"];
                rcNext.message_dt_event = (DateTime)tbRecord.Rows[0]["message_dt_event"];
                rcNext.message_event_id = (int)tbRecord.Rows[0]["message_event_id"];
                rcNext.message_vehicle_id = (string)tbRecord.Rows[0]["message_vehicle_id"];
                rcNext.message_latitude = (double)tbRecord.Rows[0]["message_latitude"];
                rcNext.message_longitude = (double)tbRecord.Rows[0]["message_longitude"];

                //if (tbRecord.Rows[0]["message_latitude"] != null)
                //    rcNext.message_latitude = (double)tbRecord.Rows[0]["message_latitude"];
                //if (tbRecord.Rows[0]["message_longitude"] != null)
                //    rcNext.message_latitude = (double)tbRecord.Rows[0]["message_latitude"];

                //rcNext.message_comment = (string)tbRecord.Rows[0]["message_comment"].ToString();

                if (tbRecord.Rows[0]["message_comment"] != System.DBNull.Value)
                    rcNext.message_comment = (string)tbRecord.Rows[0]["message_comment"];

                rcNext.message_dt_adjusted = (DateTime)tbRecord.Rows[0]["message_dt_adjusted"];
            }
            else
                rcNext.IsHasRecord = false;
        }

        public DateTime getLastedRecordMsgOfDate(DateTime dtSelectedDate)
        {
            
            //get top 2 record of message order by id, tstam
            message m = new message(strConnSource, strConnDes);
            return m.getLastTimeRecordOfCurrentDate(dtSelectedDate);            
        }

        public void getNextRecordMsgOfDateByDT(DateTime dtRecord)
        {
            //get top 2 record of message order by id, tstam
            message m = new message(strConnSource, strConnDes);
            DataTable tbRecord = m.getNextRecordMsgOfDateByDateTime("tbRecord", selectedDate.ToString("MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US"))
                , selectedDate.ToString("MM/dd/yyyy ", CultureInfo.GetCultureInfo("en-US")) + dtRecord.ToString("hh:mm:ss tt"));
            if (tbRecord.Rows.Count > 0)
            {
                rcNext.IsHasRecord = true;
                rcNext.message_id = (Int64)tbRecord.Rows[0]["message_id"];
                rcNext.message_dt_received = (DateTime)tbRecord.Rows[0]["message_dt_received"];
                rcNext.message_dt_event = (DateTime)tbRecord.Rows[0]["message_dt_event"];
                rcNext.message_event_id = (int)tbRecord.Rows[0]["message_event_id"];
                rcNext.message_vehicle_id = (string)tbRecord.Rows[0]["message_vehicle_id"];
                rcNext.message_latitude = (double)tbRecord.Rows[0]["message_latitude"];
                rcNext.message_longitude = (double)tbRecord.Rows[0]["message_longitude"];

                if (tbRecord.Rows[0]["message_comment"] != System.DBNull.Value)
                    rcNext.message_comment = (string)tbRecord.Rows[0]["message_comment"];

                rcNext.message_dt_adjusted = (DateTime)tbRecord.Rows[0]["message_dt_adjusted"];
            }
            else
                rcNext.IsHasRecord = false;
        }

        public int DeleteByDate(DateTime dtToDelete)
        {
            message m = new message(strConnSource, strConnDes);
            return m.DeleteByDate(dtToDelete);
        }

        /// <summary>
        /// this method will be run at the first time the thread start
        /// </summary>
        /// <param name="ins_speed"></param>
        /// <param name="ins_dateSelected"></param>
        /// <param name="sleepTime"></param>
        public void SetSpeed(double ins_speed, DateTime ins_dateSelected, int sleepTime, DateTime ins_currentSysTemDate)
        {
            enumTT = eTimeType.eSpeed;
            speed = ins_speed;
            selectedDate = ins_dateSelected;
            dtCurrentSysTime = DateTime.Now;
            dtConvertedSysTime = ins_currentSysTemDate;
            m_sleepMilisecond = sleepTime;
            //get lastest record id of selected date
            message m = new message(strConnSource, strConnDes);
            m_lastRecordIDOfSelectedDate = m.getMaxIdFromDate(selectedDate);
            m_firstRecordIDOfSelectedDate = m.getMinIdFromDate(selectedDate);
        }
        
        /// <summary>
        /// this method will be run at the first time the thread start
        /// </summary>
        /// <param name="ins_periodTime"></param>
        /// <param name="ins_dateSelected"></param>
        /// <param name="sleepTime"></param>
        public void SetPeriod(double ins_periodTime, DateTime ins_dateSelected, int sleepTime, DateTime ins_currentSysTemDate)
        {
            enumTT = eTimeType.ePeriod;
            periodTime = ins_periodTime;
            selectedDate = ins_dateSelected;
            dtCurrentSysTime = ins_currentSysTemDate;
            m_sleepMilisecond = sleepTime;
            //get lastest record id of selected date
            message m = new message(strConnSource, strConnDes);
            m_lastRecordIDOfSelectedDate = m.getMaxIdFromDate(selectedDate);
        }

        public bool ConvertBaseTypeSelected()
        {
            bool bRs = false;
            if (!rcNext.IsHasRecord)
                return bRs;
            //convert message_dt_received
            //DateTime dtCurrentSysTime, dtConvertedSysTime, dtActualRecordTime, dtBeginDate;
            //dtCurrentSysTime = DateTime.Now;

            //dtBeginDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            //TimeSpan tsDiff = dtCurrentSysTime - dtBeginDate;

            DateTime dtRecord = rcNext.message_dt_adjusted;
            dtActualRecordTime = new DateTime(dtCurrentSysTime.Year, dtCurrentSysTime.Month, dtCurrentSysTime.Day,
                dtRecord.Hour, dtRecord.Minute, dtRecord.Second, dtRecord.Millisecond);
            
            //switch (enumTT)
            //{
            //    case eTimeType.eSpeed:
            //        double miliSecondDiff = tsDiff.TotalMilliseconds * speed;
            //        int iTotalMiliSect = Convert.ToInt32(miliSecondDiff);
            //        tsDiff = new TimeSpan(0, 0, 0, 0, iTotalMiliSect);
            //        dtConvertedSysTime = dtBeginDate;
            //        dtConvertedSysTime = dtConvertedSysTime.Add(tsDiff);

            //        dtConverted = dtConvertedSysTime;
            //        bRs = true;
            //        break;
            //    case eTimeType.ePeriod:
            //        //tsDiff = tsDiff * speed;
            //        break;
            //}
            return bRs;
        }

        public void GetActualRecordTime()
        {
            DateTime dtRecord = rcNext.message_dt_adjusted;
            dtActualRecordTime = new DateTime(dtCurrentSysTime.Year, dtCurrentSysTime.Month, dtCurrentSysTime.Day,
                dtRecord.Hour, dtRecord.Minute, dtRecord.Second, dtRecord.Millisecond);
        }

        public bool IsDeleteExistRecords;

        public int InsertNextRecordToDestTable()
        {
            int iRs = 0;
            message m = new message(strConnSource, strConnDes);
            if (isIdentity)
            {
                //insert with identity attribute
                iRs = m.InsertWithYesIdentity(rcNext.message_id, dtActualRecordTime, m_option.DBNAMESOURCE, m_option.DBNAMEDES);
            }
            else
            {
                //insert with no identity attribute
                iRs = m.InsertWithNoIdentity(rcNext.message_id, NextIdInsert, dtActualRecordTime);                
            }
            if(iRs > 0)
                nextIdInsert++;
            return iRs;
        }
        public int InitSPMessage()
        {
            int iRs = 0;
            message m = new message(strConnSource, strConnDes);
            iRs = m.InitSPMessage();
            return iRs;
        }

    }
}
