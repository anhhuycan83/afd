using System;
using System.Collections.Generic;
using System.Text;
using HPConFigXml;
using FreeSMS.MyLib;


namespace AFD.HPLib
{
    class MyClsUserManagerment
    {
        private List<FreeSMS.MyLib.MyClsUser> lsUser;

        Xmlconfig xcfg;

        public List<FreeSMS.MyLib.MyClsUser> LsUser
        {
            get { return lsUser; }
            set { lsUser = value; }
        }

        public MyClsUserManagerment(Xmlconfig ins_xcfg)
        {
            xcfg = ins_xcfg;
            if (xcfg != null)
            {
                lsUser = loadUser();
            }
            else
            {
                lsUser = new List<FreeSMS.MyLib.MyClsUser>();
            }
        }

        private List<FreeSMS.MyLib.MyClsUser> loadUser()
        {
            List<FreeSMS.MyLib.MyClsUser> lsUser;
            //bool result = true;
            //txtMaSo.Text = ps_maSo;
            //txtTenTV.Text = ps_tenTV;
            lsUser = new List<FreeSMS.MyLib.MyClsUser>();
            MyClsUser myClsUser;
            foreach (ConfigSetting cs in xcfg.Settings.GetNamedChildren("User"))
            {
                myClsUser = new FreeSMS.MyLib.MyClsUser(cs["UserName"].Value, cs["Password"].Value, cs["AccType"].Value, cs["Note"].Value);
                lsUser.Add(myClsUser);
            }
            return lsUser;
            //m_loadedUser = false;
            //cbUserName.DisplayMember = "UserName";
            //cbUserName.ValueMember = "UserName";
            //cbUserName.DataSource = lsUser;
            //m_loadedUser = true;
            //if (cbUserName.SelectedItem != null)
            //{
            //    //cbUserName.SelectedIndex = 0; 
            //    cbUserItemChanged();
            //}
            //if (lsPhoneBook.Count > 0)
            //{
            //    cbPhoneBook.SelectedIndex = 0;
            //    cbItemChaged();
            //}

            //return result;
        }

        public void addUser(string ins_userName, string ins_password, string ins_accType, string ins_note)
        {
            ConfigSetting newUser = xcfg.Settings["User##"];
            newUser.Value = "2.1";
            //ConfigSetting newUser = xcfg.Settings["PhoneBook/Vcard##"];
            newUser["UserName"].Value = ins_userName;
            newUser["Password"].Value = ins_password;
            newUser["AccType"].Value = ins_accType;
            newUser["Note"].Value = ins_note;



            lsUser.Add(new MyClsUser(ins_userName, ins_password, ins_accType, ins_note));
        }

        public void updateUser(string ins_userName, string ins_password, string ins_accType, string ins_note
            , string ins_newUserName, string ins_newPassword, string ins_newAccType, string ins_newNote)
        {
            //first update to config file
            foreach (ConfigSetting cs in xcfg.Settings.GetNamedChildren("User"))
            {
                //Console.Write(cs["method"].Value + " ");
                if (cs["UserName"].Value == ins_userName && cs["Password"].Value == ins_password)
                {
                    //cs.Remove();
                    cs["UserName"].Value = ins_newUserName;
                    cs["Password"].Value = ins_newPassword;
                    cs["AccType"].Value = ins_newAccType;
                    cs["Note"].Value = ins_newNote;
                    break;
                }
            }
            //second update to list user
            foreach (MyClsUser entity in lsUser)
            {
                if (entity.UserName == ins_userName && entity.Password == ins_password)
                {
                    //lsUser.Remove(pb);
                    entity.UserName = ins_newUserName;
                    entity.Password = ins_newPassword;
                    entity.AccType = ins_newAccType;
                    entity.Note = ins_newNote;
                    break;
                }
            }
            //add new item

            //ConfigSetting newUser = xcfg.Settings["Vcard##"];
            //newUser.Value = "2.1";
            ////ConfigSetting newUser = xcfg.Settings["PhoneBook/Vcard##"];
            //newUser["UserName"].Value = ins_newUserName;
            //newUser["Password"].Value = ins_newPassword;
            //newUser["Server"].Value = ins_newServer;
            //newUser["Note"].Value = ins_newNote;

            //lsUser.Add(new MyClsUser(ins_newUserName, ins_newPassword, ins_newServer, ins_newNote));
        }

        public void deleteUser(string ins_userName, string ins_password, string ins_accType, string ins_note)
        {
            //first remove this item to config file
            foreach (ConfigSetting cs in xcfg.Settings.GetNamedChildren("User"))
            {
                //Console.Write(cs["method"].Value + " ");
                if (cs["UserName"].Value == ins_userName && cs["Password"].Value == ins_password)
                {
                    cs.Remove();
                    break;
                }
            }
            foreach (MyClsUser entity in lsUser)
            {
                //Console.Write(cs["method"].Value + " ");
                if (entity.UserName == ins_userName && entity.Password == ins_password)
                {
                    lsUser.Remove(entity);
                    break;
                }
            }
        }
        //public int GetUserCount()
    }
}
