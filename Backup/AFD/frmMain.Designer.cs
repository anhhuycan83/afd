namespace AFD
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mitClearAll = new System.Windows.Forms.ToolStripMenuItem();
            this.mitCallIdm = new System.Windows.Forms.ToolStripMenuItem();
            this.toolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mitOption = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtLinksToGet = new System.Windows.Forms.TextBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnAddFile = new System.Windows.Forms.Button();
            this.txtLeechInfor = new System.Windows.Forms.TextBox();
            this.btnDirect = new System.Windows.Forms.Button();
            this.lblProccess = new System.Windows.Forms.Label();
            this.pgbStatus = new System.Windows.Forms.ProgressBar();
            this.btnGetLink = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dtpSystemClock = new System.Windows.Forms.DateTimePicker();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.lblSpeedSysTime = new System.Windows.Forms.Label();
            this.lbl10 = new System.Windows.Forms.Label();
            this.lblActualRecordTime = new System.Windows.Forms.Label();
            this.lbl20 = new System.Windows.Forms.Label();
            this.lblCurrentSysTime = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.nudSleep = new System.Windows.Forms.NumericUpDown();
            this.btnSet = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.nudPeriod = new System.Windows.Forms.NumericUpDown();
            this.nudSpeed = new System.Windows.Forms.NumericUpDown();
            this.rbPeriod = new System.Windows.Forms.RadioButton();
            this.rbSpeed = new System.Windows.Forms.RadioButton();
            this.btnLoadConfig = new System.Windows.Forms.Button();
            this.lblTotalRecordOfSeletedDate = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnResume = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnPause = new System.Windows.Forms.Button();
            this.btnStar = new System.Windows.Forms.Button();
            this.lbltotalRecordInserted = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblIDRemain = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblLastID = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtIDSource = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNextID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chbDeleteExistRecord = new System.Windows.Forms.CheckBox();
            this.cbSeletedDate = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.timerSysClock = new System.Windows.Forms.Timer(this.components);
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.dtpSystemClockBus = new System.Windows.Forms.DateTimePicker();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.lblSpeedSysTimeBus = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblActualRecordTimeBus = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblCurrentSysTimeBus = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.nudSleepBus = new System.Windows.Forms.NumericUpDown();
            this.btnSetBus = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.nudPeriodBus = new System.Windows.Forms.NumericUpDown();
            this.nudSpeedBus = new System.Windows.Forms.NumericUpDown();
            this.rbPeriodBus = new System.Windows.Forms.RadioButton();
            this.rbSpeedBus = new System.Windows.Forms.RadioButton();
            this.btnLoadConfigBus = new System.Windows.Forms.Button();
            this.lblTotalRecordOfSeletedDateBus = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.btnResumeBus = new System.Windows.Forms.Button();
            this.btnStopBus = new System.Windows.Forms.Button();
            this.btnPauseBus = new System.Windows.Forms.Button();
            this.btnStarBus = new System.Windows.Forms.Button();
            this.lbltotalRecordInsertedBus = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.lblIDRemainBus = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.lblLastIDBus = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtIDSourceBus = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtNextIDBus = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.chbDeleteExistRecordBus = new System.Windows.Forms.CheckBox();
            this.cbSeletedDateBus = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.timerSysClockBus = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSleep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSpeed)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSleepBus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPeriodBus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSpeedBus)).BeginInit();
            this.groupBox12.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.toolToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(874, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.exportToolStripMenuItem.Text = "&Export";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitClearAll,
            this.mitCallIdm});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // mitClearAll
            // 
            this.mitClearAll.Name = "mitClearAll";
            this.mitClearAll.Size = new System.Drawing.Size(113, 22);
            this.mitClearAll.Text = "&Clear All";
            // 
            // mitCallIdm
            // 
            this.mitCallIdm.Name = "mitCallIdm";
            this.mitCallIdm.Size = new System.Drawing.Size(113, 22);
            this.mitCallIdm.Text = "Call &IDM";
            // 
            // toolToolStripMenuItem
            // 
            this.toolToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitOption});
            this.toolToolStripMenuItem.Name = "toolToolStripMenuItem";
            this.toolToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.toolToolStripMenuItem.Text = "&Tools";
            // 
            // mitOption
            // 
            this.mitOption.Name = "mitOption";
            this.mitOption.Size = new System.Drawing.Size(106, 22);
            this.mitOption.Text = "&Option";
            this.mitOption.Click += new System.EventHandler(this.mitOption_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "H&elp";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.splitter1);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(874, 87);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Main";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtLog);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(339, 16);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(257, 68);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Log";
            // 
            // txtLog
            // 
            this.txtLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLog.Location = new System.Drawing.Point(3, 16);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtLog.Size = new System.Drawing.Size(251, 49);
            this.txtLog.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtLinksToGet);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox3.Location = new System.Drawing.Point(3, 16);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(336, 68);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Info";
            // 
            // txtLinksToGet
            // 
            this.txtLinksToGet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLinksToGet.Location = new System.Drawing.Point(3, 16);
            this.txtLinksToGet.Multiline = true;
            this.txtLinksToGet.Name = "txtLinksToGet";
            this.txtLinksToGet.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtLinksToGet.Size = new System.Drawing.Size(330, 49);
            this.txtLinksToGet.TabIndex = 0;
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(596, 16);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 68);
            this.splitter1.TabIndex = 3;
            this.splitter1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.btnAddFile);
            this.panel1.Controls.Add(this.txtLeechInfor);
            this.panel1.Controls.Add(this.btnDirect);
            this.panel1.Controls.Add(this.lblProccess);
            this.panel1.Controls.Add(this.pgbStatus);
            this.panel1.Controls.Add(this.btnGetLink);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(599, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(272, 68);
            this.panel1.TabIndex = 1;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(207, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(49, 23);
            this.btnSave.TabIndex = 25;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnAddFile
            // 
            this.btnAddFile.Location = new System.Drawing.Point(161, 3);
            this.btnAddFile.Name = "btnAddFile";
            this.btnAddFile.Size = new System.Drawing.Size(40, 23);
            this.btnAddFile.TabIndex = 22;
            this.btnAddFile.Text = "&Add";
            this.btnAddFile.UseVisualStyleBackColor = true;
            // 
            // txtLeechInfor
            // 
            this.txtLeechInfor.Location = new System.Drawing.Point(77, 32);
            this.txtLeechInfor.Name = "txtLeechInfor";
            this.txtLeechInfor.Size = new System.Drawing.Size(163, 20);
            this.txtLeechInfor.TabIndex = 21;
            // 
            // btnDirect
            // 
            this.btnDirect.Location = new System.Drawing.Point(105, 3);
            this.btnDirect.Name = "btnDirect";
            this.btnDirect.Size = new System.Drawing.Size(53, 23);
            this.btnDirect.TabIndex = 5;
            this.btnDirect.Text = "&Direct";
            this.btnDirect.UseVisualStyleBackColor = true;
            // 
            // lblProccess
            // 
            this.lblProccess.AutoSize = true;
            this.lblProccess.Location = new System.Drawing.Point(87, 35);
            this.lblProccess.Name = "lblProccess";
            this.lblProccess.Size = new System.Drawing.Size(0, 13);
            this.lblProccess.TabIndex = 3;
            // 
            // pgbStatus
            // 
            this.pgbStatus.Location = new System.Drawing.Point(2, 30);
            this.pgbStatus.Name = "pgbStatus";
            this.pgbStatus.Size = new System.Drawing.Size(73, 23);
            this.pgbStatus.TabIndex = 2;
            // 
            // btnGetLink
            // 
            this.btnGetLink.Location = new System.Drawing.Point(3, 3);
            this.btnGetLink.Name = "btnGetLink";
            this.btnGetLink.Size = new System.Drawing.Size(41, 23);
            this.btnGetLink.TabIndex = 0;
            this.btnGetLink.Text = "&Get";
            this.btnGetLink.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(50, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(49, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.dtpSystemClock);
            this.groupBox6.Controls.Add(this.groupBox8);
            this.groupBox6.Controls.Add(this.groupBox7);
            this.groupBox6.Controls.Add(this.btnLoadConfig);
            this.groupBox6.Controls.Add(this.lblTotalRecordOfSeletedDate);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this.groupBox5);
            this.groupBox6.Controls.Add(this.lbltotalRecordInserted);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Controls.Add(this.groupBox2);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.chbDeleteExistRecord);
            this.groupBox6.Controls.Add(this.cbSeletedDate);
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Location = new System.Drawing.Point(6, 114);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(868, 202);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Message Table";
            // 
            // dtpSystemClock
            // 
            this.dtpSystemClock.CustomFormat = "hh:mm:ss tt";
            this.dtpSystemClock.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSystemClock.Location = new System.Drawing.Point(94, 43);
            this.dtpSystemClock.Name = "dtpSystemClock";
            this.dtpSystemClock.Size = new System.Drawing.Size(171, 20);
            this.dtpSystemClock.TabIndex = 14;
            this.dtpSystemClock.ValueChanged += new System.EventHandler(this.dtpSystemClock_ValueChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.lblSpeedSysTime);
            this.groupBox8.Controls.Add(this.lbl10);
            this.groupBox8.Controls.Add(this.lblActualRecordTime);
            this.groupBox8.Controls.Add(this.lbl20);
            this.groupBox8.Controls.Add(this.lblCurrentSysTime);
            this.groupBox8.Controls.Add(this.label10);
            this.groupBox8.Location = new System.Drawing.Point(271, 119);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(246, 77);
            this.groupBox8.TabIndex = 13;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Time converted";
            // 
            // lblSpeedSysTime
            // 
            this.lblSpeedSysTime.AutoSize = true;
            this.lblSpeedSysTime.Location = new System.Drawing.Point(103, 38);
            this.lblSpeedSysTime.Name = "lblSpeedSysTime";
            this.lblSpeedSysTime.Size = new System.Drawing.Size(13, 13);
            this.lblSpeedSysTime.TabIndex = 13;
            this.lblSpeedSysTime.Text = "0";
            // 
            // lbl10
            // 
            this.lbl10.AutoSize = true;
            this.lbl10.Location = new System.Drawing.Point(6, 38);
            this.lbl10.Name = "lbl10";
            this.lbl10.Size = new System.Drawing.Size(81, 13);
            this.lbl10.TabIndex = 12;
            this.lbl10.Text = "Speed sys time:";
            // 
            // lblActualRecordTime
            // 
            this.lblActualRecordTime.AutoSize = true;
            this.lblActualRecordTime.Location = new System.Drawing.Point(103, 58);
            this.lblActualRecordTime.Name = "lblActualRecordTime";
            this.lblActualRecordTime.Size = new System.Drawing.Size(13, 13);
            this.lblActualRecordTime.TabIndex = 11;
            this.lblActualRecordTime.Text = "0";
            // 
            // lbl20
            // 
            this.lbl20.AutoSize = true;
            this.lbl20.Location = new System.Drawing.Point(6, 58);
            this.lbl20.Name = "lbl20";
            this.lbl20.Size = new System.Drawing.Size(95, 13);
            this.lbl20.TabIndex = 10;
            this.lbl20.Text = "Actual record time:";
            // 
            // lblCurrentSysTime
            // 
            this.lblCurrentSysTime.AutoSize = true;
            this.lblCurrentSysTime.Location = new System.Drawing.Point(103, 16);
            this.lblCurrentSysTime.Name = "lblCurrentSysTime";
            this.lblCurrentSysTime.Size = new System.Drawing.Size(13, 13);
            this.lblCurrentSysTime.TabIndex = 9;
            this.lblCurrentSysTime.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Current sys time:";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.nudSleep);
            this.groupBox7.Controls.Add(this.btnSet);
            this.groupBox7.Controls.Add(this.label6);
            this.groupBox7.Controls.Add(this.nudPeriod);
            this.groupBox7.Controls.Add(this.nudSpeed);
            this.groupBox7.Controls.Add(this.rbPeriod);
            this.groupBox7.Controls.Add(this.rbSpeed);
            this.groupBox7.Location = new System.Drawing.Point(271, 20);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(246, 96);
            this.groupBox7.TabIndex = 12;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Time Method";
            // 
            // nudSleep
            // 
            this.nudSleep.Location = new System.Drawing.Point(101, 65);
            this.nudSleep.Name = "nudSleep";
            this.nudSleep.Size = new System.Drawing.Size(53, 20);
            this.nudSleep.TabIndex = 14;
            this.nudSleep.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudSleep.ValueChanged += new System.EventHandler(this.nudSleep_ValueChanged);
            // 
            // btnSet
            // 
            this.btnSet.Enabled = false;
            this.btnSet.Location = new System.Drawing.Point(176, 67);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(64, 23);
            this.btnSet.TabIndex = 13;
            this.btnSet.Text = "&Set";
            this.btnSet.UseVisualStyleBackColor = true;
            this.btnSet.Click += new System.EventHandler(this.btnSet_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Sleep";
            // 
            // nudPeriod
            // 
            this.nudPeriod.Enabled = false;
            this.nudPeriod.Location = new System.Drawing.Point(101, 42);
            this.nudPeriod.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.nudPeriod.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudPeriod.Name = "nudPeriod";
            this.nudPeriod.Size = new System.Drawing.Size(53, 20);
            this.nudPeriod.TabIndex = 3;
            this.nudPeriod.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.nudPeriod.ValueChanged += new System.EventHandler(this.nudPeriod_ValueChanged);
            // 
            // nudSpeed
            // 
            this.nudSpeed.Location = new System.Drawing.Point(101, 16);
            this.nudSpeed.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nudSpeed.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudSpeed.Name = "nudSpeed";
            this.nudSpeed.Size = new System.Drawing.Size(53, 20);
            this.nudSpeed.TabIndex = 2;
            this.nudSpeed.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudSpeed.ValueChanged += new System.EventHandler(this.nudSpeed_ValueChanged);
            // 
            // rbPeriod
            // 
            this.rbPeriod.AutoSize = true;
            this.rbPeriod.Location = new System.Drawing.Point(14, 42);
            this.rbPeriod.Name = "rbPeriod";
            this.rbPeriod.Size = new System.Drawing.Size(89, 17);
            this.rbPeriod.TabIndex = 1;
            this.rbPeriod.Text = "period (hours)";
            this.rbPeriod.UseVisualStyleBackColor = true;
            this.rbPeriod.CheckedChanged += new System.EventHandler(this.rbPeriod_CheckedChanged);
            // 
            // rbSpeed
            // 
            this.rbSpeed.AutoSize = true;
            this.rbSpeed.Checked = true;
            this.rbSpeed.Location = new System.Drawing.Point(14, 19);
            this.rbSpeed.Name = "rbSpeed";
            this.rbSpeed.Size = new System.Drawing.Size(74, 17);
            this.rbSpeed.TabIndex = 0;
            this.rbSpeed.TabStop = true;
            this.rbSpeed.Text = "speed *  X";
            this.rbSpeed.UseVisualStyleBackColor = true;
            this.rbSpeed.CheckedChanged += new System.EventHandler(this.rbSpeed_CheckedChanged);
            // 
            // btnLoadConfig
            // 
            this.btnLoadConfig.Location = new System.Drawing.Point(637, 47);
            this.btnLoadConfig.Name = "btnLoadConfig";
            this.btnLoadConfig.Size = new System.Drawing.Size(75, 23);
            this.btnLoadConfig.TabIndex = 11;
            this.btnLoadConfig.Text = "&Load config";
            this.btnLoadConfig.UseVisualStyleBackColor = true;
            this.btnLoadConfig.Click += new System.EventHandler(this.btnLoadConfig_Click);
            // 
            // lblTotalRecordOfSeletedDate
            // 
            this.lblTotalRecordOfSeletedDate.AutoSize = true;
            this.lblTotalRecordOfSeletedDate.Location = new System.Drawing.Point(183, 180);
            this.lblTotalRecordOfSeletedDate.Name = "lblTotalRecordOfSeletedDate";
            this.lblTotalRecordOfSeletedDate.Size = new System.Drawing.Size(13, 13);
            this.lblTotalRecordOfSeletedDate.TabIndex = 11;
            this.lblTotalRecordOfSeletedDate.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(165, 180);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(12, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "/";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnResume);
            this.groupBox5.Controls.Add(this.btnStop);
            this.groupBox5.Controls.Add(this.btnPause);
            this.groupBox5.Controls.Add(this.btnStar);
            this.groupBox5.Location = new System.Drawing.Point(523, 20);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(108, 141);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Player";
            // 
            // btnResume
            // 
            this.btnResume.Enabled = false;
            this.btnResume.Location = new System.Drawing.Point(18, 85);
            this.btnResume.Name = "btnResume";
            this.btnResume.Size = new System.Drawing.Size(75, 23);
            this.btnResume.TabIndex = 11;
            this.btnResume.Text = "&Resume";
            this.btnResume.UseVisualStyleBackColor = true;
            this.btnResume.Click += new System.EventHandler(this.btnResume_Click);
            // 
            // btnStop
            // 
            this.btnStop.Enabled = false;
            this.btnStop.Location = new System.Drawing.Point(18, 114);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 10;
            this.btnStop.Text = "S&top";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnPause
            // 
            this.btnPause.Enabled = false;
            this.btnPause.Location = new System.Drawing.Point(18, 56);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(75, 23);
            this.btnPause.TabIndex = 9;
            this.btnPause.Text = "&Pause";
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnStar
            // 
            this.btnStar.Location = new System.Drawing.Point(18, 27);
            this.btnStar.Name = "btnStar";
            this.btnStar.Size = new System.Drawing.Size(75, 23);
            this.btnStar.TabIndex = 8;
            this.btnStar.Text = "&Start";
            this.btnStar.UseVisualStyleBackColor = true;
            this.btnStar.Click += new System.EventHandler(this.btnStar_Click);
            // 
            // lbltotalRecordInserted
            // 
            this.lbltotalRecordInserted.AutoSize = true;
            this.lbltotalRecordInserted.Location = new System.Drawing.Point(122, 180);
            this.lbltotalRecordInserted.Name = "lbltotalRecordInserted";
            this.lbltotalRecordInserted.Size = new System.Drawing.Size(13, 13);
            this.lbltotalRecordInserted.TabIndex = 7;
            this.lbltotalRecordInserted.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Total record inserted:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblIDRemain);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.lblLastID);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtIDSource);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtNextID);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(6, 100);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(259, 73);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Next record to insert";
            // 
            // lblIDRemain
            // 
            this.lblIDRemain.AutoSize = true;
            this.lblIDRemain.Location = new System.Drawing.Point(215, 22);
            this.lblIDRemain.Name = "lblIDRemain";
            this.lblIDRemain.Size = new System.Drawing.Size(13, 13);
            this.lblIDRemain.TabIndex = 17;
            this.lblIDRemain.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(154, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "ID Remain:";
            // 
            // lblLastID
            // 
            this.lblLastID.AutoSize = true;
            this.lblLastID.Location = new System.Drawing.Point(202, 48);
            this.lblLastID.Name = "lblLastID";
            this.lblLastID.Size = new System.Drawing.Size(13, 13);
            this.lblLastID.TabIndex = 15;
            this.lblLastID.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(154, 48);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Last ID:";
            // 
            // txtIDSource
            // 
            this.txtIDSource.Location = new System.Drawing.Point(88, 45);
            this.txtIDSource.Name = "txtIDSource";
            this.txtIDSource.Size = new System.Drawing.Size(65, 20);
            this.txtIDSource.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "ID source:";
            // 
            // txtNextID
            // 
            this.txtNextID.Location = new System.Drawing.Point(88, 19);
            this.txtNextID.Name = "txtNextID";
            this.txtNextID.Size = new System.Drawing.Size(65, 20);
            this.txtNextID.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "ID destination:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "System clock:";
            // 
            // chbDeleteExistRecord
            // 
            this.chbDeleteExistRecord.AutoSize = true;
            this.chbDeleteExistRecord.Location = new System.Drawing.Point(16, 71);
            this.chbDeleteExistRecord.Name = "chbDeleteExistRecord";
            this.chbDeleteExistRecord.Size = new System.Drawing.Size(249, 17);
            this.chbDeleteExistRecord.TabIndex = 2;
            this.chbDeleteExistRecord.Text = "Delete exist records in current date before filling";
            this.chbDeleteExistRecord.UseVisualStyleBackColor = true;
            // 
            // cbSeletedDate
            // 
            this.cbSeletedDate.FormattingEnabled = true;
            this.cbSeletedDate.Location = new System.Drawing.Point(96, 17);
            this.cbSeletedDate.Name = "cbSeletedDate";
            this.cbSeletedDate.Size = new System.Drawing.Size(121, 21);
            this.cbSeletedDate.TabIndex = 1;
            this.cbSeletedDate.SelectedIndexChanged += new System.EventHandler(this.cbSeletedDate_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select date:";
            // 
            // timerSysClock
            // 
            this.timerSysClock.Tick += new System.EventHandler(this.timerSysClock_Tick);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.dtpSystemClockBus);
            this.groupBox9.Controls.Add(this.groupBox10);
            this.groupBox9.Controls.Add(this.groupBox11);
            this.groupBox9.Controls.Add(this.btnLoadConfigBus);
            this.groupBox9.Controls.Add(this.lblTotalRecordOfSeletedDateBus);
            this.groupBox9.Controls.Add(this.label19);
            this.groupBox9.Controls.Add(this.groupBox12);
            this.groupBox9.Controls.Add(this.lbltotalRecordInsertedBus);
            this.groupBox9.Controls.Add(this.label21);
            this.groupBox9.Controls.Add(this.groupBox13);
            this.groupBox9.Controls.Add(this.label28);
            this.groupBox9.Controls.Add(this.chbDeleteExistRecordBus);
            this.groupBox9.Controls.Add(this.cbSeletedDateBus);
            this.groupBox9.Controls.Add(this.label29);
            this.groupBox9.Location = new System.Drawing.Point(3, 322);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(868, 202);
            this.groupBox9.TabIndex = 15;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Busposition Table";
            // 
            // dtpSystemClockBus
            // 
            this.dtpSystemClockBus.CustomFormat = "hh:mm:ss tt";
            this.dtpSystemClockBus.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSystemClockBus.Location = new System.Drawing.Point(94, 43);
            this.dtpSystemClockBus.Name = "dtpSystemClockBus";
            this.dtpSystemClockBus.Size = new System.Drawing.Size(171, 20);
            this.dtpSystemClockBus.TabIndex = 14;
            this.dtpSystemClockBus.ValueChanged += new System.EventHandler(this.dtpSystemClockBus_ValueChanged);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.lblSpeedSysTimeBus);
            this.groupBox10.Controls.Add(this.label11);
            this.groupBox10.Controls.Add(this.lblActualRecordTimeBus);
            this.groupBox10.Controls.Add(this.label14);
            this.groupBox10.Controls.Add(this.lblCurrentSysTimeBus);
            this.groupBox10.Controls.Add(this.label16);
            this.groupBox10.Location = new System.Drawing.Point(271, 119);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(246, 77);
            this.groupBox10.TabIndex = 13;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Time converted";
            // 
            // lblSpeedSysTimeBus
            // 
            this.lblSpeedSysTimeBus.AutoSize = true;
            this.lblSpeedSysTimeBus.Location = new System.Drawing.Point(103, 38);
            this.lblSpeedSysTimeBus.Name = "lblSpeedSysTimeBus";
            this.lblSpeedSysTimeBus.Size = new System.Drawing.Size(13, 13);
            this.lblSpeedSysTimeBus.TabIndex = 13;
            this.lblSpeedSysTimeBus.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 38);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(81, 13);
            this.label11.TabIndex = 12;
            this.label11.Text = "Speed sys time:";
            // 
            // lblActualRecordTimeBus
            // 
            this.lblActualRecordTimeBus.AutoSize = true;
            this.lblActualRecordTimeBus.Location = new System.Drawing.Point(103, 58);
            this.lblActualRecordTimeBus.Name = "lblActualRecordTimeBus";
            this.lblActualRecordTimeBus.Size = new System.Drawing.Size(13, 13);
            this.lblActualRecordTimeBus.TabIndex = 11;
            this.lblActualRecordTimeBus.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 58);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(95, 13);
            this.label14.TabIndex = 10;
            this.label14.Text = "Actual record time:";
            // 
            // lblCurrentSysTimeBus
            // 
            this.lblCurrentSysTimeBus.AutoSize = true;
            this.lblCurrentSysTimeBus.Location = new System.Drawing.Point(103, 16);
            this.lblCurrentSysTimeBus.Name = "lblCurrentSysTimeBus";
            this.lblCurrentSysTimeBus.Size = new System.Drawing.Size(13, 13);
            this.lblCurrentSysTimeBus.TabIndex = 9;
            this.lblCurrentSysTimeBus.Text = "0";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 16);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(84, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "Current sys time:";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.nudSleepBus);
            this.groupBox11.Controls.Add(this.btnSetBus);
            this.groupBox11.Controls.Add(this.label17);
            this.groupBox11.Controls.Add(this.nudPeriodBus);
            this.groupBox11.Controls.Add(this.nudSpeedBus);
            this.groupBox11.Controls.Add(this.rbPeriodBus);
            this.groupBox11.Controls.Add(this.rbSpeedBus);
            this.groupBox11.Location = new System.Drawing.Point(271, 20);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(246, 96);
            this.groupBox11.TabIndex = 12;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Time Method";
            // 
            // nudSleepBus
            // 
            this.nudSleepBus.Location = new System.Drawing.Point(101, 65);
            this.nudSleepBus.Name = "nudSleepBus";
            this.nudSleepBus.Size = new System.Drawing.Size(53, 20);
            this.nudSleepBus.TabIndex = 14;
            this.nudSleepBus.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudSleepBus.ValueChanged += new System.EventHandler(this.nudSleepBus_ValueChanged);
            // 
            // btnSetBus
            // 
            this.btnSetBus.Enabled = false;
            this.btnSetBus.Location = new System.Drawing.Point(176, 67);
            this.btnSetBus.Name = "btnSetBus";
            this.btnSetBus.Size = new System.Drawing.Size(64, 23);
            this.btnSetBus.TabIndex = 13;
            this.btnSetBus.Text = "&Set";
            this.btnSetBus.UseVisualStyleBackColor = true;
            this.btnSetBus.Click += new System.EventHandler(this.btnSetBus_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(31, 72);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(34, 13);
            this.label17.TabIndex = 13;
            this.label17.Text = "Sleep";
            // 
            // nudPeriodBus
            // 
            this.nudPeriodBus.Enabled = false;
            this.nudPeriodBus.Location = new System.Drawing.Point(101, 42);
            this.nudPeriodBus.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.nudPeriodBus.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudPeriodBus.Name = "nudPeriodBus";
            this.nudPeriodBus.Size = new System.Drawing.Size(53, 20);
            this.nudPeriodBus.TabIndex = 3;
            this.nudPeriodBus.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.nudPeriodBus.ValueChanged += new System.EventHandler(this.nudPeriodBus_ValueChanged);
            // 
            // nudSpeedBus
            // 
            this.nudSpeedBus.Location = new System.Drawing.Point(101, 16);
            this.nudSpeedBus.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nudSpeedBus.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudSpeedBus.Name = "nudSpeedBus";
            this.nudSpeedBus.Size = new System.Drawing.Size(53, 20);
            this.nudSpeedBus.TabIndex = 2;
            this.nudSpeedBus.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudSpeedBus.ValueChanged += new System.EventHandler(this.nudSpeedBus_ValueChanged);
            // 
            // rbPeriodBus
            // 
            this.rbPeriodBus.AutoSize = true;
            this.rbPeriodBus.Location = new System.Drawing.Point(14, 42);
            this.rbPeriodBus.Name = "rbPeriodBus";
            this.rbPeriodBus.Size = new System.Drawing.Size(89, 17);
            this.rbPeriodBus.TabIndex = 1;
            this.rbPeriodBus.Text = "period (hours)";
            this.rbPeriodBus.UseVisualStyleBackColor = true;
            // 
            // rbSpeedBus
            // 
            this.rbSpeedBus.AutoSize = true;
            this.rbSpeedBus.Checked = true;
            this.rbSpeedBus.Location = new System.Drawing.Point(14, 19);
            this.rbSpeedBus.Name = "rbSpeedBus";
            this.rbSpeedBus.Size = new System.Drawing.Size(74, 17);
            this.rbSpeedBus.TabIndex = 0;
            this.rbSpeedBus.TabStop = true;
            this.rbSpeedBus.Text = "speed *  X";
            this.rbSpeedBus.UseVisualStyleBackColor = true;
            // 
            // btnLoadConfigBus
            // 
            this.btnLoadConfigBus.Location = new System.Drawing.Point(637, 47);
            this.btnLoadConfigBus.Name = "btnLoadConfigBus";
            this.btnLoadConfigBus.Size = new System.Drawing.Size(75, 23);
            this.btnLoadConfigBus.TabIndex = 11;
            this.btnLoadConfigBus.Text = "&Load config";
            this.btnLoadConfigBus.UseVisualStyleBackColor = true;
            this.btnLoadConfigBus.Click += new System.EventHandler(this.btnLoadConfigBus_Click);
            // 
            // lblTotalRecordOfSeletedDateBus
            // 
            this.lblTotalRecordOfSeletedDateBus.AutoSize = true;
            this.lblTotalRecordOfSeletedDateBus.Location = new System.Drawing.Point(183, 180);
            this.lblTotalRecordOfSeletedDateBus.Name = "lblTotalRecordOfSeletedDateBus";
            this.lblTotalRecordOfSeletedDateBus.Size = new System.Drawing.Size(13, 13);
            this.lblTotalRecordOfSeletedDateBus.TabIndex = 11;
            this.lblTotalRecordOfSeletedDateBus.Text = "0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(165, 180);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(12, 13);
            this.label19.TabIndex = 10;
            this.label19.Text = "/";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.btnResumeBus);
            this.groupBox12.Controls.Add(this.btnStopBus);
            this.groupBox12.Controls.Add(this.btnPauseBus);
            this.groupBox12.Controls.Add(this.btnStarBus);
            this.groupBox12.Location = new System.Drawing.Point(523, 20);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(108, 141);
            this.groupBox12.TabIndex = 9;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Player";
            // 
            // btnResumeBus
            // 
            this.btnResumeBus.Enabled = false;
            this.btnResumeBus.Location = new System.Drawing.Point(18, 85);
            this.btnResumeBus.Name = "btnResumeBus";
            this.btnResumeBus.Size = new System.Drawing.Size(75, 23);
            this.btnResumeBus.TabIndex = 11;
            this.btnResumeBus.Text = "&Resume";
            this.btnResumeBus.UseVisualStyleBackColor = true;
            this.btnResumeBus.Click += new System.EventHandler(this.btnResumeBus_Click);
            // 
            // btnStopBus
            // 
            this.btnStopBus.Enabled = false;
            this.btnStopBus.Location = new System.Drawing.Point(18, 114);
            this.btnStopBus.Name = "btnStopBus";
            this.btnStopBus.Size = new System.Drawing.Size(75, 23);
            this.btnStopBus.TabIndex = 10;
            this.btnStopBus.Text = "S&top";
            this.btnStopBus.UseVisualStyleBackColor = true;
            this.btnStopBus.Click += new System.EventHandler(this.btnStopBus_Click);
            // 
            // btnPauseBus
            // 
            this.btnPauseBus.Enabled = false;
            this.btnPauseBus.Location = new System.Drawing.Point(18, 56);
            this.btnPauseBus.Name = "btnPauseBus";
            this.btnPauseBus.Size = new System.Drawing.Size(75, 23);
            this.btnPauseBus.TabIndex = 9;
            this.btnPauseBus.Text = "&Pause";
            this.btnPauseBus.UseVisualStyleBackColor = true;
            this.btnPauseBus.Click += new System.EventHandler(this.btnPauseBus_Click);
            // 
            // btnStarBus
            // 
            this.btnStarBus.Location = new System.Drawing.Point(18, 27);
            this.btnStarBus.Name = "btnStarBus";
            this.btnStarBus.Size = new System.Drawing.Size(75, 23);
            this.btnStarBus.TabIndex = 8;
            this.btnStarBus.Text = "&Start";
            this.btnStarBus.UseVisualStyleBackColor = true;
            this.btnStarBus.Click += new System.EventHandler(this.btnStarBus_Click);
            // 
            // lbltotalRecordInsertedBus
            // 
            this.lbltotalRecordInsertedBus.AutoSize = true;
            this.lbltotalRecordInsertedBus.Location = new System.Drawing.Point(122, 180);
            this.lbltotalRecordInsertedBus.Name = "lbltotalRecordInsertedBus";
            this.lbltotalRecordInsertedBus.Size = new System.Drawing.Size(13, 13);
            this.lbltotalRecordInsertedBus.TabIndex = 7;
            this.lbltotalRecordInsertedBus.Text = "0";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(13, 180);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(107, 13);
            this.label21.TabIndex = 6;
            this.label21.Text = "Total record inserted:";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.lblIDRemainBus);
            this.groupBox13.Controls.Add(this.label23);
            this.groupBox13.Controls.Add(this.lblLastIDBus);
            this.groupBox13.Controls.Add(this.label25);
            this.groupBox13.Controls.Add(this.txtIDSourceBus);
            this.groupBox13.Controls.Add(this.label26);
            this.groupBox13.Controls.Add(this.txtNextIDBus);
            this.groupBox13.Controls.Add(this.label27);
            this.groupBox13.Location = new System.Drawing.Point(6, 100);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(259, 73);
            this.groupBox13.TabIndex = 5;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Next record to insert";
            // 
            // lblIDRemainBus
            // 
            this.lblIDRemainBus.AutoSize = true;
            this.lblIDRemainBus.Location = new System.Drawing.Point(215, 22);
            this.lblIDRemainBus.Name = "lblIDRemainBus";
            this.lblIDRemainBus.Size = new System.Drawing.Size(13, 13);
            this.lblIDRemainBus.TabIndex = 17;
            this.lblIDRemainBus.Text = "0";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(154, 22);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(60, 13);
            this.label23.TabIndex = 16;
            this.label23.Text = "ID Remain:";
            // 
            // lblLastIDBus
            // 
            this.lblLastIDBus.AutoSize = true;
            this.lblLastIDBus.Location = new System.Drawing.Point(202, 48);
            this.lblLastIDBus.Name = "lblLastIDBus";
            this.lblLastIDBus.Size = new System.Drawing.Size(13, 13);
            this.lblLastIDBus.TabIndex = 15;
            this.lblLastIDBus.Text = "0";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(154, 48);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(44, 13);
            this.label25.TabIndex = 14;
            this.label25.Text = "Last ID:";
            // 
            // txtIDSourceBus
            // 
            this.txtIDSourceBus.Location = new System.Drawing.Point(88, 45);
            this.txtIDSourceBus.Name = "txtIDSourceBus";
            this.txtIDSourceBus.Size = new System.Drawing.Size(65, 20);
            this.txtIDSourceBus.TabIndex = 3;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(5, 48);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(56, 13);
            this.label26.TabIndex = 2;
            this.label26.Text = "ID source:";
            // 
            // txtNextIDBus
            // 
            this.txtNextIDBus.Location = new System.Drawing.Point(88, 19);
            this.txtNextIDBus.Name = "txtNextIDBus";
            this.txtNextIDBus.Size = new System.Drawing.Size(65, 20);
            this.txtNextIDBus.TabIndex = 1;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(5, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(75, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "ID destination:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(13, 47);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(73, 13);
            this.label28.TabIndex = 3;
            this.label28.Text = "System clock:";
            // 
            // chbDeleteExistRecordBus
            // 
            this.chbDeleteExistRecordBus.AutoSize = true;
            this.chbDeleteExistRecordBus.Location = new System.Drawing.Point(16, 71);
            this.chbDeleteExistRecordBus.Name = "chbDeleteExistRecordBus";
            this.chbDeleteExistRecordBus.Size = new System.Drawing.Size(249, 17);
            this.chbDeleteExistRecordBus.TabIndex = 2;
            this.chbDeleteExistRecordBus.Text = "Delete exist records in current date before filling";
            this.chbDeleteExistRecordBus.UseVisualStyleBackColor = true;
            // 
            // cbSeletedDateBus
            // 
            this.cbSeletedDateBus.FormattingEnabled = true;
            this.cbSeletedDateBus.Location = new System.Drawing.Point(96, 17);
            this.cbSeletedDateBus.Name = "cbSeletedDateBus";
            this.cbSeletedDateBus.Size = new System.Drawing.Size(121, 21);
            this.cbSeletedDateBus.TabIndex = 1;
            this.cbSeletedDateBus.SelectedIndexChanged += new System.EventHandler(this.cbSeletedDateBus_SelectedIndexChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(13, 20);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(64, 13);
            this.label29.TabIndex = 0;
            this.label29.Text = "Select date:";
            // 
            // timerSysClockBus
            // 
            this.timerSysClockBus.Tick += new System.EventHandler(this.timerSysClockBus_Tick);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 529);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "frmMain";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSleep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSpeed)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSleepBus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPeriodBus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSpeedBus)).EndInit();
            this.groupBox12.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mitClearAll;
        private System.Windows.Forms.ToolStripMenuItem mitCallIdm;
        private System.Windows.Forms.ToolStripMenuItem toolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mitOption;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtLinksToGet;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnAddFile;
        private System.Windows.Forms.TextBox txtLeechInfor;
        private System.Windows.Forms.Button btnDirect;
        private System.Windows.Forms.Label lblProccess;
        private System.Windows.Forms.ProgressBar pgbStatus;
        private System.Windows.Forms.Button btnGetLink;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chbDeleteExistRecord;
        private System.Windows.Forms.ComboBox cbSeletedDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtIDSource;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNextID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbltotalRecordInserted;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Button btnStar;
        private System.Windows.Forms.Label lblTotalRecordOfSeletedDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnLoadConfig;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.NumericUpDown nudPeriod;
        private System.Windows.Forms.NumericUpDown nudSpeed;
        private System.Windows.Forms.RadioButton rbPeriod;
        private System.Windows.Forms.RadioButton rbSpeed;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnSet;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label lblSpeedSysTime;
        private System.Windows.Forms.Label lbl10;
        private System.Windows.Forms.Label lblActualRecordTime;
        private System.Windows.Forms.Label lbl20;
        private System.Windows.Forms.Label lblCurrentSysTime;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnResume;
        private System.Windows.Forms.NumericUpDown nudSleep;
        private System.Windows.Forms.Label lblIDRemain;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblLastID;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtpSystemClock;
        private System.Windows.Forms.Timer timerSysClock;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DateTimePicker dtpSystemClockBus;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label lblSpeedSysTimeBus;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblActualRecordTimeBus;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblCurrentSysTimeBus;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.NumericUpDown nudSleepBus;
        private System.Windows.Forms.Button btnSetBus;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown nudPeriodBus;
        private System.Windows.Forms.NumericUpDown nudSpeedBus;
        private System.Windows.Forms.RadioButton rbPeriodBus;
        private System.Windows.Forms.RadioButton rbSpeedBus;
        private System.Windows.Forms.Button btnLoadConfigBus;
        private System.Windows.Forms.Label lblTotalRecordOfSeletedDateBus;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button btnResumeBus;
        private System.Windows.Forms.Button btnStopBus;
        private System.Windows.Forms.Button btnPauseBus;
        private System.Windows.Forms.Button btnStarBus;
        private System.Windows.Forms.Label lbltotalRecordInsertedBus;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label lblIDRemainBus;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lblLastIDBus;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtIDSourceBus;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtNextIDBus;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.CheckBox chbDeleteExistRecordBus;
        private System.Windows.Forms.ComboBox cbSeletedDateBus;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Timer timerSysClockBus;
    }
}

