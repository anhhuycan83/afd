using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using HPConFigXml;
using FreeSMS.MyLib;
using AFD.HPLib;

namespace FreeSMS
{
    public partial class frmUserManager : Form
    {
        private int currentRowIndexRun = -1;
        private bool gb_isNewRun = true;
        //public Xmlconfig xcfg;
        MyClsOption m_option;
        private bool m_keyEnter = false;
        private DataSet gb_dst;
        private MyLib.MyClsCollectionToDataSet<List<MyClsUser>> gb_MyClsDstFromList;
        //private byte m_MaxMsgCharLength = 157;

        public frmUserManager(MyClsOption ins_option)
        {
            //xcfg = ins_xcfg;
            m_option = ins_option;
            InitializeComponent();
        }

        private void frmUserManager_Load(object sender, EventArgs e)
        {
            //if (!GetValues())
            //    this.Close();
            ////get runs
            loadServer();
            getAllRun(true);
            //set button
            enbaleCtrlInputRun(false);
            
            //enableCtrlInputStop(false);
        }

        private void getAllRun(bool isClickFirstRow)
        {
            
            if (m_option.UserManager.LsUser.Count > 0)
            {
                //FuctionClass Func = new FuctionClass();
                //dgvData.DataSource = dst.Tables["THU_SK"];
                //if (!dst.Tables["RUN"].Columns.Contains("STT"))
                //{
                //    dst.Tables["RUN"].Columns.Add("STT");
                //}

                ////Func.AutoNumber(dst, "TBL_TEACHER", "STT");
                //Func.AutoNumber(dst, "RUN", "STT");
                //dgvUser.DataSource = null;
                //dgvUser.DataSource = m_option.UserManager.LsUser;
                gb_MyClsDstFromList = new FreeSMS.MyLib.MyClsCollectionToDataSet<List<MyClsUser>>(m_option.UserManager.LsUser);
                gb_dst = gb_MyClsDstFromList.CreateDataSet();

                dgvUser.DataSource = gb_dst.Tables[0];


                //gb_dst.ReadXml(Application.StartupPath + "\\" + FreeSMS.frmMain.fileConfig, XmlReadMode.Auto);


                //dgvUser.DataMember = gb_dst.Tables[0];

                dgvBuilTableSyteRun();
                dgvUser.ClearSelection();
                //dgvUser.DataMember = "RUN";

                //dgvBuilTableSyteRun();
                //click firs run
                if (isClickFirstRow)
                {
                    DataGridViewCellEventArgs e = new DataGridViewCellEventArgs(0, 0);
                    object sender = new object();
                    dgvUser_CellClick(sender, e);
                    dgvUser.ClearSelection();
                    dgvUser.Rows[0].Selected = true;
                }

            }
            else
            {
                dgvUser.DataSource = null;
                clearRun();
                stateDefaultNoDataRun();
            }
        }


        public bool loadServer()
        {
            bool result = true;
            //txtMaSo.Text = ps_maSo;
            //txtTenTV.Text = ps_tenTV;

            ArrayList alServer = new ArrayList();
            foreach (string s in Enum.GetNames(typeof(MyClsFile.MyUrlType)))
            {
                alServer.Add(new MyClsServer(s, s));
            }

            
            
            cbAccType.DisplayMember = "Ten";
            cbAccType.ValueMember = "KyHieu";
            cbAccType.DataSource = alServer;


            return result;
        }

        //public bool loadServer()
        //{
        //    bool result = true;
        //    //txtMaSo.Text = ps_maSo;
        //    //txtTenTV.Text = ps_tenTV;

        //    ArrayList alServer = new ArrayList();
        //    alServer.Add(new MyClsServer("Mobifone", "Mobifone"));
        //    alServer.Add(new MyClsServer("Viettel", "Viettel"));
        //    alServer.Add(new MyClsServer("Vinaphone", "Vinaphone"));

        //    cbAccType.DisplayMember = "Ten";
        //    cbAccType.ValueMember = "KyHieu";
        //    cbAccType.DataSource = alServer;


        //    return result;
        //}

        private void btnRunNew_Click(object sender, EventArgs e)
        {
            gb_isNewRun = true;
            stateCreateClickRun();
        }

        private void stateCreateClickRun()
        {
            clearRun();
            btnRunNew.Enabled = false;
            btnRunEdit.Enabled = false;
            btnRunSave.Enabled = true;
            btnRunCancel.Enabled = true;
            btnRunDelete.Enabled = false;

            //enableCtrlInputStop(false);

            //create auto id

            enbaleCtrlInputRun(true);

            txtUserName.Focus();
        }

        private void clearRun()
        {
            //currentRowIndexRun = -1;
            txtUserName.Clear();
            txtPassword.Clear();
            //cbServer.Clear();
            txtNote.Clear();
        }

        private void enbaleCtrlInputRun(bool isEnable)
        {
            txtPassword.Enabled = isEnable;
            txtUserName.Enabled = isEnable;
            cbAccType.Enabled = isEnable;
            txtNote.Enabled = isEnable;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRunCancel_Click(object sender, EventArgs e)
        {
            //currentRowIndexStop = -1;
            enbaleCtrlInputRun(false);
            if (currentRowIndexRun != -1)
            {
                runCellClick(currentRowIndexRun);
            }
            else
            {
                clearRun();
                stateDefaultNoDataRun();
                dgvUser.DataSource = null;
            }
            //dgvUser
        }

        private void stateDefaultNoDataRun()
        {
            btnRunNew.Enabled = true;
            btnRunEdit.Enabled = false;
            btnRunSave.Enabled = false;
            btnRunCancel.Enabled = false;
            btnRunDelete.Enabled = false;
            enbaleCtrlInputRun(false);
        }

       private void btnRunEdit_Click(object sender, EventArgs e)
        {
            gb_isNewRun = false;
            //clearRun();
            //clearStop();
            //dgvStop.DataSource = null;

            btnRunNew.Enabled = false;
            btnRunEdit.Enabled = false;
            btnRunSave.Enabled = true;
            btnRunCancel.Enabled = true;
            btnRunDelete.Enabled = false;

            enbaleCtrlInputRun(true);

            //create auto id
            //FuctionClass fc = new FuctionClass();
            //txtMyRunId.Text = fc.getNextId("RUN", "MY_RUN_ID", strConnect);
            //enbaleCtrlInputRun(true);

            txtUserName.Focus();
        }

        private void btnRunSave_Click(object sender, EventArgs e)
        {
            //controls runs and stop
            enbaleCtrlInputRun(false);
            //save and refresh
            txtUserName.Text = txtUserName.Text.Trim();
            txtPassword.Text = txtPassword.Text.Trim();
            //cbServer.Text = cbServer.Text.Trim();
            txtNote.Text = txtNote.Text.Trim();
            //save
            #region Check Value
            //ErrorEntity err = new ErrorEntity();
            //if (txtMaSo.Text == "" || txtTenTV.Text == "")
            //{
            //    err.alert_string("Tên và mã số phải tồn tại");
            //    return;
            //}
            #endregion

            if (gb_isNewRun)
            {
                if (true)
                {
                    //MyServerLeechType serType = Convert.ToString(cbAccType.SelectedValue));
                    m_option.UserManager.addUser(txtUserName.Text, txtPassword.Text, Convert.ToString(cbAccType.SelectedValue), txtNote.Text);
                    //MyLib.MyCm_option.UserManager.LsUser.addUser(m_option.UserManager.LsUser, xcfg, txtUserName.Text, txtPassword.Text, Convert.ToString(cbAccType.SelectedValue)
                    //    , txtNote.Text);

                    //MyLib.MyCm_option.UserManager.LsUser cm_option.UserManager.LsUser = new FreeSMS.MyLib.MyCm_option.UserManager.LsUser(txtUserName.Text, txtPhoneNumber.Text, txtAddress.Text);



                    //refresh
                    getAllRun(false);
                    //select this run in grid

                    CurrencyManager cm = (CurrencyManager)this.BindingContext[dgvUser.DataSource, dgvUser.DataMember];
                    DataView dview = (DataView)cm.List;

                    //string runID = dview[i]["MY_RUN_ID"].ToString();
                    bool isFouded = false;
                    for (int i = 0; i < dview.Count; i++)
                    {

                        if (dview[i]["UserName"].ToString() == txtUserName.Text)
                        {
                            //this is current index
                            currentRowIndexRun = i;
                            isFouded = true;
                            break;
                        }
                    }

                    if (isFouded)
                    {
                        runCellClick(currentRowIndexRun);
                        dgvUser.Rows[currentRowIndexRun].Selected = true;
                    }
                }
                else
                {
                    MessageBox.Show("Thêm mới thất bại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    getAllRun(true);
                }
            }
            else//edit
            {
                if (true)
                {
                    CurrencyManager cm = (CurrencyManager)this.BindingContext[dgvUser.DataSource, dgvUser.DataMember];
                    DataView dview = (DataView)cm.List;
                    
                    m_option.UserManager.updateUser(dview[currentRowIndexRun]["UserName"].ToString()
                        , dview[currentRowIndexRun]["Password"].ToString()
                        , dview[currentRowIndexRun]["AccType"].ToString()
                        , dview[currentRowIndexRun]["Note"].ToString()
                        , txtUserName.Text, txtPassword.Text, Convert.ToString(cbAccType.SelectedValue), txtNote.Text);
                    //MyLib.MyCm_option.UserManager.LsUser.updateUser(m_option.UserManager.LsUser, xcfg, dview[currentRowIndexRun]["UserName"].ToString()
                    //    , dview[currentRowIndexRun]["Password"].ToString()
                    //    , dview[currentRowIndexRun]["AccType"].ToString()
                    //    , dview[currentRowIndexRun]["Note"].ToString()
                    //    , txtUserName.Text, txtPassword.Text, Convert.ToString(cbAccType.SelectedValue), txtNote.Text);
                    //refresh
                    getAllRun(false);
                    //select this row of run
                    runCellClick(currentRowIndexRun);
                    dgvUser.Rows[currentRowIndexRun].Selected = true;
                }
                else
                {
                    MessageBox.Show("Thêm mới thất bại", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    getAllRun(true);
                }
            }


        }

        private void btnRunDelete_Click(object sender, EventArgs e)
        {
            if (true)
            {
                m_option.UserManager.deleteUser(txtUserName.Text, txtPassword.Text, Convert.ToString(cbAccType.SelectedValue), txtNote.Text);
                //delete run
                //MyLib.MyCm_option.UserManager.LsUser.deleteUser(m_option.UserManager.LsUser, xcfg, txtUserName.Text, txtPassword.Text, Convert.ToString(cbAccType.SelectedValue), txtNote.Text);
                //refresh
                //getAllRun(false);
                gb_dst.Tables[0].Rows.RemoveAt(currentRowIndexRun);
                gb_dst.Tables[0].AcceptChanges();
                //da.close();
                //dgvUser.Rows.RemoveAt(currentRowIndexRun);
                if (dgvUser.Rows.Count > 0)
                {
                    if (currentRowIndexRun == dgvUser.Rows.Count)
                    {
                        currentRowIndexRun--;
                        runCellClick(currentRowIndexRun);
                        dgvUser.Rows[currentRowIndexRun].Selected = true;
                    }
                    else
                    {
                        runCellClick(currentRowIndexRun);
                        dgvUser.Rows[currentRowIndexRun].Selected = true;
                    }
                    dgvUser.Rows[currentRowIndexRun].Selected = true;
                }
                else
                {
                    currentRowIndexRun = -1;
                    dgvUser.DataSource = null;
                    stateDefaultNoDataRun();
                }

            }
            else
            {
                MessageBox.Show("Unable to delete this run!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //getStopByRunID(txtMyRunId.Text, true);
            }
        }


        private void runCellClick(int rowIndex)
        {
            CurrencyManager cm = (CurrencyManager)this.BindingContext[dgvUser.DataSource, dgvUser.DataMember];
            DataView dview = (DataView)cm.List;

            txtUserName.Text = dview[rowIndex]["UserName"].ToString();
            txtPassword.Text = dview[rowIndex]["Password"].ToString();
            //cbServer.Text = dgvUser.Rows[rowIndex].Cells["AccType"].Value.ToString();
            if (cbAccType.FindString(dview[rowIndex]["AccType"].ToString()) != -1)
                cbAccType.SelectedItem = cbAccType.Items[cbAccType.FindString(dview[rowIndex]["AccType"].ToString())];
            txtNote.Text = dview[rowIndex]["Note"].ToString();

            //set state of run ctrls
            stateDefaultHaveDataRun();


        }

        private void stateDefaultHaveDataRun()
        {
            btnRunNew.Enabled = true;
            btnRunEdit.Enabled = true;
            btnRunSave.Enabled = false;
            btnRunCancel.Enabled = false;
            btnRunDelete.Enabled = true;
        }

        private void dgvBuilTableSyteRun()
        {

            dgvUser.Columns.Clear();
            //dgvLichDay.ReadOnly = true;
            dgvUser.AllowUserToAddRows = false;

            #region ***Xây dựng TableStyles
            //TxtEditCol_STT
            TxtEditCol_STT_RUN = new DataGridViewTextBoxColumn();
            TxtEditCol_STT_RUN.DataPropertyName = "STT";
            TxtEditCol_STT_RUN.HeaderText = "STT";
            TxtEditCol_STT_RUN.Width = 35;
            TxtEditCol_STT_RUN.ReadOnly = true;
            //TxtEditCol_STT_RUN.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvUser.Columns.Add(TxtEditCol_STT_RUN);

            //TxtEditCol_STT
            TxtEditCol_name = new DataGridViewTextBoxColumn();
            TxtEditCol_name.DataPropertyName = "UserName";
            TxtEditCol_name.HeaderText = "UserName";
            TxtEditCol_name.Name = "UserName";
            TxtEditCol_name.Width = 90;
            TxtEditCol_name.ReadOnly = true;
            //TxtEditCol_name.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvUser.Columns.Add(TxtEditCol_name);


            ////TxtEditCol_NGAY
            //TxtEditCol_phoneNumber = new DataGridViewTextBoxColumn();
            //TxtEditCol_phoneNumber.DataPropertyName = "Password";
            //TxtEditCol_phoneNumber.Name = "Password";
            //TxtEditCol_phoneNumber.HeaderText = "Password";
            ////TxtEditCol_NGAY.DefaultCellStyle.Format = "dd/MM/yyyy";
            //TxtEditCol_phoneNumber.Width = 85;
            //TxtEditCol_phoneNumber.ReadOnly = true;
            //TxtEditCol_phoneNumber.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            ////TxtEditCol_phoneNumber.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //dgvUser.Columns.Add(TxtEditCol_phoneNumber);


            
            ////TxtEditCol_STT
            TxtEditCol_message = new DataGridViewTextBoxColumn();
            TxtEditCol_message.DataPropertyName = "AccType";
            TxtEditCol_message.Name = "AccType";
            TxtEditCol_message.HeaderText = "AccType";
            TxtEditCol_message.Width = 90;
            TxtEditCol_message.ReadOnly = true;
            //TxtEditCol_message.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvUser.Columns.Add(TxtEditCol_message);

            //TxtEditCol_NGAY
            TxtEditCol_address = new DataGridViewTextBoxColumn();
            TxtEditCol_address.DataPropertyName = "Note";
            TxtEditCol_address.Name = "Note";
            TxtEditCol_address.HeaderText = "Note";
            //TxtEditCol_address.DefaultCellStyle.Format = "hh:mm tt";
            TxtEditCol_address.Width = 250;
            TxtEditCol_address.ReadOnly = true;
            TxtEditCol_address.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            //TxtEditCol_address.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvUser.Columns.Add(TxtEditCol_address);


            ////TxtEditCol_STT
            //TxtEditCol_RUN_MAX_LOAD = new DataGridViewTextBoxColumn();
            //TxtEditCol_RUN_MAX_LOAD.DataPropertyName = "RUN_MAX_LOAD";
            //TxtEditCol_RUN_MAX_LOAD.HeaderText = "Max load";
            //TxtEditCol_RUN_MAX_LOAD.Width = 50;
            //TxtEditCol_RUN_MAX_LOAD.ReadOnly = true;
            //TxtEditCol_RUN_MAX_LOAD.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //dgvUser.Columns.Add(TxtEditCol_RUN_MAX_LOAD);

            ////TxtEditCol_STT
            //TxtEditCol_RUN_MAX_TIME = new DataGridViewTextBoxColumn();
            //TxtEditCol_RUN_MAX_TIME.DataPropertyName = "RUN_MAX_TIME";
            //TxtEditCol_RUN_MAX_TIME.HeaderText = "Max time";
            //TxtEditCol_RUN_MAX_TIME.Width = 40;
            //TxtEditCol_RUN_MAX_TIME.ReadOnly = true;
            //TxtEditCol_RUN_MAX_TIME.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //dgvUser.Columns.Add(TxtEditCol_RUN_MAX_TIME);

            ////TxtEditCol_STT
            //TxtEditCol_RUN_FREQ = new DataGridViewTextBoxColumn();
            //TxtEditCol_RUN_FREQ.DataPropertyName = "RUN_FREQ";
            //TxtEditCol_RUN_FREQ.HeaderText = "freq";
            //TxtEditCol_RUN_FREQ.Width = 50;
            //TxtEditCol_RUN_FREQ.ReadOnly = true;
            //TxtEditCol_RUN_FREQ.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //dgvUser.Columns.Add(TxtEditCol_RUN_FREQ);


            ////TxtEditCol_NGAY
            //TxtEditCol_RUN_START_DATE = new DataGridViewTextBoxColumn();
            //TxtEditCol_RUN_START_DATE.DataPropertyName = "RUN_START_DATE";
            //TxtEditCol_RUN_START_DATE.HeaderText = "Start Date";
            //TxtEditCol_RUN_START_DATE.DefaultCellStyle.Format = "hh:mm tt";
            //TxtEditCol_RUN_START_DATE.Width = 60;
            //TxtEditCol_RUN_START_DATE.ReadOnly = true;
            //TxtEditCol_RUN_START_DATE.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            //TxtEditCol_RUN_START_DATE.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //dgvUser.Columns.Add(TxtEditCol_RUN_START_DATE);

            ////TxtEditCol_NGAY
            //TxtEditCol_RUN_END_DATE = new DataGridViewTextBoxColumn();
            //TxtEditCol_RUN_END_DATE.DataPropertyName = "RUN_END_DATE";
            //TxtEditCol_RUN_END_DATE.HeaderText = "Start Date";
            //TxtEditCol_RUN_END_DATE.DefaultCellStyle.Format = "hh:mm tt";
            //TxtEditCol_RUN_END_DATE.Width = 60;
            //TxtEditCol_RUN_END_DATE.ReadOnly = true;
            //TxtEditCol_RUN_END_DATE.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            //TxtEditCol_RUN_END_DATE.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //dgvUser.Columns.Add(TxtEditCol_RUN_END_DATE);
            /*
            //DtaGridViewCol_ChBoxCol
            ChBoxDtaGridCol = new DataGridViewCheckBoxColumn();
            ChBoxDtaGridCol.DataPropertyName = "TINH_TRANG";
            ChBoxDtaGridCol.Width = 70;
            //ChBoxDtaGridCol.ReadOnly = true;
            ChBoxDtaGridCol.HeaderText = "Tình Trạng";
            dgvLichDay.Columns.Add(ChBoxDtaGridCol);
            */
            #endregion

        }

        private void dgvUser_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvUser.DataSource != null && e.RowIndex >= 0)
                {
                    //int i = dgvData.CurrentCell.RowIndex;
                    int i = e.RowIndex;

                    if (i != currentRowIndexRun)
                    {
                        currentRowIndexRun = i;
                        runCellClick(currentRowIndexRun);
                    }
                }
                else
                {
                    //POPUP_NEW_THU Popup = new POPUP_NEW_THU();
                    //Popup.ShowDialog();
                }
                //refressDataGrid_dgr();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //private void txtMessage_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    if (m_keyEnter)
        //    {
        //        e.Handled = true;
        //    }

        //}

        //private void txtMessage_KeyDown(object sender, KeyEventArgs e)
        //{
        //    m_keyEnter = false;
        //    if (e.KeyCode == Keys.Enter)
        //        m_keyEnter = true;
        //}

        //private void txtMessage_TextChanged(object sender, EventArgs e)
        //{
        //    txtMessage.Text = txtMessage.Text.Replace(Environment.NewLine, " ");
        //    txtCharacterRemain.Text = Convert.ToString(m_MaxMsgCharLength - Convert.ToByte(txtMessage.Text.Length));
        //}
   }
}