using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace CheckDB.MyLib
{
    public class HLib_ClsColumns
    {
        protected List<string> columnNames;
        public string strError;
        public HLib_ClsColumns()
        {
            columnNames = new List<string>();
        }
        public void addColumn(string colName)
        {
            columnNames.Add(colName);
        }
        public bool addColumn(string colName, int index)
        {
            bool result = true;
            try
            {
                columnNames.Insert(index, colName);
            }
            catch(Exception ex)
            {
                strError = ex.Message;
                result = false;
            }
            return result;
        }
        public bool removeColumn(string colName)
        {
            return columnNames.Remove(colName);
        }
        
        public bool removeColumn(int index)
        {
            bool result = true;
            try
            {
                columnNames.RemoveAt(index);
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                result = false;
            }
            return result;
        }
        public void removeAll()
        {
            columnNames.Clear();
        }
        public int getNumberColumn()
        {
            return columnNames.Count;
        }
        public string getColumnName(int index)
        {
            return columnNames[index];
        }
        public string[] getColumnsNameArray()
        {
            return columnNames.ToArray();
        }

        //static string[] convertToColsArray(List<HLib_ClsColumns> lsClsCols)
        //{
        //    string[] arrayResult = new string[lsClsCols.Count];
        //    int i = 0;
        //    foreach (HLib_ClsTables colItem in lsClsCols)
        //    {
        //        arrayResult[i] = colItem.getColumnsName();
        //        i++;
        //    }
        //}

        //public static HLib_ClsColumns getTableItem(List<HLib_ClsColumns> lsClsColumns, string strColumnName)
        //{
        //    HLib_ClsColumns colResult;
        //    foreach (HLib_ClsColumns colItem in lsClsColumns)
        //    {
        //        if (colItem.getColumnName().Contains(strColumnName))
        //        {
        //            colResult = colItem;
        //            break;
        //        }
        //    }
        //    return colResult;
        //}

        public static DataTable convertToTableColumn(DataTable tbInput)
        {
            //string[] arrayResult = new string[lsClsTables.Count];
            DataTable tbResult = new DataTable();
            DataColumn colColumnName = new DataColumn("COLUMN_NAME", typeof(string));
            DataRow row;
            tbResult.Columns.Add(colColumnName);
            for(int i =0; i<tbInput.Columns.Count; i++)
            {
                row = tbResult.NewRow();
                row[0] = tbInput.Columns[i].Caption;
                tbResult.Rows.Add(row);
            }
            return tbResult;
        }
    }
}

