using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace CheckDB.MyLib
{
    public class HLib_ClsTables : HLib_ClsColumns
    {
        private string strTbName;
        private string strConnection;
        public HLib_ClsTables(string tbName)
        {
            strTbName = tbName;
        }
        public void setTableName(string tbName)
        {
            strTbName = tbName;
        }
        public string getTableName()
        {
            return strTbName;
        }
        public static string[] convertToTablesArray(List<HLib_ClsTables> lsClsTables)
        {
            string[] arrayResult = new string[lsClsTables.Count];
            int i = 0;
            foreach (HLib_ClsTables tbItem in lsClsTables)
            {                
                arrayResult[i] = tbItem.getTableName();
                i++;
            }
            return arrayResult;
        }
        public static DataTable convertToTablesName(List<HLib_ClsTables> lsClsTables)
        {
            //string[] arrayResult = new string[lsClsTables.Count];
            DataTable tbResult = new DataTable();
            DataColumn colTableName = new DataColumn("TABLE_NAME", typeof(string));
            DataRow row;
            tbResult.Columns.Add(colTableName);
            foreach (HLib_ClsTables tbItem in lsClsTables)
            {
                row = tbResult.NewRow();                
                row[0] = tbItem.getTableName();
                tbResult.Rows.Add(row);
            }
            return tbResult;
        }

        public static HLib_ClsTables getTableItem(List<HLib_ClsTables> lsClsTables, string strTableName)
        {
            HLib_ClsTables tbResult = new HLib_ClsTables("");
            foreach (HLib_ClsTables tbItem in lsClsTables)
            {
                if (tbItem.getTableName().Contains(strTableName))
                {
                    tbResult = tbItem;
                    break;
                }
            }
            return tbResult;
        }

        public static int getTableItemIndex(List<HLib_ClsTables> lsClsTables, string strTableName)
        {
            int i = 0;
            
            foreach (HLib_ClsTables tbItem in lsClsTables)
            {                
                if (tbItem.getTableName().Contains(strTableName))
                {                    
                    break;
                }
                i++;
            }

            if (i > lsClsTables.Count - 1)
                return -1;
            return i;
        }

        public string getSqlSelectionString()
        {
            if (columnNames.Count == 0)
                return "";
            string strResult = "SELECT ";
            for (int i=0;i< columnNames.Count; i++)
            {
                strResult += columnNames[i];
                if (i < columnNames.Count - 1)
                {
                    strResult += ", ";
                }
            }
            strResult += " FROM " + strTbName;
            return strResult;
        }
        public void setConnectionString(string ins_strConn)
        {
            strConnection = ins_strConn;
        }

        public string getConnectionString()
        {
            return strConnection;
        }
    }
}
