using System;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;
using Microsoft.Win32;
using System.Windows.Forms;
using System.Data.OleDb;

public static class Database
{
    //public static String ConnectionString = "Data Source=XP-TESTING2\\SQLEXPRESS;Initial Catalog=bobo.MDF;Trusted_Connection=false;User ID=etm;Password=etm;";
    public static String ConnectionString = "";

    //connectionString ="Data Source=.\SQLEXPRESS;AttachDbFilename=|DataDirectory|\eStore.mdf;Integrated Security=True;User Instance=True" providerName="System.Data.SqlClient
    //Trusted_Connection=false;User ID=etm;Password=etm;
    //Data Source=.\SQLEXPRESS;AttachDbFilename=E:\Huy\projects\cus\dung\thamkhao\HVeStore\App_Data\eStore.mdf;Integrated Security=True;User Instance=True
    //public static String ConnectionString = @"Data Source=XP-TESTING2\SQLEXPRESS;Initial Catalog=ESTORE;Trusted_Connection=false;User ID=etm;Password=etm;";
    //public static String ConnectionString = @"Data Source=.\SQLEXPRESS;AttachDbFilename=D:\Users\My Documents\Visual Studio 2005\WebSites\HVeStore\App_Data\bobo.mdf;Integrated Security=True;User Instance=True";

    public static SqlConnection GetConnection()
    {
        return new SqlConnection(ConnectionString);
    }

    public static DataTable Fill(DataTable table, String sql, params Object[] parameters)
    {
        SqlCommand command = Database.CreateCommand(sql, parameters);

        new SqlDataAdapter(command).Fill(table);
        command.Connection.Close();

        return table;
    }

    public static DataTable Fill(DataTable table, String sql, CommandType ct, params Object[] parameters)
    {
        SqlCommand command = Database.CreateCommand(sql, parameters);
        command.CommandType = ct;

        new SqlDataAdapter(command).Fill(table);
        command.Connection.Close();

        return table;
    }

    public static DataTable GetData(String sql, params Object[] parameters)
    {
        return Database.Fill(new DataTable(), sql, parameters);
    }

    public static DataTable GetData(String sql, CommandType ct, params Object[] parameters)
    {
        return Database.Fill(new DataTable(), sql, ct, parameters);
    }

    public static int ExecuteNonQuery(String sql, params Object[] parameters)
    {
        SqlCommand command = Database.CreateCommand(sql, parameters);

        command.Connection.Open();
        int rows = command.ExecuteNonQuery();
        command.Connection.Close();

        return rows;
    }

    public static int ExecuteNonQuery(String sql, CommandType ct, params Object[] parameters)
    {
        SqlCommand command = Database.CreateCommand(sql, parameters);
        command.CommandType = ct;

        command.Connection.Open();
        int rows = command.ExecuteNonQuery();
        command.Connection.Close();

        return rows;
    }

    public static object ExecuteScalar(String sql, params Object[] parameters)
    {
        SqlCommand command = Database.CreateCommand(sql, parameters);

        command.Connection.Open();
        object value = command.ExecuteScalar();
        command.Connection.Close();

        return value;
    }

    private static SqlCommand CreateCommand(String sql, params Object[] parameters)
    {
        SqlCommand command = new SqlCommand(sql, Database.GetConnection());
        for (int i = 0; i < parameters.Length; i += 2)
        {
            command.Parameters.AddWithValue(parameters[i].ToString(), parameters[i + 1]);
        }
        return command;
    }

    public static string fixSQL(string ins_value)
    {
        string ls_return = null;
        if (ins_value != null)
        {
            ls_return = ins_value.Trim();
            ls_return = ls_return.Replace("'", "");
        }

        return ls_return;
    }
}
