using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Globalization;

namespace AFD.MyLib
{
    class MyClsEntity
    {
    }

    #region BusPosition Entity
    public class BusPosition
    {
        private string strConn;
        public BusPosition(string ins_strConn)
        {
            strConn = ins_strConn;
        }
        public DataTable getBusPosition(string TableName)
        {
            StringBuilder sqlString = new StringBuilder("SELECT ID, DistrictID, UnitID, Latitude, Longitude, WriteTimeStamp, PositionTimeStamp, EventID, Speed, RSSI, RouteMiles, History");
            sqlString.AppendLine("   FROM BusPosition");
            DataTable tbRs = Database.GetData(sqlString.ToString());
            return tbRs;
        }

        public DataTable getBusPositionByID(string ID)
        {
            StringBuilder sqlString = new StringBuilder("SELECT ID, DistrictID, UnitID, Latitude, Longitude, WriteTimeStamp, PositionTimeStamp, EventID, Speed, RSSI, RouteMiles, History");
            sqlString.AppendLine("   FROM BusPosition");
            sqlString.AppendLine(" WHERE ");
            sqlString.AppendLine("ID = @ID");
            DataTable result = Database.GetData(sqlString.ToString(), "@ID", ID);
            return result;
        }

        public DataTable getListDate(string ID)
        {
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("select convert(char(30),message_dt_adjusted,101) as date");
            sqlString.AppendLine("from message");
            sqlString.AppendLine("group by  convert(char(30),message_dt_adjusted,101)");
            sqlString.AppendLine("order by convert(char(30),message_dt_adjusted,101)");
            DataTable result = Database.GetData(sqlString.ToString());
            return result;
        }

        public object getColValByID(string ID, string colName)
        {
            #region FixData
            ID = Database.fixSQL(ID);
            #endregion
            StringBuilder sqlString = new StringBuilder("SELECT " + colName); sqlString.AppendLine("   FROM BusPosition");
            sqlString.AppendLine(" WHERE ");
            sqlString.AppendLine("ID = @ID");
            MyClsDBManager db = new MyClsDBManager(strConn);
            object result = db.ExecuteScalar(sqlString.ToString(), "@ID", ID);
            return result;
        }

        public int InsertWithIdentity(string DistrictID, string UnitID, string Latitude, string Longitude, string WriteTimeStamp, string PositionTimeStamp, string EventID, string Speed, string RSSI, string RouteMiles, string History)
        {
            StringBuilder sqlString = new StringBuilder("SET DATEFORMAT DMY INSERT INTO BusPosition(ID, DistrictID, UnitID, Latitude, Longitude, WriteTimeStamp, PositionTimeStamp, EventID, Speed, RSSI, RouteMiles, History)  VALUES(");
            //sqlString.AppendLine("@ID, ");
            sqlString.AppendLine("@DistrictID, ");
            sqlString.AppendLine("@UnitID, ");
            sqlString.AppendLine("@Latitude, ");
            sqlString.AppendLine("@Longitude, ");
            sqlString.AppendLine("@WriteTimeStamp, ");
            sqlString.AppendLine("@PositionTimeStamp, ");
            sqlString.AppendLine("@EventID, ");
            sqlString.AppendLine("@Speed, ");
            sqlString.AppendLine("@RSSI, ");
            sqlString.AppendLine("@RouteMiles, ");
            sqlString.AppendLine("@History)");
            MyClsDBManager db = new MyClsDBManager(strConn);
            int result = db.ExecuteNonQuery(sqlString.ToString(), "@DistrictID", DistrictID, "@UnitID", UnitID, "@Latitude", Latitude, "@Longitude", Longitude, "@WriteTimeStamp", WriteTimeStamp, "@PositionTimeStamp", PositionTimeStamp, "@EventID", EventID, "@Speed", Speed, "@RSSI", RSSI, "@RouteMiles", RouteMiles, "@History", History);
            return result;
        }

        public int InsertWithoutIdentity(string ID, string DistrictID, string UnitID, string Latitude, string Longitude, string WriteTimeStamp, string PositionTimeStamp, string EventID, string Speed, string RSSI, string RouteMiles, string History)
        {
            StringBuilder sqlString = new StringBuilder("SET DATEFORMAT DMY INSERT INTO BusPosition(ID, DistrictID, UnitID, Latitude, Longitude, WriteTimeStamp, PositionTimeStamp, EventID, Speed, RSSI, RouteMiles, History)  VALUES(");
            sqlString.AppendLine("@ID, ");
            sqlString.AppendLine("@DistrictID, ");
            sqlString.AppendLine("@UnitID, ");
            sqlString.AppendLine("@Latitude, ");
            sqlString.AppendLine("@Longitude, ");
            sqlString.AppendLine("@WriteTimeStamp, ");
            sqlString.AppendLine("@PositionTimeStamp, ");
            sqlString.AppendLine("@EventID, ");
            sqlString.AppendLine("@Speed, ");
            sqlString.AppendLine("@RSSI, ");
            sqlString.AppendLine("@RouteMiles, ");
            sqlString.AppendLine("@History)");
            MyClsDBManager db = new MyClsDBManager(strConn);
            int result = db.ExecuteNonQuery(sqlString.ToString(), "@ID", ID, "@DistrictID", DistrictID, "@UnitID", UnitID, "@Latitude", Latitude, "@Longitude", Longitude, "@WriteTimeStamp", WriteTimeStamp, "@PositionTimeStamp", PositionTimeStamp, "@EventID", EventID, "@Speed", Speed, "@RSSI", RSSI, "@RouteMiles", RouteMiles, "@History", History);
            return result;
        }

        public string getSqlQueryInsert(string ID, string DistrictID, string UnitID, string Latitude, string Longitude, string WriteTimeStamp, string PositionTimeStamp, string EventID, string Speed, string RSSI, string RouteMiles, string History)
        {
            StringBuilder sqlString = new StringBuilder("SET DATEFORMAT DMY INSERT INTO BusPosition(ID, DistrictID, UnitID, Latitude, Longitude, WriteTimeStamp, PositionTimeStamp, EventID, Speed, RSSI, RouteMiles, History)  VALUES(");
            sqlString.AppendLine("@ID, ");
            sqlString.AppendLine("@DistrictID, ");
            sqlString.AppendLine("@UnitID, ");
            sqlString.AppendLine("@Latitude, ");
            sqlString.AppendLine("@Longitude, ");
            sqlString.AppendLine("@WriteTimeStamp, ");
            sqlString.AppendLine("@PositionTimeStamp, ");
            sqlString.AppendLine("@EventID, ");
            sqlString.AppendLine("@Speed, ");
            sqlString.AppendLine("@RSSI, ");
            sqlString.AppendLine("@RouteMiles, ");
            sqlString.AppendLine("@History)");

            return sqlString.ToString();
        }

        public int Update(string ID, string DistrictID, string UnitID, string Latitude, string Longitude, string WriteTimeStamp, string PositionTimeStamp, string EventID, string Speed, string RSSI, string RouteMiles, string History)
        {
            StringBuilder sqlString = new StringBuilder("SET DATEFORMAT DMY UPDATE BusPosition SET ");
            sqlString.AppendLine(" DistrictID = @DistrictID,");
            sqlString.AppendLine(" UnitID = @UnitID,");
            sqlString.AppendLine(" Latitude = @Latitude,");
            sqlString.AppendLine(" Longitude = @Longitude,");
            sqlString.AppendLine(" WriteTimeStamp = @WriteTimeStamp,");
            sqlString.AppendLine(" PositionTimeStamp = @PositionTimeStamp,");
            sqlString.AppendLine(" EventID = @EventID,");
            sqlString.AppendLine(" Speed = @Speed,");
            sqlString.AppendLine(" RSSI = @RSSI,");
            sqlString.AppendLine(" RouteMiles = @RouteMiles,");
            sqlString.AppendLine(" History = @History");
            sqlString.AppendLine(" WHERE ");
            sqlString.AppendLine(" ID = @ID");
            MyClsDBManager db = new MyClsDBManager(strConn);
            int result = db.ExecuteNonQuery(sqlString.ToString(), "@ID", ID, "@DistrictID", DistrictID, "@UnitID", UnitID, "@Latitude", Latitude, "@Longitude", Longitude, "@WriteTimeStamp", WriteTimeStamp, "@PositionTimeStamp", PositionTimeStamp, "@EventID", EventID, "@Speed", Speed, "@RSSI", RSSI, "@RouteMiles", RouteMiles, "@History", History);
            return result;
        }

        public string getSqlQueryUpdate(string ID, string DistrictID, string UnitID, string Latitude, string Longitude, string WriteTimeStamp, string PositionTimeStamp, string EventID, string Speed, string RSSI, string RouteMiles, string History)
        {
            StringBuilder sqlString = new StringBuilder("SET DATEFORMAT DMY UPDATE BusPosition SET ");
            sqlString.AppendLine(" DistrictID = @DistrictID,");
            sqlString.AppendLine(" UnitID = @UnitID,");
            sqlString.AppendLine(" Latitude = @Latitude,");
            sqlString.AppendLine(" Longitude = @Longitude,");
            sqlString.AppendLine(" WriteTimeStamp = @WriteTimeStamp,");
            sqlString.AppendLine(" PositionTimeStamp = @PositionTimeStamp,");
            sqlString.AppendLine(" EventID = @EventID,");
            sqlString.AppendLine(" Speed = @Speed,");
            sqlString.AppendLine(" RSSI = @RSSI,");
            sqlString.AppendLine(" RouteMiles = @RouteMiles,");
            sqlString.AppendLine(" History = @History");
            sqlString.AppendLine(" WHERE ");
            sqlString.AppendLine(" ID = @ID");

            return sqlString.ToString();
        }

        public int Delete(string ID)
        {
            StringBuilder sqlString = new StringBuilder("DELETE BusPosition WHERE ");
            sqlString.AppendLine("ID = @ID;");

            MyClsDBManager db = new MyClsDBManager(strConn);
            int result = db.ExecuteNonQuery(sqlString.ToString(), "@ID", ID);
            return result;
        }

        public string getSqlQueryDelete(string ID)
        {
            StringBuilder sqlString = new StringBuilder("DELETE BusPosition WHERE ");
            sqlString.AppendLine("ID = @ID;");

            return sqlString.ToString();
        }

        //customize code

        /// <summary>
        /// get max id from the date input
        /// </summary>
        /// <param name="currentDate">format: MM/dd/yyyy</param>
        /// <returns>return max id or -1 if can not parse to int</returns>
        public Int64 getMaxIdFromDate(DateTime currentDate)
        {
            Int64 iRs;
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("select max(id) as idmax");
            sqlString.AppendLine("from BusPosition");
            sqlString.AppendLine("where");
            sqlString.AppendLine("convert(char(30),WriteTimeStamp,101) = @currentDate");

            MyClsDBManager db = new MyClsDBManager(strConn);
                        
            object result = db.ExecuteScalar(sqlString.ToString(), "@currentDate", currentDate.ToString("MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US")));
            if (!Int64.TryParse(result.ToString(), out iRs))
            {
                iRs = -1;
            }
            return iRs;
        }

        /// <summary>
        /// get next id of all data in busposition table
        /// </summary>
        /// <param name="currentDate"></param>
        /// <returns></returns>
        public int getNextId()
        {
            int iRs;
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("select max(id) + 1 as idmax");
            sqlString.AppendLine("from BusPosition");

            MyClsDBManager db = new MyClsDBManager(strConn);
            object result = db.ExecuteScalar(sqlString.ToString());
            if (!int.TryParse(result.ToString(), out iRs))
            {
                iRs = -1;
            }
            return iRs;
        }

        /// <summary>
        /// get min busposition id from the date input
        /// </summary>
        /// <param name="currentDate">format: MM/dd/yyyy</param>
        /// <returns>return min id or -1 if can not parse to int</returns>
        public int getMinIdFromDate(DateTime currentDate)
        {
            int iRs;
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("select min(id) as idmin");
            sqlString.AppendLine("from BusPosition");
            sqlString.AppendLine("where");
            sqlString.AppendLine("convert(char(30),WriteTimeStamp,101) = @currentDate");
            //sqlString.AppendLine("order by message_id");


            MyClsDBManager db = new MyClsDBManager(strConn);
            object result = db.ExecuteScalar(sqlString.ToString(), "@currentDate", currentDate.ToString("MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US")));
            if (!int.TryParse(result.ToString(), out iRs))
            {
                iRs = -1;
            }
            return iRs;
        }

        /// <summary>
        /// get the identity state of message table
        /// </summary>
        /// <returns>true or false</returns>
        public bool IsIdentity()
        {
            bool bRs = false;
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("declare @idTbBus int");
            sqlString.AppendLine("select @idTbBus = id");
            sqlString.AppendLine("FROM syscolumns");
            sqlString.AppendLine("where");
            sqlString.AppendLine("name = 'id'");
            sqlString.AppendLine("and OBJECT_NAME(id) = 'BusPosition'");
            sqlString.AppendLine("SELECT COLUMNPROPERTY (@idTbBus, 'id', 'IsIdentity') as 'IsIdentity'");
            sqlString.AppendLine("FROM syscolumns");
            sqlString.AppendLine("where");
            sqlString.AppendLine("name = 'id'");
            sqlString.AppendLine("and OBJECT_NAME(id) = 'BusPosition'");

            MyClsDBManager db = new MyClsDBManager(strConn);
            object ob = db.ExecuteScalar(sqlString.ToString());
            if (ob.ToString() == "0")
                bRs = false;
            else
                bRs = true;
            return bRs;
        }

        public DataTable getFristMsgOfDate(string TableName, string dateSelected)
        {
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("select top 1 *");
            sqlString.AppendLine("from BusPosition");
            sqlString.AppendLine("where");
            sqlString.AppendLine("convert(char(30),WriteTimeStamp,101) = @dateSelected");
            sqlString.AppendLine("order by id asc");

            MyClsDBManager db = new MyClsDBManager(strConn);
            DataTable tbRs = db.GetData(sqlString.ToString(), "@dateSelected", dateSelected);
            return tbRs;
        }

        public DataTable getNextRecordMsgOfDate(string TableName, string dateSelected, Int64 msgID)
        {
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("select top 1 *");
            sqlString.AppendLine("from BusPosition");
            sqlString.AppendLine("where");
            sqlString.AppendLine("convert(char(30),WriteTimeStamp,101) = @dateSelected");
            sqlString.AppendLine("and id > @msgID");
            sqlString.AppendLine("order by id asc");


            MyClsDBManager db = new MyClsDBManager(strConn);
            DataTable tbRs = db.GetData(sqlString.ToString(), "@dateSelected", dateSelected, "@msgID", msgID);
            return tbRs;
        }

        public DataTable getNextRecordMsgOfDateByDateTime(string TableName, string dateSelected, string nextDateAndTime)
        {
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("select top 1 *");
            sqlString.AppendLine("from BusPosition");
            sqlString.AppendLine("where");
            sqlString.AppendLine("convert(char(30),WriteTimeStamp,101) = @dateSelected");
            sqlString.AppendLine("and WriteTimeStamp > @nextDateAndTime");
            sqlString.AppendLine("order by id asc");


            MyClsDBManager db = new MyClsDBManager(strConn);
            DataTable tbRs = db.GetData(sqlString.ToString(), "@dateSelected", dateSelected, "@nextDateAndTime", nextDateAndTime);
            return tbRs;
        }

        public DateTime getLastTimeRecordOfCurrentDate(DateTime currentDate)
        {
            //int iRs;
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("select top 1 WriteTimeStamp");
            sqlString.AppendLine("from BusPosition");
            sqlString.AppendLine("where ");
            sqlString.AppendLine("convert(char(30),WriteTimeStamp,101) = @currentDate");
            sqlString.AppendLine("order by WriteTimeStamp desc");


            MyClsDBManager db = new MyClsDBManager(strConn);
            object ob = db.ExecuteScalar(sqlString.ToString(), "@currentDate", currentDate.ToString("MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US")));
            DateTime result = DateTime.MinValue;
            if (ob != null)
            {
                System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
                if (!DateTime.TryParse(ob.ToString(), ci, System.Globalization.DateTimeStyles.None, out result))
                {
                }
            }
            return result;
        }

        public DateTime getTotalRecordOfDateSeleted(DateTime currentDate)
        {
            //int iRs;
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("select count(id) as TotalRecords");
            sqlString.AppendLine("from BusPosition");
            sqlString.AppendLine("where ");
            sqlString.AppendLine("convert(char(30),WriteTimeStamp,101) = @currentDate");

            MyClsDBManager db = new MyClsDBManager(strConn);
            object ob = db.ExecuteScalar(sqlString.ToString(), "@currentDate", currentDate.ToString("MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US")));
            DateTime result = DateTime.MinValue;
            if (ob != null)
            {
                System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
                if (!DateTime.TryParse(ob.ToString(), ci, System.Globalization.DateTimeStyles.None, out result))
                {
                    //result = DateTime.MinValue;
                }
            }
            return result;
        }

        public DataTable getListOfDateAndTotalRecord()
        {
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("select convert(char(30),WriteTimeStamp,101) ListDate, count(*) as TotalRecord");
            sqlString.AppendLine("from BusPosition");
            sqlString.AppendLine("group by convert(char(30),WriteTimeStamp,101)");

            MyClsDBManager db = new MyClsDBManager(strConn);
            DataTable tbRs = db.GetData(sqlString.ToString());
            return tbRs;
        }

        public int DeleteByDate(DateTime currentDate)
        {
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("SET DATEFORMAT MDY delete BusPosition");
            sqlString.AppendLine("where");
            sqlString.AppendLine("convert(char(30),WriteTimeStamp,101) = @currentDate");
            
            //sqlString.AppendLine("delete message_attribute_student");
            //sqlString.AppendLine("where ");
            //sqlString.AppendLine("mas_message_id in ");
            //sqlString.AppendLine("(");
            //sqlString.AppendLine("select message_id from message");
            //sqlString.AppendLine("where ");
            //sqlString.AppendLine("convert(char(30),message_dt_adjusted,101) = @currentDate");
            //sqlString.AppendLine(")");
            sqlString.AppendLine("SET DATEFORMAT MDY  delete student");
            sqlString.AppendLine("where");
            sqlString.AppendLine("convert(char(30),WriteTimeStamp,101) = @currentDate");

            MyClsDBManager db = new MyClsDBManager(strConn);
            int result = db.ExecuteNonQuery(sqlString.ToString(), "@currentDate", currentDate.ToString("MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US")));
            return result;
        }

        public int InsertWithYesIdentity(Int64 buspositionID, DateTime dtWriteTimeStamp)
        {
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("SET DATEFORMAT MDY INSERT INTO [BusPosition]");
            sqlString.AppendLine("           ([DistrictID]");
            sqlString.AppendLine("           ,[UnitID]");
            sqlString.AppendLine("           ,[Latitude]");
            sqlString.AppendLine("           ,[Longitude]");
            sqlString.AppendLine("           ,[WriteTimeStamp]");
            sqlString.AppendLine("           ,[PositionTimeStamp]");
            sqlString.AppendLine("           ,[EventID]");
            sqlString.AppendLine("           ,[Speed]");
            sqlString.AppendLine("           ,[RSSI]");
            sqlString.AppendLine("           ,[RouteMiles])");
            sqlString.AppendLine("select ");
            sqlString.AppendLine("            [DistrictID]");
            sqlString.AppendLine("           ,[UnitID]");
            sqlString.AppendLine("           ,[Latitude]");
            sqlString.AppendLine("           ,[Longitude]");
            sqlString.AppendLine("           ,@dtWriteTimeStamp");
            sqlString.AppendLine("           ,@dtWriteTimeStamp");
            sqlString.AppendLine("           ,[EventID]");
            sqlString.AppendLine("           ,[Speed]");
            sqlString.AppendLine("           ,[RSSI]");
            sqlString.AppendLine("           ,[RouteMiles]");
            sqlString.AppendLine("from BusPosition");
            sqlString.AppendLine("where");
            sqlString.AppendLine("id = @buspositionID");


            MyClsDBManager db = new MyClsDBManager(strConn);
            int result = db.ExecuteNonQuery(sqlString.ToString(), "@buspositionID", buspositionID, "@dtWriteTimeStamp", dtWriteTimeStamp);
            return result;
        }

        /// <summary>
        /// not use because etvdc automatically built student table by messageid in message table
        /// </summary>
        /// <param name="buspositionID"></param>
        /// <param name="dtWriteTimeStamp"></param>
        /// <param name="messageIdOutput"></param>
        /// <returns></returns>
        public int InsertWithYesIdentitySp(Int64 buspositionID, DateTime dtWriteTimeStamp, out Int64 messageIdOutput)
        {
            messageIdOutput = 0;
            MyClsDBManager db = new MyClsDBManager(strConn);
            //object obMsgId = new object();
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SpInsertWithYesIdentityBusposition");            
            System.Data.SqlClient.SqlParameter pBuspositionID = new System.Data.SqlClient.SqlParameter("@buspositionID", buspositionID);
            System.Data.SqlClient.SqlParameter pDtWriteTimeStamp = new System.Data.SqlClient.SqlParameter("@dtWriteTimeStamp", dtWriteTimeStamp);
            System.Data.SqlClient.SqlParameter pMessageIdOutput = new System.Data.SqlClient.SqlParameter("@messageIdOutput", messageIdOutput);
            pMessageIdOutput.Direction = ParameterDirection.Output;

            cmd.Parameters.Add(pBuspositionID);
            cmd.Parameters.Add(pDtWriteTimeStamp);
            cmd.Parameters.Add(pMessageIdOutput);
            
            //int result = db.ExecuteNonQuery("SpInsertWithYesIdentityBusposition", CommandType.StoredProcedure,
            //    "@buspositionID", buspositionID, "@dtWriteTimeStamp", dtWriteTimeStamp, "@messageIdOutput", rcNext.IDMessage);

            //MyClsDBManager db = new MyClsDBManager(strConn);
            //int result = db.ExecuteNonQuery(sqlString.ToString(), "@buspositionID", buspositionID, "@dtWriteTimeStamp", dtWriteTimeStamp);
            //MyClsDBManager db = new MyClsDBManager(strConn);
            int result = db.ExecuteNonQuerySP(cmd);
            messageIdOutput = (Int64)pMessageIdOutput.Value;
            return result;
        }

        public int InsertWithNoIdentity(Int64 buspositionID, Int64 nextID, DateTime dtWriteTimeStamp)
        {
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("SET DATEFORMAT MDY insert into BusPosition");
            sqlString.AppendLine("           (ID, [DistrictID]");
            sqlString.AppendLine("           ,[UnitID]");
            sqlString.AppendLine("           ,[Latitude]");
            sqlString.AppendLine("           ,[Longitude]");
            sqlString.AppendLine("           ,[WriteTimeStamp]");
            sqlString.AppendLine("           ,[PositionTimeStamp]");
            sqlString.AppendLine("           ,[EventID]");
            sqlString.AppendLine("           ,[Speed]");
            sqlString.AppendLine("           ,[RSSI]");
            sqlString.AppendLine("           ,[RouteMiles])");
            sqlString.AppendLine("select ");
            sqlString.AppendLine("@nextID");
            sqlString.AppendLine("            ,[DistrictID]");
            sqlString.AppendLine("           ,[UnitID]");
            sqlString.AppendLine("           ,[Latitude]");
            sqlString.AppendLine("           ,[Longitude]");
            sqlString.AppendLine("           ,@dtWriteTimeStamp");
            sqlString.AppendLine("           ,@dtWriteTimeStamp");
            sqlString.AppendLine("           ,[EventID]");
            sqlString.AppendLine("           ,[Speed]");
            sqlString.AppendLine("           ,[RSSI]");
            sqlString.AppendLine("           ,[RouteMiles]");
            sqlString.AppendLine("from BusPosition");
            sqlString.AppendLine("where");
            sqlString.AppendLine("id = @buspositionID");

            MyClsDBManager db = new MyClsDBManager(strConn);
            int result = db.ExecuteNonQuery(sqlString.ToString(), "@buspositionID", buspositionID, "@nextID", nextID, "@dtWriteTimeStamp", dtWriteTimeStamp);
            return result;
        }

        //this is for student
        public int InsertStudent(DateTime dtSelectedSource, DateTime dtWriteTimeStamp)
        {
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("SET DATEFORMAT MDY INSERT INTO [Student]");
            sqlString.AppendLine("           ([StudentID]");
            sqlString.AppendLine("           ,[Status]");
            sqlString.AppendLine("           ,[Latitude]");
            sqlString.AppendLine("           ,[Longitude]");
            sqlString.AppendLine("           ,[WriteTimeStamp]");
            sqlString.AppendLine("           ,[PositionTimeStamp]");
            sqlString.AppendLine("           ,[UnitID])");
            sqlString.AppendLine("select [StudentID]");
            sqlString.AppendLine("           ,[Status]");
            sqlString.AppendLine("           ,[Latitude]");
            sqlString.AppendLine("           ,[Longitude]");
            sqlString.AppendLine("           ,@dtWriteTimeStamp");
            sqlString.AppendLine("           ,@dtWriteTimeStamp");
            sqlString.AppendLine("           ,[UnitID]");
            sqlString.AppendLine("from [Student]");
            sqlString.AppendLine("where");
            sqlString.AppendLine("WriteTimeStamp = convert(char(10),@dtSelectedSource,101) + ' ' + convert(char(12),@dtWriteTimeStamp,114)");


            MyClsDBManager db = new MyClsDBManager(strConn);
            int result = db.ExecuteNonQuery(sqlString.ToString(), "@dtSelectedSource", dtSelectedSource, "@dtWriteTimeStamp", dtWriteTimeStamp);
            return result;
        }

    }
    #endregion

    #region message Entity
    public class message
    {
        private string strConn;
        public message(string ins_strConn)
        {
            strConn = ins_strConn;
        }
        public DataTable getmessage(string TableName)
        {
            StringBuilder sqlString = new StringBuilder("SELECT message_id, message_dt_received, message_dt_event, message_event_id, message_vehicle_id, message_latitude, message_longitude, message_comment, message_dt_adjusted");
            sqlString.AppendLine("   FROM message");
            DataTable tbRs = Database.GetData(sqlString.ToString());
            return tbRs;
        }

        public DataTable getmessageByID(string message_id)
        {
            StringBuilder sqlString = new StringBuilder("SELECT message_id, message_dt_received, message_dt_event, message_event_id, message_vehicle_id, message_latitude, message_longitude, message_comment, message_dt_adjusted");
            sqlString.AppendLine("   FROM message");
            sqlString.AppendLine(" WHERE ");
            sqlString.AppendLine("message_id = @message_id");
            DataTable result = Database.GetData(sqlString.ToString(), "@message_id", message_id);
            return result;
        }

        public object getColValByID(string message_id, string colName)
        {
            #region FixData
            message_id = Database.fixSQL(message_id);
            #endregion
            StringBuilder sqlString = new StringBuilder("SELECT " + colName); sqlString.AppendLine("   FROM message");
            sqlString.AppendLine(" WHERE ");
            sqlString.AppendLine("message_id = @message_id");
            MyClsDBManager db = new MyClsDBManager(strConn);
            object result = db.ExecuteScalar(sqlString.ToString(), "@message_id", message_id);
            return result;
        }

        public int InsertWithIdentity(string message_id, string message_dt_received, string message_dt_event, string message_event_id, string message_vehicle_id, string message_latitude, string message_longitude, string message_comment, string message_dt_adjusted)
        {
            StringBuilder sqlString = new StringBuilder("SET DATEFORMAT DMY INSERT INTO message(message_dt_received, message_dt_event, message_event_id, message_vehicle_id, message_latitude, message_longitude, message_comment, message_dt_adjusted)  VALUES(");
            //sqlString.AppendLine("@message_id, ");
            sqlString.AppendLine("@message_dt_received, ");
            sqlString.AppendLine("@message_dt_event, ");
            sqlString.AppendLine("@message_event_id, ");
            sqlString.AppendLine("@message_vehicle_id, ");
            sqlString.AppendLine("@message_latitude, ");
            sqlString.AppendLine("@message_longitude, ");
            sqlString.AppendLine("@message_comment, ");
            sqlString.AppendLine("@message_dt_adjusted)");
            MyClsDBManager db = new MyClsDBManager(strConn);
            int result = db.ExecuteNonQuery(sqlString.ToString(), "@message_dt_received", message_dt_received, "@message_dt_event", message_dt_event, "@message_event_id", message_event_id, "@message_vehicle_id", message_vehicle_id, "@message_latitude", message_latitude, "@message_longitude", message_longitude, "@message_comment", message_comment, "@message_dt_adjusted", message_dt_adjusted);
            return result;
        }

        public int InsertWithoutIdentity(string message_id, string message_dt_received, string message_dt_event, string message_event_id, string message_vehicle_id, string message_latitude, string message_longitude, string message_comment, string message_dt_adjusted)
        {
            StringBuilder sqlString = new StringBuilder("SET DATEFORMAT DMY INSERT INTO message(message_id, message_dt_received, message_dt_event, message_event_id, message_vehicle_id, message_latitude, message_longitude, message_comment, message_dt_adjusted)  VALUES(");
            sqlString.AppendLine("@message_id, ");
            sqlString.AppendLine("@message_dt_received, ");
            sqlString.AppendLine("@message_dt_event, ");
            sqlString.AppendLine("@message_event_id, ");
            sqlString.AppendLine("@message_vehicle_id, ");
            sqlString.AppendLine("@message_latitude, ");
            sqlString.AppendLine("@message_longitude, ");
            sqlString.AppendLine("@message_comment, ");
            sqlString.AppendLine("@message_dt_adjusted)");
            MyClsDBManager db = new MyClsDBManager(strConn);
            int result = db.ExecuteNonQuery(sqlString.ToString(), "@message_id", message_id, "@message_dt_received", message_dt_received, "@message_dt_event", message_dt_event, "@message_event_id", message_event_id, "@message_vehicle_id", message_vehicle_id, "@message_latitude", message_latitude, "@message_longitude", message_longitude, "@message_comment", message_comment, "@message_dt_adjusted", message_dt_adjusted);
            return result;
        }

        public string getSqlQueryInsert(string message_id, string message_dt_received, string message_dt_event, string message_event_id, string message_vehicle_id, string message_latitude, string message_longitude, string message_comment, string message_dt_adjusted)
        {
            StringBuilder sqlString = new StringBuilder("SET DATEFORMAT DMY INSERT INTO message(message_id, message_dt_received, message_dt_event, message_event_id, message_vehicle_id, message_latitude, message_longitude, message_comment, message_dt_adjusted)  VALUES(");
            sqlString.AppendLine("@message_id, ");
            sqlString.AppendLine("@message_dt_received, ");
            sqlString.AppendLine("@message_dt_event, ");
            sqlString.AppendLine("@message_event_id, ");
            sqlString.AppendLine("@message_vehicle_id, ");
            sqlString.AppendLine("@message_latitude, ");
            sqlString.AppendLine("@message_longitude, ");
            sqlString.AppendLine("@message_comment, ");
            sqlString.AppendLine("@message_dt_adjusted)");

            return sqlString.ToString();
        }

        public int Update(string message_id, string message_dt_received, string message_dt_event, string message_event_id, string message_vehicle_id, string message_latitude, string message_longitude, string message_comment, string message_dt_adjusted)
        {
            StringBuilder sqlString = new StringBuilder("SET DATEFORMAT DMY UPDATE message SET ");
            sqlString.AppendLine(" message_dt_received = @message_dt_received,");
            sqlString.AppendLine(" message_dt_event = @message_dt_event,");
            sqlString.AppendLine(" message_event_id = @message_event_id,");
            sqlString.AppendLine(" message_vehicle_id = @message_vehicle_id,");
            sqlString.AppendLine(" message_latitude = @message_latitude,");
            sqlString.AppendLine(" message_longitude = @message_longitude,");
            sqlString.AppendLine(" message_comment = @message_comment,");
            sqlString.AppendLine(" message_dt_adjusted = @message_dt_adjusted");
            sqlString.AppendLine(" WHERE ");
            sqlString.AppendLine(" message_id = @message_id");
            MyClsDBManager db = new MyClsDBManager(strConn);
            int result = db.ExecuteNonQuery(sqlString.ToString(), "@message_id", message_id, "@message_dt_received", message_dt_received, "@message_dt_event", message_dt_event, "@message_event_id", message_event_id, "@message_vehicle_id", message_vehicle_id, "@message_latitude", message_latitude, "@message_longitude", message_longitude, "@message_comment", message_comment, "@message_dt_adjusted", message_dt_adjusted);
            return result;
        }

        public string getSqlQueryUpdate(string message_id, string message_dt_received, string message_dt_event, string message_event_id, string message_vehicle_id, string message_latitude, string message_longitude, string message_comment, string message_dt_adjusted)
        {
            StringBuilder sqlString = new StringBuilder("SET DATEFORMAT DMY UPDATE message SET ");
            sqlString.AppendLine(" message_dt_received = @message_dt_received,");
            sqlString.AppendLine(" message_dt_event = @message_dt_event,");
            sqlString.AppendLine(" message_event_id = @message_event_id,");
            sqlString.AppendLine(" message_vehicle_id = @message_vehicle_id,");
            sqlString.AppendLine(" message_latitude = @message_latitude,");
            sqlString.AppendLine(" message_longitude = @message_longitude,");
            sqlString.AppendLine(" message_comment = @message_comment,");
            sqlString.AppendLine(" message_dt_adjusted = @message_dt_adjusted");
            sqlString.AppendLine(" WHERE ");
            sqlString.AppendLine(" message_id = @message_id");

            return sqlString.ToString();
        }

        public int Delete(string message_id)
        {
            StringBuilder sqlString = new StringBuilder("DELETE message WHERE ");
            sqlString.AppendLine("message_id = @message_id;");

            MyClsDBManager db = new MyClsDBManager(strConn);
            int result = db.ExecuteNonQuery(sqlString.ToString(), "@message_id", message_id);
            return result;
        }

        public string getSqlQueryDelete(string message_id)
        {
            StringBuilder sqlString = new StringBuilder("DELETE message WHERE ");
            sqlString.AppendLine("message_id = @message_id;");

            return sqlString.ToString();
        }

        //customize code

        /// <summary>
        /// get max message id from the date input
        /// </summary>
        /// <param name="currentDate">format: MM/dd/yyyy</param>
        /// <returns>return max id or -1 if can not parse to int</returns>
        public Int64 getMaxIdFromDate(DateTime currentDate)
        {
            Int64 iRs;
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("select max(message_id) as idmax");
            sqlString.AppendLine("from message");
            sqlString.AppendLine("where");
            sqlString.AppendLine("convert(char(30),message_dt_adjusted,101) = @currentDate");

            MyClsDBManager db = new MyClsDBManager(strConn);
            object result = db.ExecuteScalar(sqlString.ToString(), "@currentDate", currentDate.ToString("MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US")));
            if (!Int64.TryParse(result.ToString(), out iRs))
            {
                iRs = -1;
            }
            return iRs;
        }

        /// <summary>
        /// get next id of all data in message table
        /// </summary>
        /// <param name="currentDate"></param>
        /// <returns></returns>
        public int getNextId()
        {
            int iRs;
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("select max(message_id) + 1 as idmax");
            sqlString.AppendLine("from message");            

            MyClsDBManager db = new MyClsDBManager(strConn);
            object result = db.ExecuteScalar(sqlString.ToString());
            if (!int.TryParse(result.ToString(), out iRs))
            {
                iRs = -1;
            }
            return iRs;
        }

        /// <summary>
        /// get min message id from the date input
        /// </summary>
        /// <param name="currentDate">format: MM/dd/yyyy</param>
        /// <returns>return min id or -1 if can not parse to int</returns>
        public int getMinIdFromDate(DateTime currentDate)
        {
            int iRs;
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("select min(message_id) as idmin");
            sqlString.AppendLine("from message");
            sqlString.AppendLine("where");
            sqlString.AppendLine("convert(char(30),message_dt_adjusted,101) = @currentDate");
            //sqlString.AppendLine("order by message_id");


            MyClsDBManager db = new MyClsDBManager(strConn);
            object result = db.ExecuteScalar(sqlString.ToString(), "@currentDate", currentDate.ToString("MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US")));
            if (!int.TryParse(result.ToString(), out iRs))
            {
                iRs = -1;
            }
            return iRs;
        }

        /// <summary>
        /// get the identity state of message table
        /// </summary>
        /// <returns>true or false</returns>
        public bool IsIdentity()
        {
            bool bRs = false;
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("declare @idTbMessage int");
            sqlString.AppendLine("select @idTbMessage = id");
            sqlString.AppendLine("FROM syscolumns");
            sqlString.AppendLine("where");
            sqlString.AppendLine("name = 'message_id'");
            sqlString.AppendLine("and OBJECT_NAME(id) = 'message'");
            sqlString.AppendLine("SELECT COLUMNPROPERTY (@idTbMessage, 'message_id', 'IsIdentity') as 'IsIdentity'");
            sqlString.AppendLine("FROM syscolumns");
            sqlString.AppendLine("where");
            sqlString.AppendLine("name = 'message_id'");
            sqlString.AppendLine("and OBJECT_NAME(id) = 'message'");


            MyClsDBManager db = new MyClsDBManager(strConn);
            object ob=db.ExecuteScalar(sqlString.ToString());
            if (ob.ToString() == "0")
                bRs = false;
            else
                bRs = true;
            return bRs;
        }

        public DataTable getFristMsgOfDate(string TableName, string dateSelected)
        {
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("select top 1 *");
            sqlString.AppendLine("from message");
            sqlString.AppendLine("where");
            sqlString.AppendLine("convert(char(30),message_dt_adjusted,101) = @dateSelected");
            sqlString.AppendLine("order by message_id asc");

            MyClsDBManager db = new MyClsDBManager(strConn);
            DataTable tbRs = db.GetData(sqlString.ToString(), "@dateSelected", dateSelected);            
            return tbRs;
        }

        public DataTable getNextRecordMsgOfDate(string TableName, string dateSelected, Int64 msgID)
        {
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("select top 1 *");
            sqlString.AppendLine("from message");
            sqlString.AppendLine("where");
            sqlString.AppendLine("convert(char(30),message_dt_adjusted,101) = @dateSelected");
            sqlString.AppendLine("and message_id > @msgID");
            sqlString.AppendLine("order by message_id asc");


            MyClsDBManager db = new MyClsDBManager(strConn);
            DataTable tbRs = db.GetData(sqlString.ToString(), "@dateSelected", dateSelected, "@msgID", msgID);
            return tbRs;
        }

        public DataTable getNextRecordMsgOfDateByDateTime(string TableName, string dateSelected, string nextDateAndTime)
        {
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("select top 1 *");
            sqlString.AppendLine("from message");
            sqlString.AppendLine("where");
            sqlString.AppendLine("convert(char(30),message_dt_adjusted,101) = @dateSelected");
            sqlString.AppendLine("and message_dt_adjusted > @nextDateAndTime");
            sqlString.AppendLine("order by message_id asc");


            MyClsDBManager db = new MyClsDBManager(strConn);
            DataTable tbRs = db.GetData(sqlString.ToString(), "@dateSelected", dateSelected, "@nextDateAndTime", nextDateAndTime);
            return tbRs;
        }

        public DateTime getLastTimeRecordOfCurrentDate(DateTime currentDate)
        {
            //int iRs;
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("select top 1 message_dt_adjusted");
            sqlString.AppendLine("from message");
            sqlString.AppendLine("where ");
            sqlString.AppendLine("convert(char(30),message_dt_adjusted,101) = @currentDate");
            sqlString.AppendLine("order by message_dt_adjusted desc");


            MyClsDBManager db = new MyClsDBManager(strConn);
            object ob = db.ExecuteScalar(sqlString.ToString(), "@currentDate", currentDate.ToString("MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US")));
            DateTime result = DateTime.MinValue;
            if (ob != null)
            {
                System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
                if (!DateTime.TryParse(ob.ToString(), ci, System.Globalization.DateTimeStyles.None, out result))
                {                    
                }
            }
            return result;
        }

        public DateTime getTotalRecordOfDateSeleted(DateTime currentDate)
        {
            //int iRs;
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("select count(message_id) as TotalRecords");
            sqlString.AppendLine("from message");
            sqlString.AppendLine("where ");
            sqlString.AppendLine("convert(char(30),message_dt_adjusted,101) = @currentDate");

            MyClsDBManager db = new MyClsDBManager(strConn);
            object ob = db.ExecuteScalar(sqlString.ToString(), "@currentDate", currentDate.ToString("MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US")));
            DateTime result = DateTime.MinValue;
            if (ob != null)
            {
                System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
                if (!DateTime.TryParse(ob.ToString(), ci, System.Globalization.DateTimeStyles.None, out result))
                {
                    //result = DateTime.MinValue;
                }
            }
            return result;
        }

        public DataTable getListOfDateAndTotalRecord()
        {
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("select convert(char(30),message_dt_adjusted,101) ListDate, count(*) as TotalRecord");
            sqlString.AppendLine("from message");
            sqlString.AppendLine("group by convert(char(30),message_dt_adjusted,101)");

            MyClsDBManager db = new MyClsDBManager(strConn);
            DataTable tbRs = db.GetData(sqlString.ToString());
            return tbRs;
        }

        public int DeleteByDate(DateTime currentDate)
        {
            StringBuilder sqlString = new StringBuilder();
            sqlString.AppendLine("delete message");
            sqlString.AppendLine("where");
            sqlString.AppendLine("convert(char(30),message_dt_adjusted,101) = @currentDate");

            MyClsDBManager db = new MyClsDBManager(strConn);
            int result = db.ExecuteNonQuery(sqlString.ToString(), "@currentDate", currentDate.ToString("MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US")));
            return result;
        }

        public int InsertWithYesIdentity(Int64 messageID, DateTime dtAdjusted)
        {
            //StringBuilder sqlString = new StringBuilder();
            //sqlString.AppendLine("SET DATEFORMAT DMY insert into message");
            //sqlString.AppendLine(" ([message_dt_received]");
            //sqlString.AppendLine("           ,[message_dt_event]");
            //sqlString.AppendLine("           ,[message_event_id]");
            //sqlString.AppendLine("           ,[message_vehicle_id]");
            //sqlString.AppendLine("           ,[message_latitude]");
            //sqlString.AppendLine("           ,[message_longitude]");
            //sqlString.AppendLine("           ,[message_comment]");
            //sqlString.AppendLine("           ,[message_dt_adjusted])");
            //sqlString.AppendLine("select ");
            //sqlString.AppendLine("[message_dt_received]");
            //sqlString.AppendLine("           ,[message_dt_event]");
            //sqlString.AppendLine("           ,[message_event_id]");
            //sqlString.AppendLine("           ,[message_vehicle_id]");
            //sqlString.AppendLine("           ,[message_latitude]");
            //sqlString.AppendLine("           ,[message_longitude]");
            //sqlString.AppendLine("           ,[message_comment]");
            //sqlString.AppendLine("           ,@dtAdjusted");
            //sqlString.AppendLine("from message");
            //sqlString.AppendLine("where");
            //sqlString.AppendLine("message_id = @messageID");

            //MyClsDBManager db = new MyClsDBManager(strConn);
            //int result = db.ExecuteNonQuery(sqlString.ToString(), "@messageID", messageID, "@dtAdjusted", dtAdjusted);                
            //return result;

            Int64 messageIdOutput = 0;
            MyClsDBManager db = new MyClsDBManager(strConn);
            //object obMsgId = new object();
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("SpInsertWithYesIdentityMessage");
            System.Data.SqlClient.SqlParameter _messageID = new System.Data.SqlClient.SqlParameter("@messageID", messageID);
            System.Data.SqlClient.SqlParameter _dtAdjusted = new System.Data.SqlClient.SqlParameter("@dtAdjusted", dtAdjusted);
            System.Data.SqlClient.SqlParameter _messageIdOutput = new System.Data.SqlClient.SqlParameter("@messageIdOutput", messageIdOutput);
            _messageIdOutput.Direction = ParameterDirection.Output;

            cmd.Parameters.Add(_messageID);
            cmd.Parameters.Add(_dtAdjusted);
            cmd.Parameters.Add(_messageIdOutput);

            //int result = db.ExecuteNonQuery("SpInsertWithYesIdentityBusposition", CommandType.StoredProcedure,
            //    "@buspositionID", buspositionID, "@dtWriteTimeStamp", dtWriteTimeStamp, "@messageIdOutput", rcNext.IDMessage);

            //MyClsDBManager db = new MyClsDBManager(strConn);
            //int result = db.ExecuteNonQuery(sqlString.ToString(), "@buspositionID", buspositionID, "@dtWriteTimeStamp", dtWriteTimeStamp);
            //MyClsDBManager db = new MyClsDBManager(strConn);
            int result = db.ExecuteNonQuerySP(cmd);
            messageIdOutput = (Int64)_messageIdOutput.Value;
            return result;
        }

        public int InsertWithNoIdentity(Int64 messageID, Int64 nextID, DateTime dtAdjusted)
        {
            return 0;
            //StringBuilder sqlString = new StringBuilder();
            //sqlString.AppendLine("insert into message");
            //sqlString.AppendLine("([message_id]");
            //sqlString.AppendLine("           ,[message_dt_received]");
            //sqlString.AppendLine("           ,[message_dt_event]");
            //sqlString.AppendLine("           ,[message_event_id]");
            //sqlString.AppendLine("           ,[message_vehicle_id]");
            //sqlString.AppendLine("           ,[message_latitude]");
            //sqlString.AppendLine("           ,[message_longitude]");
            //sqlString.AppendLine("           ,[message_comment]");
            //sqlString.AppendLine("           ,[message_dt_adjusted])");
            //sqlString.AppendLine("select ");
            //sqlString.AppendLine("@nextID");
            //sqlString.AppendLine("           ,[message_dt_received]");
            //sqlString.AppendLine("           ,[message_dt_event]");
            //sqlString.AppendLine("           ,[message_event_id]");
            //sqlString.AppendLine("           ,[message_vehicle_id]");
            //sqlString.AppendLine("           ,[message_latitude]");
            //sqlString.AppendLine("           ,[message_longitude]");
            //sqlString.AppendLine("           ,[message_comment]");
            //sqlString.AppendLine("           ,@dtAdjusted");
            //sqlString.AppendLine("from message");
            //sqlString.AppendLine("where");
            //sqlString.AppendLine("message_id = @messageID");

            //MyClsDBManager db = new MyClsDBManager(strConn);
            //int result = db.ExecuteNonQuery(sqlString.ToString(), "@messageID", messageID, "@nextID", nextID, "@dtAdjusted", dtAdjusted);
            //return result;
        }
    }
    #endregion

}
