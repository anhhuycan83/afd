﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using nsDataAccess;
using System.Data.SqlClient;
using System.Data;
//using nsSupport;
using Microsoft.Win32;
using System.IO;
using ConfigXmlFile;
using System.Data.OleDb;

namespace DatabaseClasses
{	
	#region Fuction Class (Share)
	public class FuctionClass
	{
		ErrorEntity err;
		Support sp;
		public FuctionClass()
		{
			err = new ErrorEntity();
			sp = new Support();
		}
		/// <summary>
		/// Automatic add number from 1 to rows.count of table in the column name input
		/// </summary>
		/// <param name="dst"></param>
		/// <param name="Table_Name"></param>
		/// <param name="Column_Name"></param>
		public void AutoNumber(DataSet dst, string Table_Name, string Column_Name)
		{
			for (int i = 0; i < dst.Tables[Table_Name].Rows.Count; i++)
			{
				string Temp = Convert.ToString(i + 1);
				dst.Tables[Table_Name].Rows[i][Column_Name] = Temp;
			}
		}

        public void AutoNumber(DataTable Table_Name, string Column_Name)
        {
            for (int i = 0; i <Table_Name.Rows.Count; i++)
            {
                string Temp = Convert.ToString(i + 1);
                Table_Name.Rows[i][Column_Name] = Temp;
            }
        }
        public void AutoNumber(DataGridView dgvData, string Column_Name)
        {
            string Temp;
            for (int i = 0; i < dgvData.Rows.Count; i++)
            {
                Temp = Convert.ToString(i + 1);
                dgvData.Rows[i].Cells[Column_Name].Value = Temp;
            }
        }

		/// <summary>
		/// Check wherethere the key is exist in the input table. True if exist, else return false
		/// </summary>
		/// <param name="dst"></param>
		/// <param name="Table_Name"></param>
		/// <param name="Column_NameId"></param>
		/// <param name="key"></param>
		/// <returns></returns>
		public bool testEsixtIdInTalbe(DataSet dst, string Table_Name, string Column_NameId, string key)
		{
			for (int i = 0; i < dst.Tables[Table_Name].Rows.Count; i++)
			{
				if (dst.Tables[Table_Name].Rows[i][Column_NameId].ToString() == key)
					return true;
			}
			return false;
		}
		/// <summary>
		/// input: DayOfWeek Enumeration type - output: string MS_THU type
		/// </summary>
		/// <param name="d"></param>
		/// <returns></returns>
		//        public string getMSThu(DayOfWeek d)
		//        {
		//            string thu;
		//            switch (d)
		//            {
		//                case DayOfWeek.Sunday:
		//                    thu = "CN";
		//                    break;
		//                case DayOfWeek.Monday:
		//                    thu = "T2";
		//                    break;
		//                case DayOfWeek.Tuesday:
		//                    thu = "T3";
		//                    break;
		//                case DayOfWeek.Wednesday:
		//                    thu = "T4";
		//                    break;
		//                case DayOfWeek.Thursday:
		//                    thu = "T5";
		//                    break;
		//                case DayOfWeek.Friday:
		//                    thu = "T6";
		//                    break;
		//                case DayOfWeek.Saturday:
		//                    thu = "T7";
		//                    break;
		//                default:
		//                    thu = "";
		//                    break;
		//            }
		//            return thu;
		//        }
		//        public bool testDaPhanCongMonHocByIdMaSoHe(string maSoHe)
		//        {
//
		//            PHU_TRACH phuTrach = new PHU_TRACH();
		//            string sqlString = "SELECT HE_MH.MS_MH"
		//                + " FROM HE_MH"
		//                + " WHERE HE_MH.MS_HE='" + maSoHe + "'";
//
		//            DataAccess da = new DataAccess();
		//            da.open();
		//            DataSet ds = da.GetDataSetFromDB(sqlString);
//
		//            return da.testExistValueInTable(ds);
		//        }

		/// <summary>
		/// return Number max in the column id, if error occur in compare, return -1
		/// </summary>
		/// <param name="dview"></param>
		/// <param name="colIdName"></param>
		/// <returns></returns>
		public int getMaxId(DataView dview, string colIdName)
		{
			try
			{
				//string ms = "MS_GV";
				int result = 0;
				for (int i = 0; i < dview.Count; i++)
				{
					if (result < Convert.ToInt32(dview[i][colIdName].ToString()))
					{
						result = Convert.ToInt32(dview[i][colIdName].ToString());
					}
				}
				return result;
				/*
                refressDataGrid_dgrTcher();
                btnEdit.Enabled = false;
				 */
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
				return -1;
			}
		}

		
		/// <summary>
		/// return Number max in the column id, if error occur in compare, return -1
		/// </summary>
		/// <param name="dview"></param>
		/// <param name="colIdName"></param>
		/// <returns></returns>
		public string getNextId(string ins_tableName, string colIdName)
		{
			string result = "";
			try
			{
				DataSet ds = new DataSet();
				DataAccess da = new DataAccess();
				da.open();
				string sqlStr = "SELECT MAX(" + colIdName + ") AS MAX_ID FROM " +  ins_tableName;
				
				ds = da.getDataSet_Table(sqlStr, "MAX_ID");
				da.close();
				if(da.testExistValueInTableAndHaveRow(ds))
				{
					if(ds.Tables[0].Rows[0][0] is DBNull)
						result = "1";
					else						
						result = Convert.ToString(Convert.ToInt16(ds.Tables[0].Rows[0][0]) + 1);
				}				
				else
					result = "1";
				
				/*
                refressDataGrid_dgrTcher();
                btnEdit.Enabled = false;
				 */
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
				result = "";
			}
			return result;
		}
		/*
        public string GetStrFileName(string StrFileName)
        {
            string StrName = "";
            for (int i = 0; i < StrFileName.Length; i++)
            {
                string str = StrFileName.Substring(i, 1);
                if (str == @"\")
                    StrName = "";
                else
                {
                    StrName = StrName + str;
                }
            }
            return StrName;
        }
		 */
		public string GetStrDate(string StrDate)
		{
			if (StrDate != "")
			{
				DateTime dTime = new DateTime();
				dTime = Convert.ToDateTime(StrDate);
				StrDate = dTime.Day.ToString() + "/" + dTime.Month.ToString() + "/" + dTime.Year.ToString();
			}
			string StrD = "";
			for (int i = 0; i < StrDate.Length; i++)
			{
				string str = StrDate.Substring(i, 1);
				if (str == @" ")
					return StrD;
				else
					StrD = StrD + str;
			}
			return StrD;

		}
		public void Fill_DataSet(DataSet dst, string Table_Name, string strSql)
		{
			try
			{
				DataAccess da = new DataAccess();
				da.open();
				DataSet ds = da.GetDataSetFromDB(strSql);
				da.close();
				sp.fillDataSetTable(dst, ds.Tables[0], Table_Name);
			}
			catch
			{
				err.alert_string("Lỗi kết nối CSDL");
			}
		}        
	}
	#endregion

	#region ErrorEntity class
	public class ErrorEntity
	{
		public ErrorEntity()
		{
		}

		public string err_nullData = "Không chấp nhận dữ liệu rỗng";
		public string err_notInteger = "Dữ liệu nhập vào phải là một số nguyên";
		public string err_invalidData = "Dữ liệu nhập vào không hợp lệ";

		public void alert_nullData()
		{
			MessageBox.Show(err_nullData,"Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
		}

		public void alert_notInteger()
		{
			MessageBox.Show(err_notInteger,"Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
		}

		public void alert_invalidData()
		{
			MessageBox.Show(err_invalidData,"Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
		}

		public void alert_string(string ins_strErr)
		{
			MessageBox.Show(ins_strErr,"Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
		}

		
		public bool warning(string ins_warning)
		{
			if (MessageBox.Show(ins_warning, "Warning", MessageBoxButtons.OKCancel,
			                    MessageBoxIcon.Question) == DialogResult.OK)
			{
				return true;
			}else
				return false;
		}// End of warning
	}
	#endregion

	#region Support class
	public class Support
	{
		//private string prs_counter = null;
		//int codeleng = 4;
		ErrorEntity err;		
		public Support()
		{
			err = new ErrorEntity();
		}

		#region funtions not used
		/*
		public int checkYear(string yearID)
		{
			int result = 0;
			try
			{
				string sqlString = "SELECT TERM_ID FROM TBL_TERM WHERE YEAR_ID = '"+ yearID +"'";
				DataAccess da = new DataAccess();
				da.open();
				DataSet ds = da.GetDataSetFromDB(sqlString);
				da.close();
				if(ds.Tables[0].Rows.Count > 0)
					result = 1;
				else
					result = 0;
			}
			catch(Exception){}
			return result;
		}
		 */
		/*
		public string ReadRegistry(string ins_keyName)
		{
			RegistryKey rk= Registry.LocalMachine;
			RegistryKey skl = rk.OpenSubKey("SOFTWARE\\" + Application.ProductName);

			if(skl == null)
				return null;
			else
			{
				try
				{
					return (string)skl.GetValue(ins_keyName.ToUpper());
				}
				catch(Exception ex)
				{
					System.Console.WriteLine(ex.ToString());
					return null;
				}
			}

		}// End of ReadRegistry pro
		 */
		/*

		public bool WriteRegistry(string ins_keyName, string ins_keyValue)
		{
			try
			{
				RegistryKey rk = Registry.LocalMachine;
				RegistryKey skl = rk.CreateSubKey("SOFTWARE\\" + Application.ProductName);
				skl.SetValue(ins_keyName.ToUpper(),ins_keyValue);
				return true;
			}
			catch(Exception ex)
			{
				System.Console.WriteLine(ex.ToString());
				return false;
			}
		}// End of WriteRegistry pro

		public bool DeleteRegistry(string ins_keyName)
		{
			try
			{
				RegistryKey rk = Registry.LocalMachine;
				RegistryKey skl = rk.CreateSubKey("SOFTWARE\\" + Application.ProductName);
				if(skl == null)
					return true;
				else
					skl.DeleteValue(ins_keyName);
				return true;
			}
			catch(Exception ex)
			{
				System.Console.WriteLine(ex.ToString());
				return false;
			}
		}// End of DeleteRegistry pro


		// Get customer code length define in table TBL_CODE_LENGTH_DEFINE
		// If not define, return value 0
		public int getCustomerCodeLength()
		{
			string sqlString = "SELECT * FROM TBL_CODE_LENGTH_DEFINE";
			DataAccess da = new DataAccess();
			da.open();
			DataSet ds = da.GetDataSetFromDB(sqlString);
			da.close();

			if(ds.Tables[0].Rows.Count != 0)
			{
				if(ds.Tables[0].Rows[0]["CUSTOMERCODE_LENGTH"].ToString() != "")
					return Convert.ToInt32(ds.Tables[0].Rows[0]["CUSTOMERCODE_LENGTH"].ToString());
				else
					return 0;
			}
			else
				return 0;
		}// End of getCustomerCodeLength pro

		// Get subscriber code length define in table TBL_CODE_LENGTH_DEFINE
		// If not define, return value 0
		public int getSubscriberCodeLength()
		{
			string sqlString = "SELECT * FROM TBL_CODE_LENGTH_DEFINE";
			DataAccess da = new DataAccess();
			da.open();
			DataSet ds = da.GetDataSetFromDB(sqlString);
			da.close();

			if(ds.Tables[0].Rows.Count != 0)
			{
				if(ds.Tables[0].Rows[0]["SUBSCRIBERCODE_LENGTH"].ToString() != "")
					return Convert.ToInt32(ds.Tables[0].Rows[0]["SUBSCRIBERCODE_LENGTH"].ToString());
				else
					return 0;
			}
			else
				return 0;
		}// End of getSubscriberCodeLength pro

		// Get Contract code length define in table TBL_CODE_LENGTH_DEFINE
		// If not define, return value 0
		public int getContractCodeLength()
		{
			string sqlString = "SELECT * FROM TBL_CODE_LENGTH_DEFINE";
			DataAccess da = new DataAccess();
			da.open();
			DataSet ds = da.GetDataSetFromDB(sqlString);
			da.close();

			if(ds.Tables[0].Rows.Count != 0)
			{
				if(ds.Tables[0].Rows[0]["CONTRACTCODE_LENGTH"].ToString() != "")
					return Convert.ToInt32(ds.Tables[0].Rows[0]["CONTRACTCODE_LENGTH"].ToString());
				else
					return 0;
			}
			else
				return 0;
		}// End of getContractCodeLength pro

		// Get AutoIncrease code length define in table TBL_CODE_LENGTH_DEFINE
		// If not define, return value 0
		public int getAutoIncreaseCodeLength()
		{
			string sqlString = "SELECT * FROM TBL_CODE_LENGTH_DEFINE";
			DataAccess da = new DataAccess();
			da.open();
			DataSet ds = da.GetDataSetFromDB(sqlString);
			da.close();

			if(ds.Tables[0].Rows.Count != 0)
			{
				if(ds.Tables[0].Rows[0]["AUTOINCREASECODE_LENGTH"].ToString() != "")
					return Convert.ToInt32(ds.Tables[0].Rows[0]["AUTOINCREASECODE_LENGTH"].ToString());
				else
					return 0;
			}
			else
				return 0;
		}// End of getAutoIncreaseCodeLength pro

		public string getNewDeclareCode()
		{
			string sqlString = "INSERT INTO SEQ_DECLARE(FORFUN) VALUES(0)";
			DataAccess da = new DataAccess();
			da.open();
			da.UpdateToDB(sqlString);
			sqlString = "SELECT MAX(COUNTER) FROM SEQ_DECLARE";
			DataSet ds = da.GetDataSetFromDB(sqlString);
			da.close();

			int li_temp = 0;
			int li_subscriberCodeLength = 8;
			if((li_temp = getSubscriberCodeLength()) != 0)
				li_subscriberCodeLength = li_temp;

			prs_counter = ds.Tables[0].Rows[0][0].ToString();
			// Set the length of ls_counter = 6 (ex: 000012)
			prs_counter = "00000000000000000000".Substring(0,li_subscriberCodeLength -prs_counter.Length) + prs_counter;

			return prs_counter;
		}// End of getNewDeclareCode pro

		public string getNewSurveyCode()
		{
			string sqlString = "INSERT INTO SEQ_SURVEY(FORFUN) VALUES(0)";
			DataAccess da = new DataAccess();
			da.open();
			da.UpdateToDB(sqlString);
			sqlString = "SELECT MAX(COUNTER) FROM SEQ_SURVEY";
			DataSet ds = da.GetDataSetFromDB(sqlString);
			da.close();

			int li_temp = 0;
			int li_autoIncreaseCodeLength = 9;
			if((li_temp = getAutoIncreaseCodeLength()) != 0)
				li_autoIncreaseCodeLength = li_temp;

			prs_counter = ds.Tables[0].Rows[0][0].ToString();
			// Set the length of ls_counter = 6 (ex: 000012)
			prs_counter = "00000000000000000000".Substring(0,li_autoIncreaseCodeLength -2 -prs_counter.Length) + prs_counter;

			return "81" + prs_counter;
		}// End of getNewDeclareCode pro

		public string getNewInstallCode()
		{
			string sqlString = "INSERT INTO SEQ_INSTALL(FORFUN) VALUES(0)";
			DataAccess da = new DataAccess();
			da.open();
			da.UpdateToDB(sqlString);
			sqlString = "SELECT MAX(COUNTER) FROM SEQ_INSTALL";
			DataSet ds = da.GetDataSetFromDB(sqlString);
			da.close();

			int li_temp = 0;
			int li_autoIncreaseCodeLength = 9;
			if((li_temp = getAutoIncreaseCodeLength()) != 0)
				li_autoIncreaseCodeLength = li_temp;

			prs_counter = ds.Tables[0].Rows[0][0].ToString();
			// Set the length of ls_counter = 6 (ex: 000012)
			prs_counter = "00000000000000000000".Substring(0,li_autoIncreaseCodeLength -2 -prs_counter.Length) + prs_counter;

			return "82" + prs_counter;
		}// End of getNewInstallCode pro

		public string getNewTakeoverCode()
		{
			string sqlString = "INSERT INTO SEQ_TAKEOVER(FORFUN) VALUES(0)";
			DataAccess da = new DataAccess();
			da.open();
			da.UpdateToDB(sqlString);
			sqlString = "SELECT MAX(COUNTER) FROM SEQ_TAKEOVER";
			DataSet ds = da.GetDataSetFromDB(sqlString);
			da.close();

			int li_temp = 0;
			int li_autoIncreaseCodeLength = 9;
			if((li_temp = getAutoIncreaseCodeLength()) != 0)
				li_autoIncreaseCodeLength = li_temp;

			prs_counter = ds.Tables[0].Rows[0][0].ToString();
			// Set the length of ls_counter = 6 (ex: 000012)
			prs_counter = "00000000000000000000".Substring(0,li_autoIncreaseCodeLength -2 -prs_counter.Length) + prs_counter;

			return "83" + prs_counter;
		}// End of getNewTakeoverCode pro


		public string getNewSubscriberCode()
		{
			return "RTB" + prs_counter;
		}// End of getNewSubscriberCode pro

		public string generateSubscriberCode(string ins_declareCode)
		{
			string ls_subsCode = ins_declareCode;
			if(ins_declareCode != null)
				if(ins_declareCode.Length != 0)
					ls_subsCode = "RTB" + ins_declareCode;// "3" + ins_declareCode.Substring(1,ins_declareCode.Length - 1);
			return ls_subsCode;
		}// End of generateSubscriberCode


		/// <summary>
		/// Check Text box value
		/// </summary>
		/// <param name="textbox"></param>
		/// <param name="is_null"></param>
		/// <returns>True: value incorrect; False: value correct</returns>
		 */
		#endregion

		
		//        public ArrayList alSortByNBDIncrease(ArrayList ins_al)
		//        {
		//            List<ThoiGianWithMonHoc> listInput = new List<ThoiGianWithMonHoc>();
		//            for (int k = 0; k < ins_al.Count; k++)
		//            {
		//                ThoiGianWithMonHoc ptu_k = (ThoiGianWithMonHoc)ins_al[k];
		//                listInput.Add(ptu_k);
		//            }
		//            for (int i = 0; i < listInput.Count - 1; i++)
		//            {
		//                //ThoiGianWithMonHoc lc_phanTu_i = (ThoiGianWithMonHoc)ins_al[i];
		//                for (int j = i + 1; j < listInput.Count; j++)
		//                {
		//                    //ThoiGianWithMonHoc lc_phanTu_j = (ThoiGianWithMonHoc)ins_al[j];
		//                    if (listInput[i].NgayBatDau > listInput[j].NgayBatDau)
		//                    {
		//                        ThoiGianWithMonHoc temp;
		//                        temp = listInput[i];
		//                        listInput[i] = listInput[j];
		//                        listInput[j] = temp;
		//                    }
		//                }
		//            }
		//            ArrayList alResult = new ArrayList();
		//            for (int k = 0; k < ins_al.Count; k++)
		//            {
		//                alResult.Add(listInput[k]);
		//            }
		//            return alResult;
		//        }
		//        public string getMSThu(DayOfWeek d)
		//        {
		//            string thu;
		//            switch (d)
		//            {
		//                case DayOfWeek.Sunday:
		//                    thu = "CN";
		//                    break;
		//                case DayOfWeek.Monday:
		//                    thu = "T2";
		//                    break;
		//                case DayOfWeek.Tuesday:
		//                    thu = "T3";
		//                    break;
		//                case DayOfWeek.Wednesday:
		//                    thu = "T4";
		//                    break;
		//                case DayOfWeek.Thursday:
		//                    thu = "T5";
		//                    break;
		//                case DayOfWeek.Friday:
		//                    thu = "T6";
		//                    break;
		//                case DayOfWeek.Saturday:
		//                    thu = "T7";
		//                    break;
		//                default:
		//                    thu = "";
		//                    break;
		//            }
		//            return thu;
		//        }
		/// <summary>
		/// input: string datetime with format: dd/MM/yyyy
		/// output: datetime value
		/// </summary>
		/// <param name="ins_stringDateTime"></param>
		/// <returns></returns>
		public DateTime convertStringToDateTime(string ins_stringDateTime)
		{
			string day = ins_stringDateTime.Substring(0, 2);
			string month = ins_stringDateTime.Substring(3, 2);
			string year = ins_stringDateTime.Substring(6, 4);

			int intDay = Convert.ToInt32(day);
			int intMonth = Convert.ToInt32(month);
			int intYear = Convert.ToInt32(year);

			DateTime dt = new DateTime(intYear, intMonth, intDay);
			return dt;
		}
		/// <summary>
		/// input: DayOfWeek Enumeration type - output: string MS_THU type
		/// </summary>
		/// <param name="d"></param>
		/// <returns></returns>
		//        public string convertDayOfWeekToMS_THU(DayOfWeek d)
		//        {
		//            string thu;
		//            switch (d)
		//            {
		//                case DayOfWeek.Sunday:
		//                    thu = "CN";
		//                    break;
		//                case DayOfWeek.Monday:
		//                    thu = "T2";
		//                    break;
		//                case DayOfWeek.Tuesday:
		//                    thu = "T3";
		//                    break;
		//                case DayOfWeek.Wednesday:
		//                    thu = "T4";
		//                    break;
		//                case DayOfWeek.Thursday:
		//                    thu = "T5";
		//                    break;
		//                case DayOfWeek.Friday:
		//                    thu = "T6";
		//                    break;
		//                case DayOfWeek.Saturday:
		//                    thu = "T7";
		//                    break;
		//                default:
		//                    thu = "";
		//                    break;
		//            }
		//            return thu;
		//        }
		//        public void fillDayOfWeek(ArrayList al)
		//        {
		//            al.Add(new Day(DayOfWeek.Monday, "Thứ hai"));
		//            al.Add(new Day(DayOfWeek.Tuesday, "Thứ ba"));
		//            al.Add(new Day(DayOfWeek.Wednesday, "Thứ tư"));
		//            al.Add(new Day(DayOfWeek.Thursday, "Thứ năm"));
		//            al.Add(new Day(DayOfWeek.Friday, "Thứ sáu"));
		//            al.Add(new Day(DayOfWeek.Saturday, "Thứ bảy"));
		//            al.Add(new Day(DayOfWeek.Sunday, "Chủ nhật"));
		//        }
		//        public int convertMS_THUToIntValue(string ins_maSoThu)
		//        {
		//            int intDay;
		//            switch (ins_maSoThu)
		//            {
		//                case "CN":
		//                    intDay = 1;
		//                    break;
		//                case "T2":
		//                    intDay = 2;
		//                    break;
		//                case "T3":
		//                    intDay = 3;
		//                    break;
		//                case "T4":
		//                    intDay = 4;
		//                    break;
		//                case "T5":
		//                    intDay = 5;
		//                    break;
		//                case "T6":
		//                    intDay = 6;
		//                    break;
		//                case "T7":
		//                    intDay = 7;
		//                    break;
		//                default:
		//                    intDay = 0;
		//                    break;
		//            }
		//            return intDay;
		//        }
		/// <summary>
		/// INPUT: intValue:0 -> 6 OUTPUT: MS_THU: CN -> T7
		/// </summary>
		/// <param name="ins_intValue"></param>
		/// <returns></returns>
		//        public string convertIntValueToMS_THU(int ins_intValue)
		//        {
		//            string MS_THU = "";
		//            switch (ins_intValue)
		//            {
		//                case 0:
		//                    MS_THU = "CN";
		//                    break;
		//                case 1:
		//                    MS_THU = "T2";
		//                    break;
		//                case 2:
		//                    MS_THU = "T3";
		//                    break;
		//                case 3:
		//                    MS_THU = "T4";
		//                    break;
		//                case 4:
		//                    MS_THU = "T5";
		//                    break;
		//                case 5:
		//                    MS_THU = "T6";
		//                    break;
		//                case 6:
		//                    MS_THU = "T7";
		//                    break;
		//                default:
		//                    MS_THU = "";
		//                    break;
		//            }
		//            return MS_THU;
		//        }
		/// <summary>
		/// input: MS_THU(T2) -> output: StringUnicode(Thứ hai)
		/// </summary>
		/// <param name="ins_maSoThu"></param>
		/// <returns></returns>
		//        public string convertMS_THUToStringUnicode(string ins_maSoThu)
		//        {
		//            string strReturn = "";
		//            switch (ins_maSoThu)
		//            {
		//                case "CN":
		//                    strReturn = "Chủ nhật";
		//                    break;
		//                case "T2":
		//                    strReturn = "Thứ hai";
		//                    break;
		//                case "T3":
		//                    strReturn = "Thứ ba";
		//                    break;
		//                case "T4":
		//                    strReturn = "Thứ tư";
		//                    break;
		//                case "T5":
		//                    strReturn = "Thứ năm";
		//                    break;
		//                case "T6":
		//                    strReturn = "Thứ sáu";
		//                    break;
		//                case "T7":
		//                    strReturn = "Thứ bảy";
		//                    break;
		//                default:
		//                    strReturn = "Giá trị sai";
		//                    break;
		//            }
		//            return strReturn;
		//        }
		//        public int convertMS_THUToIntValueBeginWithZeroForSunday(string ins_maSoThu)
		//        {
		//            int intDay;
		//            switch (ins_maSoThu)
		//            {
		//                case "CN":
		//                    intDay = 0;
		//                    break;
		//                case "T2":
		//                    intDay = 1;
		//                    break;
		//                case "T3":
		//                    intDay = 2;
		//                    break;
		//                case "T4":
		//                    intDay = 3;
		//                    break;
		//                case "T5":
		//                    intDay = 4;
		//                    break;
		//                case "T6":
		//                    intDay = 5;
		//                    break;
		//                case "T7":
		//                    intDay = 6;
		//                    break;
		//                default:
		//                    intDay = 7;
		//                    break;
		//            }
		//            return intDay;
		//        }
		/// <summary>
		/// input: datetime datatype - output: int value(sunday = 0, saturday = 6)
		/// </summary>
		/// <param name="ins_dateTime"></param>
		/// <returns></returns>
		//        public int convertDateTimeToIntValue(DateTime ins_dateTime)
		//        {
		//            int intDay;
		//            switch (ins_dateTime.DayOfWeek)
		//            {
		//                case DayOfWeek.Sunday:
		//                    intDay = 0;
		//                    break;
		//                case DayOfWeek.Monday:
		//                    intDay = 1;
		//                    break;
		//                case DayOfWeek.Tuesday:
		//                    intDay = 2;
		//                    break;
		//                case DayOfWeek.Wednesday:
		//                    intDay = 3;
		//                    break;
		//                case DayOfWeek.Thursday:
		//                    intDay = 4;
		//                    break;
		//                case DayOfWeek.Friday:
		//                    intDay = 5;
		//                    break;
		//                case DayOfWeek.Saturday:
		//                    intDay = 6;
		//                    break;
		//                default:
		//                    intDay = 0;
		//                    break;
		//            }
		//            return intDay;
		//        }

		/// <summary>
		/// input: DayOfWeek enumeration - output: int value (0-6) begin with sunday=0
		/// </summary>
		/// <param name="ins_dayOfWeek"></param>
		/// <returns></returns>
		//        public int convertDayOfWeekToIntValue(DayOfWeek ins_dayOfWeek)
		//        {
		//            int intDay;
		//            switch (ins_dayOfWeek)
		//            {
		//                case DayOfWeek.Sunday:
		//                    intDay = 0;
		//                    break;
		//                case DayOfWeek.Monday:
		//                    intDay = 1;
		//                    break;
		//                case DayOfWeek.Tuesday:
		//                    intDay = 2;
		//                    break;
		//                case DayOfWeek.Wednesday:
		//                    intDay = 3;
		//                    break;
		//                case DayOfWeek.Thursday:
		//                    intDay = 4;
		//                    break;
		//                case DayOfWeek.Friday:
		//                    intDay = 5;
		//                    break;
		//                case DayOfWeek.Saturday:
		//                    intDay = 6;
		//                    break;
		//                default:
		//                    intDay = 0;
		//                    break;
		//            }
		//            return intDay;
		//        }
		

		public bool checkTextBox(TextBox textbox,bool is_null)
		{
			if(is_null)
				if(textbox.Text.Trim() == "")
			{
				err.alert_nullData();
				textbox.Focus();
				textbox.SelectAll();
				return true;
			}
			return false;
		}

		/// <summary>
		/// Check Text box value
		/// <param name="textbox">the textbox to check</param>
		/// <param name="is_null">set true if you want to check textbox is null</param>
		/// <param name="is_integer">set true if you want to check textbox.text is int type</param>
		/// <returns>True: value incorrect; False: value correct</returns>
		/// </summary>
		public bool checkTextBox(TextBox textbox,bool is_null,bool is_integer)
		{
			int li_value = 0;

			if(is_null)
			{
				if(textbox.Text.Trim() == "")
				{
					err.alert_nullData();
					textbox.Focus();
					textbox.SelectAll();
					return true;
				}
			}
			
			if(textbox.Text.Trim() != "")
			{
				if(is_integer)
				{
					try
					{
						li_value = Convert.ToInt32(textbox.Text);
					}
					catch(Exception)
					{
						err.alert_notInteger();
						textbox.Focus();
						textbox.SelectAll();
						return true;
					}
				}
			}

			return false;
		}

		/// <summary>
		/// Check Text box value
		/// </summary>
		/// <param name="textbox">the textbox to check</param>
		/// <param name="is_null">set true if you want to check textbox is null</param>
		/// <param name="is_integer">set true if you want to check textbox.text is int type</param>
		/// <param name="min_value"></param>
		/// <returns>True: value incorrect; False: value correct</returns>
		/// <summary>
		public bool checkTextBox(TextBox textbox,bool is_null,bool is_integer,int min_value)
		{
			int li_value = 0;

			if(is_null)
			{
				if(textbox.Text.Trim() == "")
				{
					err.alert_nullData();
					textbox.Focus();
					textbox.SelectAll();
					return true;
				}
			}
			
			if(textbox.Text.Trim() != "")
			{
				if(is_integer)
				{
					try
					{
						li_value = Convert.ToInt32(textbox.Text);
					}
					catch(Exception)
					{
						err.alert_notInteger();
						textbox.Focus();
						textbox.SelectAll();
						return true;
					}
				}

				if(li_value < min_value)
				{
					err.alert_invalidData();
					textbox.Focus();
					textbox.SelectAll();
					return true;
				}
			}

			return false;
		}
		
		/// <summary>
		/// Check Text box value.
		/// <param name="textbox">the textbox to check</param>
		/// <param name="is_null">set true if you want to check textbox is null</param>
		/// <param name="is_integer">set true if you want to check textbox.text is int type</param>
		/// <param name="min_value"></param>
		/// <param name="max_value"></param>
		/// <returns>True: value incorrect; False: value correct</returns>
		/// </summary>
		public bool checkTextBox(TextBox textbox,bool is_null,bool is_integer,int min_value, int max_value)
		{
			int li_value = 0;

			if(is_null)
			{
				if(textbox.Text.Trim() == "")
				{
					err.alert_nullData();
					textbox.Focus();
					textbox.SelectAll();
					return true;
				}
			}
			
			if(textbox.Text.Trim() != "")
			{
				if(is_integer)
				{
					try
					{
						li_value = Convert.ToInt32(textbox.Text);
					}
					catch(Exception)
					{
						err.alert_notInteger();
						textbox.Focus();
						textbox.SelectAll();
						return true;
					}
				}

				if(li_value < min_value || li_value > max_value)
				{
					err.alert_invalidData();
					textbox.Focus();
					textbox.SelectAll();
					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// fixSQL: remove all white space and ' characters from inserted string
		/// </summary>
		/// <param name="ins_value"></param>
		/// <returns>converted string</returns>
		/// <summary>
		public string fixSQL(string ins_value)
		{
			string ls_return = null;
			if(ins_value != null)
			{
				ls_return = ins_value.Trim();
				ls_return = ls_return.Replace("'","");
			}
			
			return ls_return;
		}

		
		/// <summary>
		/// insert datatable dTbl to dataset ds with table name ins_tblName
		/// </summary>
		/// <param name="ds"></param>
		/// <param name="dTbl"></param>
		/// <param name="ins_tblName"></param>
		public void fillDataSetTable(DataSet ds,DataTable dTbl, string ins_tblName)
		{
			try
			{
				DataTable dTbl_new = dTbl.Copy();
				dTbl_new.TableName = ins_tblName;
				
				if(ds.Tables.Contains(ins_tblName))
				{
					ds.Tables[ins_tblName].Clear();
					for(int i = 0; i < dTbl.Rows.Count; i++)
					{
						DataRow drow = ds.Tables[ins_tblName].NewRow();
						for(int j = 0; j < dTbl.Columns.Count-1; j++)
							drow[j] = dTbl.Rows[i][j];
						ds.Tables[ins_tblName].Rows.Add(drow);
					}

				}
				else ds.Tables.Add(dTbl_new);
			}
			catch(Exception ex)
			{
				ErrorEntity err = new ErrorEntity();
				err.alert_string(ex.ToString());
			}
		}

		/// <summary>
		/// insert datatable dTbl to dataset ds with the same name with table inserted
		/// </summary>
		/// <param name="ds"></param>
		/// <param name="dTbl"></param>
		public void fillDataSetTable(DataSet ds,DataTable dTbl)
		{
			string ls_tblName = dTbl.TableName;
			try
			{
				DataTable dTbl_new = dTbl.Copy();
				dTbl_new.TableName = ls_tblName;
				
				if(ds.Tables.Contains(ls_tblName))
				{
					ds.Tables[ls_tblName].Clear();
					for(int i = 0; i < dTbl.Rows.Count; i++)
					{
						DataRow drow = ds.Tables[ls_tblName].NewRow();
						for(int j = 0; j < dTbl.Columns.Count; j++)
							drow[j] = dTbl.Rows[i][j];
						ds.Tables[ls_tblName].Rows.Add(drow);
					}
				}
				else ds.Tables.Add(dTbl_new);
			}
			catch(Exception ex)
			{
				ErrorEntity err = new ErrorEntity();
				err.alert_string(ex.ToString());
			}
		}

		/// <summary>
		/// add rows in datatable dTbl to dataset ds with the same table name in dataset
		/// </summary>
		/// <param name="ds"></param>
		/// <param name="dTbl"></param>
		/// <param name="ins_tblName"></param>
		public void addRowDataSetTable(DataSet ds,DataTable dTbl, string ins_tblName)
		{
			try
			{
				DataTable dTbl_new = dTbl.Copy();
				dTbl_new.TableName = ins_tblName;
				
				if(ds.Tables.Contains(ins_tblName))
				{
					for(int i = 0; i < dTbl.Rows.Count; i++)
					{
						DataRow drow = ds.Tables[ins_tblName].NewRow();
						for(int j = 0; j < dTbl.Columns.Count; j++)
							drow[j] = dTbl.Rows[i][j];
						try
						{
							ds.Tables[ins_tblName].Rows.Add(drow);
						}
						catch(Exception){}
					}

				}
				else ds.Tables.Add(dTbl_new);
			}
			catch(Exception ex)
			{
				ErrorEntity err = new ErrorEntity();
				err.alert_string(ex.ToString());
			}
		}// End of addRowDataSetTable pro

		public void addRowDataSetTable(DataSet ds,DataTable dTbl)
		{
			string ls_tblName = dTbl.TableName;
			try
			{
				DataTable dTbl_new = dTbl.Copy();
				dTbl_new.TableName = ls_tblName;
				
				if(ds.Tables.Contains(ls_tblName))
				{
					for(int i = 0; i < dTbl.Rows.Count; i++)
					{
						DataRow drow = ds.Tables[ls_tblName].NewRow();
						for(int j = 0; j < dTbl.Columns.Count; j++)
							drow[j] = dTbl.Rows[i][j];
						try
						{
							ds.Tables[ls_tblName].Rows.Add(drow);
						}
						catch(Exception){}
					}
				}
				else ds.Tables.Add(dTbl_new);
			}
			catch(Exception ex)
			{
				ErrorEntity err = new ErrorEntity();
				err.alert_string(ex.ToString());
			}
		}// End of addRowDataSetTable pro


		public void fillDataSetRelation(DataSet ds, string ins_parentTable, string ins_parentColumn,
		                                string ins_childTable, string ins_childColumn, string ins_relationName)
		{
			ds.Relations.Add(new System.Data.DataRelation(ins_relationName,ds.Tables[ins_parentTable].Columns[ins_parentColumn],ds.Tables[ins_childTable].Columns[ins_childColumn]));
		}

		public string getConnString()
		{
			string m_strConn = "Integrated Security = true; Initial Catalog = TKB; Data Source = huy";
			return m_strConn;

			//"workstation id=TIENNX;packet size=4096;user id=sctv;data source=\"192.168.1.252\";p" +
			//	"ersist security info=True;initial catalog=SCTV;password=sctv";
		}

		// use to read file, then create subsCode set like: subsCode1;subsCode2;subsCode3 ...
		// use for passive and active subscriber
		#region funtions are not used
		/*
        public void GetFileLog(string ins_fileName)
		{
			StreamReader sr = null;
			try
			{
				sr = new StreamReader(new FileStream(ins_fileName, FileMode.Open, FileAccess.Read));
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
				return;
			}
			string ls_currentLine = null;
			while( (ls_currentLine = sr.ReadLine()) != null)
			{
				string[] currentSet = ls_currentLine.Split(';');

				string sqlString = "INSERT INTO tbl_FileLog(MACADDR, UPLOAD, DOWNLOAD, DATEGET, MARK) VALUES('"
					+ currentSet[1] + "','"
					+ currentSet[6] + "','"
					+ currentSet[7] + "','"
					+ DateTime.Now.ToString() + "',0)";
				DataAccess da = new DataAccess();
				da.open();

				if(da.UpdateToDB(sqlString) != 1)
				{
					if(da.m_errCode == 2627)
					{
						MessageBox.Show("Get FileLog thất bại","Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
					else
					{
						MessageBox.Show("Thất bại!","Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
					}
					return;
				}
			}
			MessageBox.Show("Get Filelog thanh cong! ");
			
		}
		public string getSubsCodeFromFile(string ins_fileName)
		{
			StreamReader sr = null;
			try
			{
				sr = new StreamReader(new FileStream(ins_fileName,FileMode.Open,FileAccess.Read));
			}
			catch(Exception)
			{
				ErrorEntity err = new ErrorEntity();
				err.alert_string("Không thể truy cập file này");
				return null;
			}

			string ls_subsCodeSet = null;
			string ls_currentLine = null;
			while( (ls_currentLine = sr.ReadLine()) != null)
			{
				string[] currentSet = ls_currentLine.Split('#');

				if(currentSet.Length > 0)
				{
					if(currentSet[0] != "")
						ls_subsCodeSet += currentSet[0].Trim() + ";";
				}
			}

			if(ls_subsCodeSet != null && ls_subsCodeSet != "")
				ls_subsCodeSet = ls_subsCodeSet.Substring(0,ls_subsCodeSet.Length - 1);

			if(sr != null)
				sr.Close();

			return ls_subsCodeSet;
		}

		public string GetCustomerCode(string sDistrict, string sWard)
		{
			int nCount;
			sDistrict = sDistrict.Trim();
			sWard = sWard.Trim();
			string[] subStringDistrict = sDistrict.Split(' ');
			string[] subStringWard = sWard.Split(' ');
			string sPreCode = null;
			if (subStringDistrict[0].Trim() != "Quận")
			{
				sPreCode = sPreCode+ subStringDistrict[0].Substring(0,1);
				if (subStringDistrict.Length >1)
					sPreCode = sPreCode+ subStringDistrict[1].Substring(0,1);
				else
				{
					if(subStringDistrict[0].Length >1)
						sPreCode = sPreCode+ subStringDistrict[0].Substring(1,1);
					else
						sPreCode = "0" + sPreCode;
				}
				
			}
			else
			{
				sPreCode = sPreCode+ subStringDistrict[1].Substring(0,1);
				if (subStringDistrict.Length >2)
					sPreCode = sPreCode+ subStringDistrict[2].Substring(0,1);
				else
				{
					if(subStringDistrict[1].Length >1)
						sPreCode = sPreCode+ subStringDistrict[1].Substring(1,1);
					else
						sPreCode = "0" + sPreCode;
				}
				
			}
			
			if (subStringWard[0].Trim() != "Phường")
			{
				if (subStringWard.Length >1)
				{
					sPreCode = sPreCode+ subStringWard[0].Substring(0,1);
					sPreCode = sPreCode+ subStringWard[1].Substring(0,1);
				}
				else
				{
					if(subStringWard[0].Length >1)
						sPreCode = sPreCode+ subStringWard[0].Substring(0,2);
					else
						sPreCode = sPreCode + "0" + subStringWard[0].Substring(0,1);
				}
				
			}
			else
			{
				if (subStringWard.Length >2)
				{
					sPreCode = sPreCode+ subStringWard[1].Substring(0,1);
					sPreCode = sPreCode+ subStringWard[2].Substring(0,1);
				}
				else
				{
					if(subStringWard[1].Length >1)
						sPreCode = sPreCode+ subStringWard[1].Substring(0,2);
					else
						sPreCode = sPreCode + "0" + subStringWard[1].Substring(0,1);
				}
			}
		 */

		/*			sPreCode = sPreCode+ subStringDistrict[0].Substring(0,1);
			if (subStringDistrict.Length >1)
				sPreCode = sPreCode+ subStringDistrict[1].Substring(0,1);
			else
				sPreCode = sPreCode+ subStringDistrict[0].Substring(1,1);
			
			sPreCode = sPreCode+ subStringWard[0].Substring(0,1);
			if (subStringWard.Length >1)
				sPreCode = sPreCode+ subStringWard[1].Substring(0,1);
			else
				sPreCode = sPreCode+ subStringWard[0].Substring(1,1);
		 */
		/*
			string sSQLString = "SELECT count(Customers.cuscode) FROM CUSTOMERS WHERE Customers.cuscode like '%" + sPreCode +"%'";
			
			DataAccess da = new DataAccess();
			da.open();
			DataSet ds = da.GetDataSetFromDB(sSQLString);
			da.close();
			
			string prs_counter = ds.Tables[0].Rows[0][0].ToString();
			nCount = Convert.ToInt32(prs_counter);
			string sCusCode = null;
			while (true)
			{
				sCusCode = sPreCode + "00000000000000000000".Substring(0,codeleng - nCount.ToString().Length) + nCount.ToString();
				try
				{
					if (CheckCode(sCusCode,"CUSTOMERS"))
					{
						nCount++;//co the xay ra van de toi!- nhung rat it
						if (nCount == 10000)
							nCount = 0;
					}
					else
						break;
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message);
					break;
				}
			}
			da.close();
			return sCusCode;
		}

		public bool CheckCode(string sCode, string sTableName)
		{
			if (sTableName == "CUSTOMERS")
			{
				string sqlstring = "SELECT CUSTOMERS.CUSCODE FROM CUSTOMERS WHERE CUSCODE = '" + sCode + "'";
				DataAccess da = new DataAccess();
				da.open();
				DataSet ds = da.GetDataSetFromDB(sqlstring);
				if (ds.Tables[0].Rows.Count > 0)
					return true;
			}
			else
			{
				if(sTableName == "TBL_SUBSCRIBER")
				{
					string sqlstring = "SELECT TBL_SUBSCRIBER.SUBSCODE FROM TBL_SUBSCRIBER WHERE SUBSCODE = '" + sCode + "'";
					DataAccess da = new DataAccess();
					da.open();
					DataSet ds = da.GetDataSetFromDB(sqlstring);
					if (ds.Tables[0].Rows.Count > 0)
						return true;
				}
			}
			return false;
		}
		
		public string GetCustomerCode(string sPreCode)
		{
			string sSQLString = "SELECT count(Customers.cuscode) FROM CUSTOMERS WHERE Customers.cuscode like '%" + sPreCode +"%'";
			
			DataAccess da = new DataAccess();
			da.open();
			DataSet ds = da.GetDataSetFromDB(sSQLString);
						
			string sCounter = ds.Tables[0].Rows[0][0].ToString();
			int nCount = Convert.ToInt32(sCounter);
			nCount++;
			string sCusCode = null;
			while (true)
			{
				sCusCode = sPreCode + "00000000000000000000".Substring(0,codeleng - nCount.ToString().Length) + nCount.ToString();
				if (CheckCode(sCusCode,"CUSTOMERS"))
				{
					nCount++;//co the xay ra van de toi!- nhung rat it
					if (nCount == 10000)
						nCount = 0;
				}
				else
					break;
			}
			da.close();
			return sCusCode;
		}

		public string GetSubscriberCode(string sDistrict, string sWard)
		{
			int nCount;
			sDistrict = sDistrict.Trim();
			string[] subStringDistrict = sDistrict.Split(' ');
			sWard = sWard.Trim();
			string[] subStringWard = sWard.Split(' ');
			string sPreCode = null;
			if (subStringDistrict[0].Trim() != "Quận")
			{
				sPreCode = sPreCode+ subStringDistrict[0].Substring(0,1);
				if (subStringDistrict.Length >1)
					sPreCode = sPreCode+ subStringDistrict[1].Substring(0,1);
				else
				{
					if(subStringDistrict[0].Length >1)
						sPreCode = sPreCode+ subStringDistrict[0].Substring(1,1);
					else
						sPreCode = "0" + sPreCode;
				}
				
			}
			else
			{
				sPreCode = sPreCode+ subStringDistrict[1].Substring(0,1);
				if (subStringDistrict.Length >2)
					sPreCode = sPreCode+ subStringDistrict[2].Substring(0,1);
				else
				{
					if(subStringDistrict[1].Length >1)
						sPreCode = sPreCode+ subStringDistrict[1].Substring(1,1);
					else
						sPreCode = "0" + sPreCode;
				}
				
			}
			
			if (subStringWard[0].Trim() != "Phường")
			{
				if (subStringWard.Length >1)
				{
					sPreCode = sPreCode+ subStringWard[0].Substring(0,1);
					sPreCode = sPreCode+ subStringWard[1].Substring(0,1);
				}
				else
				{
					if(subStringWard[0].Length >1)
						sPreCode = sPreCode+ subStringWard[0].Substring(0,2);
					else
						sPreCode = sPreCode + "0" + subStringWard[0].Substring(0,1);
				}
				
			}
			else
			{
				if (subStringWard.Length >2)
				{
					sPreCode = sPreCode+ subStringWard[1].Substring(0,1);
					sPreCode = sPreCode+ subStringWard[2].Substring(0,1);
				}
				else
				{
					if(subStringWard[1].Length > 1)
						sPreCode = sPreCode+ subStringWard[1].Substring(0,2);
					else
						sPreCode = sPreCode + "0" + subStringWard[1].Substring(0,1);
				}
			}


			string sSQLString = "SELECT count(TBL_SUBSCRIBER.SUBSCODE) FROM TBL_SUBSCRIBER WHERE TBL_SUBSCRIBER.SUBSCODE like '%" + sPreCode +"%'";
			
			DataAccess da = new DataAccess();
			da.open();
			DataSet ds = da.GetDataSetFromDB(sSQLString);
						
			string prs_counter = ds.Tables[0].Rows[0][0].ToString();
			nCount = Convert.ToInt32(prs_counter);
			nCount++;
			string sSubCode = null;
			while (true)
			{
				sSubCode = sPreCode + "00000000000000000000".Substring(0,codeleng - nCount.ToString().Length) + nCount.ToString();
				if (CheckCode(sPreCode, "TBL_SUBSCRIBER"))
				{
					nCount++;
					if (nCount == 10000)
						nCount = 0;
				}
				else break;
			}
			da.close();
			return sSubCode;
		}

		public string GetContractCode(string sDateSign)
		{
			string[] subStringDateSign = sDateSign.Split('/');
			string sPreCode = null;
			int nCount;
//			sPreCode = sPreCode + subStringDateSign[0];
			if (subStringDateSign[0].Length < 2)
			{
				sPreCode = "0" + subStringDateSign[0];
			}
			else
				sPreCode = sPreCode + subStringDateSign[0].Substring(0,2);

			if (subStringDateSign[1].Length < 2)
			{
				sPreCode = "0" + subStringDateSign[1];
			}else
				sPreCode = sPreCode + subStringDateSign[1].Substring(0,2);
			if (subStringDateSign[2].Length >= 2)
				sPreCode = sPreCode + subStringDateSign[2].Substring(subStringDateSign[2].Length-2,2);
			else
			{
				err.alert_string("Xem lai cach nhap ngay thang nam dd/mm/yyyy");
				return null;
			}
			
			string sSQLString = "SELECT count(CONTRACTS.CONTRACTNO) FROM CONTRACTS WHERE CONTRACTS.CONTRACTNO like '%" + sPreCode +"%'";
			DataAccess da = new DataAccess();
			da.open();
			DataSet ds = da.GetDataSetFromDB(sSQLString);
			da.close();
			
			string prs_counter = ds.Tables[0].Rows[0][0].ToString();
			nCount = Convert.ToInt32(prs_counter);
			nCount++;
			return sPreCode + "00000000000000000000".Substring(0,codeleng - nCount.ToString().Length) + nCount.ToString();
		}

		public string GetSubscriberCode(string sWardID)
		{
			string sSqlString = "SELECT TBL_DISTRICT.DIST_NAME, TBL_WARD.WARD_NAME FROM TBL_DISTRICT INNER JOIN TBL_WARD ON TBL_DISTRICT.DIST_ID = TBL_WARD.DIST_ID WHERE (TBL_WARD.WARD_ID = " + sWardID.Trim() + ")";
			DataAccess da = new DataAccess();
			da.open();
			DataSet ds = da.GetDataSetFromDB(sSqlString);
			da.close();
			return GetSubscriberCode(ds.Tables[0].Rows[0][0].ToString(),ds.Tables[0].Rows[0][1].ToString());
		}

		public string GetMacAdd(string ins_SubID)
		{
			string sqlstring = "SELECT TOP 1 * FROM TBL_MAC_SUBSCRIBER WHERE SUBSID = " + ins_SubID + " ORDER BY INTAL_DATE";
			DataAccess da = new DataAccess();
			da.open();
			DataSet ds = da.GetDataSetFromDB(sqlstring);
			if ((ds.Tables[0] != null)&(ds.Tables[0].Rows.Count == 1)//&& (ds.Tables[0].Rows[0][1].ToString().Length == 17))
				return ds.Tables[0].Rows[0][1].ToString();
			return null;
		}// End of update pro

		public bool CheckCashCurrentSubs(string ins_SubsID)
		{
			DataSet ds;
			DataAccess da = new DataAccess();
			da.open();
			
			string sSqlString = "SELECT *FROM TBL_CASH_CURRENT WHERE SUBSID =" + ins_SubsID;
			
			try
			{
				ds = da.GetDataSetFromDB(sSqlString);
//				DataSet ds = SqlHelper.ExecuteDataset(sqlConnection_main, CommandType.Text,	sSqlString);
				if (ds.Tables[0].Rows.Count == 0)
				{
					sSqlString = "INSERT INTO TBL_CASH_CURRENT (SUBSID,MONEY_SECURITY,MONEY_TARIFF,MONEY_PROVISONLLY)values (" + ins_SubsID + ",0,0,0)";
					int rs = da.UpdateToDB(sSqlString);
				}
			}
			catch (Exception ex)
			{
				err.alert_string("Kiem tra quy tien mat bi noi" + ex.Message);
				return false;
			}
			return true;
		}

		public bool ConvertCode(string sPreCode, string ins_CusID)
		{
			int nCount;
			string sqlString = "SELECT count(CUSTOMERS.CUSCODE) FROM CUSTOMERS WHERE CUSCODE LIKE '%" + sPreCode + "%'";
			DataAccess da = new DataAccess();
			da.open();
			DataSet ds = da.GetDataSetFromDB(sqlString);
			
			string prs_counter = ds.Tables[0].Rows[0][0].ToString();
			nCount = Convert.ToInt32(prs_counter);
			nCount++;
			
			string sCusCode = sPreCode + "00000000000000000000".Substring(0,codeleng - nCount.ToString().Length) + nCount.ToString();
			
			sqlString = "UPDATE CUSTOMERS SET"
				+ " CUSCODE = '" + sCusCode +"' WHERE"
				+ " CUSID ='" + ins_CusID + "'";
			
//			DataAccess da = new DataAccess();

			bool lb_return = true;
			if(da.UpdateToDB(sqlString) != 1)
			{
				lb_return = false;
				MessageBox.Show("convert thất bại!\n\r\n\rSystem Caution: " + da.m_strErr + "","Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			da.close();

			return lb_return;
        }
		 */
		#endregion
	}//end class Support
	#endregion

	

    #region CheckBD Entity

    public class CheckBD
    {
        ErrorEntity err;
        Support sp;
        string strConnect1 = "";
        string strConnect2 = "";
        public CheckBD(string ins_strConnect1, string ins_strConnect2)
        {
            strConnect1 = ins_strConnect1;
            strConnect2 = ins_strConnect2;
            err = new ErrorEntity();
            sp = new Support();
        }
        /// <summary>
        /// Get all rows in this table, return true if have rows, else return false
        /// </summary>
        /// <param name="ds_Bulk"></param>
        /// <param name="TableName"></param>
        /// <returns></returns>
        public bool getTableNameDB1(DataSet ds_Bulk, string TableName)
        {
           
            string sqlString = "sp_tables";

            //sqlString="select * from event where busnumber = 'b1' order by tstamp";
            //sqlString = "select * from dbo.VehiclesLive where busnumber = 'BE3426' order by tstamp";

            DataAccess da = new DataAccess(strConnect1);
            da.open();
            if (da.m_isConnected)
            {
                DataSet ds = da.GetDataSetFromDB(sqlString);
                if (da.testExistValueInTableAndHaveRow(ds))
                {
                    da.close();
                    sp.fillDataSetTable(ds_Bulk, ds.Tables[0], TableName);
                    return true;
                }
                da.close();
            }
            else
            {
                MessageBox.Show("Error: " + da.m_strErr);
            }
            return false;
        }

        public bool getTableNameDB2(DataSet ds_Bulk, string TableName)
        {

            string sqlString = "sp_tables";

            //sqlString="select * from event where busnumber = 'b1' order by tstamp";
            //sqlString = "select * from dbo.VehiclesLive where busnumber = 'BE3426' order by tstamp";

            DataAccess da = new DataAccess(strConnect2);
            da.open();
            if (da.m_isConnected)
            {
                DataSet ds = da.GetDataSetFromDB(sqlString);
                if (da.testExistValueInTableAndHaveRow(ds))
                {
                    da.close();
                    sp.fillDataSetTable(ds_Bulk, ds.Tables[0], TableName);
                    return true;
                }
                da.close();
            }
            else
            {
                MessageBox.Show("Error: " + da.m_strErr);
            }
            return false;
        }

        public bool getColumnsDB1(DataSet ds_Bulk, string TableName, string ins_tableNameToGet)
        {

            string sqlString = "sp_columns " + ins_tableNameToGet;

            //sqlString="select * from event where busnumber = 'b1' order by tstamp";
            //sqlString = "select * from dbo.VehiclesLive where busnumber = 'BE3426' order by tstamp";

            DataAccess da = new DataAccess(strConnect1);
            da.open();
            if (da.m_isConnected)
            {
                DataSet ds = da.GetDataSetFromDB(sqlString);
                if (da.testExistValueInTableAndHaveRow(ds))
                {
                    da.close();
                    sp.fillDataSetTable(ds_Bulk, ds.Tables[0], TableName);
                    return true;
                }
                da.close();
            }
            else
            {
                MessageBox.Show("Error: " + da.m_strErr);
            }
            return false;
        }

        public bool getColumnsDB2(DataSet ds_Bulk, string TableName, string ins_tableNameToGet)
        {

            string sqlString = "sp_columns " + ins_tableNameToGet;

            //sqlString="select * from event where busnumber = 'b1' order by tstamp";
            //sqlString = "select * from dbo.VehiclesLive where busnumber = 'BE3426' order by tstamp";

            DataAccess da = new DataAccess(strConnect2);
            da.open();
            if (da.m_isConnected)
            {
                DataSet ds = da.GetDataSetFromDB(sqlString);
                if (da.testExistValueInTableAndHaveRow(ds))
                {
                    da.close();
                    sp.fillDataSetTable(ds_Bulk, ds.Tables[0], TableName);
                    return true;
                }
                da.close();
            }
            else
            {
                MessageBox.Show("Error: " + da.m_strErr);
            }
            return false;
        }

        public bool getTable1(DataSet ds_Bulk, string TableName, string ins_tbNameToGet)
        {

            string sqlString = "SELECT * FROM " + ins_tbNameToGet;
            switch (ins_tbNameToGet.ToUpper())
            {
                case "EVENT":
                    sqlString = "SELECT BusNumber,TStamp,Event,Condition,X,Y,StopRunID,SchoolCode,Description,Location,PlannedStopSeq,EarlyLate,WindowEarly,WindowLate,EventID,ScheduledArrival,RunID FROM " + ins_tbNameToGet;
                    sqlString += " order by BusNumber, TStamp, StopRunID, event";
                    break;
                case "RUNDIRS":
                    sqlString = "SELECT RunRoute, X, Y, StopID, TotalMileage, Node From " + ins_tbNameToGet;
                    sqlString += " order by RunRoute, Sequence";
                    break;
                case "STOPSLIVE":
                    sqlString = "SELECT StopRunID, Status, CurrentETA, ActualArrival, EarlyLate, TStamp, Condition, Duration, Departure, LiveX, LiveY FROM " + ins_tbNameToGet;
                    sqlString += " ORDER BY StopRunID, ActualArrival, TStamp, StopsLiveID";
                    break;
                case "VEHICLEMAP":
                    sqlString += " order by vmTrackerVehicleID, vmVendorVehicleID";
                    break;
                default:
                    break;
            }
            

            //sqlString="select * from event where busnumber = 'b1' order by tstamp";
            //sqlString = "select * from dbo.VehiclesLive where busnumber = 'BE3426' order by tstamp";

            DataAccess da = new DataAccess(strConnect1);
            da.open();
            if (da.m_isConnected)
            {
                DataSet ds = da.GetDataSetFromDB(sqlString);
                if (da.testExistValueInTableAndHaveRow(ds))
                {
                    da.close();
                    sp.fillDataSetTable(ds_Bulk, ds.Tables[0], TableName);
                    return true;
                }
                da.close();
            }
            else
            {
                MessageBox.Show("Error: " + da.m_strErr);
            }
            return false;
        }

        public bool getTable2(DataSet ds_Bulk, string TableName, string ins_tbNameToGet)
        {

            string sqlString = "SELECT * FROM " + ins_tbNameToGet;
            switch (ins_tbNameToGet.ToUpper())
            {
                case "EVENT":
                    sqlString = "SELECT BusNumber,TStamp,Event,Condition,X,Y,StopRunID,SchoolCode,Description,Location,PlannedStopSeq,EarlyLate,WindowEarly,WindowLate,EventID,ScheduledArrival,RunID FROM " + ins_tbNameToGet;
                    sqlString += " order by BusNumber, TStamp, StopRunID, event";
                    break;
                case "RUNDIRS":
                    sqlString = "SELECT RunRoute, X, Y, StopID, TotalMileage, Node From " + ins_tbNameToGet;
                    sqlString += " order by RunRoute, Sequence";                    
                    break;
                case "STOPSLIVE":
                    sqlString = "SELECT StopRunID, Status, CurrentETA, ActualArrival, EarlyLate, TStamp, Condition, Duration, Departure, LiveX, LiveY FROM " + ins_tbNameToGet;
                    sqlString += " ORDER BY StopRunID, ActualArrival, TStamp, StopsLiveID";
                    break;
                case "VEHICLEMAP":
                    sqlString += " order by vmTrackerVehicleID, vmVendorVehicleID";
                    break;
                default:
                    break;
            }
            //sqlString="select * from event where busnumber = 'b1' order by tstamp";
            //sqlString = "select * from dbo.VehiclesLive where busnumber = 'BE3426' order by tstamp";
           

            DataAccess da = new DataAccess(strConnect2);
            da.open();
            if (da.m_isConnected)
            {
                DataSet ds = da.GetDataSetFromDB(sqlString);
                if (da.testExistValueInTableAndHaveRow(ds))
                {
                    da.close();
                    sp.fillDataSetTable(ds_Bulk, ds.Tables[0], TableName);
                    return true;
                }
                da.close();
            }
            else
            {
                MessageBox.Show("Error: " + da.m_strErr);
            }
            return false;
        }
    }

    #endregion

    #region CheckBDFox Entity

    public class CheckBDFox
    {
        ErrorEntity err;
        Support sp;
        string strConnect1 = "";
        string strConnect2 = "";
        public CheckBDFox(string ins_strConnect1, string ins_strConnect2)
        {
            strConnect1 = ins_strConnect1;
            strConnect2 = ins_strConnect2;
            err = new ErrorEntity();
            sp = new Support();
        }
        /// <summary>
        /// Get all rows in this table, return true if have rows, else return false
        /// </summary>
        /// <param name="ds_Bulk"></param>
        /// <param name="TableName"></param>
        /// <returns></returns>
        public bool getTableNameDB1(DataSet ds_Bulk, string TableName)
        {

            string sqlString = "sp_tables";

            //sqlString="select * from event where busnumber = 'b1' order by tstamp";
            //sqlString = "select * from dbo.VehiclesLive where busnumber = 'BE3426' order by tstamp";

            DataAccess da = new DataAccess(strConnect1);
            da.open();
            if (da.m_isConnected)
            {
                DataSet ds = da.GetDataSetFromDB(sqlString);
                if (da.testExistValueInTableAndHaveRow(ds))
                {
                    da.close();
                    sp.fillDataSetTable(ds_Bulk, ds.Tables[0], TableName);
                    return true;
                }
                da.close();
            }
            else
            {
                MessageBox.Show("Error: " + da.m_strErr);
            }
            return false;
        }

        public bool getTableNameDB2(DataSet ds_Bulk, string TableName)
        {

            string sqlString = "sp_tables";

            //sqlString="select * from event where busnumber = 'b1' order by tstamp";
            //sqlString = "select * from dbo.VehiclesLive where busnumber = 'BE3426' order by tstamp";

            DataAccess da = new DataAccess(strConnect2);
            da.open();
            if (da.m_isConnected)
            {
                DataSet ds = da.GetDataSetFromDB(sqlString);
                if (da.testExistValueInTableAndHaveRow(ds))
                {
                    da.close();
                    sp.fillDataSetTable(ds_Bulk, ds.Tables[0], TableName);
                    return true;
                }
                da.close();
            }
            else
            {
                MessageBox.Show("Error: " + da.m_strErr);
            }
            return false;
        }

        public bool getColumnsDB1(DataSet ds_Bulk, string TableName, string ins_tableNameToGet)
        {

            string sqlString = "sp_columns " + ins_tableNameToGet;

            //sqlString="select * from event where busnumber = 'b1' order by tstamp";
            //sqlString = "select * from dbo.VehiclesLive where busnumber = 'BE3426' order by tstamp";

            DataAccess da = new DataAccess(strConnect1);
            da.open();
            if (da.m_isConnected)
            {
                DataSet ds = da.GetDataSetFromDB(sqlString);
                if (da.testExistValueInTableAndHaveRow(ds))
                {
                    da.close();
                    sp.fillDataSetTable(ds_Bulk, ds.Tables[0], TableName);
                    return true;
                }
                da.close();
            }
            else
            {
                MessageBox.Show("Error: " + da.m_strErr);
            }
            return false;
        }

        public bool getColumnsDB2(DataSet ds_Bulk, string TableName, string ins_tableNameToGet)
        {

            string sqlString = "sp_columns " + ins_tableNameToGet;

            //sqlString="select * from event where busnumber = 'b1' order by tstamp";
            //sqlString = "select * from dbo.VehiclesLive where busnumber = 'BE3426' order by tstamp";

            DataAccess da = new DataAccess(strConnect2);
            da.open();
            if (da.m_isConnected)
            {
                DataSet ds = da.GetDataSetFromDB(sqlString);
                if (da.testExistValueInTableAndHaveRow(ds))
                {
                    da.close();
                    sp.fillDataSetTable(ds_Bulk, ds.Tables[0], TableName);
                    return true;
                }
                da.close();
            }
            else
            {
                MessageBox.Show("Error: " + da.m_strErr);
            }
            return false;
        }

        public bool getTable1(DataSet ds_Bulk, string TableName, string ins_tbNameToGet)
        {

            string sqlString = "SELECT * FROM " + ins_tbNameToGet;
            switch (ins_tbNameToGet.ToUpper())
            {
                case "EVENT":
                    sqlString = "SELECT BusNumber,TStamp,Event,Condition,X,Y,StopRunID,SchoolCode,Description,Location,PlannedStopSeq,EarlyLate,WindowEarly,WindowLate,EventID,ScheduledArrival,RunID FROM " + ins_tbNameToGet;
                    sqlString += " order by BusNumber, TStamp, StopRunID, event";
                    break;
                case "RUNDIRS":
                    sqlString = "SELECT RunRoute, X, Y, StopID, TotalMileage, Node From " + ins_tbNameToGet;
                    sqlString += " order by RunRoute, Sequence";
                    break;
                case "STOPSLIVE":
                    sqlString = "SELECT StopRunID, Status, CurrentETA, ActualArrival, EarlyLate, TStamp, Condition, Duration, Departure, LiveX, LiveY FROM " + ins_tbNameToGet;
                    sqlString += " ORDER BY StopRunID, ActualArrival, TStamp, StopsLiveID";
                    break;
                case "VEHICLEMAP":
                    sqlString += " order by vmTrackerVehicleID, vmVendorVehicleID";
                    break;
                default:
                    break;
            }


            //sqlString="select * from event where busnumber = 'b1' order by tstamp";
            //sqlString = "select * from dbo.VehiclesLive where busnumber = 'BE3426' order by tstamp";

            DataAccess da = new DataAccess(strConnect1);
            da.open();
            if (da.m_isConnected)
            {
                DataSet ds = da.GetDataSetFromDB(sqlString);
                if (da.testExistValueInTableAndHaveRow(ds))
                {
                    da.close();
                    sp.fillDataSetTable(ds_Bulk, ds.Tables[0], TableName);
                    return true;
                }
                da.close();
            }
            else
            {
                MessageBox.Show("Error: " + da.m_strErr);
            }
            return false;
        }

        public bool getTable2(DataSet ds_Bulk, string TableName, string ins_tbNameToGet)
        {

            string sqlString = "SELECT * FROM " + ins_tbNameToGet;
            switch (ins_tbNameToGet.ToUpper())
            {
                case "EVENT":
                    sqlString = "SELECT BusNumber,TStamp,Event,Condition,X,Y,StopRunID,SchoolCode,Description,Location,PlannedStopSeq,EarlyLate,WindowEarly,WindowLate,EventID,ScheduledArrival,RunID FROM " + ins_tbNameToGet;
                    sqlString += " order by BusNumber, TStamp, StopRunID, event";
                    break;
                case "RUNDIRS":
                    sqlString = "SELECT RunRoute, X, Y, StopID, TotalMileage, Node From " + ins_tbNameToGet;
                    sqlString += " order by RunRoute, Sequence";
                    break;
                case "STOPSLIVE":
                    sqlString = "SELECT StopRunID, Status, CurrentETA, ActualArrival, EarlyLate, TStamp, Condition, Duration, Departure, LiveX, LiveY FROM " + ins_tbNameToGet;
                    sqlString += " ORDER BY StopRunID, ActualArrival, TStamp, StopsLiveID";
                    break;
                case "VEHICLEMAP":
                    sqlString += " order by vmTrackerVehicleID, vmVendorVehicleID";
                    break;
                default:
                    break;
            }
            //sqlString="select * from event where busnumber = 'b1' order by tstamp";
            //sqlString = "select * from dbo.VehiclesLive where busnumber = 'BE3426' order by tstamp";


            DataAccess da = new DataAccess(strConnect2);
            da.open();
            if (da.m_isConnected)
            {
                DataSet ds = da.GetDataSetFromDB(sqlString);
                if (da.testExistValueInTableAndHaveRow(ds))
                {
                    da.close();
                    sp.fillDataSetTable(ds_Bulk, ds.Tables[0], TableName);
                    return true;
                }
                da.close();
            }
            else
            {
                MessageBox.Show("Error: " + da.m_strErr);
            }
            return false;
        }

        public bool getTableRunDirsDaily(DataSet ds_Bulk)
        {

            string sqlString = "select rd.runroute, rd.sequence, rd.stopid, rd.node, rd.directdesc "
+ "   FROM RunDirs rd  "
+ "   where rd.stopid <> ''"
+ "   order by "
+ "   rd.runroute, rd.sequence, rd.stopid";
            


            //sqlString="select * from event where busnumber = 'b1' order by tstamp";
            //sqlString = "select * from dbo.VehiclesLive where busnumber = 'BE3426' order by tstamp";

            DataAccess da = new DataAccess(strConnect1);
            da.open();
            if (da.m_isConnected)
            {
                DataSet ds = da.GetDataSetFromDB(sqlString);
                if (da.testExistValueInTableAndHaveRow(ds))
                {
                    da.close();
                    sp.fillDataSetTable(ds_Bulk, ds.Tables[0], "RunDirsDaily");
                    return true;
                }
                da.close();
            }
            else
            {
                MessageBox.Show("Error: " + da.m_strErr);
            }
            return false;
        }

        public bool getTableRunDirsFox(DataSet ds_Bulk)
        {

            string sqlString = "SELECT "
+ "   dirdesc.run_id,"
+ "   dirdesc.dir_pos,"
+ "   dirdesc.node1,"
+ "   dirdesc.node2,"
+ "   dirdesc.mileage, "
+ "   stops.stop_id___,"
+ "   dirdesc.fstop_id,"
+ "   dirdesc.cover1,"
+ "   stops.x_________,"
+ "   stops.y_________ , "
+ "   dirdesc.direct_des,"
+ "   rundir2.acc_mileag,"
+ "   rundir2.dacc_milea,"
+ "   rundir2.cacc_milea"
+ "  FROM "
+ "   rundir2,"
+ "   dirdesc,"
+ "   runtrn,"
+ "   stptrn,"
+ "   stops"
+ "  WHERE"
+ "   stops.stop_id___ =  stptrn.stop_id___"
+ "   and runtrn.fstop_id__ = stptrn.fstop_id__"
+ "   and runtrn.stpruncomb = rundir2.stpruncomb"
+ "   AND rundir2.covercomb_ = dirdesc.covercomb"
+ "   AND runtrn.run_id____ != ' '"
+ "  ORDER BY dirdesc.run_id, dirdesc.stop_pos,dirdesc.dir_pos";



            //sqlString="select * from event where busnumber = 'b1' order by tstamp";
            //sqlString = "select * from dbo.VehiclesLive where busnumber = 'BE3426' order by tstamp";

            DataAccessFox da = new DataAccessFox(strConnect2);
            da.open();
            if (da.m_isConnected)
            {
                DataSet ds = da.GetDataSetFromDB(sqlString);
                if (da.testExistValueInTableAndHaveRow(ds))
                {
                    da.close();
                    sp.fillDataSetTable(ds_Bulk, ds.Tables[0], "RunDirsFox");
                    return true;
                }
                da.close();
            }
            else
            {
                MessageBox.Show("Error: " + da.m_strErr);
            }
            return false;
        }
    }

    #endregion



    #region Auto Entity

    public class Auto
    {
        ErrorEntity err;
        Support sp;
        string strConnect = "";
        public Auto(string ins_strConnect)
        {
            strConnect = ins_strConnect;
            err = new ErrorEntity();
            sp = new Support();
        }
        /// <summary>
        /// Get all rows in this table, return true if have rows, else return false
        /// </summary>
        /// <param name="ds_Bulk"></param>
        /// <param name="TableName"></param>
        /// <returns></returns>
        public bool getEvent(DataSet ds_Bulk, string TableName)
        {
            /*
             * 
             * select * 
from event as e
where 
e.event in ('Bus Arrived','Bus Departed', 'At School', 'Leave School')
order by e.schoolcode, e.busnumber, e.tstamp, event

             *
             * 
             * 
             */


            string sqlString = "SELECT VehNumber,TStamp, '' as NewTStamp,Event,Condition,LiveLongitude,LiveLatitude,StopRunID,Description,Location,PlannedStopSeq,EarlyLate,EventID,ScheduledArrival,RunID,ID,Fill from event e order by e.tstamp";

            DataAccess da = new DataAccess(strConnect);
            da.open();
            if (da.m_isConnected)
            {
                DataSet ds = da.GetDataSetFromDB(sqlString);
                if (da.testExistValueInTableAndHaveRow(ds))
                {
                    da.close();
                    sp.fillDataSetTable(ds_Bulk, ds.Tables[0], TableName);
                    return true;
                }
                da.close();
            }
            else
            {
                MessageBox.Show("Error: " + da.m_strErr);
            }
            da.close();
            return false;
        }
        public bool getEventResult(DataSet ds_Bulk, string TableName)
        {
            /*
             * 
             * select * 
from event as e
where 
e.event in ('Bus Arrived','Bus Departed', 'At School', 'Leave School')
order by e.schoolcode, e.busnumber, e.tstamp, event

             *
             * 
             * 
             */


            string sqlString = "SELECT * from event e order by e.tstamp";

            DataAccess da = new DataAccess(strConnect);
            da.open();
            if (da.m_isConnected)
            {
                DataSet ds = da.GetDataSetFromDB(sqlString);
                if (da.testExistValueInTableAndHaveRow(ds))
                {
                    da.close();
                    sp.fillDataSetTable(ds_Bulk, ds.Tables[0], TableName);
                    return true;
                }
                da.close();
            }
            else
            {
                MessageBox.Show("Error: " + da.m_strErr);
            }
            da.close();
            return false;
        }
        public bool getCountResult(DataSet ds_Bulk, string TableName)
        {
            /*
             * 
             * select * 
from event as e
where 
e.event in ('Bus Arrived','Bus Departed', 'At School', 'Leave School')
order by e.schoolcode, e.busnumber, e.tstamp, event

             *
             * 
             * 
             */


            string sqlString = "SELECT count(*) from event e order by e.tstamp";

            DataAccess da = new DataAccess(strConnect);
            da.open();
            if (da.m_isConnected)
            {
                DataSet ds = da.GetDataSetFromDB(sqlString);
                if (da.testExistValueInTableAndHaveRow(ds))
                {
                    da.close();
                    sp.fillDataSetTable(ds_Bulk, ds.Tables[0], TableName);
                    return true;
                }
                da.close();
            }
            else
            {
                MessageBox.Show("Error: " + da.m_strErr);
            }
            da.close();
            return false;
        }
        public bool getBusRunId(DataSet ds_Bulk, string TableName)
        {
            /*
             * 
             * select * 
from event as e
where 
e.event in ('Bus Arrived','Bus Departed', 'At School', 'Leave School')
order by e.schoolcode, e.busnumber, e.tstamp, event

             *
             * 
             * 
             */


            string sqlString = "select distinct vr.vehnumber, vr.runid from FTVehRuns vr inner join ftstops s on s.runid = vr.runid order by 1,2";

            DataAccess da = new DataAccess(strConnect);
            da.open();
            if (da.m_isConnected)
            {
                DataSet ds = da.GetDataSetFromDB(sqlString);
                if (da.testExistValueInTableAndHaveRow(ds))
                {
                    da.close();
                    sp.fillDataSetTable(ds_Bulk, ds.Tables[0], TableName);
                    return true;
                }
                da.close();
            }
            else
            {
                MessageBox.Show("Error: " + da.m_strErr);
            }
            da.close();
            return false;
        }

        public bool getSADByBusNumbers(DataSet ds_Bulk, string TableName, string busNumbers)
        {

            /*
             * 
             * select * 
from event as e
where 
e.event in ('Bus Arrived','Bus Departed', 'At School', 'Leave School')
order by e.schoolcode, e.busnumber, e.tstamp, event

             *
             * 
             * 
             */


            string sqlString = "SELECT *"
                + " FROM event as e"
                + " inner join stops s"
                + " on e.stoprunid = s.stoprunid"
                + " WHERE e.event in ('Bus Arrived','Bus Departed', 'At School', 'Leave School')"
                + " and s.stopid not like '%uns%'"
                + " and e.busnumber in (" + busNumbers + ")"
                + " order by e.schoolcode, e.tstamp, e.event";

            DataAccess da = new DataAccess(strConnect);
            da.open();
            if (da.m_isConnected)
            {
                DataSet ds = da.GetDataSetFromDB(sqlString);
                if (da.testExistValueInTableAndHaveRow(ds))
                {
                    da.close();
                    sp.fillDataSetTable(ds_Bulk, ds.Tables[0], TableName);
                    return true;
                }
                da.close();
            }
            else
            {
                MessageBox.Show("Error: " + da.m_strErr);
            }
            da.close();
            return false;
        }

        public bool getSADBySchoolCodes(DataSet ds_Bulk, string TableName, string schoolCodes)
        {

            /*
             * 
             * select * 
from event as e
where 
e.event in ('Bus Arrived','Bus Departed', 'At School', 'Leave School')
order by e.schoolcode, e.busnumber, e.tstamp, event

             *
             * 
             * 
             */


            string sqlString = "SELECT *"
                + " FROM event as e"
                + " inner join stops s"
                + " on e.stoprunid = s.stoprunid"
                + " WHERE e.event in ('Bus Arrived','Bus Departed', 'At School', 'Leave School')"
                + " and s.stopid not like '%uns%'"
                + " and e.SchoolCode in (" + schoolCodes + ")"
                + " order by e.schoolcode, e.tstamp, e.event";

            DataAccess da = new DataAccess(strConnect);
            da.open();
            if (da.m_isConnected)
            {
                DataSet ds = da.GetDataSetFromDB(sqlString);
                if (da.testExistValueInTableAndHaveRow(ds))
                {
                    da.close();
                    sp.fillDataSetTable(ds_Bulk, ds.Tables[0], TableName);
                    return true;
                }
                da.close();
            }
            else
            {
                MessageBox.Show("Error: " + da.m_strErr);
            }
            da.close();
            return false;
        }

        /// <summary>
        /// Get one row in this table if match the id input, return true if have row, else return false
        /// </summary>
        /// <param name="ds_Bulk"></param>
        /// <param name="TableName"></param>
        /// <param name="placeId"></param>
        /// <returns></returns>
        public bool getBusNumber(DataSet ds_Bulk, string TableName)
        {
            /*
             * select distinct t.BusNumber
from
(
select distinct BusNumber
from Event

union

select distinct BusNumber
from VehicleLocation
) as t
order by t.BusNumber
             */
            string sqlString = "SELECT distinct t.BusNumber"
                + " FROM"
                + " ("
                + " select distinct BusNumber"
            + " from Event"
            + " union"
            + " select distinct BusNumber"
            + " from VehicleLocation"
            + " ) as t"
            + " order by t.BusNumber";

            DataAccess da = new DataAccess(strConnect);
            da.open();
            if (da.m_isConnected)
            {
                DataSet ds = da.GetDataSetFromDB(sqlString);
                if (da.testExistValueInTableAndHaveRow(ds))
                {
                    da.close();
                    sp.fillDataSetTable(ds_Bulk, ds.Tables[0], TableName);
                    return true;
                }
                da.close();
            }
            else
            {
                MessageBox.Show("Error: " + da.m_strErr);
            }
            return false;
        }


        /// <summary>
        /// insert values to this table. Return 1 if it's successful, else return 0.
        /// </summary>
        /// <param name="ins_id"></param>
        /// <param name="ins_name"></param>
        /// <returns></returns>
        public int insert(string NAME_PLACE)
        {
            #region FixData
            //ID_PLACE = sp.fixSQL(ID_PLACE);
            NAME_PLACE = sp.fixSQL(NAME_PLACE);
            #endregion
            int result = 0;
            FuctionClass fn = new FuctionClass();
            //string id = Convert.ToString(fn.getMaxId("PLACE", "ID_PLACE") + 1;
            string sqlString = "INSERT INTO PLACE(ID_PLACE, NAME_PLACE) VALUES("
                + "" + fn.getNextId("PLACE", "ID_PLACE") + ","
                + "'" + NAME_PLACE + "')";

            DataAccess da = new DataAccess();
            da.open();
            if (da.UpdateToDB(sqlString) == 1)
                result = 1;
            else
                result = 0;
            da.close();
            return result;
        }

        /// <summary>
        /// update values in this table. Return 1 if it's successful, else return 0.
        /// </summary>
        /// <param name="ins_id"></param>
        /// <param name="ins_name"></param>
        /// <returns></returns>		
        public int update(string ins_oldId, string ins_newId, string ins_newName)
        {
            #region FixData
            ins_oldId = sp.fixSQL(ins_oldId);
            ins_newId = sp.fixSQL(ins_newId);
            ins_newName = sp.fixSQL(ins_newName);
            #endregion
            int result = 0;
            string sqlString = "UPDATE PLACE SET "
                + " ID_PLACE = " + ins_newId + ","
                + " NAME_PLACE = '" + ins_newName + "'"
                + " WHERE ID_PLACE=" + ins_oldId + "";

            DataAccess da = new DataAccess();
            da.open();
            if (da.UpdateToDB(sqlString) == 1)
                result = 1;
            else
                result = 0;
            da.close();
            return result;
        }

        /// <summary>
        /// Delete values in this table. Return 1 if it's successful, else return 0.
        /// </summary>
        /// <param name="ins_id"></param>
        /// <returns></returns>
        public int Delete(string ins_id)
        {
            int result = 0;
            string sqlString = "DELETE PLACE WHERE ID_PLACE = " + ins_id + "";
            DataAccess da = new DataAccess();
            da.open();
            if (da.UpdateToDB(sqlString) == 1)
                result = 1;
            else
                result = 0;
            da.close();
            return result;
        }
    }

    #endregion

    ///////////////////////////////////////////////////

	

}
