using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace AFD.MyLib
{
    class MyClsDBManager
    {
        public String ConnectionString = "";

        //connectionString ="Data Source=.\SQLEXPRESS;AttachDbFilename=|DataDirectory|\eStore.mdf;Integrated Security=True;User Instance=True" providerName="System.Data.SqlClient
        //Trusted_Connection=false;User ID=etm;Password=etm;
        //Data Source=.\SQLEXPRESS;AttachDbFilename=E:\Huy\projects\cus\dung\thamkhao\HVeStore\App_Data\eStore.mdf;Integrated Security=True;User Instance=True
        //public String ConnectionString = @"Data Source=XP-TESTING2\SQLEXPRESS;Initial Catalog=ESTORE;Trusted_Connection=false;User ID=etm;Password=etm;";
        //public String ConnectionString = @"Data Source=.\SQLEXPRESS;AttachDbFilename=D:\Users\My Documents\Visual Studio 2005\WebSites\HVeStore\App_Data\bobo.mdf;Integrated Security=True;User Instance=True";
        public MyClsDBManager(string ins_conn)
        {
            ConnectionString = ins_conn;
        }
        public SqlConnection GetConnection()
        {
            return new SqlConnection(ConnectionString);
        }

        public DataTable Fill(DataTable table, String sql, params Object[] parameters)
        {
            SqlCommand command = CreateCommand(sql, parameters);

            new SqlDataAdapter(command).Fill(table);
            command.Connection.Close();

            return table;
        }

        public DataTable Fill(DataTable table, String sql, CommandType ct, params Object[] parameters)
        {
            SqlCommand command = CreateCommand(sql, parameters);
            command.CommandType = ct;

            new SqlDataAdapter(command).Fill(table);
            command.Connection.Close();

            return table;
        }

        public DataTable GetData(String sql, params Object[] parameters)
        {
            return Fill(new DataTable(), sql, parameters);
        }

        public DataTable GetData(String sql, CommandType ct, params Object[] parameters)
        {
            return Fill(new DataTable(), sql, ct, parameters);
        }

        public int ExecuteNonQuery(String sql, params Object[] parameters)
        {
            SqlCommand command = CreateCommand(sql, parameters);

            command.Connection.Open();
            int rows = command.ExecuteNonQuery();
            command.Connection.Close();

            return rows;
        }

        public int ExecuteNonQuerySP(SqlCommand command)
        {
            //command = new SqlCommand(sql, GetConnection());
            command.Connection = GetConnection();
            //foreach (SqlParameter pItem in parameters)
            //    command.Parameters.Add(pItem);            
            command.CommandType = CommandType.StoredProcedure;
            command.Connection.Open();
            int rows = command.ExecuteNonQuery();
            command.Connection.Close();

            return rows;
        }

        public int ExecuteNonQuery(String sql, CommandType ct, params Object[] parameters)
        {
            SqlCommand command = CreateCommand(sql, parameters);
            command.CommandType = ct;            
            command.Connection.Open();
            int rows = command.ExecuteNonQuery();
            command.Connection.Close();

            return rows;
        }

        //private void setParameterOutput(SqlCommand command)
        //{
        //    foreach (SqlParameter pItem in command.Parameters)
        //    {
        //        if (pItem.ToString().Contains("Output"))
        //            pItem.Direction = ParameterDirection.Output;
        //    }
        //}

        //private void getValueParameterOutput(SqlCommand command, params Object[] param)
        //{
        //    for (int i = 1; i <= command.Parameters.Count; i++)
        //    {
        //        if (command.Parameters[i-1].ToString().Contains("Output"))
        //            param[(i*2) - 1] = command.Parameters[i-1].Value;
        //    }
        //}

        public object ExecuteScalar(String sql, params Object[] parameters)
        {
            SqlCommand command = CreateCommand(sql, parameters);

            command.Connection.Open();
            object value = command.ExecuteScalar();
            command.Connection.Close();

            return value;
        }

        private SqlCommand CreateCommand(String sql, params Object[] parameters)
        {
            SqlCommand command = new SqlCommand(sql, GetConnection());
            for (int i = 0; i < parameters.Length; i += 2)
            {
                command.Parameters.AddWithValue(parameters[i].ToString(), parameters[i + 1]);
            }
            return command;
        }

        public string fixSQL(string ins_value)
        {
            string ls_return = null;
            if (ins_value != null)
            {
                ls_return = ins_value.Trim();
                ls_return = ls_return.Replace("'", "");
            }

            return ls_return;
        }

    }
}
