using System;
using System.Globalization;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using AFD.HPLib;
using System.Threading;
using System.IO;
////using AFD.MyLib;
////using upstop.MyLib;
using System.Net;
using System.Web;
using System.Diagnostics;
using HPConFigXml;
using RealMultiThreading.MultiThreadEngine;
using System.Text.RegularExpressions;
//using WebAutomation.WebAutomationTasks;
using HPLib;
using AFD.HpLib;


namespace AFD
{
    public partial class frmMain : Form
    {
        //WEB BROWSER INTERGRATED
        public string strHttpContain;

        #region Asynchronous BackgroundWorker Thread
        private string m_strError = "";

        private BackgroundWorker m_AsyncWorker = new BackgroundWorker();

        private void doAsysc()
        {
            // If the background thread is running then clicking this
            // button causes a cancel, otherwise clicking this button
            // launches the background thread.
            if (m_AsyncWorker.IsBusy)
            {
                //btnGetLink.Enabled = false;
                txtLog.AppendText(DateTime.Now.ToString("hh:mm:ss tt ") + "Canceling..." + Environment.NewLine);

                // Notify the worker thread that a cancel has been requested.
                // The cancel will not actually happen until the thread in the 
                // DoWork checks the bwAsync.CancellationPending flag, for this
                // reason we set the label to "Canceling...", because we haven't 
                // actually cancelled yet.
                m_AsyncWorker.CancelAsync();
            }
            else
            {
                //if (string.IsNullOrEmpty(txtLinksToGet.Text))
                //{
                //    MessageBox.Show("Please enter links to get!");
                //    txtLog.SelectAll();
                //    txtLog.Focus();
                //    return;
                //}
                pgbStatus.Style = ProgressBarStyle.Marquee;

                btnGetLink.Text = "&Cancel";
                txtLog.AppendText(DateTime.Now.ToString("hh:mm:ss tt ") + "Running..." + Environment.NewLine);

                // Kickoff the worker thread to begin it's DoWork function.
                m_AsyncWorker.RunWorkerAsync();
            }
        }

        private bool setStatusMsg(BackgroundWorker bwAsync, DoWorkEventArgs e, string strMessage, bool isPrinTime, bool isAppendLine)
        {
            bool resultCancel = false;
            //txtLog.AppendText("Converting Long, Lat to X, Y";            
            if (isPrinTime)
                txtLog.AppendText(DateTime.Now.ToString("hh:mm:ss tt ") + strMessage);
            else
                txtLog.AppendText(strMessage);
            if (isAppendLine)
                txtLog.AppendText(Environment.NewLine);

            if (bwAsync.CancellationPending)
            {
                // Pause for bit to demonstrate that there is time between 
                // "Cancelling..." and "Canceled".
                Thread.Sleep(1200);

                // Set the e.Cancel flag so that the WorkerCompleted event
                // knows that the process was canceled.
                e.Cancel = true;
                resultCancel = true; ;
            }
            return resultCancel;
        }

        private void bwAsync_DoWork(object sender, DoWorkEventArgs e)
        {
            // The Sender is the BackgroundWorker object we need it to
            // report progress and check for cancellation.
            gb_isDownLoading = true;
            BackgroundWorker bwAsync = sender as BackgroundWorker;

            // Do some long running operation here
            //int iCount = new Random().Next(20, 50);
            //for (int i = 0; i < iCount; i++)
            //{
            //    // Working...
            //    Thread.Sleep(100);

            //    // Periodically report progress to the main thread so that it can
            //    // update the UI.  In most cases you'll just need to send an integer
            //    // that will update a ProgressBar, but there is an OverLoad for the
            //    // ReportProgress function so that you can supply some other information
            //    // as well, perhaps a status label?
            //    //bwAsync.ReportProgress(Convert.ToInt32(i * (100.0 / iCount)));

            //    // Periodically check if a Cancellation request is pending.  If the user
            //    // clicks cancel the line m_AsyncWorker.CancelAsync(); if ran above.  This
            //    // sets the CancellationPending to true.  You must check this flag in here and
            //    // react to it.  We react to it by setting e.Cancel to true and leaving.
            //    txtLog.AppendText(DateTime.Now.ToString("hh:mm:ss tt ") + i.ToString();
            //    if (bwAsync.CancellationPending)
            //    {
            //        // Pause for bit to demonstrate that there is time between 
            //        // "Cancelling..." and "Canceled".
            //        Thread.Sleep(1200);

            //        // Set the e.Cancel flag so that the WorkerCompleted event
            //        // knows that the process was canceled.
            //        e.Cancel = true;
            //        return;
            //    }
            //}

            //now do your job
            try
            {
                //if (chbYahoo.Checked)
                //{
                //    GetLinkYahoo();
                //}
                //else
                //{
                //    if (chbDownLoad.Checked)
                //    {
                //        DownLoadLink(sender, e);
                //    }
                //    else
                //    {
                //        if (gb_isMegashareVnnVnGetDirect)
                //        {
                //            if (setStatusMsg(bwAsync, e, "Getting direct link megashareVnnVn files", true, true))
                //                return;
                //            GetMegashareDirectLink3(sender, e);
                //            //GetMegashareDirectLink2();
                //            if (setStatusMsg(bwAsync, e, "Finished getting direct link megashareVnnVn files", true, true))
                //                return;
                //        }
                //        else
                //        {
                //            if (setStatusMsg(bwAsync, e, "Getting direct links", true, true))
                //                return;
                //            GetLink(sender, e);
                //            if (setStatusMsg(bwAsync, e, "Finished getting direct links", true, true))
                //                return;
                //        }
                //    }
                //}
            }
            catch (Exception e1)
            {
                //MessageBox.Show("Lat and Long value is incorrect or not found in edulog system!");
                MessageBox.Show(e1.Message);
                if (setStatusMsg(bwAsync, e, "Error: " + e1.Message, true, true))
                    return;
                return;
            }
            //finished your job
            bwAsync.ReportProgress(100);
        }

        private void bwAsync_DoWork_notused(object sender, DoWorkEventArgs e)
        {
            // The Sender is the BackgroundWorker object we need it to
            // report progress and check for cancellation.
            BackgroundWorker bwAsync = sender as BackgroundWorker;


            //now do your job
            try
            {


            }
            catch (Exception e1)
            {
                //MessageBox.Show("Lat and Long value is incorrect or not found in edulog system!");
                MessageBox.Show(e1.Message);
                if (setStatusMsg(bwAsync, e, "Error: " + e1.Message, true, true))
                    return;
                return;
            }
            //finished your job
            bwAsync.ReportProgress(100);
        }

        private void bwAsync_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // The background process is complete. We need to inspect 
            // our response to see if an error occured, a cancel was 
            // requested or if we completed succesfully.

            btnGetLink.Text = "&Getlink";
            //btnGetLink.Enabled = true;

            // Check to see if an error occured in the 
            // background process.
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message);
                return;
            }

            // Check to see if the background process was cancelled.
            if (e.Cancelled)
            {
                txtLog.AppendText(DateTime.Now.ToString("hh:mm:ss tt ") + "Cancelled..." + Environment.NewLine);
                if (gb_isCloseWhenCompleted)
                {
                    m_MTManager.StopAllWokder();
                    int repeat = 0;
                    while (m_MTManager.LsLWorker.Count > 0 && repeat < 20)
                    {
                        repeat++;
                        Thread.Sleep(1000);
                    }
                    this.Close();
                }
            }
            else
            {
                // Everything completed normally.
                // process the response using e.Result
                if (m_strError == string.Empty)
                {
                    txtLog.AppendText(DateTime.Now.ToString("hh:mm:ss tt ") + "Completed..." + Environment.NewLine);
                }
            }
            pgbStatus.Style = ProgressBarStyle.Blocks;
            gb_isDownLoading = false;
            //chbException.Enabled = true;
        }

        private void bwAsync_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // This function fires on the UI thread so it's safe to edit
            // the UI control directly, no funny business with Control.Invoke.
            // Update the progressBar with the integer supplide to us from the 
            // ReportProgress() function.  Note, e.UserState is a "tag" property
            // that can be used to send other information from the BackgroundThread
            // to the UI thread.

            pgbStatus.Value = e.ProgressPercentage;
        }

        private bool isWorkerCancelling(object sender, DoWorkEventArgs e)
        {
            bool bResult = true;
            BackgroundWorker bwAsync = sender as BackgroundWorker;
            if (bwAsync.CancellationPending)
            {
                Thread.Sleep(1200);
                e.Cancel = true;
                bResult = true;
            }
            else
            {
                //lblProccess.Text = "100%";
                bResult = false;
            }
            return bResult;
        }

        #endregion

        DataTable gb_tbListFile;
        List<MyClsFile> gb_lsFile;
        bool gb_isMegashareVnnVnGetDirect = false;
        private string gb_strCurrentFileNameDownloading;
        bool gb_isFileDowloaded = false;
        //int gb_maxRepeatDownloadProccess = 10;

        private bool gb_isCloseWhenCompleted = false;
        private bool gb_isDownLoading = false;
        public MyClsOption m_option;
        private MTManager m_MTManager;
        private int m_wkYahoo = -1;

        public int WkYahoo
        {
            get { return m_wkYahoo; }
            set { m_wkYahoo = value; }
        }

        private MyClsErrorHelper m_lastedErrorCode;

        internal MyClsErrorHelper LastedErrorCode
        {
            get { return m_lastedErrorCode; }
            set { m_lastedErrorCode = value; }
        }

        FileSystemWatcher _watchFolder;

        public MyClsRecordManager RcManager;
        public MyClsRecordManagerBus RcManagerBus;

        private string m_connSource;
        private string m_connDes;

        private string m_connSourceBus;
        private string m_connDesBus;

        public frmMain()
        {
            InitializeComponent();
            // Create a background worker thread that ReportsProgress & SupportsCancellation
            // Hook up the appropriate events.
            m_AsyncWorker.WorkerReportsProgress = true;
            m_AsyncWorker.WorkerSupportsCancellation = true;
            m_AsyncWorker.ProgressChanged += new ProgressChangedEventHandler(bwAsync_ProgressChanged);
            m_AsyncWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwAsync_RunWorkerCompleted);
            m_AsyncWorker.DoWork += new DoWorkEventHandler(bwAsync_DoWork);
            Label.CheckForIllegalCrossThreadCalls = false;
            TextBox.CheckForIllegalCrossThreadCalls = false;
            CheckBox.CheckForIllegalCrossThreadCalls = false;
            Button.CheckForIllegalCrossThreadCalls = false;
            DataGridView.CheckForIllegalCrossThreadCalls = false;
            ProgressBar.CheckForIllegalCrossThreadCalls = false;

            gb_tbListFile = new DataTable();
            gb_lsFile = new List<MyClsFile>();

            m_MTManager = new MTManager(this);
            m_lastedErrorCode = new MyClsErrorHelper();
        }

        private void exitProgram(bool isCloseWhenCompleted, bool isFromClosing)
        {
            gb_isCloseWhenCompleted = isCloseWhenCompleted;
            // If the background thread is running then clicking this
            // button causes a cancel, otherwise clicking this button
            // launches the background thread.
            if (m_AsyncWorker.IsBusy)
            {
                ////btnGetLink.Enabled = false;
                //txtLog.AppendText("Canceling...");

                // Notify the worker thread that a cancel has been requested.
                // The cancel will not actually happen until the thread in the 
                // DoWork checks the bwAsync.CancellationPending flag, for this
                // reason we set the label to "Canceling...", because we haven't 
                // actually cancelled yet.
                m_AsyncWorker.CancelAsync();
            }
            else
            {
                //int repeat = 0;
                txtLog.AppendText("Closing worker....");
                m_MTManager.StopAllWokder();
                //while (m_MTManager.LsLWorker.Count > 0 && repeat < 20)
                //{                    
                //    repeat++;
                //    Thread.Sleep(1000);
                //}
                if (!isFromClosing)
                    this.Close();                
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            exitProgram(true, false);
        }

        private void WaitingWithPostMsgInfor(BackgroundWorker bwAsync, DoWorkEventArgs e, int miliSecToWait, int miliSecToSendMsg)
        {
            int secEslap = 0;
            bool isWaiting = true;
            if (setStatusMsg(bwAsync, e, "Waiting about " + Convert.ToString(miliSecToWait / 1000) + " seconds", true, true))
                return;
            while (isWaiting)
            {
                if (secEslap < miliSecToWait)
                {
                    if (setStatusMsg(bwAsync, e, "Estimated left time: " + Convert.ToString((miliSecToWait - secEslap) / 1000) + " seconds", true, true))
                        return;
                    Thread.Sleep(miliSecToSendMsg);
                    secEslap += miliSecToSendMsg;
                    //secEslap = secToWait - secToSendMsg;
                }
                else
                    isWaiting = false;
            }
        }

        private void SetFileStatus(MyClsFile fileTemp, MyClsFile.MyFileStatus status, bool isRefreshOderFileInforOnGrid)
        {
            //bool bRs = false;
            int rowIndex = GetRowIndexByFile(fileTemp);
            if (rowIndex != -1)
            {
                fileTemp.FileStatus = status;
                gb_tbListFile.Rows[rowIndex]["FILE_STATUS"] = status.ToString();
                if (isRefreshOderFileInforOnGrid)
                {
                    //row["STT"] = Convert.ToString(gb_tbListFile.Rows.Count + 1);
                    gb_tbListFile.Rows[rowIndex]["FILE_ID"] = fileTemp.FileID;
                    gb_tbListFile.Rows[rowIndex]["FILE_NAME"] = fileTemp.FileName;
                    //gb_tbListFile.Rows[rowIndex]["FILE_STATUS"] = fileTemp.FileStatus.ToString();
                    gb_tbListFile.Rows[rowIndex]["FILE_SIZE"] = fileTemp.FileSize;
                    gb_tbListFile.Rows[rowIndex]["FILE_URL"] = fileTemp.FileUrl;
                    gb_tbListFile.Rows[rowIndex]["FILE_PASSWORD"] = fileTemp.FilePassword;
                    gb_tbListFile.Rows[rowIndex]["FILE_DIRECTLINK"] = fileTemp.FileDirectLink;
                    gb_tbListFile.Rows[rowIndex]["FILE_DOWNCOUNT"] = fileTemp.FileDownCount;
                    gb_tbListFile.Rows[rowIndex]["FILE_CATCHA"] = fileTemp.catCha;
                }
                //bRs = true;
            }
            //return bRs;
        }

        private bool MonitorFileDownloaded(object sender, DoWorkEventArgs e, MyClsFile fileTemp)
        {
            BackgroundWorker bwAsync = sender as BackgroundWorker;

            bool bRs = false;
            //now monitor this file
            _watchFolder = new FileSystemWatcher();
            // This is the path we want to monitor
            _watchFolder.Path = m_option.SaveTo;

            // Make sure you use the OR on each Filter because we need to monitor
            // all of those activities

            _watchFolder.NotifyFilter = System.IO.NotifyFilters.DirectoryName;

            _watchFolder.NotifyFilter =
            _watchFolder.NotifyFilter | System.IO.NotifyFilters.FileName;
            _watchFolder.NotifyFilter =
            _watchFolder.NotifyFilter | System.IO.NotifyFilters.Attributes;

            // Now hook the triggers(events) to our handler (eventRaised)
            _watchFolder.Changed += new FileSystemEventHandler(eventRaised);
            _watchFolder.Created += new FileSystemEventHandler(eventRaised);
            _watchFolder.Deleted += new FileSystemEventHandler(eventRaised);

            // Occurs when a file or directory is renamed in the specific path
            _watchFolder.Renamed += new System.IO.RenamedEventHandler(eventRenameRaised);

            // And at last.. We connect our EventHandles to the system API (that is all
            // wrapped up in System.IO)
            try
            {
                _watchFolder.EnableRaisingEvents = true;
                //now watching
                //int second = m_option.TimeOut * 60;


                int second = 0;
                if (setStatusMsg(bwAsync, e, "Monitoring file...", true, false))
                    return false;
                while (!gb_isFileDowloaded && (second < m_option.TimeOut * 60))
                {
                    if (isWorkerCancelling(bwAsync, e))
                    {
                        stopActivityMonitoring();
                        SetFileStatus(fileTemp, MyClsFile.MyFileStatus.CANCELED, false);
                        return false;
                    }
                    Thread.Sleep(5000);
                    second += 5;
                }
                stopActivityMonitoring();
                if (setStatusMsg(bwAsync, e, "OK!", false, true))
                    return false;
                if (second < m_option.TimeOut * 60)
                {
                    SetFileStatus(fileTemp, MyClsFile.MyFileStatus.DOWNLOADED, false);
                    if (setStatusMsg(bwAsync, e, "File Downloded", true, true))
                        return false;

                    bRs = true;
                }
                else //time out
                {
                    SetFileStatus(fileTemp, MyClsFile.MyFileStatus.TIME_OUT, false);
                    if (setStatusMsg(bwAsync, e, "Download time out", true, true))
                        return false;
                    bRs = false;
                }
            }
            catch (ArgumentException)
            {
                //abortAcitivityMonitoring();
                //fileTemp.FileStatus = "Monitor Error";
                SetFileStatus(fileTemp, MyClsFile.MyFileStatus.FAILED, false);
            }

            return bRs;
        }

        #region watching folder
        /// <summary>
        /// This just stops the monitoring process.
        /// </summary>
        private void stopActivityMonitoring()
        {
            _watchFolder.EnableRaisingEvents = false;
        }

        //private void abortAcitivityMonitoring()
        //{
        //    btnStart_Stop.Text = "Start";
        //    txtActivity.Focus();
        //}


        /// <summary>
        /// Because the thread that is triggering the event, and will
        /// finaly end up doing your code is not created on our GUI-thread
        /// so we need to create a delegate and invoke the method.
        /// </summary>
        /// <param name="text">string; What text to add to txtAcitivty</param>
        private delegate void AddLogText(string text);
        private void TS_AddLogText(string text)
        {
            if (this.InvokeRequired)
            {
                AddLogText del = new AddLogText(TS_AddLogText);
                Invoke(del, text);
            }
            else
            {
                txtLog.AppendText(text + Environment.NewLine);
            }
        }

        /// <summary>
        /// Triggerd when an event is raised from the folder acitivty monitoring.
        /// All types exists in System.IO
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">containing all data send from the event that got executed.</param>
        private void eventRaised(object sender, System.IO.FileSystemEventArgs e)
        {
            switch (e.ChangeType)
            {
                case WatcherChangeTypes.Changed:
                    //TS_AddLogText(string.Format("File {0} has been modified\r\n", e.FullPath));
                    if (gb_strCurrentFileNameDownloading == e.Name)
                        gb_isFileDowloaded = true;

                    break;
                case WatcherChangeTypes.Created:
                    //TS_AddLogText(string.Format("File {0} has been created\r\n", e.FullPath));
                    //
                    if (gb_strCurrentFileNameDownloading == e.Name)
                        gb_isFileDowloaded = true;
                    break;
                case WatcherChangeTypes.Deleted:
                    //TS_AddLogText(string.Format("File {0} has been deleted\r\n", e.FullPath));

                    break;
                default: // Another action
                    break;
            }
        }

        /// <summary>
        /// When a folder or file is renamed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void eventRenameRaised(object sender, System.IO.RenamedEventArgs e)
        {
            //TS_AddLogText(string.Format("File {0} has been renamed to {1}\r\n", e.OldName, e.Name));
            if (gb_strCurrentFileNameDownloading == e.Name)
                gb_isFileDowloaded = true;
        }

        //private void btnStart_Stop_Click(object sender, EventArgs e)
        //{
        //    if (btnStart_Stop.Text.Equals("Start"))
        //    {
        //        btnStart_Stop.Text = "Stop";
        //        startActivityMonitoring(txtFolderPath.Text);
        //    }
        //    else
        //    {
        //        btnStart_Stop.Text = "Start";
        //        stopActivityMonitoring();
        //    }
        //}

        #endregion

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            timerSysClock.Stop();
            timerSysClockBus.Stop();
            SaveAllFile();
            exitProgram(false, true);
        }

        private void SaveAllFile()
        {
            m_option.SaveAllFile(gb_lsFile);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            exitProgram(true, false);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            qldv.About about = new qldv.About();
            about.ShowDialog();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            //set title
            this.Text += " v" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            m_option = new MyClsOption();
            btnStarBus.Enabled = btnStar.Enabled = LoadConfigMessage();
            
            //load all delegate:
            LoadAllDelegate();
            LoadAllDelegateBus();

            ////ConfigSetting newUser = xcfg.Settings["Vcard##"];
            ////newUser.Value = "2.1";
            //////ConfigSetting newUser = xcfg.Settings["PhoneBook/Vcard##"];
            ////newUser["UserName"].Value = ins_newUserName;
            ////newUser["Password"].Value = ins_newPassword;
            ////newUser["Server"].Value = ins_newServer;
            ////newUser["Note"].Value = ins_newNote;

            ////CreateTa
            //CreateTableFile();
            ////load list of files from config
            //LoadAllFileFromConfig();

            //RefreshTableFile();
            ////RUN entityObject2 = new RUN(strConnect);
            ////DataSet dst = new DataSet();
            ////if (entityObject2.getRUNByID(dst, "RUN", ins_runID))
            ////{
            ////    FuctionClass Func = new FuctionClass();
            ////    //dgvData.DataSource = dst.Tables["THU_SK"];
            ////    if (!dst.Tables["RUN"].Columns.Contains("STT"))
            ////    {
            ////        dst.Tables["RUN"].Columns.Add("STT");
            ////    }

            ////    //Func.AutoNumber(dst, "TBL_TEACHER", "STT");
            ////    Func.AutoNumber(dst, "RUN", "STT");
            ////    dgvRun.DataSource = dst;
            ////    dgvRun.DataMember = "RUN";

            ////    dgvBuilTableSyteRun();
            ////}
            ////else
            ////{
            ////    dgvRun.DataSource = null;
            ////    clearRun();
            ////}
            //chbDownLoad_CheckedChanged(sender, e);
        }

        private void CreateTableFile()
        {
            DataColumn clItem = new DataColumn("STT", typeof(string));
            gb_tbListFile.Columns.Add(clItem);
            clItem = new DataColumn("FILE_ID", typeof(string));
            gb_tbListFile.Columns.Add(clItem);
            clItem = new DataColumn("FILE_NAME", typeof(string));
            gb_tbListFile.Columns.Add(clItem);
            clItem = new DataColumn("FILE_CATCHA", typeof(Bitmap));
            gb_tbListFile.Columns.Add(clItem);
            clItem = new DataColumn("STRING_CATCHA", typeof(string));
            gb_tbListFile.Columns.Add(clItem);
            clItem = new DataColumn("FILE_STATUS", typeof(string));
            gb_tbListFile.Columns.Add(clItem);
            clItem = new DataColumn("FILE_SIZE", typeof(string));
            gb_tbListFile.Columns.Add(clItem);
            clItem = new DataColumn("FILE_URL", typeof(string));
            gb_tbListFile.Columns.Add(clItem);
            clItem = new DataColumn("FILE_PASSWORD", typeof(string));
            gb_tbListFile.Columns.Add(clItem);
            clItem = new DataColumn("FILE_DIRECTLINK", typeof(string));
            gb_tbListFile.Columns.Add(clItem);
            clItem = new DataColumn("FILE_DOWNCOUNT", typeof(string));
            gb_tbListFile.Columns.Add(clItem);

            //gb_tbListFile.a
            //clFileName = new DataColumn("", typeof(string));
            //gb_tbListFile.Columns.Add(clFileName);
        }

        delegate void updateDataTableDelegate(DataRow dr);

        private MyClsFile GetFileByRowIndex(int rowIndex)
        {
            MyClsFile fileResutrn = null;
            if (gb_tbListFile.Rows.Count != gb_lsFile.Count)
                return null;
            for (int i = 0; i < gb_tbListFile.Rows.Count; i++)
            {
                if (gb_tbListFile.Rows[rowIndex]["FILE_URL"].ToString() == gb_lsFile[i].FileUrl)
                {
                    fileResutrn = gb_lsFile[i];
                    break;
                }
            }
            return fileResutrn;
        }

        private int GetRowIndexByFile(MyClsFile fileInput)
        {
            int rowIndex = -1;
            if (gb_tbListFile.Rows.Count != gb_lsFile.Count)
                return -1;
            for (int i = 0; i < gb_tbListFile.Rows.Count; i++)
            {
                if (gb_tbListFile.Rows[i]["FILE_URL"].ToString().ToLower() == fileInput.FileUrl.ToLower())
                {
                    rowIndex = i;
                    break;
                }
            }
            return rowIndex;
        }

        private void callIDM(string linkDirect)
        {
            //start call idm to download in silent mode
            //now net send to huy
            Process myProcess = new Process();


            myProcess.StartInfo.FileName = @"c:\Program Files\Internet Download Manager\IDMan.exe";

            //Do not receive an event when the process exits.
            myProcess.EnableRaisingEvents = false;

            // Parameters
            //myProcess.StartInfo.Arguments = @"C:\ELT\Live\Server\Edulognt.cfg 1 0 1 1 1 1";
            myProcess.StartInfo.Arguments = "/n /a /d \"" + linkDirect + "\"";

            // Modify the following to hide / show the window
            //show
            //myProcess.StartInfo.CreateNoWindow = false;
            //myProcess.StartInfo.UseShellExecute = true;
            //myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Maximized;
            //hide
            myProcess.StartInfo.CreateNoWindow = true;
            myProcess.StartInfo.UseShellExecute = false;
            myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

            myProcess.Start();
            myProcess.WaitForExit(2000);
            //if (setStatusMsg(bwAsync, e, "Start sending a messge!"))
            //    return;
            //myProcess.Start();
            //myProcess.WaitForExit(5000);
            //if (setStatusMsg(bwAsync, e, "A messge was sent!"))
            //    return;
        }

        private void CallIDMafterLeech(MyClsFile fileTemp)
        {
            //start call idm to download in silent mode
            //now net send to huy
            Process myProcess = new Process();


            //myProcess.StartInfo.FileName = @"c:\Program Files\Internet Download Manager\IDMan.exe";
            myProcess.StartInfo.FileName = m_option.IDMPath;

            //Do not receive an event when the process exits.
            myProcess.EnableRaisingEvents = false;

            // Parameters
            //myProcess.StartInfo.Arguments = @"C:\ELT\Live\Server\Edulognt.cfg 1 0 1 1 1 1";

            //myProcess.StartInfo.Arguments = "/n /d \"" + fileTemp.FileDirectLink + "\"" + " /p \"" + m_option.SaveTo + "\"";
            myProcess.StartInfo.Arguments = "/n /d \"" + fileTemp.FileDirectLink + "\"" + " /p \"" + m_option.SaveTo + "\""
            + " /f \"" + fileTemp.FileName + "\"";
            //if (m_option.SaveTo.Contains(" "))
            //    myProcess.StartInfo.Arguments = "/n /d \"" + fileTemp.FileDirectLink + "\"" + " /p \"" + m_option.SaveTo + "\"";
            //else
            //    myProcess.StartInfo.Arguments = "/n /d \"" + fileTemp.FileDirectLink + "\"" + " /p " + m_option.SaveTo;

            // Modify the following to hide / show the window
            //show
            //myProcess.StartInfo.CreateNoWindow = false;
            //myProcess.StartInfo.UseShellExecute = true;
            //myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Maximized;
            //hide
            myProcess.StartInfo.CreateNoWindow = true;
            myProcess.StartInfo.UseShellExecute = false;
            myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

            myProcess.Start();
            myProcess.WaitForExit(2000);
            //if (setStatusMsg(bwAsync, e, "Start sending a messge!"))
            //    return;
            //myProcess.Start();
            //myProcess.WaitForExit(5000);
            //if (setStatusMsg(bwAsync, e, "A messge was sent!"))
            //    return;
        }

        private void CallFlashGetafterLeech(MyClsFile fileTemp)
        {
            //start call idm to download in silent mode
            //now net send to huy
            Process myProcess = new Process();


            //myProcess.StartInfo.FileName = @"c:\Program Files\FlashGet\flashget.exe";
            myProcess.StartInfo.FileName = m_option.FlashGetPath;

            //Do not receive an event when the process exits.
            myProcess.EnableRaisingEvents = false;

            // Parameters
            //myProcess.StartInfo.Arguments = @"C:\ELT\Live\Server\Edulognt.cfg 1 0 1 1 1 1";
            myProcess.StartInfo.Arguments = "\"" + fileTemp.FileDirectLink + "\"" + " \"" + m_option.SaveTo + "\"";

            // Modify the following to hide / show the window
            //show
            //myProcess.StartInfo.CreateNoWindow = false;
            //myProcess.StartInfo.UseShellExecute = true;
            //myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Maximized;
            //hide
            myProcess.StartInfo.CreateNoWindow = true;
            myProcess.StartInfo.UseShellExecute = false;
            myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

            myProcess.Start();
            myProcess.WaitForExit(2000);
            //if (setStatusMsg(bwAsync, e, "Start sending a messge!"))
            //    return;
            //myProcess.Start();
            //myProcess.WaitForExit(5000);
            //if (setStatusMsg(bwAsync, e, "A messge was sent!"))
            //    return;
        }

        private void mitOption_Click(object sender, EventArgs e)
        {
            frmOption entity = new frmOption(m_option);
            entity.ShowDialog();
            btnStarBus.Enabled = btnStar.Enabled = LoadConfigMessage();
            //btnStarBus.Enabled = LoadConfigMessage();
        }

        private void cbSeletedDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblTotalRecordOfSeletedDate.Text = cbSeletedDate.SelectedValue.ToString();
        }

        private void btnLoadConfig_Click(object sender, EventArgs e)
        {
            btnStar.Enabled = LoadConfigMessage();
        }

        private bool LoadConfigMessage()
        {
            bool bRs = false;
            try
            {                
                //setPlayerBtnState(false);
                //setTimeType();
                //cbCurrentDate.Text = DateTime.Now.ToString("MM/dd/yyyy");

                m_connSource = "Data Source=" + m_option.DBSERVERNAMESOURCE + ";";
                m_connSource += "Initial Catalog=" + m_option.DBNAMESOURCE + ";";
                m_connSource += "User ID=" + m_option.DBUSERSOURCE + ";";
                m_connSource += "Password=" + m_option.DBPASSSOURCE;

                m_connDes = "Data Source=" + m_option.DBSERVERNAMEDES + ";";
                m_connDes += "Initial Catalog=" + m_option.DBNAMEDES + ";";
                m_connDes += "User ID=" + m_option.DBUSERDES + ";";
                m_connDes += "Password=" + m_option.DBPASSDES;

                //now load all date and total record of each date in message
                
                RcManager = new MyClsRecordManager(m_connSource, m_connDes, chbDeleteExistRecord.Checked);
                cbSeletedDate.DisplayMember = "ListDate";
                cbSeletedDate.ValueMember = "TotalRecord";
                cbSeletedDate.DataSource = RcManager.TbListDate;

                //for ADF bus 

                m_connSourceBus = "Data Source=" + m_option.DBSERVERNAMESOURCEBUS + ";";
                m_connSourceBus += "Initial Catalog=" + m_option.DBNAMESOURCEBUS + ";";
                m_connSourceBus += "User ID=" + m_option.DBUSERSOURCEBUS + ";";
                m_connSourceBus += "Password=" + m_option.DBPASSSOURCEBUS;

                m_connDesBus = "Data Source=" + m_option.DBSERVERNAMEDESBUS + ";";
                m_connDesBus += "Initial Catalog=" + m_option.DBNAMEDESBUS + ";";
                m_connDesBus += "User ID=" + m_option.DBUSERDESBUS + ";";
                m_connDesBus += "Password=" + m_option.DBPASSDESBUS;

                //now load all date and total record of each date in message

                RcManagerBus = new MyClsRecordManagerBus(m_connSourceBus, m_connDesBus, chbDeleteExistRecordBus.Checked);
                cbSeletedDateBus.DisplayMember = "ListDate";
                cbSeletedDateBus.ValueMember = "TotalRecord";
                cbSeletedDateBus.DataSource = RcManagerBus.TbListDate;
                //end bus
                //setPlayerBtnState(true);
                bRs = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Some errors orcur in config file. Please recheck config secsion!\nDetail: " + ex.Message);
            }
            return bRs;
        }

        private void setPlayerBtnState(bool IsEnable)
        {
            btnStar.Enabled = IsEnable;
            btnPause.Enabled = IsEnable;
            btnStop.Enabled = IsEnable;
        }

        private void btnStar_Click(object sender, EventArgs e)
        {
            PlayerStart();
            startAFDThread();
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            PlayerPause();
            
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            PlayerStop();
            m_MTManager.CancelWorker(wkID_AFDThread);
        }

        int wkID_AFDThread;
        bool isAdd;
        

        private void startAFDThread()
        {
            //set recordmanager variable
            SetTimeTypeToRecordManager();
            RcManager.IsDeleteExistRecords = chbDeleteExistRecord.Checked;
            //start thread
            isAdd = false;
            wkID_AFDThread = m_MTManager.AddWorkder(MyWokerType.AFD_MESSAGE_TABLE);
            //m_MTManager.SetFormMain(wkID, this);
            m_MTManager.SetTxtWorker(wkID_AFDThread, txtLog);
            //m_MTManager.SetPbWorker(wkID_AFDThread, pgbStatus);//not use anymore, use delegate to send complete message event
            //m_MTManager.SetBtWorker(wkID_AFDThread, btnStar, "&Start", false);
            m_MTManager.StartWokder(wkID_AFDThread, RcManager);
        }

        public void LoadAllDelegate()
        {
            m_MTManager.RaiseUpdateTimeGUI += new MTManager.UpdateTimeGUI(OnUpdateTimeGUI);
            m_MTManager.RaiseOnCompleteAFDThread += new MTManager.OnCompleteAFDThread(CompleteAFDThread);
        }

        private Int64 m_totalRecordInserted;
        private void OnUpdateTimeGUI(MyClsRecordManager ins_RcManager, bool bIsInserted)
        {
            if (ins_RcManager != null)
            {
                //update time
                lblCurrentSysTime.Text = ins_RcManager.dtCurrentSysTime.ToString("dd/MM hh:mm:ss.fff tt", CultureInfo.GetCultureInfo("en-US"));
                lblSpeedSysTime.Text = ins_RcManager.dtConvertedSysTime.ToString("dd/MM hh:mm:ss.fff tt", CultureInfo.GetCultureInfo("en-US"));
                lblActualRecordTime.Text = ins_RcManager.dtActualRecordTime.ToString("dd/MM hh:mm:ss.fff tt", CultureInfo.GetCultureInfo("en-US"));
                //update next record information
                if(!RcManager.IsIdentity)
                    txtNextID.Text = ins_RcManager.NextIdInsert.ToString();
                txtIDSource.Text = ins_RcManager.RcNext.message_id.ToString();
                if(bIsInserted)
                    m_totalRecordInserted++;

                lbltotalRecordInserted.Text = m_totalRecordInserted.ToString();
                lblLastID.Text = ins_RcManager.LastRecordIDOfSelectedDate.ToString();                
                Int64 idRemain = ins_RcManager.LastRecordIDOfSelectedDate - ins_RcManager.RcNext.message_id;
                lblIDRemain.Text = idRemain.ToString();
                //pgbStatus.Minimum =(int) ins_RcManager.FirstRecordIDOfSelectedDate;
                //pgbStatus.Minimum = (int)ins_RcManager.RcNext.message_id;
                //pgbStatus.Maximum = (int)ins_RcManager.LastRecordIDOfSelectedDate;
                pgbStatus.Minimum = 0;
                pgbStatus.Maximum = (int)ins_RcManager.LastRecordIDOfSelectedDate - (int)ins_RcManager.RcNext.message_id;
                //pgbStatus.Value = (int)ins_RcManager.RcNext.message_id;
                //pgbStatus.Value = pgbStatus.Minimum + (int)m_totalRecordInserted;
                if (m_totalRecordInserted > pgbStatus.Maximum)
                {
                    pgbStatus.Value = pgbStatus.Maximum;
                }
                else
                    pgbStatus.Value = pgbStatus.Minimum + (int)m_totalRecordInserted;
            }
        }

        private void CompleteAFDThread()
        {
            PlayerStop();
            //MessageBox.Show("Finshed filling data!");
        }

        private void PlayerStart()
        {
            btnLoadConfig.Enabled = false;
            btnStar.Enabled = false;
            btnPause.Enabled = true;
            btnResume.Enabled = false;
            btnStop.Enabled = true;
            btnLoadConfig.Enabled = false;
            //rbSpeed.Enabled = true;
            //rbPeriod.Enabled = true;
            //nudSpeed.Enabled = true;
            //nudPeriod.Enabled = true;
            //setTimeType();
            cbSeletedDate.Enabled = false;
            chbDeleteExistRecord.Enabled = false;

            //set timer start
            timerSysClock.Enabled = true;
            timerSysClock.Start();
            //set progresbar
            //pgbStatus.Style = ProgressBarStyle.Continuous;
        }

        private void PlayerPause()
        {
            btnStar.Enabled = false;
            btnPause.Enabled = false;
            btnResume.Enabled = true;
            btnStop.Enabled = true;
            m_MTManager.SetPlayerPause(wkID_AFDThread, true);

            //timerSysClock.Stop();
            //set progresbar
            //pgbStatus.Value = 50;
            //pgbStatus.Style = ProgressBarStyle.Blocks;
        }

        private void PlayerResume()
        {
            btnStar.Enabled = false;
            btnPause.Enabled = true;
            btnResume.Enabled = false;
            btnStop.Enabled = true;
            m_MTManager.SetPlayerPause(wkID_AFDThread, false);

            //timerSysClock.Start();
            //set progresbar
            //pgbStatus.Style = ProgressBarStyle.Blocks;
        }

        private void PlayerStop()
        {
            btnLoadConfig.Enabled = true;
            btnStar.Enabled = true;
            btnPause.Enabled = false;
            btnResume.Enabled = false;
            btnStop.Enabled = false;
            btnLoadConfig.Enabled = true;
            //rbSpeed.Enabled = true;
            //rbPeriod.Enabled = true;
            //setTimeType();
            cbSeletedDate.Enabled = true;
            chbDeleteExistRecord.Enabled = true;
            //set progresbar
            //pgbStatus.Style = ProgressBarStyle.Blocks;

            timerSysClock.Stop();
            timerSysClock.Enabled = false;
        }

        private void rbSpeed_CheckedChanged(object sender, EventArgs e)
        {
            setTimeType();
            btnSet.Enabled = true;
        }

        private void rbPeriod_CheckedChanged(object sender, EventArgs e)
        {
            setTimeType();
            btnSet.Enabled = true;
        }

        private void setTimeType()
        {
            nudSpeed.Enabled = rbSpeed.Checked;
            nudPeriod.Enabled = rbPeriod.Checked;
        }

        private void nudSpeed_ValueChanged(object sender, EventArgs e)
        {
            btnSet.Enabled = true;
        }

        private void nudPeriod_ValueChanged(object sender, EventArgs e)
        {
            btnSet.Enabled = true;
        }

        private void btnSet_Click(object sender, EventArgs e)
        {            
            if (RcManager != null)
            {
                SetTimeTypeToRecordManager();
                btnSet.Enabled = false;
            }
            else
            {
                MessageBox.Show("Please set the config to correct value!");
                btnSet.Enabled = true;
            }
        }

        private void SetTimeTypeToRecordManager()
        {
            //DateTime dtDateSelected = DateTime.MinValue;
            //if (DateTime.TryParse(cbSeletedDate.Text, out dtDateSelected))
            //{
            //    if (rbSpeed.Checked)
            //        RcManager.SetSpeed(Convert.ToDouble(nudSpeed.Value), dtDateSelected, Convert.ToInt32(nudSleep.Value));
            //    else
            //        RcManager.SetPeriod(Convert.ToDouble(nudPeriod.Value), dtDateSelected, Convert.ToInt32(nudSleep.Value));
            //    RcManager.IsDeleteExistRecords = chbDeleteExistRecord.Checked;
            //}
            //else
            //{
            //    MessageBox.Show("Selected date is invalid!");
            //    btnStop_Click(this, new EventArgs());
            //}

            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US");
            DateTime dtDateSelected = DateTime.MinValue;
            if (DateTime.TryParse(cbSeletedDate.Text, ci, System.Globalization.DateTimeStyles.None, out dtDateSelected))
            {
                timerSysClock.Interval = (int)(1000 / nudSpeed.Value);
                if (rbSpeed.Checked)
                    RcManager.SetSpeed(Convert.ToDouble(nudSpeed.Value), dtDateSelected, Convert.ToInt32(nudSleep.Value), dtpSystemClock.Value);
                else
                    RcManager.SetPeriod(Convert.ToDouble(nudPeriod.Value), dtDateSelected, Convert.ToInt32(nudSleep.Value), dtpSystemClock.Value);
                RcManager.IsDeleteExistRecords = chbDeleteExistRecord.Checked;
            }
            else
            {
                MessageBox.Show("Selected date is invalid!");
                btnStop_Click(this, new EventArgs());
            }
        }

        private void SetTimeTypeToRecordManagerBus()
        {
            //DateTime dtDateSelected = DateTime.MinValue;
            //if (DateTime.TryParse(cbSeletedDate.Text, out dtDateSelected))
            //{
            //    if (rbSpeed.Checked)
            //        RcManager.SetSpeed(Convert.ToDouble(nudSpeed.Value), dtDateSelected, Convert.ToInt32(nudSleep.Value));
            //    else
            //        RcManager.SetPeriod(Convert.ToDouble(nudPeriod.Value), dtDateSelected, Convert.ToInt32(nudSleep.Value));
            //    RcManager.IsDeleteExistRecords = chbDeleteExistRecord.Checked;
            //}
            //else
            //{
            //    MessageBox.Show("Selected date is invalid!");
            //    btnStop_Click(this, new EventArgs());
            //}
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-US"); ;
            DateTime dtDateSelected = DateTime.MinValue;
            if (DateTime.TryParse(cbSeletedDateBus.Text, ci, System.Globalization.DateTimeStyles.None, out dtDateSelected))
            {
                timerSysClockBus.Interval = (int)(1000 / nudSpeedBus.Value);
                if (rbSpeedBus.Checked)
                    RcManagerBus.SetSpeed(Convert.ToDouble(nudSpeedBus.Value), dtDateSelected, Convert.ToInt32(nudSleepBus.Value), dtpSystemClockBus.Value);
                else
                    RcManagerBus.SetPeriod(Convert.ToDouble(nudPeriodBus.Value), dtDateSelected, Convert.ToInt32(nudSleepBus.Value), dtpSystemClockBus.Value);
                RcManagerBus.IsDeleteExistRecords = chbDeleteExistRecordBus.Checked;
            }
            else
            {
                MessageBox.Show("Selected date is invalid!");
                btnStop_Click(this, new EventArgs());
            }
        }

        private void btnResume_Click(object sender, EventArgs e)
        {
            PlayerResume();
        }

        private void nudSleep_ValueChanged(object sender, EventArgs e)
        {
            btnSet.Enabled = true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //m_MTManager.callRaiseCompleate();
        }

        private void timerSysClock_Tick(object sender, EventArgs e)
        {
            //lblCurrentSysTime.Text = RcManager.dtCurrentSysTime.ToString("hh:mm:ss tt");
            //m_MTManager.callRaiseUpdateUI();
            OnUpdateTimeGUI(RcManager, false);
            m_MTManager.timerTickEvent();            
        }

        private void dtpSystemClock_ValueChanged(object sender, EventArgs e)
        {
            btnSet.Enabled = true;
        }

        private void btnLoadConfigBus_Click(object sender, EventArgs e)
        {
            btnStar.Enabled = LoadConfigMessage();
        }

        private void cbSeletedDateBus_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblTotalRecordOfSeletedDateBus.Text = cbSeletedDateBus.SelectedValue.ToString();
        }

        private void dtpSystemClockBus_ValueChanged(object sender, EventArgs e)
        {
            btnSetBus.Enabled = true;
        }

        private void nudSpeedBus_ValueChanged(object sender, EventArgs e)
        {
            btnSetBus.Enabled = true;
        }

        private void nudPeriodBus_ValueChanged(object sender, EventArgs e)
        {
            btnSetBus.Enabled = true;
        }

        private void nudSleepBus_ValueChanged(object sender, EventArgs e)
        {
            btnSetBus.Enabled = true;
        }

        private void btnSetBus_Click(object sender, EventArgs e)
        {
            if (RcManagerBus != null)
            {
                SetTimeTypeToRecordManagerBus();
                btnSetBus.Enabled = false;
            }
            else
            {
                MessageBox.Show("Please set the config to correct value!");
                btnSetBus.Enabled = true;
            }
        }

        private void btnStarBus_Click(object sender, EventArgs e)
        {
            PlayerStartBus();
            startAFDThreadBus();
        }

        private void btnPauseBus_Click(object sender, EventArgs e)
        {
            PlayerPauseBus();
        }

        private void btnResumeBus_Click(object sender, EventArgs e)
        {
            PlayerResumeBus();
        }

        private void btnStopBus_Click(object sender, EventArgs e)
        {
            PlayerStopBus();
            m_MTManager.CancelWorker(wkID_AFDThreadBus);
        }

        int wkID_AFDThreadBus;
        bool isAddBus;


        private void startAFDThreadBus()
        {
            //set recordmanager variable
            SetTimeTypeToRecordManagerBus();
            RcManagerBus.IsDeleteExistRecords = chbDeleteExistRecordBus.Checked;
            //start thread
            isAddBus = false;
            wkID_AFDThreadBus = m_MTManager.AddWorkder(MyWokerType.AFD_BUSPOSITION_TABLE);
            //m_MTManager.SetFormMain(wkID, this);
            m_MTManager.SetTxtWorker(wkID_AFDThreadBus, txtLog);
            //m_MTManager.SetPbWorker(wkID_AFDThread, pgbStatus);//not use anymore, use delegate to send complete message event
            //m_MTManager.SetBtWorker(wkID_AFDThread, btnStar, "&Start", false);
            m_MTManager.StartWokder(wkID_AFDThreadBus, RcManagerBus);
        }

        public void LoadAllDelegateBus()
        {
            m_MTManager.RaiseUpdateTimeGUIBus += new MTManager.UpdateTimeGUIBus(OnUpdateTimeGUIBus);
            m_MTManager.RaiseOnCompleteAFDThreadBus += new MTManager.OnCompleteAFDThreadBus(CompleteAFDThreadBus);
        }

        private Int64 m_totalRecordInsertedBus;
        private void OnUpdateTimeGUIBus(MyClsRecordManagerBus ins_RcManagerBus, bool bIsInserted)
        {
            if (ins_RcManagerBus != null)
            {
                //update time
                //update time
                lblCurrentSysTimeBus.Text = ins_RcManagerBus.dtCurrentSysTime.ToString("dd/MM hh:mm:ss.fff tt", CultureInfo.GetCultureInfo("en-US"));                
                lblSpeedSysTimeBus.Text = ins_RcManagerBus.dtConvertedSysTime.ToString("dd/MM hh:mm:ss.fff tt", CultureInfo.GetCultureInfo("en-US"));
                lblActualRecordTimeBus.Text = ins_RcManagerBus.dtActualRecordTime.ToString("dd/MM hh:mm:ss.fff tt", CultureInfo.GetCultureInfo("en-US"));
                //update next record information
                if (!RcManagerBus.IsIdentity)
                    txtNextIDBus.Text = ins_RcManagerBus.NextIdInsert.ToString();
                txtIDSourceBus.Text = ins_RcManagerBus.RcNext.ID.ToString();
                if(bIsInserted)
                    m_totalRecordInsertedBus++;
                lbltotalRecordInsertedBus.Text = m_totalRecordInsertedBus.ToString();
                lblLastIDBus.Text = ins_RcManagerBus.LastRecordIDOfSelectedDate.ToString();
                Int64 idRemainBus = ins_RcManagerBus.LastRecordIDOfSelectedDate - ins_RcManagerBus.RcNext.ID;
                lblIDRemainBus.Text = idRemainBus.ToString();
                //pgbStatus.Minimum =(int) ins_RcManager.FirstRecordIDOfSelectedDate;
                pgbStatus.Minimum = 0;
                pgbStatus.Maximum = (int)ins_RcManagerBus.LastRecordIDOfSelectedDate - (int)ins_RcManagerBus.RcNext.ID;
                //pgbStatus.Value = (int)ins_RcManager.RcNext.message_id;
                if (m_totalRecordInsertedBus > pgbStatus.Maximum)
                {
                    pgbStatus.Value = pgbStatus.Maximum;
                }
                else
                    pgbStatus.Value = pgbStatus.Minimum + (int)m_totalRecordInsertedBus;
            }
        }

        private void CompleteAFDThreadBus()
        {
            PlayerStopBus();
            //MessageBox.Show("Finshed filling data!");
        }

        private void PlayerStartBus()
        {
            btnLoadConfigBus.Enabled = false;
            btnStarBus.Enabled = false;
            btnPauseBus.Enabled = true;
            btnResumeBus.Enabled = false;
            btnStopBus.Enabled = true;
            btnLoadConfigBus.Enabled = false;
            //rbSpeed.Enabled = true;
            //rbPeriod.Enabled = true;
            //nudSpeed.Enabled = true;
            //nudPeriod.Enabled = true;
            //setTimeType();
            cbSeletedDateBus.Enabled = false;
            chbDeleteExistRecordBus.Enabled = false;

            //set timer start
            timerSysClockBus.Enabled = true;
            timerSysClockBus.Start();
            //set progresbar
            //pgbStatus.Style = ProgressBarStyle.Continuous;
        }

        private void PlayerPauseBus()
        {
            btnStarBus.Enabled = false;
            btnPauseBus.Enabled = false;
            btnResumeBus.Enabled = true;
            btnStopBus.Enabled = true;
            m_MTManager.SetPlayerPause(wkID_AFDThreadBus, true);

            //timerSysClock.Stop();
            //set progresbar
            //pgbStatus.Value = 50;
            //pgbStatus.Style = ProgressBarStyle.Blocks;
        }

        private void PlayerResumeBus()
        {
            btnStarBus.Enabled = false;
            btnPauseBus.Enabled = true;
            btnResumeBus.Enabled = false;
            btnStopBus.Enabled = true;
            m_MTManager.SetPlayerPause(wkID_AFDThreadBus, false);

            //timerSysClock.Start();
            //set progresbar
            //pgbStatus.Style = ProgressBarStyle.Blocks;
        }

        private void PlayerStopBus()
        {
            btnLoadConfigBus.Enabled = true;
            btnStarBus.Enabled = true;
            btnPauseBus.Enabled = false;
            btnResumeBus.Enabled = false;
            btnStopBus.Enabled = false;
            btnLoadConfigBus.Enabled = true;
            //rbSpeed.Enabled = true;
            //rbPeriod.Enabled = true;
            //setTimeType();
            cbSeletedDateBus.Enabled = true;
            chbDeleteExistRecordBus.Enabled = true;
            //set progresbar
            //pgbStatus.Style = ProgressBarStyle.Blocks;

            timerSysClockBus.Stop();
            timerSysClockBus.Enabled = false;
        }

        private void timerSysClockBus_Tick(object sender, EventArgs e)
        {
            //lblCurrentSysTime.Text = RcManager.dtCurrentSysTime.ToString("hh:mm:ss tt");
            //m_MTManager.callRaiseUpdateUI();
            OnUpdateTimeGUIBus(RcManagerBus, false);
            m_MTManager.timerTickEventBus();
        }

    }
}