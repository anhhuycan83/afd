namespace AFD
{
    partial class frmOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabGeneral = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.nudCycleYahoo = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.nudWaitBeforeClose = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.nudWaitAfterSetText = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtCookieRS = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtCookieMU = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCookieHF = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbServerLeech = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnFGPath = new System.Windows.Forms.Button();
            this.txtFlashGetPath = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnIDMPath = new System.Windows.Forms.Button();
            this.rbFlashGet = new System.Windows.Forms.RadioButton();
            this.txtIDMPath = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.rbIDM = new System.Windows.Forms.RadioButton();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.nudRepeatCycle = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTimeOut = new System.Windows.Forms.TextBox();
            this.nudLinkNo = new System.Windows.Forms.NumericUpDown();
            this.btnSaveTo = new System.Windows.Forms.Button();
            this.txtSaveTo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabCbox = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.nudMaxRead = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.nudShowMsg = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.nudReadCbox = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.tabDatabase = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtDBName = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtDBPass = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtDBUser = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtDBServer = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tabFillMessage = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.cbDesDBName = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtMsgDesPass = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtMsgDesUser = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtMsgDesDBServer = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.cbSourceDBName = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtMsgSourcePass = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtMsgSourceUser = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtMsgSourceDBServer = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tabFillBusposition = new System.Windows.Forms.TabPage();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.cbBusDesDBName = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txtBusDesPass = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtBusDesUser = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.txtBusDesDBServer = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.cbBusSourceDBName = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtBusSourcePass = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.txtBusSourceUser = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.txtBusSourceDBServer = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnAcc = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.tabMain.SuspendLayout();
            this.tabGeneral.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCycleYahoo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudWaitBeforeClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudWaitAfterSetText)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRepeatCycle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLinkNo)).BeginInit();
            this.tabCbox.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxRead)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShowMsg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudReadCbox)).BeginInit();
            this.tabDatabase.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tabFillMessage.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tabFillBusposition.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tabGeneral);
            this.tabMain.Controls.Add(this.tabCbox);
            this.tabMain.Controls.Add(this.tabDatabase);
            this.tabMain.Controls.Add(this.tabFillMessage);
            this.tabMain.Controls.Add(this.tabFillBusposition);
            this.tabMain.Location = new System.Drawing.Point(3, 1);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(278, 460);
            this.tabMain.TabIndex = 0;
            // 
            // tabGeneral
            // 
            this.tabGeneral.Controls.Add(this.groupBox4);
            this.tabGeneral.Controls.Add(this.groupBox3);
            this.tabGeneral.Controls.Add(this.groupBox2);
            this.tabGeneral.Controls.Add(this.groupBox1);
            this.tabGeneral.Controls.Add(this.groupBox7);
            this.tabGeneral.Location = new System.Drawing.Point(4, 22);
            this.tabGeneral.Name = "tabGeneral";
            this.tabGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.tabGeneral.Size = new System.Drawing.Size(270, 434);
            this.tabGeneral.TabIndex = 0;
            this.tabGeneral.Text = "General";
            this.tabGeneral.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.nudCycleYahoo);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.nudWaitBeforeClose);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.nudWaitAfterSetText);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Location = new System.Drawing.Point(7, 360);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(238, 68);
            this.groupBox4.TabIndex = 23;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Yahoo";
            // 
            // nudCycleYahoo
            // 
            this.nudCycleYahoo.Location = new System.Drawing.Point(65, 44);
            this.nudCycleYahoo.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudCycleYahoo.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudCycleYahoo.Name = "nudCycleYahoo";
            this.nudCycleYahoo.Size = new System.Drawing.Size(36, 20);
            this.nudCycleYahoo.TabIndex = 25;
            this.nudCycleYahoo.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.nudCycleYahoo.ValueChanged += new System.EventHandler(this.nudCycleYahoo_ValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 47);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Cycle(s):";
            // 
            // nudWaitBeforeClose
            // 
            this.nudWaitBeforeClose.Location = new System.Drawing.Point(183, 18);
            this.nudWaitBeforeClose.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudWaitBeforeClose.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudWaitBeforeClose.Name = "nudWaitBeforeClose";
            this.nudWaitBeforeClose.Size = new System.Drawing.Size(36, 20);
            this.nudWaitBeforeClose.TabIndex = 23;
            this.nudWaitBeforeClose.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudWaitBeforeClose.ValueChanged += new System.EventHandler(this.nudWaitBeforeClose_ValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(122, 21);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Wait Close:";
            // 
            // nudWaitAfterSetText
            // 
            this.nudWaitAfterSetText.Location = new System.Drawing.Point(65, 18);
            this.nudWaitAfterSetText.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudWaitAfterSetText.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudWaitAfterSetText.Name = "nudWaitAfterSetText";
            this.nudWaitAfterSetText.Size = new System.Drawing.Size(36, 20);
            this.nudWaitAfterSetText.TabIndex = 21;
            this.nudWaitAfterSetText.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudWaitAfterSetText.ValueChanged += new System.EventHandler(this.nudWaitAfterSetText_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Wait Send:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtCookieRS);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.txtCookieMU);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.txtCookieHF);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Location = new System.Drawing.Point(7, 259);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(238, 95);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Cookies";
            // 
            // txtCookieRS
            // 
            this.txtCookieRS.Location = new System.Drawing.Point(52, 70);
            this.txtCookieRS.Name = "txtCookieRS";
            this.txtCookieRS.Size = new System.Drawing.Size(180, 20);
            this.txtCookieRS.TabIndex = 22;
            this.txtCookieRS.TextChanged += new System.EventHandler(this.txtCookieRS_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 73);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "RS:";
            // 
            // txtCookieMU
            // 
            this.txtCookieMU.Location = new System.Drawing.Point(52, 44);
            this.txtCookieMU.Name = "txtCookieMU";
            this.txtCookieMU.Size = new System.Drawing.Size(180, 20);
            this.txtCookieMU.TabIndex = 20;
            this.txtCookieMU.TextChanged += new System.EventHandler(this.txtCookieMU_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 47);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "MU:";
            // 
            // txtCookieHF
            // 
            this.txtCookieHF.Location = new System.Drawing.Point(51, 18);
            this.txtCookieHF.Name = "txtCookieHF";
            this.txtCookieHF.Size = new System.Drawing.Size(180, 20);
            this.txtCookieHF.TabIndex = 18;
            this.txtCookieHF.TextChanged += new System.EventHandler(this.txtCookieHF_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "HF:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbServerLeech);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(6, 205);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(238, 48);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Leech Setting";
            // 
            // cbServerLeech
            // 
            this.cbServerLeech.FormattingEnabled = true;
            this.cbServerLeech.Location = new System.Drawing.Point(46, 18);
            this.cbServerLeech.Name = "cbServerLeech";
            this.cbServerLeech.Size = new System.Drawing.Size(186, 21);
            this.cbServerLeech.TabIndex = 13;
            this.cbServerLeech.SelectedIndexChanged += new System.EventHandler(this.cbServerLeech_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Server:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnFGPath);
            this.groupBox1.Controls.Add(this.txtFlashGetPath);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.btnIDMPath);
            this.groupBox1.Controls.Add(this.rbFlashGet);
            this.groupBox1.Controls.Add(this.txtIDMPath);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.rbIDM);
            this.groupBox1.Location = new System.Drawing.Point(6, 102);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(238, 97);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dowload Manager";
            // 
            // btnFGPath
            // 
            this.btnFGPath.Location = new System.Drawing.Point(209, 66);
            this.btnFGPath.Name = "btnFGPath";
            this.btnFGPath.Size = new System.Drawing.Size(24, 23);
            this.btnFGPath.TabIndex = 28;
            this.btnFGPath.Text = "...";
            this.btnFGPath.UseVisualStyleBackColor = true;
            this.btnFGPath.Click += new System.EventHandler(this.btnFGPath_Click);
            // 
            // txtFlashGetPath
            // 
            this.txtFlashGetPath.Location = new System.Drawing.Point(52, 68);
            this.txtFlashGetPath.Name = "txtFlashGetPath";
            this.txtFlashGetPath.Size = new System.Drawing.Size(157, 20);
            this.txtFlashGetPath.TabIndex = 27;
            this.txtFlashGetPath.Text = "c:\\Program Files\\FlashGet\\flashget.exe";
            this.txtFlashGetPath.TextChanged += new System.EventHandler(this.txtFlashGetPath_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(2, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "FG Path:";
            // 
            // btnIDMPath
            // 
            this.btnIDMPath.Location = new System.Drawing.Point(209, 40);
            this.btnIDMPath.Name = "btnIDMPath";
            this.btnIDMPath.Size = new System.Drawing.Size(24, 23);
            this.btnIDMPath.TabIndex = 25;
            this.btnIDMPath.Text = "...";
            this.btnIDMPath.UseVisualStyleBackColor = true;
            this.btnIDMPath.Click += new System.EventHandler(this.btnIDMPath_Click);
            // 
            // rbFlashGet
            // 
            this.rbFlashGet.AutoSize = true;
            this.rbFlashGet.Location = new System.Drawing.Point(66, 19);
            this.rbFlashGet.Name = "rbFlashGet";
            this.rbFlashGet.Size = new System.Drawing.Size(67, 17);
            this.rbFlashGet.TabIndex = 1;
            this.rbFlashGet.Text = "&FlashGet";
            this.rbFlashGet.UseVisualStyleBackColor = true;
            this.rbFlashGet.CheckedChanged += new System.EventHandler(this.rbFlashGet_CheckedChanged);
            // 
            // txtIDMPath
            // 
            this.txtIDMPath.Location = new System.Drawing.Point(52, 42);
            this.txtIDMPath.Name = "txtIDMPath";
            this.txtIDMPath.Size = new System.Drawing.Size(157, 20);
            this.txtIDMPath.TabIndex = 24;
            this.txtIDMPath.Text = "c:\\Program Files\\Internet Download Manager\\IDMan.exe";
            this.txtIDMPath.TextChanged += new System.EventHandler(this.txtIDMPath_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(2, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "IDM Path:";
            // 
            // rbIDM
            // 
            this.rbIDM.AutoSize = true;
            this.rbIDM.Checked = true;
            this.rbIDM.Location = new System.Drawing.Point(6, 19);
            this.rbIDM.Name = "rbIDM";
            this.rbIDM.Size = new System.Drawing.Size(45, 17);
            this.rbIDM.TabIndex = 0;
            this.rbIDM.TabStop = true;
            this.rbIDM.Text = "&IDM";
            this.rbIDM.UseVisualStyleBackColor = true;
            this.rbIDM.CheckedChanged += new System.EventHandler(this.rbIDM_CheckedChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.nudRepeatCycle);
            this.groupBox7.Controls.Add(this.label4);
            this.groupBox7.Controls.Add(this.txtTimeOut);
            this.groupBox7.Controls.Add(this.nudLinkNo);
            this.groupBox7.Controls.Add(this.btnSaveTo);
            this.groupBox7.Controls.Add(this.txtSaveTo);
            this.groupBox7.Controls.Add(this.label3);
            this.groupBox7.Controls.Add(this.label2);
            this.groupBox7.Controls.Add(this.label1);
            this.groupBox7.Location = new System.Drawing.Point(6, 6);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(238, 90);
            this.groupBox7.TabIndex = 8;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Download settings";
            // 
            // nudRepeatCycle
            // 
            this.nudRepeatCycle.Location = new System.Drawing.Point(180, 69);
            this.nudRepeatCycle.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.nudRepeatCycle.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudRepeatCycle.Name = "nudRepeatCycle";
            this.nudRepeatCycle.Size = new System.Drawing.Size(36, 20);
            this.nudRepeatCycle.TabIndex = 22;
            this.nudRepeatCycle.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudRepeatCycle.ValueChanged += new System.EventHandler(this.nudRepeatCycle_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(100, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Repeat Cycle:";
            // 
            // txtTimeOut
            // 
            this.txtTimeOut.Location = new System.Drawing.Point(180, 43);
            this.txtTimeOut.Name = "txtTimeOut";
            this.txtTimeOut.Size = new System.Drawing.Size(43, 20);
            this.txtTimeOut.TabIndex = 20;
            this.txtTimeOut.Text = "30";
            this.txtTimeOut.TextChanged += new System.EventHandler(this.txtTimeOut_TextChanged);
            // 
            // nudLinkNo
            // 
            this.nudLinkNo.Location = new System.Drawing.Point(52, 43);
            this.nudLinkNo.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudLinkNo.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudLinkNo.Name = "nudLinkNo";
            this.nudLinkNo.Size = new System.Drawing.Size(36, 20);
            this.nudLinkNo.TabIndex = 19;
            this.nudLinkNo.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudLinkNo.ValueChanged += new System.EventHandler(this.nudLinkNo_ValueChanged);
            // 
            // btnSaveTo
            // 
            this.btnSaveTo.Location = new System.Drawing.Point(209, 15);
            this.btnSaveTo.Name = "btnSaveTo";
            this.btnSaveTo.Size = new System.Drawing.Size(24, 23);
            this.btnSaveTo.TabIndex = 18;
            this.btnSaveTo.Text = "...";
            this.btnSaveTo.UseVisualStyleBackColor = true;
            this.btnSaveTo.Click += new System.EventHandler(this.btnSaveTo_Click);
            // 
            // txtSaveTo
            // 
            this.txtSaveTo.Location = new System.Drawing.Point(52, 17);
            this.txtSaveTo.Name = "txtSaveTo";
            this.txtSaveTo.Size = new System.Drawing.Size(157, 20);
            this.txtSaveTo.TabIndex = 17;
            this.txtSaveTo.TextChanged += new System.EventHandler(this.txtSaveTo_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(98, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Time out (min):";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Link No:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Save To:";
            // 
            // tabCbox
            // 
            this.tabCbox.Controls.Add(this.groupBox5);
            this.tabCbox.Location = new System.Drawing.Point(4, 22);
            this.tabCbox.Name = "tabCbox";
            this.tabCbox.Padding = new System.Windows.Forms.Padding(3);
            this.tabCbox.Size = new System.Drawing.Size(270, 434);
            this.tabCbox.TabIndex = 1;
            this.tabCbox.Text = "Cbox";
            this.tabCbox.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.nudMaxRead);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.nudShowMsg);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.nudReadCbox);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Location = new System.Drawing.Point(12, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(238, 68);
            this.groupBox5.TabIndex = 24;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Cbox (Seconds)";
            // 
            // nudMaxRead
            // 
            this.nudMaxRead.Location = new System.Drawing.Point(65, 44);
            this.nudMaxRead.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudMaxRead.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudMaxRead.Name = "nudMaxRead";
            this.nudMaxRead.Size = new System.Drawing.Size(36, 20);
            this.nudMaxRead.TabIndex = 25;
            this.nudMaxRead.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nudMaxRead.ValueChanged += new System.EventHandler(this.nudMaxRead_ValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 47);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(59, 13);
            this.label14.TabIndex = 24;
            this.label14.Text = "Max Read:";
            // 
            // nudShowMsg
            // 
            this.nudShowMsg.Location = new System.Drawing.Point(192, 18);
            this.nudShowMsg.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudShowMsg.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudShowMsg.Name = "nudShowMsg";
            this.nudShowMsg.Size = new System.Drawing.Size(36, 20);
            this.nudShowMsg.TabIndex = 23;
            this.nudShowMsg.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudShowMsg.ValueChanged += new System.EventHandler(this.nudShowMsg_ValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(122, 21);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(60, 13);
            this.label15.TabIndex = 22;
            this.label15.Text = "Show Msg:";
            // 
            // nudReadCbox
            // 
            this.nudReadCbox.Location = new System.Drawing.Point(65, 18);
            this.nudReadCbox.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudReadCbox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudReadCbox.Name = "nudReadCbox";
            this.nudReadCbox.Size = new System.Drawing.Size(36, 20);
            this.nudReadCbox.TabIndex = 21;
            this.nudReadCbox.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nudReadCbox.ValueChanged += new System.EventHandler(this.nudReadCbox_ValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(63, 13);
            this.label16.TabIndex = 20;
            this.label16.Text = "Read Cbox:";
            // 
            // tabDatabase
            // 
            this.tabDatabase.Controls.Add(this.groupBox6);
            this.tabDatabase.Location = new System.Drawing.Point(4, 22);
            this.tabDatabase.Name = "tabDatabase";
            this.tabDatabase.Padding = new System.Windows.Forms.Padding(3);
            this.tabDatabase.Size = new System.Drawing.Size(270, 434);
            this.tabDatabase.TabIndex = 2;
            this.tabDatabase.Text = "Database";
            this.tabDatabase.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtDBName);
            this.groupBox6.Controls.Add(this.label20);
            this.groupBox6.Controls.Add(this.txtDBPass);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.Controls.Add(this.txtDBUser);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Controls.Add(this.txtDBServer);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.Location = new System.Drawing.Point(6, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(238, 123);
            this.groupBox6.TabIndex = 15;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Sql server";
            // 
            // txtDBName
            // 
            this.txtDBName.Location = new System.Drawing.Point(65, 97);
            this.txtDBName.Name = "txtDBName";
            this.txtDBName.Size = new System.Drawing.Size(167, 20);
            this.txtDBName.TabIndex = 24;
            this.txtDBName.TextChanged += new System.EventHandler(this.txtDBName_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 99);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(56, 13);
            this.label20.TabIndex = 23;
            this.label20.Text = "Database:";
            // 
            // txtDBPass
            // 
            this.txtDBPass.Location = new System.Drawing.Point(65, 70);
            this.txtDBPass.Name = "txtDBPass";
            this.txtDBPass.Size = new System.Drawing.Size(167, 20);
            this.txtDBPass.TabIndex = 22;
            this.txtDBPass.TextChanged += new System.EventHandler(this.txtDBPass_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 73);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(33, 13);
            this.label17.TabIndex = 21;
            this.label17.Text = "Pass:";
            // 
            // txtDBUser
            // 
            this.txtDBUser.Location = new System.Drawing.Point(65, 44);
            this.txtDBUser.Name = "txtDBUser";
            this.txtDBUser.Size = new System.Drawing.Size(167, 20);
            this.txtDBUser.TabIndex = 20;
            this.txtDBUser.TextChanged += new System.EventHandler(this.txtDBUser_TextChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 47);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(32, 13);
            this.label18.TabIndex = 19;
            this.label18.Text = "User:";
            // 
            // txtDBServer
            // 
            this.txtDBServer.Location = new System.Drawing.Point(64, 18);
            this.txtDBServer.Name = "txtDBServer";
            this.txtDBServer.Size = new System.Drawing.Size(167, 20);
            this.txtDBServer.TabIndex = 18;
            this.txtDBServer.TextChanged += new System.EventHandler(this.txtDBServer_TextChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 21);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(59, 13);
            this.label19.TabIndex = 12;
            this.label19.Text = "DB Server:";
            // 
            // tabFillMessage
            // 
            this.tabFillMessage.Controls.Add(this.groupBox9);
            this.tabFillMessage.Controls.Add(this.groupBox8);
            this.tabFillMessage.Location = new System.Drawing.Point(4, 22);
            this.tabFillMessage.Name = "tabFillMessage";
            this.tabFillMessage.Size = new System.Drawing.Size(270, 434);
            this.tabFillMessage.TabIndex = 3;
            this.tabFillMessage.Text = "Fill Message";
            this.tabFillMessage.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.cbDesDBName);
            this.groupBox9.Controls.Add(this.label25);
            this.groupBox9.Controls.Add(this.txtMsgDesPass);
            this.groupBox9.Controls.Add(this.label26);
            this.groupBox9.Controls.Add(this.txtMsgDesUser);
            this.groupBox9.Controls.Add(this.label27);
            this.groupBox9.Controls.Add(this.txtMsgDesDBServer);
            this.groupBox9.Controls.Add(this.label28);
            this.groupBox9.Location = new System.Drawing.Point(12, 150);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(238, 123);
            this.groupBox9.TabIndex = 25;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Des DB";
            // 
            // cbDesDBName
            // 
            this.cbDesDBName.FormattingEnabled = true;
            this.cbDesDBName.Location = new System.Drawing.Point(64, 96);
            this.cbDesDBName.Name = "cbDesDBName";
            this.cbDesDBName.Size = new System.Drawing.Size(168, 21);
            this.cbDesDBName.TabIndex = 25;
            this.cbDesDBName.SelectedIndexChanged += new System.EventHandler(this.cbDesDBName_SelectedIndexChanged);
            this.cbDesDBName.DropDown += new System.EventHandler(this.cbDesDBName_DropDown);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 99);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(56, 13);
            this.label25.TabIndex = 23;
            this.label25.Text = "Database:";
            // 
            // txtMsgDesPass
            // 
            this.txtMsgDesPass.Location = new System.Drawing.Point(65, 70);
            this.txtMsgDesPass.Name = "txtMsgDesPass";
            this.txtMsgDesPass.Size = new System.Drawing.Size(167, 20);
            this.txtMsgDesPass.TabIndex = 22;
            this.txtMsgDesPass.Text = "etm";
            this.txtMsgDesPass.TextChanged += new System.EventHandler(this.txtMsgDesPass_TextChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 73);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(33, 13);
            this.label26.TabIndex = 21;
            this.label26.Text = "Pass:";
            // 
            // txtMsgDesUser
            // 
            this.txtMsgDesUser.Location = new System.Drawing.Point(65, 44);
            this.txtMsgDesUser.Name = "txtMsgDesUser";
            this.txtMsgDesUser.Size = new System.Drawing.Size(167, 20);
            this.txtMsgDesUser.TabIndex = 20;
            this.txtMsgDesUser.Text = "etm";
            this.txtMsgDesUser.TextChanged += new System.EventHandler(this.txtMsgDesUser_TextChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 47);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(32, 13);
            this.label27.TabIndex = 19;
            this.label27.Text = "User:";
            // 
            // txtMsgDesDBServer
            // 
            this.txtMsgDesDBServer.Location = new System.Drawing.Point(64, 18);
            this.txtMsgDesDBServer.Name = "txtMsgDesDBServer";
            this.txtMsgDesDBServer.Size = new System.Drawing.Size(167, 20);
            this.txtMsgDesDBServer.TabIndex = 18;
            this.txtMsgDesDBServer.Text = "xp-testing2\\sqlexpress";
            this.txtMsgDesDBServer.TextChanged += new System.EventHandler(this.txtMsgDesDBServer_TextChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 21);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(59, 13);
            this.label28.TabIndex = 12;
            this.label28.Text = "DB Server:";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.cbSourceDBName);
            this.groupBox8.Controls.Add(this.label21);
            this.groupBox8.Controls.Add(this.txtMsgSourcePass);
            this.groupBox8.Controls.Add(this.label22);
            this.groupBox8.Controls.Add(this.txtMsgSourceUser);
            this.groupBox8.Controls.Add(this.label23);
            this.groupBox8.Controls.Add(this.txtMsgSourceDBServer);
            this.groupBox8.Controls.Add(this.label24);
            this.groupBox8.Location = new System.Drawing.Point(12, 12);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(238, 123);
            this.groupBox8.TabIndex = 16;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Source DB";
            // 
            // cbSourceDBName
            // 
            this.cbSourceDBName.FormattingEnabled = true;
            this.cbSourceDBName.Location = new System.Drawing.Point(64, 96);
            this.cbSourceDBName.Name = "cbSourceDBName";
            this.cbSourceDBName.Size = new System.Drawing.Size(168, 21);
            this.cbSourceDBName.TabIndex = 24;
            this.cbSourceDBName.SelectedIndexChanged += new System.EventHandler(this.cbSourceDBName_SelectedIndexChanged);
            this.cbSourceDBName.DropDown += new System.EventHandler(this.cbSourceDBName_DropDown);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 99);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(56, 13);
            this.label21.TabIndex = 23;
            this.label21.Text = "Database:";
            // 
            // txtMsgSourcePass
            // 
            this.txtMsgSourcePass.Location = new System.Drawing.Point(65, 70);
            this.txtMsgSourcePass.Name = "txtMsgSourcePass";
            this.txtMsgSourcePass.Size = new System.Drawing.Size(167, 20);
            this.txtMsgSourcePass.TabIndex = 22;
            this.txtMsgSourcePass.Text = "etm";
            this.txtMsgSourcePass.TextChanged += new System.EventHandler(this.txtMsgSourcePass_TextChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 73);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(33, 13);
            this.label22.TabIndex = 21;
            this.label22.Text = "Pass:";
            // 
            // txtMsgSourceUser
            // 
            this.txtMsgSourceUser.Location = new System.Drawing.Point(65, 44);
            this.txtMsgSourceUser.Name = "txtMsgSourceUser";
            this.txtMsgSourceUser.Size = new System.Drawing.Size(167, 20);
            this.txtMsgSourceUser.TabIndex = 20;
            this.txtMsgSourceUser.Text = "etm";
            this.txtMsgSourceUser.TextChanged += new System.EventHandler(this.txtMsgSourceUser_TextChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 47);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(32, 13);
            this.label23.TabIndex = 19;
            this.label23.Text = "User:";
            // 
            // txtMsgSourceDBServer
            // 
            this.txtMsgSourceDBServer.Location = new System.Drawing.Point(64, 18);
            this.txtMsgSourceDBServer.Name = "txtMsgSourceDBServer";
            this.txtMsgSourceDBServer.Size = new System.Drawing.Size(167, 20);
            this.txtMsgSourceDBServer.TabIndex = 18;
            this.txtMsgSourceDBServer.Text = "xp-testing2\\sqlexpress";
            this.txtMsgSourceDBServer.TextChanged += new System.EventHandler(this.txtMsgSourceDBServer_TextChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 21);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(59, 13);
            this.label24.TabIndex = 12;
            this.label24.Text = "DB Server:";
            // 
            // tabFillBusposition
            // 
            this.tabFillBusposition.Controls.Add(this.groupBox13);
            this.tabFillBusposition.Controls.Add(this.groupBox12);
            this.tabFillBusposition.Location = new System.Drawing.Point(4, 22);
            this.tabFillBusposition.Name = "tabFillBusposition";
            this.tabFillBusposition.Size = new System.Drawing.Size(270, 434);
            this.tabFillBusposition.TabIndex = 4;
            this.tabFillBusposition.Text = "Fill Busposition";
            this.tabFillBusposition.UseVisualStyleBackColor = true;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.cbBusDesDBName);
            this.groupBox13.Controls.Add(this.label41);
            this.groupBox13.Controls.Add(this.txtBusDesPass);
            this.groupBox13.Controls.Add(this.label42);
            this.groupBox13.Controls.Add(this.txtBusDesUser);
            this.groupBox13.Controls.Add(this.label43);
            this.groupBox13.Controls.Add(this.txtBusDesDBServer);
            this.groupBox13.Controls.Add(this.label44);
            this.groupBox13.Location = new System.Drawing.Point(12, 150);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(238, 123);
            this.groupBox13.TabIndex = 26;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Des DB";
            // 
            // cbBusDesDBName
            // 
            this.cbBusDesDBName.FormattingEnabled = true;
            this.cbBusDesDBName.Location = new System.Drawing.Point(64, 96);
            this.cbBusDesDBName.Name = "cbBusDesDBName";
            this.cbBusDesDBName.Size = new System.Drawing.Size(168, 21);
            this.cbBusDesDBName.TabIndex = 25;
            this.cbBusDesDBName.SelectedIndexChanged += new System.EventHandler(this.cbBusDesDBName_SelectedIndexChanged);
            this.cbBusDesDBName.DropDown += new System.EventHandler(this.cbBusDesDBName_DropDown);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(6, 99);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(56, 13);
            this.label41.TabIndex = 23;
            this.label41.Text = "Database:";
            // 
            // txtBusDesPass
            // 
            this.txtBusDesPass.Location = new System.Drawing.Point(65, 70);
            this.txtBusDesPass.Name = "txtBusDesPass";
            this.txtBusDesPass.Size = new System.Drawing.Size(167, 20);
            this.txtBusDesPass.TabIndex = 22;
            this.txtBusDesPass.Text = "etm";
            this.txtBusDesPass.TextChanged += new System.EventHandler(this.txtBusDesPass_TextChanged);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(6, 73);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(33, 13);
            this.label42.TabIndex = 21;
            this.label42.Text = "Pass:";
            // 
            // txtBusDesUser
            // 
            this.txtBusDesUser.Location = new System.Drawing.Point(65, 44);
            this.txtBusDesUser.Name = "txtBusDesUser";
            this.txtBusDesUser.Size = new System.Drawing.Size(167, 20);
            this.txtBusDesUser.TabIndex = 20;
            this.txtBusDesUser.Text = "etm";
            this.txtBusDesUser.TextChanged += new System.EventHandler(this.txtBusDesUser_TextChanged);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(6, 47);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(32, 13);
            this.label43.TabIndex = 19;
            this.label43.Text = "User:";
            // 
            // txtBusDesDBServer
            // 
            this.txtBusDesDBServer.Location = new System.Drawing.Point(64, 18);
            this.txtBusDesDBServer.Name = "txtBusDesDBServer";
            this.txtBusDesDBServer.Size = new System.Drawing.Size(167, 20);
            this.txtBusDesDBServer.TabIndex = 18;
            this.txtBusDesDBServer.Text = "xp-testing2\\sqlexpress";
            this.txtBusDesDBServer.TextChanged += new System.EventHandler(this.txtBusDesDBServer_TextChanged);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(6, 21);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(59, 13);
            this.label44.TabIndex = 12;
            this.label44.Text = "DB Server:";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.cbBusSourceDBName);
            this.groupBox12.Controls.Add(this.label37);
            this.groupBox12.Controls.Add(this.txtBusSourcePass);
            this.groupBox12.Controls.Add(this.label38);
            this.groupBox12.Controls.Add(this.txtBusSourceUser);
            this.groupBox12.Controls.Add(this.label39);
            this.groupBox12.Controls.Add(this.txtBusSourceDBServer);
            this.groupBox12.Controls.Add(this.label40);
            this.groupBox12.Location = new System.Drawing.Point(12, 12);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(238, 123);
            this.groupBox12.TabIndex = 17;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Source DB";
            // 
            // cbBusSourceDBName
            // 
            this.cbBusSourceDBName.FormattingEnabled = true;
            this.cbBusSourceDBName.Location = new System.Drawing.Point(64, 96);
            this.cbBusSourceDBName.Name = "cbBusSourceDBName";
            this.cbBusSourceDBName.Size = new System.Drawing.Size(168, 21);
            this.cbBusSourceDBName.TabIndex = 24;
            this.cbBusSourceDBName.SelectedIndexChanged += new System.EventHandler(this.cbBusSourceDBName_SelectedIndexChanged);
            this.cbBusSourceDBName.DropDown += new System.EventHandler(this.cbBusSourceDBName_DropDown);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(6, 99);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(56, 13);
            this.label37.TabIndex = 23;
            this.label37.Text = "Database:";
            // 
            // txtBusSourcePass
            // 
            this.txtBusSourcePass.Location = new System.Drawing.Point(65, 70);
            this.txtBusSourcePass.Name = "txtBusSourcePass";
            this.txtBusSourcePass.Size = new System.Drawing.Size(167, 20);
            this.txtBusSourcePass.TabIndex = 22;
            this.txtBusSourcePass.Text = "etm";
            this.txtBusSourcePass.TextChanged += new System.EventHandler(this.txtBusSourcePass_TextChanged);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(6, 73);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(33, 13);
            this.label38.TabIndex = 21;
            this.label38.Text = "Pass:";
            // 
            // txtBusSourceUser
            // 
            this.txtBusSourceUser.Location = new System.Drawing.Point(65, 44);
            this.txtBusSourceUser.Name = "txtBusSourceUser";
            this.txtBusSourceUser.Size = new System.Drawing.Size(167, 20);
            this.txtBusSourceUser.TabIndex = 20;
            this.txtBusSourceUser.Text = "etm";
            this.txtBusSourceUser.TextChanged += new System.EventHandler(this.txtBusSourceUser_TextChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 47);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(32, 13);
            this.label39.TabIndex = 19;
            this.label39.Text = "User:";
            // 
            // txtBusSourceDBServer
            // 
            this.txtBusSourceDBServer.Location = new System.Drawing.Point(64, 18);
            this.txtBusSourceDBServer.Name = "txtBusSourceDBServer";
            this.txtBusSourceDBServer.Size = new System.Drawing.Size(167, 20);
            this.txtBusSourceDBServer.TabIndex = 18;
            this.txtBusSourceDBServer.Text = "xp-testing2\\sqlexpress";
            this.txtBusSourceDBServer.TextChanged += new System.EventHandler(this.txtBusSourceDBServer_TextChanged);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(6, 21);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(59, 13);
            this.label40.TabIndex = 12;
            this.label40.Text = "DB Server:";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(87, 467);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(46, 23);
            this.btnOk.TabIndex = 10;
            this.btnOk.Text = "&OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(149, 467);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(46, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(211, 467);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(46, 23);
            this.btnApply.TabIndex = 12;
            this.btnApply.Text = "&Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnAcc
            // 
            this.btnAcc.Location = new System.Drawing.Point(25, 467);
            this.btnAcc.Name = "btnAcc";
            this.btnAcc.Size = new System.Drawing.Size(46, 23);
            this.btnAcc.TabIndex = 13;
            this.btnAcc.Text = "&Acc";
            this.btnAcc.UseVisualStyleBackColor = true;
            this.btnAcc.Click += new System.EventHandler(this.btnAcc_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.comboBox1);
            this.groupBox10.Controls.Add(this.label29);
            this.groupBox10.Controls.Add(this.textBox1);
            this.groupBox10.Controls.Add(this.label30);
            this.groupBox10.Controls.Add(this.textBox2);
            this.groupBox10.Controls.Add(this.label31);
            this.groupBox10.Controls.Add(this.textBox3);
            this.groupBox10.Controls.Add(this.label32);
            this.groupBox10.Location = new System.Drawing.Point(12, 150);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(238, 123);
            this.groupBox10.TabIndex = 25;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Des DB";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(64, 96);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(168, 21);
            this.comboBox1.TabIndex = 25;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 99);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(56, 13);
            this.label29.TabIndex = 23;
            this.label29.Text = "Database:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(65, 70);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(167, 20);
            this.textBox1.TabIndex = 22;
            this.textBox1.Text = "etm";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 73);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(33, 13);
            this.label30.TabIndex = 21;
            this.label30.Text = "Pass:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(65, 44);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(167, 20);
            this.textBox2.TabIndex = 20;
            this.textBox2.Text = "etm";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 47);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(32, 13);
            this.label31.TabIndex = 19;
            this.label31.Text = "User:";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(64, 18);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(167, 20);
            this.textBox3.TabIndex = 18;
            this.textBox3.Text = "xp-testing2\\sqlexpress";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 21);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(59, 13);
            this.label32.TabIndex = 12;
            this.label32.Text = "DB Server:";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.comboBox2);
            this.groupBox11.Controls.Add(this.label33);
            this.groupBox11.Controls.Add(this.textBox4);
            this.groupBox11.Controls.Add(this.label34);
            this.groupBox11.Controls.Add(this.textBox5);
            this.groupBox11.Controls.Add(this.label35);
            this.groupBox11.Controls.Add(this.textBox6);
            this.groupBox11.Controls.Add(this.label36);
            this.groupBox11.Location = new System.Drawing.Point(12, 12);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(238, 123);
            this.groupBox11.TabIndex = 16;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Source DB";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(64, 96);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(168, 21);
            this.comboBox2.TabIndex = 24;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(6, 99);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(56, 13);
            this.label33.TabIndex = 23;
            this.label33.Text = "Database:";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(65, 70);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(167, 20);
            this.textBox4.TabIndex = 22;
            this.textBox4.Text = "etm";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 73);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(33, 13);
            this.label34.TabIndex = 21;
            this.label34.Text = "Pass:";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(65, 44);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(167, 20);
            this.textBox5.TabIndex = 20;
            this.textBox5.Text = "etm";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 47);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(32, 13);
            this.label35.TabIndex = 19;
            this.label35.Text = "User:";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(64, 18);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(167, 20);
            this.textBox6.TabIndex = 18;
            this.textBox6.Text = "xp-testing2\\sqlexpress";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(6, 21);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(59, 13);
            this.label36.TabIndex = 12;
            this.label36.Text = "DB Server:";
            // 
            // frmOption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(283, 502);
            this.Controls.Add(this.btnAcc);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.tabMain);
            this.Controls.Add(this.btnOk);
            this.Name = "frmOption";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Option";
            this.Load += new System.EventHandler(this.frmOption_Load);
            this.tabMain.ResumeLayout(false);
            this.tabGeneral.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCycleYahoo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudWaitBeforeClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudWaitAfterSetText)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRepeatCycle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLinkNo)).EndInit();
            this.tabCbox.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxRead)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudShowMsg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudReadCbox)).EndInit();
            this.tabDatabase.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tabFillMessage.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.tabFillBusposition.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabGeneral;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox txtTimeOut;
        private System.Windows.Forms.NumericUpDown nudLinkNo;
        private System.Windows.Forms.Button btnSaveTo;
        private System.Windows.Forms.TextBox txtSaveTo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.NumericUpDown nudRepeatCycle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnFGPath;
        private System.Windows.Forms.TextBox txtFlashGetPath;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnIDMPath;
        private System.Windows.Forms.TextBox txtIDMPath;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cbServerLeech;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton rbFlashGet;
        private System.Windows.Forms.RadioButton rbIDM;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCookieRS;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtCookieMU;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtCookieHF;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.NumericUpDown nudWaitBeforeClose;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown nudWaitAfterSetText;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown nudCycleYahoo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnAcc;
        private System.Windows.Forms.TabPage tabCbox;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.NumericUpDown nudMaxRead;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown nudShowMsg;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown nudReadCbox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TabPage tabDatabase;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtDBPass;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtDBUser;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtDBServer;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtDBName;
        private System.Windows.Forms.TabPage tabFillMessage;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtMsgDesPass;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtMsgDesUser;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtMsgDesDBServer;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtMsgSourcePass;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtMsgSourceUser;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtMsgSourceDBServer;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cbDesDBName;
        private System.Windows.Forms.ComboBox cbSourceDBName;
        private System.Windows.Forms.TabPage tabFillBusposition;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.ComboBox cbBusDesDBName;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtBusDesPass;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtBusDesUser;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txtBusDesDBServer;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.ComboBox cbBusSourceDBName;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtBusSourcePass;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtBusSourceUser;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtBusSourceDBServer;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label36;
    }
}