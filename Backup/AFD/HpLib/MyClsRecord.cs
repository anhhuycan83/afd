using System;
using System.Collections.Generic;
using System.Text;

namespace AFD.HpLib
{
    class MyClsRecord
    {
        public MyClsRecord()
        {
            IsHasRecord = false;
        }
        
        private bool isHasRecord;

        public bool IsHasRecord
        {
            get { return isHasRecord; }
            set { isHasRecord = value; }
        }

        private Int64 m_message_id;
        public Int64 message_id
        {
            get { return m_message_id; }
            set { m_message_id = value; }
        }
        private DateTime m_message_dt_received;
        public DateTime message_dt_received
        {
            get { return m_message_dt_received; }
            set { m_message_dt_received = value; }
        }
        private DateTime m_message_dt_event;
        public DateTime message_dt_event
        {
            get { return m_message_dt_event; }
            set { m_message_dt_event = value; }
        }
        private int m_message_event_id;
        public int message_event_id
        {
            get { return m_message_event_id; }
            set { m_message_event_id = value; }
        }
        private string m_message_vehicle_id;
        public string message_vehicle_id
        {
            get { return m_message_vehicle_id; }
            set { m_message_vehicle_id = value; }
        }
        private double m_message_latitude;
        public double message_latitude
        {
            get { return m_message_latitude; }
            set { m_message_latitude = value; }
        }
        private double m_message_longitude;
        public double message_longitude
        {
            get { return m_message_longitude; }
            set { m_message_longitude = value; }
        }
        private string m_message_comment;
        public string message_comment
        {
            get { return m_message_comment; }
            set { m_message_comment = value; }
        }
        private DateTime m_message_dt_adjusted;
        public DateTime message_dt_adjusted
        {
            get { return m_message_dt_adjusted; }
            set { m_message_dt_adjusted = value; }
        }

    }
}
