using System;
using System.Collections.Generic;
using System.Text;

namespace AFD.HpLib
{
    class MyClsRecordBus
    {
        public MyClsRecordBus()
        {
            IsHasRecord = false;
        }
        private bool isHasRecord;

        public bool IsHasRecord
        {
            get { return isHasRecord; }
            set { isHasRecord = value; }
        }

        private Int64 m_ID;
        public Int64 ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        private Int64 m_IDMessage;
        public Int64 IDMessage
        {
            get { return m_IDMessage; }
            set { m_IDMessage = value; }
        }
        private int m_DistrictID;
        public int DistrictID
        {
            get { return m_DistrictID; }
            set { m_DistrictID = value; }
        }
        private string m_UnitID;
        public string UnitID
        {
            get { return m_UnitID; }
            set { m_UnitID = value; }
        }
        private float m_Latitude;
        public float Latitude
        {
            get { return m_Latitude; }
            set { m_Latitude = value; }
        }
        private float m_Longitude;
        public float Longitude
        {
            get { return m_Longitude; }
            set { m_Longitude = value; }
        }
        private DateTime m_WriteTimeStamp;
        public DateTime WriteTimeStamp
        {
            get { return m_WriteTimeStamp; }
            set { m_WriteTimeStamp = value; }
        }
        private DateTime m_PositionTimeStamp;
        public DateTime PositionTimeStamp
        {
            get { return m_PositionTimeStamp; }
            set { m_PositionTimeStamp = value; }
        }
        private int m_EventID;
        public int EventID
        {
            get { return m_EventID; }
            set { m_EventID = value; }
        }
        private float m_Speed;
        public float Speed
        {
            get { return m_Speed; }
            set { m_Speed = value; }
        }
        private float m_RSSI;
        public float RSSI
        {
            get { return m_RSSI; }
            set { m_RSSI = value; }
        }
        private float m_RouteMiles;
        public float RouteMiles
        {
            get { return m_RouteMiles; }
            set { m_RouteMiles = value; }
        }
    }
}
