using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Web;
using System.Drawing;
using System.Text.RegularExpressions;
using System.IO;


namespace AFD.HPLib
{
    public enum MyServerLeechType
    {
        DOILACADINH = 1,
        TEENHK = 2,
        DAT4 = 3,
        KRALKLIPLER = 4,        
        Expressleech = 5,
        LinkleechSv2 = 6,
        AwsSv3 = 7,
        //Leech173 = 8,
        Mega24h = 9,
        Yoyocici = 10,
        Freerapidshareleech = 11,
        Irleech = 12,
        Nightflare = 13        
    }
    
    public class MyClsFile
    {
        public enum MyReturnCode
        {
            SUCCESSFUL = 1,
            VUI_LONG_CHO_LUOT_DOWN_KE_TIEP = 2,
            FILE_WAS_DELETED = 3,
            UN_KNOWN = 4
        }
        public enum MyUrlType
        {
            RAPIDSHARE = 1,
            MEGAUPLOAD = 2,
            HOTFILE = 3,
            MEDIAFIRE_FILE = 4,
            MEDIAFIRE_FOLDER = 5,
            DIRECT_LINK = 6,
            ADF_LINK = 7,
            FILESERVE = 8,
            FILESONIC = 9,
            NETLOAD = 10,
            MEGASHAREDOTCOM= 11,
            UPLOADED = 12,
            FILEFACORY = 13,
            DEPOSITFILES = 14,
            AccDied = 15,
            HOTFILEFOLDER = 16
        }
        public enum MyFileStatus
        {
            ADDED = 0,
            QUEUED = 1,
            DOWNLOADING = 2,
            DOWNLOADED = 3,
            WORKING = 4,
            FAILED = 5,
            TIME_OUT = 6,
            PASSWORD_INCORRECT = 7,
            CANCELED = 8,
            NOT_SUPPORT = 9,
            FILE_DELEDTED = 10
        }
        private static string m_logFile = @"c:\log.txt";

        public static string LogFile
        {
            get { return MyClsFile.m_logFile; }
            set { MyClsFile.m_logFile = value; }
        }

        private string m_fileName;

        public string FileName
        {
            get { return m_fileName; }
            set { m_fileName = value; }
        }

        private string m_fileUrl;

        public string FileUrl
        {
            get { return m_fileUrl; }
            set { m_fileUrl = value; }
        }

        private MyClsFile.MyUrlType m_fileUrlType;

        internal MyClsFile.MyUrlType FileUrlType
        {
            get { return m_fileUrlType; }
            set { m_fileUrlType = value; }
        }

        private string m_fileID;

        public string FileID
        {
            get { return m_fileID; }
            set { m_fileID = value; }
        }
        private string m_filePassword;

        public string FilePassword
        {
            get { return m_filePassword; }
            set { m_filePassword = value; }
        }

        private string m_fileSize;

        public string FileSize
        {
            get { return m_fileSize; }
            set { m_fileSize = value; }
        }

        private string m_fileDownCount;

        public string FileDownCount
        {
            get { return m_fileDownCount; }
            set { m_fileDownCount = value; }
        }

        public string m_fileDirectLink;

        public string FileDirectLink
        {
            get { return m_fileDirectLink; }
            set { m_fileDirectLink = value; }
        }

        private string m_fileStrCatcha;

        public string FileStrCatcha
        {
            get { return m_fileStrCatcha; }
            set { m_fileStrCatcha = value; }
        }

        private IntPtr m_hMsg;

        public IntPtr HMsg
        {
            get { return m_hMsg; }
            set { m_hMsg = value; }
        }

        private IntPtr hContain;

        public IntPtr HContain
        {
            get { return hContain; }
            set { hContain = value; }
        }

        private string m_yUser;

        public string YUser
        {
            get { return m_yUser; }
            set { m_yUser = value; }
        }

        private MyFileStatus m_fileStatus;

        public MyFileStatus FileStatus
        {
            get { return m_fileStatus; }
            set { m_fileStatus = value; }
        }

        //add new member
        public CookieCollection m_cookieCollection;
        public CookieCollection m_cookieCollection2;

        public Bitmap catCha;

        public MyClsFile()
        {
            //catCha = new Bitmap(
            m_cookieCollection = new CookieCollection();
            m_cookieCollection2 = new CookieCollection();
        }

        public static string MyFormatStr(string ins_String, int width, bool rightAligned)
        {
            string strResult;
            string strFormat;
            if (rightAligned)
            {
                strFormat = "{0, " + width.ToString() + "}";

            }
            else
            {
                strFormat = "{0, -" + width.ToString() + "}";
            }

            if (ins_String == null)
                ins_String = "";

            if (ins_String.Length > width)
            {
                return ins_String = ins_String.Substring(0, width);
            }

            return strResult = string.Format(strFormat, ins_String);
        }

        public static MyClsFile.MyUrlType GetLinkType(MyClsFile fileItem)
        {
            MyClsFile.MyUrlType fileType = MyUrlType.DIRECT_LINK;
            string s1;
            MatchCollection matchCollection1;
            if (fileItem.FileUrl.Contains("rapidshare"))
            {
                fileType = MyUrlType.RAPIDSHARE;
            }
            else
            {
                if (fileItem.FileUrl.Contains("megaupload.com"))
                {
                    //http://www.megaupload.com/vn/?d=I9CFZBDT
                    s1 = @"megaupload.com.+(\?d=\w+)";
                    matchCollection1 = Regex.Matches(fileItem.FileUrl, s1, RegexOptions.IgnoreCase);

                    if (matchCollection1.Count > 0)
                    {
                        fileItem.FileUrl = "http://www.megaupload.com/" + matchCollection1[0].Groups[1].Value;
                        fileType = MyUrlType.MEGAUPLOAD;
                    }
                }
                else
                {
                    if (fileItem.FileUrl.Contains("hotfile"))
                    {
                        fileType = MyUrlType.HOTFILE;
                    }
                    else
                    {
                        if (fileItem.FileUrl.Contains("mediafire"))
                        {
                            //http://www.mediafire.com/download.php?wkjzzwkemyy
                            fileItem.FileUrl = fileItem.FileUrl.Replace("http://www.mediafire.com/download.php?", "http://www.mediafire.com/?");
                            fileItem.FileUrl = fileItem.FileUrl.Replace("http://mediafire.com/download.php?", "http://www.mediafire.com/?");

                            //http://www.mediafire.com/file/ejj8n8dw71at7l9/InTouch Lock 3.4.rar http://www.mediafire.com/?ejj8n8dw71at7l9
                            s1 = @"http://.+/file/(.+)/.+";
                            matchCollection1 = Regex.Matches(fileItem.FileUrl, s1, RegexOptions.IgnoreCase);

                            if (matchCollection1.Count > 0)
                            {
                                fileItem.FileUrl = "http://www.mediafire.com/?" + matchCollection1[0].Groups[1].Value;
                            }
                            if (fileItem.FileUrl.Contains("mediafire.com/?sharekey="))
                                fileType = MyUrlType.MEDIAFIRE_FOLDER;
                            else
                                fileType = MyUrlType.MEDIAFIRE_FILE;
                        }
                        else
                        {
                            s1 = @".*adf.ly/.+";
                            matchCollection1 = Regex.Matches(fileItem.FileUrl, s1, RegexOptions.IgnoreCase);

                            if (matchCollection1.Count > 0)
                            {
                                fileType = MyUrlType.ADF_LINK;
                            }                            
                            else
                            {
                                s1 = @".*fileserve.com/file.+";
                                matchCollection1 = Regex.Matches(fileItem.FileUrl, s1, RegexOptions.IgnoreCase);

                                if (matchCollection1.Count > 0)
                                {
                                    fileType = MyUrlType.FILESERVE;
                                }
                                else
                                {
                                    //http://www.filesonic.com/file/37142125/Gmail Notifier v1.0.0.86.rar
                                    s1 = @".*filesonic.com/file.+";
                                    matchCollection1 = Regex.Matches(fileItem.FileUrl, s1, RegexOptions.IgnoreCase);

                                    if (matchCollection1.Count > 0)
                                    {
                                        fileType = MyUrlType.FILESONIC;
                                    }
                                    else
                                    {
                                        //netload.in
                                        s1 = @".*netload.in.+";
                                        matchCollection1 = Regex.Matches(fileItem.FileUrl, s1, RegexOptions.IgnoreCase);

                                        if (matchCollection1.Count > 0)
                                        {
                                            fileType = MyUrlType.NETLOAD;
                                        }
                                        else
                                        {
                                            //megashare.com
                                            s1 = @".*megashare.com.+";
                                            matchCollection1 = Regex.Matches(fileItem.FileUrl, s1, RegexOptions.IgnoreCase);

                                            if (matchCollection1.Count > 0)
                                            {
                                                fileType = MyUrlType.MEGASHAREDOTCOM;
                                            }
                                            else
                                            {
                                                //uploaded.to
                                                s1 = @".*uploaded.to.+";
                                                matchCollection1 = Regex.Matches(fileItem.FileUrl, s1, RegexOptions.IgnoreCase);

                                                if (matchCollection1.Count > 0)
                                                {
                                                    fileType = MyUrlType.UPLOADED;
                                                }
                                                else
                                                {
                                                    //filefactory.com
                                                    s1 = @".*filefactory.com.+";
                                                    matchCollection1 = Regex.Matches(fileItem.FileUrl, s1, RegexOptions.IgnoreCase);

                                                    if (matchCollection1.Count > 0)
                                                    {
                                                        fileType = MyUrlType.FILEFACORY;
                                                    }
                                                    else
                                                    {
                                                        fileType = MyUrlType.DIRECT_LINK;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return fileType;
        }

        public static string GetFileNameFromDirectLink(string ins_strDirectLink)
        {            
            string sRs = "";
            string[] strArr = ins_strDirectLink.Split(new char[] { '/' });
            if (strArr.Length > 1)
                sRs = strArr[strArr.Length - 1];
            return sRs;
        }

        public static void ReplaceIrregularFileName(MyClsFile fileItem, string strToReplace)
        {
            fileItem.FileName = HttpUtility.UrlDecode(fileItem.FileName);
            fileItem.FileName = fileItem.FileName.Replace("'", strToReplace);
            fileItem.FileName = fileItem.FileName.Replace("/", strToReplace);
            fileItem.FileName = fileItem.FileName.Replace("?", strToReplace);
            fileItem.FileName = fileItem.FileName.Replace("+", strToReplace);
        }

        public static void ConvertFileNameForCbox(MyClsFile fileItem, string strToReplace)
        {
            fileItem.FileName = HttpUtility.UrlDecode(fileItem.FileName);
            fileItem.FileName = fileItem.FileName.Replace(" ", strToReplace);
            fileItem.FileName = fileItem.FileName.Replace("[", strToReplace);
            fileItem.FileName = fileItem.FileName.Replace("]", strToReplace);
            //fileItem.FileName = fileItem.FileName.Replace("+", strToReplace);
        }
        public static string RexStringMaker(string strToMake)
        {
            string strRs = "";
            strRs = strToMake.Replace("(", @"\(");
            strRs = strRs.Replace(")", @"\)");
            strRs = strRs.Replace("[", @"\[");
            strRs = strRs.Replace("]", @"\]");
            return strRs;
        }
        public static void MyLog(string ins_log)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(ins_log);
            File.AppendAllText(m_logFile, sb.ToString(), Encoding.UTF8);
        }

        public static void MyLog(string ins_log, Encoding encoding)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(ins_log);
            File.AppendAllText(m_logFile, sb.ToString(), encoding);
        }
        public static void MyLogByteArray(byte[] ins_byteArrayToWrite)
        {
            File.WriteAllBytes(m_logFile, ins_byteArrayToWrite);
        }
    }
}
