using System;
using System.Collections.Generic;
using System.Text;

namespace FreeSMS.MyLib
{
    class MyClsServer
    {
        private string myTen;
        private string myKyHieu;

        public MyClsServer(string strTen, string strKyHieu)
        {
            this.myTen = strTen;
            this.myKyHieu = strKyHieu;
        }

        public string Ten
        {
            get
            {
                return myTen;
            }
        }

        public string KyHieu
        {

            get
            {
                return myKyHieu;
            }
        }
    }
}
