using System;
using System.Collections.Generic;
using System.Text;
using HPConFigXml;
using AFD.HPLib;

namespace FreeSMS.MyLib
{
    public class MyClsUser
    {

        //private string m_vcard;

        private string m_accType;

        public string AccType
        {
            get { return m_accType; }
            set { m_accType = value; }
        }

        private string m_userName;

        public string UserName
        {
            get { return m_userName; }
            set { m_userName = value; }
        }



        private string m_password;

        public string Password
        {
            get { return m_password; }
            set { m_password = value; }
        }

        private string m_note;

        public string Note
        {
            get { return m_note; }
            set { m_note = value; }
        }

        public MyClsUser(string ins_userName, string ins_password, string ins_accType, string ins_note)
        {
            m_userName = ins_userName;
            m_password = ins_password;
            m_accType = ins_accType;
            m_note = ins_note;
        }

        

    }
}
