using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using AFD.HPLib;
using AFD;
using AFD.HpLib;
using System.Threading;
//using AFD.MyLib;
using System.Text.RegularExpressions;
//using upstop.MyLib;
using System.Runtime.InteropServices;

namespace RealMultiThreading.MultiThreadEngine
{
    public enum MyWokerType
    {
        MAIN_PROCCESS = 1,
        EXTRACT_MEDIA_FOLDER = 2,
        LEECH_FILE = 3,
        ADF_GET_HIDE_LINK = 4,
        YAHOO_AUTO_GET_LINK = 5,
        AFD_MESSAGE_TABLE = 6,
        AFD_BUSPOSITION_TABLE = 7
    }
    class MTManager
    {

        //private Form _FormManager;
        private int _NumThreads;
        private int _iNumJobs;
        private int _iNumJobsDone;
        private int _LastSentThread;
        private int _iPiNumbers;
        private List<MTWorker> _lsLWorker;

        public List<MTWorker> LsLWorker
        {
            get { return _lsLWorker; }
            set { _lsLWorker = value; }
        }

        private frmMain m_frmMain;

        public frmMain FrmMain
        {
            get { return m_frmMain; }
            set { m_frmMain = value; }
        }
        //
        private MyClsFile m_fileTemp;

        //public Form FormManager
        //{
        //    get { return _FormManager; }
        //    set { _FormManager = value; }
        //}
        public int NumThreads
        {
            get { return _NumThreads; }
            set { _NumThreads = value; }
        }

        private int m_threadId = -1;

        public int ThreadId
        {
            get { return m_threadId; }
            //set { m_threadId = value; }
        }

        #region Constructores
        public MTManager() { _lsLWorker = new List<MTWorker>();}
        public MTManager(frmMain fFormManager)
            : this()
        {
            //_FormManager = fFormManager;
            m_frmMain = fFormManager;
            //_NumThreads = iNumThreads;
            _LastSentThread = -1;            
        }
        #endregion

        public void InitManager()
        {
            //_LastSentThread = -1;
            //_iNumJobs = 25;
            //_iNumJobsDone = 0;
            //_iPiNumbers = 200;
            ////Generation and configuration of the Workers (one for each thread we need)
            //_lsLWorker = new List<MTWorker>();
            //MTWorker workerTemp;
            //for (int i = 0; i < _NumThreads; i++)
            //{
            //    workerTemp = new MTWorker(_FormManager, i, );
            //    ConfigureWorker(workerTemp);
            //    _lsLWorker.Add(workerTemp);
            //}
        }

        public int AddWorkder(MyWokerType ins_wkType)
        {
            //int iRs = -1;
            //iRs = _lsLWorker.Count;
            m_threadId++;
            MTWorker wkTemp = new MTWorker(m_threadId, ins_wkType);
            _lsLWorker.Add(wkTemp);            
            return m_threadId;
        }
        public void StartWokder(int wkID, object ins_object)
        {            
            foreach (MTWorker wk in _lsLWorker)
            {
                if (wk.WkID == wkID)
                {
                    ConfigureWorker(wk);
                    wk.RunWorkerAsync(ins_object);
                    break;
                }
            }
        }
        public void StopAllWokder()
        {
            foreach (BackgroundWorker bwk in _lsLWorker)
                bwk.CancelAsync();
        }
        
        private void ConfigureWorker(MTWorker MTW)
        {
            //We associate the events of the worker
            
            MTW.ProgressChanged += MTWorker_ProgressChanged;
            MTW.RunWorkerCompleted += MTWorker_RunWorkerCompleted;
            MTW.DoWork += MTWorker_DoWork;
        }
        private void MTWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //// Do not access the form's BackgroundWorker reference directly.
            //// Instead, use the reference provided by the sender parameter.
            ////BackgroundWorker bw = sender as BackgroundWorker;
            //MTWorker LWorker = sender as MTWorker;
            //String PI = (String)e.UserState;
            ////We could even show the progress of the job showing the pi number but not necessary. debug & watch it :)

            ////Here we associate a Progress bar to see the progress of each job being done by each worker
            //string PBName = "ProgressPB_" + LWorker.IdxLWorker.ToString();
            //ProgressBar PB;
            //PB = (ProgressBar)FormManager.Controls[PBName];
            //PB.Value = e.ProgressPercentage;

            ////Here we identify the Worker and the actual Job being done...
            //String lbl_Name = "lbl_" + LWorker.IdxLWorker.ToString();
            //Label LB = (Label)FormManager.Controls[lbl_Name];
            //LB.Text = LWorker.IdxLWorker.ToString() + " - " + LWorker.JobId.ToString();

            ////Yeah, we could have done the construction of the controls  dinamically too, but I was lazy. If someone does this 
            //// but please, with a usercontrol that autopositions itself to a given area - maybe with scroll... - send me the code. 
            ////that would be nice.


            MTWorker LWorker = sender as MTWorker;
            // This function fires on the UI thread so it's safe to edit
            // the UI control directly, no funny business with Control.Invoke.
            // Update the progressBar with the integer supplide to us from the 
            // ReportProgress() function.  Note, e.UserState is a "tag" property
            // that can be used to send other information from the BackgroundThread
            // to the UI thread.
            if (LWorker.PbWorker != null)
                LWorker.PbWorker.Value = e.ProgressPercentage;
        }
        private void MTWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MTWorker LWorker = sender as MTWorker;
            //String PBName;
            //String lbl_Name;
            //MTWorker LWorker = sender as MTWorker;
            
            //ProgressBar PB;
            //String result = "";

            //LWorker.JobId = 0; //Now the Worker is not doing any job...

            ////We Empty the text..
            //PBName = "ProgressPB_" + LWorker.IdxLWorker.ToString();
            //PB = (ProgressBar)FormManager.Controls[PBName];
            //PB.Value = 0;
            //lbl_Name = "lbl_" + LWorker.IdxLWorker.ToString();
            //Label LB = (Label)FormManager.Controls[lbl_Name];
            //LB.Text = LWorker.IdxLWorker.ToString() + " - " + LWorker.JobId.ToString();

            //// First, handle the case where an exception was thrown.
            //if (e.Error != null)
            //{
            //    MessageBox.Show(e.Error.Message);
            //    return;
            //}
            //else if (e.Cancelled)
            //{
            //    // Next, handle the case where the user canceled 
            //    // the operation.
            //    // Note that due to a race condition in 
            //    // the DoWork event handler, the Cancelled
            //    // flag may not have been set, even though
            //    // CancelAsync was called.
            //    return;
            //}
            //else
            //{
            //    // Finally, handle the case where the operation 
            //    // succeeded.
            //    _iNumJobsDone = _iNumJobsDone + 1; //We control here the jobs done.
            //    result = (String)e.Result;
            //}
            //AssignWorkers();

            // The background process is complete. We need to inspect 
            // our response to see if an error occured, a cancel was 
            // requested or if we completed succesfully.
            //set pause to false
            LWorker.IsPause = false;
            if (LWorker.BtnRun != null)
            {
                if (LWorker.BtnRunEnableDisable)
                    LWorker.BtnRun.Enabled = true;
                else
                    LWorker.BtnRun.Text = LWorker.StrBtnName;
            }
            //btnGetLink.Enabled = true;

            // Check to see if an error occured in the 
            // background process.
            if (e.Error != null)
            {
                LWorker.setStatusMsg("Error: " + e.Error.Message, true, true);
                CompleWorkerEx(LWorker);
                return;
            }

            // Check to see if the background process was cancelled.
            if (e.Cancelled)
            {
                //txtWorker.AppendText(DateTime.Now.ToString("hh:mm:ss tt ") + "Cancelled..." + Environment.NewLine);
                LWorker.setStatusMsg(LWorker.WkName + " was cancelled!", true, true);
                //if (gb_isCloseWhenCompleted)
                //    this.Close();
            }
            else
            {
                // Everything completed normally.
                // process the response using e.Result
                //if (m_strError == string.Empty)
                //{
                //    txtLog.AppendText(DateTime.Now.ToString("hh:mm:ss tt ") + "Completed..." + Environment.NewLine);
                //}
                LWorker.setStatusMsg(LWorker.WkName + " was completed!", true, true);
            }

            if (LWorker.PbWorker != null)
                LWorker.SetProgressBarStyle(ProgressBarStyle.Blocks);
            //gb_isDownLoading = false;
            //chbException.Enabled = true;
            CompleWorkerEx(LWorker);
            _lsLWorker.Remove(LWorker);
        }

        private void MTWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            //MyWokerType wkType = (MyWokerType)e.Argument;
            MTWorker LWorker = sender as MTWorker;
            LWorker.SetBtnBeforRun();
            LWorker.SetProgressBarStyle(ProgressBarStyle.Marquee);
            MyClsFile m_fileTemp;
            switch (LWorker.WkType)
            {
                case MyWokerType.MAIN_PROCCESS:
                    break;
                case MyWokerType.LEECH_FILE:
                    break;
                case MyWokerType.EXTRACT_MEDIA_FOLDER:
                    m_fileTemp = (MyClsFile)e.Argument;
                    //frmMain f = (frmMain)_FormManager;
                    //MediafireFolderExtraction(LWorker, m_fileTemp.FileUrl, false);
                    //object[] are = (object[])e.Argument;
                    //_FormManager.AddFileTempToListFile
                    // are[0]
                    break;
                case MyWokerType.ADF_GET_HIDE_LINK:
                    m_fileTemp = (MyClsFile)e.Argument;
                    //frmMain f = (frmMain)_FormManager;
                    AdfGetHideLink(LWorker, m_fileTemp);
                    //object[] are = (object[])e.Argument;
                    //_FormManager.AddFileTempToListFile
                    // are[0]
                    break;
                case MyWokerType.AFD_MESSAGE_TABLE:
                    //m_fileTemp = (MyClsFile)e.Argument;
                    //frmMain f = (frmMain)_FormManager;
                    AFDMessage(LWorker, e);
                    //object[] are = (object[])e.Argument;
                    //_FormManager.AddFileTempToListFile
                    // are[0]
                    break;
                case MyWokerType.AFD_BUSPOSITION_TABLE:
                    //m_fileTemp = (MyClsFile)e.Argument;
                    //frmMain f = (frmMain)_FormManager;
                    AFDBusposition(LWorker, e);
                    //object[] are = (object[])e.Argument;
                    //_FormManager.AddFileTempToListFile
                    // are[0]
                    break;
                default:
                    break;
            }
        }


        private void SetForMainWorker(frmMain ins_f)
        {
            m_frmMain = ins_f;
        }

        public bool setStatusMsg(int wkID, string strMessage, bool isPrinTime, bool isAppendLine)
        {
            bool bRs = false;
            foreach (MTWorker wk in _lsLWorker)
            {
                if (wk.WkID == wkID)
                {
                    bRs = wk.setStatusMsg(strMessage, isPrinTime, isAppendLine);
                    break;
                }
            }
            return bRs;
        }

        public void SetTxtWorker(int wkID, TextBox ins_txtWorker)
        {
            //bool bRs = false;
            foreach (MTWorker wk in _lsLWorker)
            {
                if (wk.WkID == wkID)
                {
                    wk.SetTxtWorker(ins_txtWorker);
                    break;
                }
            }
            //return bRs;
        }
        public void SetLbWorker(int wkID, Label ins_lbWorker)
        {
            //bool bRs = false;
            foreach (MTWorker wk in _lsLWorker)
            {
                if (wk.WkID == wkID)
                {
                    wk.SetLbWorker(ins_lbWorker);
                    break;
                }
            }
            //return bRs;
        }

        public void SetPbWorker(int wkID, ProgressBar ins_pbWorker)
        {
            foreach (MTWorker wk in _lsLWorker)
            {
                if (wk.WkID == wkID)
                {
                    wk.SetPbWorker(ins_pbWorker);                    
                    break;
                }
            }
        }
        public void SetBtWorker(int wkID, Button ins_btnRun, string ins_btnText, bool isEnableDisable)
        {
            foreach (MTWorker wk in _lsLWorker)
            {
                if (wk.WkID == wkID)
                {
                    wk.SetBtWorker(ins_btnRun, ins_btnText, isEnableDisable);
                    break;
                }
            }
        }

        //public void SetFormMain(int ins_workderIndex, frmMain ins_f)
        //{
        //    _lsLWorker[ins_workderIndex].SetForMainWorker(ins_f);
        //}

        //public void SetWorkInfo(int wkIndex, frmMain f, TextBox txtLog)
        //{
        //    //SetFormMain(wkIndex, f);
        //    SetTxtWorker(wkIndex, txtLog);            
        //}

        public void CancelWorker(int wkID)
        {
            //bool bRs = false;
            foreach (MTWorker wk in _lsLWorker)
            {
                if (wk.WkID == wkID)
                {
                    wk.CancelAsync();
                    //bRs = true;
                    break;
                }
            }
            //return bRs;
        }

        public bool IsWorkerBusy(int wkID)
        {
            bool bRs = false;
            foreach (MTWorker wk in _lsLWorker)
            {
                if (wk.WkID == wkID)
                {
                    bRs = wk.IsBusy;
                    break;
                }
            }
            return bRs;
        }

        public bool IsWorkerBusy(string ins_wkName)
        {
            bool bRs = false;
            foreach (MTWorker wk in _lsLWorker)
            {
                if (wk.WkName == ins_wkName)
                {
                    bRs = wk.IsBusy;
                    break;
                }
            }
            return bRs;
        }

        public bool SetPlayerPause(int wkID, bool bIsPause)
        {
            bool bRs = false;
            foreach (MTWorker wk in _lsLWorker)
            {
                if (wk.WkID == wkID)
                {
                    wk.IsPause = bIsPause;
                    bRs = true;
                    break;
                }
            }
            return bRs;
        }

        private void CompleWorkerEx(MTWorker LWorker)
        {
            switch (LWorker.WkType)
            {
                case MyWokerType.MAIN_PROCCESS:
                    break;
                case MyWokerType.LEECH_FILE:
                    break;
                case MyWokerType.EXTRACT_MEDIA_FOLDER:

                    break;
                case MyWokerType.AFD_MESSAGE_TABLE:
                    if (RaiseOnCompleteAFDThread != null)
                        RaiseOnCompleteAFDThread();
                    break;
                
                default:
                    break;
            }
        }

        private void AdfGetHideLink(object sender, MyClsFile fileTemp)
        {
            //MTWorker LWorker = sender as MTWorker;
            ////extract mediafire folder
            //MyClsAdfManager adfManager = new MyClsAdfManager("", "");
            //if (LWorker.setStatusMsg("Getting hide link: ", true, false))
            //    return;
            //if (adfManager.GetHideLink(fileTemp) == System.Net.HttpStatusCode.OK)
            //{
            //    if (LWorker.setStatusMsg("OK!", false, true))
            //        return;
            //    //now print list of files                                                
            //    if (LWorker.setStatusMsg("Add and Print this list to text box: ", true, false))
            //        return;
            //    //int fileUrlWide = mediafireManager.gb_lsFile[j].FileUrl, mediafireManager.gb_lsFile[j].FileUrl.Length + 10
            //    if (m_frmMain != null)
            //        m_frmMain.AddFileTempToListFile(fileTemp);
            //    if (LWorker.setStatusMsg("OK!", false, true))
            //        return;
            //    //isFounded = true;
            //}
            //else
            //{
            //    //MessageBox.Show("Can not get this folder page");
            //    //sb.AppendLine("Can not get this folder: " + line);
            //    if (LWorker.setStatusMsg("Fail: Can not get hide link of this link: " + fileTemp.FileUrl, true, false))
            //        return;
            //}
        }

        private MyClsRecordManager rcManager;
        private void AFDMessage(MTWorker sender, DoWorkEventArgs e)
        {
            MTWorker LWorker = sender;
            rcManager = e.Argument as MyClsRecordManager;
            ////extract mediafire folder
            //MyClsAdfManager adfManager = new MyClsAdfManager("", "");
            //if (LWorker.setStatusMsg("Getting hide link: ", true, false))
            //    return;
            //if (adfManager.GetHideLink(fileTemp) == System.Net.HttpStatusCode.OK)
            //{
            //    if (LWorker.setStatusMsg("OK!", false, true))
            //        return;
            //    //now print list of files                                                
            //    if (LWorker.setStatusMsg("Add and Print this list to text box: ", true, false))
            //        return;
            //    //int fileUrlWide = mediafireManager.gb_lsFile[j].FileUrl, mediafireManager.gb_lsFile[j].FileUrl.Length + 10
            //    if (m_frmMain != null)
            //        m_frmMain.AddFileTempToListFile(fileTemp);
            //    if (LWorker.setStatusMsg("OK!", false, true))
            //        return;
            //    //isFounded = true;
            //}
            //else
            //{
            //    //MessageBox.Show("Can not get this folder page");
            //    //sb.AppendLine("Can not get this folder: " + line);
            //    if (LWorker.setStatusMsg("Fail: Can not get hide link of this link: " + fileTemp.FileUrl, true, false))
            //        return;
            //}

            //init next record

            //this instance of record manager class is inited and it has a time conveted type
            //now it must init first record
            
            if (rcManager.IsDeleteExistRecords)
            {
                //if delete => delate all data of current date in message
                rcManager.DeleteByDate(DateTime.Now);
                //now get the first record of selected date
                rcManager.getFristRecordMsgOfDate();
            }
            else
            {
                //get lasted datetime of all current records date
                //DateTime dtLastRCDestination = rcManager.getLastedRecordMsgOfDate(DateTime.Now);
                if (rcManager.LastDTOfCurrentDate != DateTime.MinValue)
                {
                    rcManager.getNextRecordMsgOfDateByDT(rcManager.LastDTOfCurrentDate);
                }
                else//current date not has any record
                {
                    //now get the first record of current date
                    rcManager.getFristRecordMsgOfDate();
                }
            }
            
                        
            ////first step, it proccess the time converted operator
            //while (!LWorker.isWorkerCancelling() && rcManager.RcNext.IsHasRecord == true)
            //{
            //    //if (rcManager.ConvertBaseTypeSelected())
            //    {
            //        rcManager.GetActualRecordTime();
            //        //check if this worker is in pause status
            //        if (LWorker.IsPause)
            //        {
            //            System.Threading.Thread.Sleep(1000);
            //            continue;
            //        }
            //        //check if time of current record > current time after converted => waiting                    
            //        if (rcManager.dtActualRecordTime > rcManager.dtConvertedSysTime)
            //        {
            //            System.Threading.Thread.Sleep(1000);
            //            continue;
            //        }
            //        //first step: it must send the all times object to main to update GUI => use delegate
            //        if (RaiseUpdateTimeGUI != null)
            //        {
            //            RaiseUpdateTimeGUI(rcManager);
            //        }
            //        //insert to destination table
            //        if (rcManager.InsertNextRecordToDestTable() == 0)
            //        {
            //            if (LWorker.setStatusMsg("Fail to insert to message table with record id: "
            //                + rcManager.RcNext.message_id.ToString(), true, true))
            //                return;
            //        }

            //        if(rcManager.SleepMilisecond > 0)
            //            Thread.Sleep(rcManager.SleepMilisecond);
            //        //now get next record:
            //        rcManager.getNextRecordMsgOfDateByMsgID(rcManager.RcNext.message_id);
            //        //break;
            //    }
            //}

            while (!LWorker.isWorkerCancelling() && rcManager.RcNext.IsHasRecord == true)
            {   
                rcManager.dtCurrentSysTime = DateTime.Now;
                //check if this worker is in pause status
                if (LWorker.IsPause)
                {
                    System.Threading.Thread.Sleep(1000);
                    continue;
                }
                rcManager.GetActualRecordTime();
                //check if time of current record > current time after converted => waiting                    
                if (rcManager.dtActualRecordTime <= rcManager.dtConvertedSysTime)
                {
                    //first step: it must send the all times object to main to update GUI => use delegate
                    if (RaiseUpdateTimeGUI != null)
                    {
                        RaiseUpdateTimeGUI(rcManager, true);
                    }
                    //insert to destination table
                    if (rcManager.InsertNextRecordToDestTable() == 0)
                    {
                        if (LWorker.setStatusMsg("Fail to insert to Message table with record id: "
                            + rcManager.RcNext.message_id.ToString(), true, true))
                            return;
                    }

                    //now get next record:
                    rcManager.getNextRecordMsgOfDateByMsgID(rcManager.RcNext.message_id);
                    //break;
                }
                if (rcManager.SleepMilisecond > 0)
                    Thread.Sleep(rcManager.SleepMilisecond);
            }


            if (LWorker.setStatusMsg("Completed!", true, false))
                return;            
            //if (RaiseOnCompleteAFDThread != null)
            //    RaiseOnCompleteAFDThread();
        }

        //private bool IsBusy;

        public bool MessageProccess()
        {
            bool bRs = true;
            //if (IsBusy)
            //    return false;
            //IsBusy = true;
            //rcManager.GetActualRecordTime();
            ////check if this worker is in pause status
            ////if (LWorker.IsPause)
            ////{
            ////    System.Threading.Thread.Sleep(1000);
            ////    continue;
            ////}
            ////check if time of current record > current time after converted => waiting                    
            //if (rcManager.dtActualRecordTime <= rcManager.dtCurrentSysTime)
            //{
            //    //first step: it must send the all times object to main to update GUI => use delegate
            //    if (RaiseUpdateTimeGUI != null)
            //    {
            //        RaiseUpdateTimeGUI(rcManager);
            //    }
            //    //insert to destination table
            //    if (rcManager.InsertNextRecordToDestTable() == 0)
            //    {
            //        //if (LWorker.setStatusMsg("Fail to insert to message table with record id: "
            //        //    + rcManager.RcNext.message_id.ToString(), true, true))
            //        //    return;
            //        bRs = false;
            //    }

            //    //if (rcManager.SleepMilisecond > 0)
            //    //    Thread.Sleep(rcManager.SleepMilisecond);
            //    //now get next record:
            //    rcManager.getNextRecordMsgOfDateByMsgID(rcManager.RcNext.message_id);
            //    //break;
            //}
            //IsBusy = false;

            return bRs;
        }

        public void callRaiseCompleate()
        {
            RaiseOnCompleteAFDThread();
        }

        public void callRaiseUpdateUI()
        {
            RaiseUpdateTimeGUI(rcManager, false);
        }

        public void timerTickEvent()
        {
            rcManager.dtConvertedSysTime = rcManager.dtConvertedSysTime.AddSeconds(1);
        }
        // delegate update time UI
        public delegate void UpdateTimeGUI(MyClsRecordManager rcM, bool bIsInserted);
        public event UpdateTimeGUI RaiseUpdateTimeGUI = null;

        // delegate on finished
        public delegate void OnCompleteAFDThread();
        public event OnCompleteAFDThread RaiseOnCompleteAFDThread = null;

        ///////// afd for fill busposition
        private MyClsRecordManagerBus rcManagerBus;
        private void AFDBusposition(MTWorker sender, DoWorkEventArgs e)
        {
            MTWorker LWorker = sender;
            rcManagerBus = e.Argument as MyClsRecordManagerBus;
            ////extract mediafire folder
            //MyClsAdfManager adfManager = new MyClsAdfManager("", "");
            //if (LWorker.setStatusMsg("Getting hide link: ", true, false))
            //    return;
            //if (adfManager.GetHideLink(fileTemp) == System.Net.HttpStatusCode.OK)
            //{
            //    if (LWorker.setStatusMsg("OK!", false, true))
            //        return;
            //    //now print list of files                                                
            //    if (LWorker.setStatusMsg("Add and Print this list to text box: ", true, false))
            //        return;
            //    //int fileUrlWide = mediafireManager.gb_lsFile[j].FileUrl, mediafireManager.gb_lsFile[j].FileUrl.Length + 10
            //    if (m_frmMain != null)
            //        m_frmMain.AddFileTempToListFile(fileTemp);
            //    if (LWorker.setStatusMsg("OK!", false, true))
            //        return;
            //    //isFounded = true;
            //}
            //else
            //{
            //    //MessageBox.Show("Can not get this folder page");
            //    //sb.AppendLine("Can not get this folder: " + line);
            //    if (LWorker.setStatusMsg("Fail: Can not get hide link of this link: " + fileTemp.FileUrl, true, false))
            //        return;
            //}

            //init next record

            //this instance of record manager class is inited and it has a time conveted type
            //now it must init first record

            if (rcManagerBus.IsDeleteExistRecords)
            {
                //if delete => delate all data of current date in message
                rcManagerBus.DeleteByDate(DateTime.Now);
                //now get the first record of selected date
                rcManagerBus.getFristRecordMsgOfDate();
            }
            else
            {
                //get lasted datetime of all current records date
                //DateTime dtLastRCDestination = rcManager.getLastedRecordMsgOfDate(DateTime.Now);
                if (rcManagerBus.LastDTOfCurrentDate != DateTime.MinValue)
                {
                    rcManagerBus.getNextRecordMsgOfDateByDT(rcManagerBus.LastDTOfCurrentDate);
                }
                else//current date not has any record
                {
                    //now get the first record of current date
                    rcManagerBus.getFristRecordMsgOfDate();
                }
            }


            ////first step, it proccess the time converted operator
            //while (!LWorker.isWorkerCancelling() && rcManager.RcNext.IsHasRecord == true)
            //{
            //    //if (rcManager.ConvertBaseTypeSelected())
            //    {
            //        rcManager.GetActualRecordTime();
            //        //check if this worker is in pause status
            //        if (LWorker.IsPause)
            //        {
            //            System.Threading.Thread.Sleep(1000);
            //            continue;
            //        }
            //        //check if time of current record > current time after converted => waiting                    
            //        if (rcManager.dtActualRecordTime > rcManager.dtConvertedSysTime)
            //        {
            //            System.Threading.Thread.Sleep(1000);
            //            continue;
            //        }
            //        //first step: it must send the all times object to main to update GUI => use delegate
            //        if (RaiseUpdateTimeGUI != null)
            //        {
            //            RaiseUpdateTimeGUI(rcManager);
            //        }
            //        //insert to destination table
            //        if (rcManager.InsertNextRecordToDestTable() == 0)
            //        {
            //            if (LWorker.setStatusMsg("Fail to insert to message table with record id: "
            //                + rcManager.RcNext.message_id.ToString(), true, true))
            //                return;
            //        }

            //        if(rcManager.SleepMilisecond > 0)
            //            Thread.Sleep(rcManager.SleepMilisecond);
            //        //now get next record:
            //        rcManager.getNextRecordMsgOfDateByMsgID(rcManager.RcNext.message_id);
            //        //break;
            //    }
            //}

            while (!LWorker.isWorkerCancelling() && rcManagerBus.RcNext.IsHasRecord == true)
            {
                rcManagerBus.dtCurrentSysTime = DateTime.Now;//for display only
                //check if this worker is in pause status
                if (LWorker.IsPause)
                {
                    System.Threading.Thread.Sleep(1000);
                    continue;
                }
                rcManagerBus.GetActualRecordTime();
                //check if time of current record > current time after converted => waiting                    
                if (rcManagerBus.dtActualRecordTime <= rcManagerBus.dtConvertedSysTime)
                {
                    //first step: it must send the all times object to main to update GUI => use delegate
                    if (RaiseUpdateTimeGUIBus != null)
                    {
                        RaiseUpdateTimeGUIBus(rcManagerBus, true);
                    }
                    //insert to destination BusPosition table
                    if (rcManagerBus.InsertNextRecordToDestTable() == 0)
                    {
                        if (LWorker.setStatusMsg("Fail to insert to busposition table with record id: "
                            + rcManagerBus.RcNext.ID.ToString(), true, true))
                            return;
                    }
                    //insert to destination student table
                    //the result may be 0 value because this time has no student event
                    if (rcManagerBus.InsertToStudentTable() == 0)
                    {
                        //if (LWorker.setStatusMsg("Fail to insert to busposition table with record id: "
                        //    + rcManagerBus.RcNext.ID.ToString(), true, true))
                        //    return;                        
                    }
                    //now get next record:
                    rcManagerBus.getNextRecordMsgOfDateByMsgID(rcManagerBus.RcNext.ID);
                    //break;
                }
                else
                    Thread.Sleep(1000);
                if (rcManagerBus.SleepMilisecond > 0)
                    Thread.Sleep(rcManagerBus.SleepMilisecond);
            }


            if (LWorker.setStatusMsg("Thread busposition completed running!", true, false))
                return;
            //if (RaiseOnCompleteAFDThread != null)
            //    RaiseOnCompleteAFDThread();
        }

        //private bool IsBusy;

        public bool MessageProccessBus()
        {
            bool bRs = true;
            //if (IsBusy)
            //    return false;
            //IsBusy = true;
            //rcManager.GetActualRecordTime();
            ////check if this worker is in pause status
            ////if (LWorker.IsPause)
            ////{
            ////    System.Threading.Thread.Sleep(1000);
            ////    continue;
            ////}
            ////check if time of current record > current time after converted => waiting                    
            //if (rcManager.dtActualRecordTime <= rcManager.dtCurrentSysTime)
            //{
            //    //first step: it must send the all times object to main to update GUI => use delegate
            //    if (RaiseUpdateTimeGUI != null)
            //    {
            //        RaiseUpdateTimeGUI(rcManager);
            //    }
            //    //insert to destination table
            //    if (rcManager.InsertNextRecordToDestTable() == 0)
            //    {
            //        //if (LWorker.setStatusMsg("Fail to insert to message table with record id: "
            //        //    + rcManager.RcNext.message_id.ToString(), true, true))
            //        //    return;
            //        bRs = false;
            //    }

            //    //if (rcManager.SleepMilisecond > 0)
            //    //    Thread.Sleep(rcManager.SleepMilisecond);
            //    //now get next record:
            //    rcManager.getNextRecordMsgOfDateByMsgID(rcManager.RcNext.message_id);
            //    //break;
            //}
            //IsBusy = false;

            return bRs;
        }

        public void callRaiseCompleateBus()
        {
            RaiseOnCompleteAFDThreadBus();
        }

        public void callRaiseUpdateUIBus()
        {
            RaiseUpdateTimeGUIBus(rcManagerBus, false);
        }

        public void timerTickEventBus()
        {
            rcManagerBus.dtConvertedSysTime = rcManagerBus.dtConvertedSysTime.AddSeconds(1);
        }
        // delegate update time UI
        public delegate void UpdateTimeGUIBus(MyClsRecordManagerBus rcM, bool bIsInserted);
        public event UpdateTimeGUIBus RaiseUpdateTimeGUIBus = null;

        // delegate on finished
        public delegate void OnCompleteAFDThreadBus();
        public event OnCompleteAFDThreadBus RaiseOnCompleteAFDThreadBus = null;
        //////////////////////yahoo
 
    }
}