using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AFD.MyLib;
using System.Globalization;

namespace AFD.HpLib
{
    public class MyClsRecordManagerBus
    {
        public enum eTimeType
        {
            eSpeed = 0,
            ePeriod = 1
        }

        private string strConnSource;

        private string strConnDes;

        private eTimeType enumTT;

        internal eTimeType EnumTT
        {
            get { return enumTT; }
            set { enumTT = value; }
        }

        private DateTime selectedDate;

        public MyClsRecordManagerBus(string connSource, string connDes, bool ins_IsDeleteExistRecords)
        {
            //IsHasRecord = false;
            currentIdProccess = -1;
            strConnSource = connSource;
            strConnDes = connDes;
            
            rcFirstDate = new MyClsRecordBus();
            rcNext = new MyClsRecordBus();
            IsDeleteExistRecords = ins_IsDeleteExistRecords;
            init();
        }

        private MyClsRecordBus rcFirstDate;

        private MyClsRecordBus rcNext;

        internal MyClsRecordBus RcNext
        {
            get { return rcNext; }
            set { rcNext = value; }
        }

        private DateTime dtConverted;

        public DateTime DtConverted
        {
            get { return dtConverted; }
            set { dtConverted = value; }
        }

        private double speed;

        public double Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        private double periodTime;

        public double PeriodTime
        {
            get { return periodTime; }
            set { periodTime = value; }
        }

        private bool isIdentity;

        public bool IsIdentity
        {
            get { return isIdentity; }
            set { isIdentity = value; }
        }

        private int nextIdInsert;

        public int NextIdInsert
        {
            get { return nextIdInsert; }
            set { nextIdInsert = value; }
        }

        private int currentIdProccess;

        public int CurrentIdProccess
        {
            get { return currentIdProccess; }
            set { currentIdProccess = value; }
        }

        private int m_sleepMilisecond;

        public int SleepMilisecond
        {
            get { return m_sleepMilisecond; }
            set { m_sleepMilisecond = value; }
        }

        private DateTime m_lastDTOfCurrentDate;

        public DateTime LastDTOfCurrentDate
        {
            get { return m_lastDTOfCurrentDate; }
            set { m_lastDTOfCurrentDate = value; }
        }

        private DataTable m_tbListDate;

        public DataTable TbListDate
        {
            get { return m_tbListDate; }
            set { m_tbListDate = value; }
        }

        public DateTime dtCurrentSysTime, dtConvertedSysTime, dtActualRecordTime, dtBeginDate;

        private Int64 m_lastRecordIDOfSelectedDate;

        public Int64 LastRecordIDOfSelectedDate
        {
            get { return m_lastRecordIDOfSelectedDate; }
            set { m_lastRecordIDOfSelectedDate = value; }
        }

        private Int64 m_firstRecordIDOfSelectedDate;

        public Int64 FirstRecordIDOfSelectedDate
        {
            get { return m_firstRecordIDOfSelectedDate; }
            set { m_firstRecordIDOfSelectedDate = value; }
        }

        //for student table
        private bool isIdentityStudent;

        public bool IsIdentityStudent
        {
            get { return isIdentityStudent; }
            set { isIdentityStudent = value; }
        }

        public bool init()
        {
            //check if messate_id column is identity
            //if no => get next id to insert
            BusPosition m = new BusPosition(strConnSource);
            isIdentity = m.IsIdentity();
            if (!IsIdentity)
                nextIdInsert = m.getNextId();
            //get last record of current date in BusPosition table to check whethere exists data in current date to continue filling or delete
            m_lastDTOfCurrentDate = m.getLastTimeRecordOfCurrentDate(DateTime.Now);
            //get list of date and total records for each date
            m_tbListDate = m.getListOfDateAndTotalRecord();
            
            
            return true;
        }

        //public int deleteBusPosition(DateTime ins_dateToDelete)
        //{
        //    BusPosition m = new BusPosition(strConnSource);            
        //}

        public void getFristRecordMsgOfDate()
        {
            //get top 2 record of BusPosition order by id, tstam
            BusPosition m = new BusPosition(strConnSource);
            DataTable tbRecord = m.getFristMsgOfDate("tbRecord", selectedDate.ToString("MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US")));
            if (tbRecord.Rows.Count > 0)
            {
                rcNext.IsHasRecord = true;
                DataRow currentRow = tbRecord.Rows[0];
                int ns = 0;
                double ds = 0;
                DateTime dtTemp;
                System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.CurrentCulture;

                ns = 0;
                int.TryParse(currentRow["DistrictID"].ToString(), out ns);
                rcNext.DistrictID = ns;


                if (string.IsNullOrEmpty(currentRow["UnitID"].ToString()))
                    rcNext.UnitID = "";
                else
                    rcNext.UnitID = currentRow["UnitID"].ToString();


                ds = 0;
                double.TryParse(currentRow["Latitude"].ToString(), out ds);
                rcNext.Latitude = (float) ds;

                ds = 0;
                double.TryParse(currentRow["Longitude"].ToString(), out ds);
                rcNext.Longitude = (float)ds;


                dtTemp = DateTime.MinValue;
                DateTime.TryParse(currentRow["WriteTimeStamp"].ToString(), ci, DateTimeStyles.None, out dtTemp);
                rcNext.WriteTimeStamp = dtTemp;


                dtTemp = DateTime.MinValue;
                DateTime.TryParse(currentRow["PositionTimeStamp"].ToString(), ci, DateTimeStyles.None, out dtTemp);
                rcNext.PositionTimeStamp = dtTemp;


                ns = 0;
                int.TryParse(currentRow["EventID"].ToString(), out ns);
                rcNext.EventID = ns;


                ds = 0;
                double.TryParse(currentRow["Speed"].ToString(), out ds);
                rcNext.Speed = (float)ds;


                ds = 0;
                double.TryParse(currentRow["RSSI"].ToString(), out ds);
                rcNext.RSSI = (float)ds;


                ds = 0;
                double.TryParse(currentRow["RouteMiles"].ToString(), out ds);
                rcNext.RouteMiles = (float)ds;
                
                ds = 0;
                double.TryParse(currentRow["ID"].ToString(), out ds);
                rcNext.ID = (Int64)ds;

            }
            else
                rcNext.IsHasRecord = false;

        }

        public void getNextRecordMsgOfDateByMsgID(Int64 BusPositionID)
        {
            //get top 2 record of BusPosition order by id, tstam
            BusPosition m = new BusPosition(strConnSource);
            DataTable tbRecord = m.getNextRecordMsgOfDate("tbRecord", selectedDate.ToString("MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US")), BusPositionID);
            if (tbRecord.Rows.Count > 0)
            {
                rcNext.IsHasRecord = true;
                DataRow currentRow = tbRecord.Rows[0];
                int ns = 0;
                double ds = 0;
                DateTime dtTemp;
                System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.CurrentCulture;

                ns = 0;
                int.TryParse(currentRow["DistrictID"].ToString(), out ns);
                rcNext.DistrictID = ns;


                if (string.IsNullOrEmpty(currentRow["UnitID"].ToString()))
                    rcNext.UnitID = "";
                else
                    rcNext.UnitID = currentRow["UnitID"].ToString();


                ds = 0;
                double.TryParse(currentRow["Latitude"].ToString(), out ds);
                rcNext.Latitude = (float)ds;


                ds = 0;
                double.TryParse(currentRow["Longitude"].ToString(), out ds);
                rcNext.Longitude = (float)ds;


                dtTemp = DateTime.MinValue;
                DateTime.TryParse(currentRow["WriteTimeStamp"].ToString(), ci, DateTimeStyles.None, out dtTemp);
                rcNext.WriteTimeStamp = dtTemp;


                dtTemp = DateTime.MinValue;
                DateTime.TryParse(currentRow["PositionTimeStamp"].ToString(), ci, DateTimeStyles.None, out dtTemp);
                rcNext.PositionTimeStamp = dtTemp;


                ns = 0;
                int.TryParse(currentRow["EventID"].ToString(), out ns);
                rcNext.EventID = ns;


                ds = 0;
                double.TryParse(currentRow["Speed"].ToString(), out ds);
                rcNext.Speed = (float)ds;


                ds = 0;
                double.TryParse(currentRow["RSSI"].ToString(), out ds);
                rcNext.RSSI = (float)ds;


                ds = 0;
                double.TryParse(currentRow["RouteMiles"].ToString(), out ds);
                rcNext.RouteMiles = (float)ds;

                ds = 0;
                double.TryParse(currentRow["ID"].ToString(), out ds);
                rcNext.ID = (Int64)ds;
            }
            else
                rcNext.IsHasRecord = false;
        }

        public DateTime getLastedRecordMsgOfDate(DateTime dtSelectedDate)
        {
            
            //get top 2 record of BusPosition order by id, tstam
            BusPosition m = new BusPosition(strConnSource);
            return m.getLastTimeRecordOfCurrentDate(dtSelectedDate);            
        }

        public void getNextRecordMsgOfDateByDT(DateTime dtRecord)
        {
            //get top 2 record of BusPosition order by id, tstam
            BusPosition m = new BusPosition(strConnSource);
            DataTable tbRecord = m.getNextRecordMsgOfDateByDateTime("tbRecord", selectedDate.ToString("MM/dd/yyyy", CultureInfo.GetCultureInfo("en-US"))
                , selectedDate.ToString("MM/dd/yyyy ", CultureInfo.GetCultureInfo("en-US")) + dtRecord.ToString("hh:mm:ss tt"));
            if (tbRecord.Rows.Count > 0)
            {
                rcNext.IsHasRecord = true;
                DataRow currentRow = tbRecord.Rows[0];
                int ns = 0;
                double ds = 0;
                DateTime dtTemp;
                System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.CurrentCulture;

                ns = 0;
                int.TryParse(currentRow["DistrictID"].ToString(), out ns);
                rcNext.DistrictID = ns;


                if (string.IsNullOrEmpty(currentRow["UnitID"].ToString()))
                    rcNext.UnitID = "";
                else
                    rcNext.UnitID = currentRow["UnitID"].ToString();


                ds = 0;
                double.TryParse(currentRow["Latitude"].ToString(), out ds);
                rcNext.Latitude = (float)ds;


                ds = 0;
                double.TryParse(currentRow["Longitude"].ToString(), out ds);
                rcNext.Longitude = (float)ds;


                dtTemp = DateTime.MinValue;
                DateTime.TryParse(currentRow["WriteTimeStamp"].ToString(), ci, DateTimeStyles.None, out dtTemp);
                rcNext.WriteTimeStamp = dtTemp;


                dtTemp = DateTime.MinValue;
                DateTime.TryParse(currentRow["PositionTimeStamp"].ToString(), ci, DateTimeStyles.None, out dtTemp);
                rcNext.PositionTimeStamp = dtTemp;


                ns = 0;
                int.TryParse(currentRow["EventID"].ToString(), out ns);
                rcNext.EventID = ns;


                ds = 0;
                double.TryParse(currentRow["Speed"].ToString(), out ds);
                rcNext.Speed = (float)ds;


                ds = 0;
                double.TryParse(currentRow["RSSI"].ToString(), out ds);
                rcNext.RSSI = (float)ds;


                ds = 0;
                double.TryParse(currentRow["RouteMiles"].ToString(), out ds);
                rcNext.RouteMiles = (float)ds;

                ds = 0;
                double.TryParse(currentRow["ID"].ToString(), out ds);
                rcNext.ID = (Int64)ds;
            }
            else
                rcNext.IsHasRecord = false;
        }

        public int DeleteByDate(DateTime dtToDelete)
        {
            BusPosition m = new BusPosition(strConnSource);
            return m.DeleteByDate(dtToDelete);
        }

        /// <summary>
        /// this method will be run at the first time the thread start
        /// </summary>
        /// <param name="ins_speed"></param>
        /// <param name="ins_dateSelected"></param>
        /// <param name="sleepTime"></param>
        public void SetSpeed(double ins_speed, DateTime ins_dateSelected, int sleepTime, DateTime ins_currentSysTemDate)
        {
            enumTT = eTimeType.eSpeed;
            speed = ins_speed;
            selectedDate = ins_dateSelected;
            dtCurrentSysTime = ins_currentSysTemDate;
            dtConvertedSysTime = ins_currentSysTemDate;
            m_sleepMilisecond = sleepTime;
            //get lastest record id of selected date
            BusPosition m = new BusPosition(strConnSource);
            m_lastRecordIDOfSelectedDate = m.getMaxIdFromDate(selectedDate);
            m_firstRecordIDOfSelectedDate = m.getMinIdFromDate(selectedDate);
        }
        
        /// <summary>
        /// this method will be run at the first time the thread start
        /// </summary>
        /// <param name="ins_periodTime"></param>
        /// <param name="ins_dateSelected"></param>
        /// <param name="sleepTime"></param>
        public void SetPeriod(double ins_periodTime, DateTime ins_dateSelected, int sleepTime, DateTime ins_currentSysTemDate)
        {
            enumTT = eTimeType.ePeriod;
            periodTime = ins_periodTime;
            selectedDate = ins_dateSelected;
            dtCurrentSysTime = ins_currentSysTemDate;
            m_sleepMilisecond = sleepTime;
            //get lastest record id of selected date
            BusPosition m = new BusPosition(strConnSource);
            m_lastRecordIDOfSelectedDate = m.getMaxIdFromDate(selectedDate);
        }

        public bool ConvertBaseTypeSelected()
        {
            bool bRs = false;
            if (!rcNext.IsHasRecord)
                return bRs;
            //convert BusPosition_dt_received
            //DateTime dtCurrentSysTime, dtConvertedSysTime, dtActualRecordTime, dtBeginDate;
            //dtCurrentSysTime = DateTime.Now;

            //dtBeginDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            //TimeSpan tsDiff = dtCurrentSysTime - dtBeginDate;

            DateTime dtRecord = rcNext.WriteTimeStamp;
            dtActualRecordTime = new DateTime(dtCurrentSysTime.Year, dtCurrentSysTime.Month, dtCurrentSysTime.Day,
                dtRecord.Hour, dtRecord.Minute, dtRecord.Second, dtRecord.Millisecond);
            
            //switch (enumTT)
            //{
            //    case eTimeType.eSpeed:
            //        double miliSecondDiff = tsDiff.TotalMilliseconds * speed;
            //        int iTotalMiliSect = Convert.ToInt32(miliSecondDiff);
            //        tsDiff = new TimeSpan(0, 0, 0, 0, iTotalMiliSect);
            //        dtConvertedSysTime = dtBeginDate;
            //        dtConvertedSysTime = dtConvertedSysTime.Add(tsDiff);

            //        dtConverted = dtConvertedSysTime;
            //        bRs = true;
            //        break;
            //    case eTimeType.ePeriod:
            //        //tsDiff = tsDiff * speed;
            //        break;
            //}
            return bRs;
        }

        public void GetActualRecordTime()
        {
            DateTime dtRecord = rcNext.WriteTimeStamp;
            dtActualRecordTime = new DateTime(dtCurrentSysTime.Year, dtCurrentSysTime.Month, dtCurrentSysTime.Day,
                dtRecord.Hour, dtRecord.Minute, dtRecord.Second, dtRecord.Millisecond);
        }

        public bool IsDeleteExistRecords;

        public int InsertNextRecordToDestTable()
        {
            int iRs = 0;
            BusPosition m = new BusPosition(strConnSource);
            if (isIdentity)
            {
                //insert with identity attribute
                //iRs = m.InsertWithYesIdentity(rcNext.ID, dtActualRecordTime);
                Int64 mesId = 0;
                iRs = m.InsertWithYesIdentitySp(rcNext.ID, dtActualRecordTime, out mesId);
                rcNext.IDMessage = mesId;
            }
            else
            {
                //insert with no identity attribute
                iRs = m.InsertWithNoIdentity(rcNext.ID, NextIdInsert, dtActualRecordTime);                
            }
            nextIdInsert++;
            return iRs;
        }
        public int InsertToStudentTable()
        {
            int iRs = 0;
            BusPosition m = new BusPosition(strConnSource);
            //get [PositionTimeStamp] of student of the past            
            iRs = m.InsertStudent(selectedDate, rcNext.WriteTimeStamp);
            //if (isIdentityStudent)
            //{
            //    //insert with identity attribute
            //    iRs = m.InsertWithYesIdentity(rcNext.ID, dtActualRecordTime);
            //}
            //else
            //{
            //    //insert with no identity attribute
            //    iRs = m.InsertWithNoIdentity(rcNext.ID, NextIdInsert, dtActualRecordTime);
            //}
            //nextIdInsert++;
            return iRs;
        }
    }
}
