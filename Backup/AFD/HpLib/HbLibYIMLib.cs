using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace AFD.HPLib
{
    class HbLibYIMLib
    {
        [DllImport("HpYIMManager.dll", CharSet = CharSet.Unicode)]
        //public static extern bool GetNewIM(ref IntPtr buf, ref IntPtr strUser, ref IntPtr hIMsg, ref IntPtr hContain);
        public static extern bool GetNewIM(ref string buf, ref string strUser, ref IntPtr hIMsg, ref IntPtr hContain);
        //public static extern bool GetNewIM(StringBuilder buf, StringBuilder strUser, ref IntPtr hIMsg, ref IntPtr hContain);
        //public static extern bool GetNewIM(ref StringBuilder buf, ref StringBuilder strUser, ref IntPtr hIMsg, ref IntPtr hContain);

        [DllImport("HpYIMManager.dll", CharSet = CharSet.Unicode)]
        //public static extern void GetContainText(ref IntPtr buf2, int size);
        public static extern void GetContainText(ref string buf2, int size);

        [DllImport("HpYIMManager.dll", CharSet = CharSet.Unicode)]
        public static extern void SetContainText(string buf2, int size);

        [DllImport("HpYIMManager.dll", CharSet = CharSet.Unicode)]
        public static extern bool CloseIM();

        [DllImport("HpYIMManager.dll", CharSet = CharSet.Unicode)]
        public static extern bool GetNewAddNickWnd(ref IntPtr hAddNick, ref IntPtr hBtnNext, ref IntPtr hBtnFinish);

        [DllImport("HpYIMManager.dll", CharSet = CharSet.Unicode)]
        public static extern void ClickBtn(IntPtr hBtnToClick);



    }
}
