using System;
using System.Collections.Generic;
using System.Text;
using HPConFigXml;

namespace FreeSMS.MyLib
{
    public class MyClsUserLogin
    {
        //private string m_vcard;


        private string m_name;

        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }

        

        
        private string m_phoneNumber;

        public string PhoneNumber
        {
            get { return m_phoneNumber; }
            set { m_phoneNumber = value; }
        }
        private string m_address;

        public string Address
        {
            get { return m_address; }
            set { m_address = value; }
        }

        private string m_message;

        public string Message
        {
            get { return m_message; }
            set { m_message = value; }
        }

        public MyClsUserLogin(string ins_name, string ins_phoneNumber, string ins_address, string ins_message)
        {
            m_name = ins_name;
            m_phoneNumber = ins_phoneNumber;
            m_address = ins_address;
            m_message = ins_message;
        }
        static public void addVcard(List<MyLib.MyClsUserLogin> lsPhoneBook, HPConFigXml.Xmlconfig xcfg, string ins_name, string ins_phoneNumber, string ins_address, string ins_message)
        {
            ConfigSetting newVcard = xcfg.Settings["Vcard##"];
            newVcard.Value = "2.1";
            //ConfigSetting newVcard = xcfg.Settings["PhoneBook/Vcard##"];
            newVcard["Name"].Value = ins_name;
            newVcard["PhoneNumber"].Value = ins_phoneNumber;
            newVcard["Address"].Value = ins_address;
            newVcard["Message"].Value = ins_message;



            lsPhoneBook.Add(new MyClsUserLogin(ins_name, ins_phoneNumber, ins_address, ins_message));
        }
        static public void updateVcard(List<MyLib.MyClsUserLogin> lsPhoneBook, HPConFigXml.Xmlconfig xcfg, string ins_name, string ins_phoneNumber, string ins_address, string ins_message
            , string ins_newName, string ins_newPhoneNumber, string ins_newAddress, string ins_newMessage)
        {
            //first remove this item
            foreach (ConfigSetting cs in xcfg.Settings.GetNamedChildren("Vcard"))
            {
                //Console.Write(cs["method"].Value + " ");
                if (cs["Name"].Value == ins_name && cs["PhoneNumber"].Value == ins_phoneNumber)
                {
                    //cs.Remove();
                    cs["Name"].Value = ins_newName;
                    cs["PhoneNumber"].Value = ins_newPhoneNumber;
                    cs["Address"].Value = ins_newAddress;
                    cs["Message"].Value = ins_newMessage;
                    break;
                }
            }
            foreach (MyClsUserLogin pb in lsPhoneBook)
            {
                if (pb.Name == ins_name && pb.PhoneNumber == ins_phoneNumber)
                {
                    //lsPhoneBook.Remove(pb);
                    pb.Name = ins_newName;
                    pb.PhoneNumber = ins_newPhoneNumber;
                    pb.Address = ins_newAddress;
                    pb.Message = ins_newMessage;
                    break;
                }
            }
            //add new item

            //ConfigSetting newVcard = xcfg.Settings["Vcard##"];
            //newVcard.Value = "2.1";
            ////ConfigSetting newVcard = xcfg.Settings["PhoneBook/Vcard##"];
            //newVcard["Name"].Value = ins_newName;
            //newVcard["PhoneNumber"].Value = ins_newPhoneNumber;
            //newVcard["Address"].Value = ins_newAddress;
            //newVcard["Message"].Value = ins_newMessage;

            //lsPhoneBook.Add(new MyClsUserLogin(ins_newName, ins_newPhoneNumber, ins_newAddress, ins_newMessage));
        }

        static public void deleteVcard(List<MyLib.MyClsUserLogin> lsPhoneBook, HPConFigXml.Xmlconfig xcfg, string ins_name, string ins_phoneNumber, string ins_address, string ins_message)
        {
            //first remove this item
            foreach (ConfigSetting cs in xcfg.Settings.GetNamedChildren("Vcard"))
            {
                //Console.Write(cs["method"].Value + " ");
                if (cs["Name"].Value == ins_name && cs["PhoneNumber"].Value == ins_phoneNumber)
                {
                    cs.Remove();
                    break;
                }
            }
            foreach (MyClsUserLogin pb in lsPhoneBook)
            {
                //Console.Write(cs["method"].Value + " ");
                if (pb.Name == ins_name && pb.PhoneNumber == ins_phoneNumber)
                {
                    lsPhoneBook.Remove(pb);
                    break;
                }
            }
        }
    }
}
    