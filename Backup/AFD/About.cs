using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace qldv
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
        }

        private void About_Load(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            //sb.AppendLine("Quản lý động vật v1.0.0.8");
            sb.AppendLine("Name v1.0.0.0");
            //sb.AppendLine("Database v1.0.0.3");
            sb.AppendLine("Copyright HuyPham Corporation 2008-2009. All rights reserved.");
            sb.AppendLine("This product is licensed to:");
            sb.AppendLine("");
            //sb.AppendLine("Trương Ngọc Đăng");
            sb.AppendLine("Public users");
            sb.AppendLine("");
            //sb.AppendLine("Website: http://www.saigonzoo.net");
            //sb.AppendLine("Email: ngocdang1010@yahoo.com");
            sb.AppendLine("Product ID: hp2010-06-001");
            sb.AppendLine("AppId: 4DBD0B3C-566B-4A9D-8865-1A74AA51E1CF");
            sb.AppendLine("");
            sb.AppendLine("Warning: This computer program is protected by copyright law and international treaties. Unauthorized reproduction or distribution of this program, or any portion of it, may result in severe civil and criminal penalties, and will be prosecuted to the maximum extent possible under the law.");
            sb.AppendLine("");
            //sb.AppendLine("HuyPham Corporation http://www.huypham.com.vn");
            //sb.AppendLine("Ho Chi Minh City");
            //sb.AppendLine("Huynh Tan Phat Street, Phu Thuan Ward, District 7");
            //sb.AppendLine("Mobile: +84 0909220277");
            sb.AppendLine("Yahoo Mail: anhhuycan83@yahoo.com");
            sb.AppendLine("Google Mail: anhhuycan83@gmail.com");
            txtAbout.Text = sb.ToString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}